<?php

namespace Application\Controller;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Admin\Controller\RoleController;
use Zend\Stdlib\Parameters;

class IndexControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {

        $this->_controller = new IndexController();

    }

    public function tearDown()
    {

    }

    public function testindexAction()
    {
        $_SESSION['Userid']=1;
        $post = array('userFirst' => '123123', 'userLast' => '354345', 'userglobalsearch' => 'test',
        'rolesave' => 'sadd', 'groupSearch' => 'asdasd', 'grupperolesichern' => 'asdasd');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'isAllowed', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(true));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('Doctrine\ORM\EntityManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock("Zend\ServiceManager\ServiceManager", array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $route = new RouteMatch(array('controller' => 'role', 'action' => 'index'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);

        $this->_controller->indexAction();
    }
    public function testindexActionohne()
    {



    }
}