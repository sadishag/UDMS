<?php
namespace RoleAPI\Entity;
class RoleAPIEntitiesTest extends \PHPUnit_Framework_TestCase
{

    public function setUp()
    {

    }
    public function testBranchNameEntity(){
        $branchName=new BranchName();
        $Name="bla";
        $Location="dir://";
        $branchName->setName($Name);
        $branchName->setLocation($Location);
        $this->assertEquals($Name, $branchName->getName());
        $this->assertEquals($Location,$branchName->getLocation());
        $branchName->getBranchId();$branchName->getId();
        $branchName->getTrash();$branchName->setTrash(1);
        $branchName->getTrashInfo();$branchName->setTrashInfo(1);
    }
    public function testGroupNameEntity(){
        $groupName=new GroupName();
        $Name="bla";
        $BranchId=1;
        $groupName->setName($Name);
        $groupName->setBranchId($BranchId);
        $this->assertEquals($Name, $groupName->getName());
        $this->assertEquals($BranchId, $groupName->getBranchId());
        $groupName->getGroupId(); $groupName->getId();
    }
    public function testGroupsEntity(){
        $groups=new Groups();
        $GroupId=1;
        $UserId=1;
        $Temp=1;
        $Hash=md5($GroupId);
        $groups->setGroupId($GroupId);
        $groups->setHash($Hash);
        $groups->setTemp($Temp);
        $groups->setUserId($UserId);
        $this->assertEquals($GroupId, $groups->getGroupId());
        $this->assertEquals($Hash, $groups->getHash());
        $this->assertEquals($Temp, $groups->getTemp());
        $this->assertEquals($UserId, $groups->getUserId());
        $groups->getId();
    }
    public function testMetaDataEntity(){
        /*$meta=new MetaData();
        $Author="abc";
        $Owner="abc";
        $Branche=1;
        $Creation=12345;
        $meta->setAuthor($Author);
        $meta->setBranch($Branche);
        $meta->setCreation($Creation);
        $meta->setOwner($Owner);
        $this->assertEquals($Author, $meta->getAuthor());
        $this->assertEquals($Owner, $meta->getOwner());
        $this->assertEquals($Branche, $meta->getBranch());
        $this->assertEquals($Creation, $meta->getCreation());
        $meta->getMetaId();*/

    }
    public function testNoteEntity(){
        /*$note=New \RoleAPI\Entity\Note();
        $Content="aha";
        $MetaId=1;
        $Name="hah";
        $Version="not";
        $note->setContent($Content);
        $note->setVersion($Version);
        $note->setName($Name);
        $note->setMetaId($MetaId);
        $this->assertEquals($Content, $note->getContent());
        $this->assertEquals($MetaId, $note->getMetaId());
        $this->assertEquals($Name, $note->getName());
        $this->assertEquals($Version, $note->getVersion());
        $note->getId();*/
    }
    public function testRoleName(){
        $role=New RoleName();
        $Name="abc";
        $RightKey="cba";
        $role->setName($Name);
        $role->setRightKey($RightKey);
        $this->assertEquals($Name, $role->getName());
        $this->assertEquals($RightKey, $role->getRightKey());
        $role->getRoleId();$role->getId();
    }
    public function testRoles(){
        $role = new Roles();
        $UserId=1;
        $GroupId=1;
        $RoleId=1;
        $role->setGroupId($GroupId);
        $role->setRoleId($RoleId);
        $role->setUserId($UserId);
        $this->assertEquals($UserId, $role->getUserId());
        $this->assertEquals($GroupId, $role->getGroupId());
        $this->assertEquals($RoleId, $role->getRoleId());
        $role->getId();
    }
    public function testUser(){
        $user = new User();
        $Email="abc@d.e";
        $FirstName="ich";
        $LastName="due";
        $Login="ich.du";
        $Password="12345";
        $user->setPassword($Password);
        $user->setLogin($Login);
        $user->setLastName($LastName);
        $user->setFirstName($FirstName);
        $user->setEmail($Email);


        $this->assertEquals($Email, $user->getEmail());
        $this->assertEquals($FirstName, $user->getFirstName());
        $this->assertEquals($LastName, $user->getLastName());
        $this->assertEquals($Login, $user->getLogin());
        $this->assertEquals($Password, $user->getPassword());
        $user->getUserId();$user->getId();
        $user->setKey(123);$user->getKey();
        $user->setStyleSettings(1);$user->getStyleSettings();
    }
    public function tearDown()
    {

    }
}