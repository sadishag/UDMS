<?php

/**
 * FileController.php
 *
 * @package Data\Controller
 * @copyright Copyright (c) 2013 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Thomas Kröning <thomas.kroening@unister.de>
 * @author Roland Starke <roland.starke@unister.de>
 */
namespace Data\Controller;

use Notice\Service\NoticeActionValidator;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Doctrine\ORM\EntityManager;

/**
 * Class of the FileController
 *
 * Shows all information of a file.
 *
 * @package Data\Controller
 * @copyright Copyright (c) 2013 Unister GmbH
 */
class FileController extends AbstractActionController
{
    /**
     * Gets all information of a file.
     *
     * @return JsonModel as array
     */
    public function indexAction()
    {
        $validator = new NoticeActionValidator($this->getServiceLocator());
        $location = 'dir://';
        $version = 'newwest';
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $this->getRequest()->getPost();
            if (isset($post['dirLocation'])) {
                $location = $post['dirLocation'];
            }
            if (isset($post['version'])) {
                $version = (int) $post['version'];
            }
            if (isset($post['fileId'])) {
                $fileId = (int) $post['fileId'];
            }
        }
        $api = $this->getServiceLocator()->get('RoleAPI');
        $em = $this->getServiceLocator()->get('ObjectManager');

        try {
            $branchId = $api->getBranchId($location);

            $fileRepo = $em->getRepository('MetaAPI\Entity\File');
            $fileVerRepo = $em->getRepository('DataAPI\Entity\FileVersion');
            $userRepo = $em->getRepository('RoleAPI\Entity\User');
            $branchRepo = $em->getRepository('RoleAPI\Entity\BranchName');
            if (isset($fileId)) {
                $file = $fileRepo->findOneBy(array('FileId' => $fileId));
            } else {
                $file = $fileRepo->findOneBy(array('Branch' => $branchId));
            }
            if ($validator->validate($_SESSION['Userid'], $file->getFileId(), 'read')) {
                if ($file !== null) {
                    $fileInfo['fileId'] = $file->getFileId();
                    $fileInfo['name'] = $file->getName();
                    $fileInfo['verAmount'] = $file->getVerAmount();
                    $fileInfo['type'] = $file->getType();
                    $fileInfo['location'] = $api->getBranchLocation($file->getBranch());
                    $fileInfo['parent'] = $api->getParent($fileInfo['location']);
                    if ($fileInfo['parent'] !== 'dir://') {
                        $fileInfo['parentName'] = $branchRepo->findOneBy(array('Location' => $fileInfo['parent']))->getName();
                    }else{
                        $fileInfo['parentName'] = 'Basisordner';
                    }

                    if ($version === 'newwest') {
                        $version = $fileInfo['verAmount'];
                    }
                    $fileVersion = $fileVerRepo->findOneBy(
                    array('FileId' => $fileInfo['fileId'], 'VerNr' => $version));
                    if ($fileVersion !== null) {
                        $content = $fileVersion->getContent();
                        /*
                         * don't send big files browser craches when displaying
                         * that data
                         */
                        if ($fileVersion->getHash() === hash('sha256', $fileVersion->getTime() . $content)) {
                            if (strlen($content) < 1024 * 1024 * 0.2) { /*
                                                                         * 0.2
                                                                         * MB
                                                                         */
                                if (strpos($fileInfo['type'], 'text') === false) {
                                    $fileInfo['content'] = 'data:' . $fileInfo['type'] . ';base64,' .
                                     base64_encode($content);
                                } else {
                                    $fileInfo['content'] = $content;
                                }
                            }

                            $fileInfo['time'] = date('\a\m d.m.Y \u\m H:i:s', $fileVersion->getTime());

                            $editor = $userRepo->find($fileVersion->getEditor());

                            $fileInfo['editor'] = /*$editor->getFirstName() . ' ' . $editor->getLastName() . ' < ' . */$editor->getLogin() /*. ' >' */;

                            $owner = $userRepo->find($fileVersion->getOwner());
                            $fileInfo['owner'] = /*$owner->getFirstName() . ' ' . $owner->getLastName() . ' < ' . */$owner->getLogin() /*. ' >'*/;
                            $fileInfo['version'] = $fileVersion->getVerNr();
                        } else {
                            $fileInfo['error'] = 'Hash des Files stimmt nicht überein';
                        }
                    } else {
                        $fileInfo['error'] = 'Version Nicht Gefunden';
                    }
                } else {
                    $fileInfo['error'] = 'Datei Nicht Gefunden';
                }
            } else {
                $fileInfo['error'] = 'Keine Berechtigung';
            }
        } catch (\Exception $e) {
            $fileInfo['error'] = 'Pfad nicht Gefunden' . $e->getMessage();
        }

        return new JsonModel($fileInfo);
    }

}
