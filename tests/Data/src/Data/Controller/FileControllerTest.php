<?php

use Zend\Stdlib\Parameters;
use Data\Controller\FileController;

class FileControllerTest extends PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {
        $this->_controller = new FileController();
    }

    public function provideTestIndexAction()
    {
        return array(
        array(array('dirLocation' => 'dir://', 'version' => 'newwest', 'fileId' => 1), ''),
        array(array('dirLocation' => 'dir://', 'version' => 'newwest', 'fileId' => 1), 'text'),
        array(array('dirLocation' => 'dir://', 'fileId' => 1), ''),
        array(array(), 1)
        );
    }

    /**
     * @dataProvider provideTestIndexAction
     */
    public function testIndexAction($post,$type)
    {
        $_SESSION['Userid'] = 1;

        $content = 'test';
        $stream = fopen('php://memory','r+');
        fwrite($stream, $content);
        rewind($stream);

        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $userMock = $this->getMock('RoleAPI\Entity\User', array('getLogin'));

        $fileMock = $this->getMock('MetaAPI\Entity\File', array('getHash', 'getVerNr', 'getType', 'getOwner', 'getFileId', 'getContent', 'getEditor', 'getTime'));
        $fileMock->expects($this->any())
            ->method('getFileId')
            ->will($this->returnValue(1));
        $fileMock->expects($this->any())
            ->method('getType')
            ->will($this->returnValue($type));
        $fileMock->expects($this->any())
            ->method('getContent')
            ->will($this->returnValue($stream));

        $repo = $this->getMock('Repository', array('findOneBy', 'find'));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($fileMock));

        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($userMock));

        $em = $this->getMock('ObjectManager', array('getRepository', 'getBranchId', 'getBranchLocation', 'getParent'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }

    public function tearDown()
    {

    }
}