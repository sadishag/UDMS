<?php
namespace User\Controller;

use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use User\Controller\CreateController;
use Zend\Stdlib\Parameters;

class CreateControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {

        $this->_controller = new CreateController();

    }

    public function tearDown()
    {

    }

    public function testindexAction()
    {
        $_SESSION['Userid']='1';
        $post = array('save'=>'true');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','isAllowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(true));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }
    public function testindexActionpwunterschied()
    {
        $_SESSION['Userid']='1';
        $post = array('save'=>'true','pass1'=>'asdas','pass2'=>'asds22');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','isAllowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(true));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }
    public function testindexActionpwgleichkeinegültigemail()
    {
        $_SESSION['Userid']='1';
        $post = array('save'=>'true','pass1'=>'1','pass2'=>'1');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue(false));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','isAllowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(true));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }
    public function testindexActionpwgleicheinegültigemail()
    {
        $_SESSION['Userid']='1';
        $post = array('save'=>'true','pass1'=>'1','pass2'=>'1','email'=>'asdas');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));



        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','isAllowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(true));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }
    public function testindexActionpwgleicheinegültigemail2()
    {
        $_SESSION['Userid']='1';
        $post = array('save'=>'true','pass1'=>'1','pass2'=>'1','email'=>'asdas','userFirst'=>'5');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));



        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','isAllowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(true));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }
    public function testindexActionnotallowed()
    {
        $_SESSION['Userid']='1';
        $post = array('save'=>'true','pass1'=>'1','pass2'=>'1','email'=>'asdas','userFirst'=>'5');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));



        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','isAllowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(false));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $mvcEvent = new MvcEvent();
        $mvcEvent->setResponse(new Response());
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }

}