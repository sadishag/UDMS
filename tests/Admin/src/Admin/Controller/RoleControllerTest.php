<?php

namespace Admin\Controller;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Admin\Controller\RoleController;
use Zend\Stdlib\Parameters;

class RoleControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {

        $this->_controller = new RoleController();

    }

    public function tearDown()
    {

    }

    public function testindexAction()
    {

        $post = array('nutzer' => '354345', 'userglobalsearch' => 'test',
        'rolesave' => 'sadd', 'groupSearch' => 'asdasd', 'grupperolesichern' => 'asdasd');
        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
            ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValueMap($mapmock));

        $route = new RouteMatch(array('controller' => 'role', 'action' => 'index'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();

    }

    public function testroleaction()
    {
        $i = 1;
        $post = array('rolenname' => 'm', 'rolechange' => '123', 'createrole' => 'asdas', 'changerole' => 'asdas',
        'deleterole' => 'asdas', 'rolechangesave' => 'dadad', 'checkbox' . $i => '55');
        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\RolenName', null);

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue($albumMock));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository"), array(), "", false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $apiMock = $this->getMock('RoleAPI', array("createNewRole", 'getRightArrayByRoleId', 'deleteRole','getStandardRoleArrays'),
        array(), "", false);
        $apiMock->expects($this->any())
            ->method('createNewRole')
            ->will($this->returnValue('asdas'));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
        $apiMock->expects($this->any())
            ->method('getRightArrayByRoleId')
            ->will($this->returnValue(array('$key' => '3', '$value' => true)));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->roleaction();

    }

    public function testroleactioohnepost()
    {
        $i = 1;
        $post = array('createrole' => 'asdas', 'changerole' => 'asdas', 'deleterole' => 'asdas',
        'rolechangesave' => 'dadad', 'checkbox' . $i => '55');
        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\RolenName', null);

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue($albumMock));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository"), array(), "", false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
        $apiMock->expects($this->any())
            ->method('getRightArrayByRoleId')
            ->will($this->returnValue(array('$key' => '3', '$value' => true)));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->roleaction();

    }
    public function testindexActionohnepost()
    {

        $post = array('nutzer' => '', 'userLast' => '', 'userglobalsearch' => 'test',
        'rolesave' => 'sadd', 'groupSearch' => '', 'grupperolesichern' => 'asdasd');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue(NULL));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $route = new RouteMatch(array('controller' => 'role', 'action' => 'index'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();

    }

    public function testindexActionohnepost2()
    {

        $post = array('nutzer' => '', 'userLast' => 'asdas', 'userglobalsearch' => 'test');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue(NULL));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $route = new RouteMatch(array('controller' => 'role', 'action' => 'index'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();

    }
    public function testindexActionohnepost3()
    {

        $post = array('nutzer' => '', 'userLast' => 'asdas', 'userglobalsearch' => 'test','groupSearch'=>'test');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue(NULL));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $route = new RouteMatch(array('controller' => 'role', 'action' => 'index'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();

    }

}