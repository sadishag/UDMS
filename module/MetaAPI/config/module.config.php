<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'meta_entities' => array(
                'class' => '\Doctrine\ORM\Mapping\Driver\AnnotationDriver', 'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/MetaAPI/Entity'
                )
            ),
            'udms_meta' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\DriverChain',
                'drivers' => array(
                    'MetaAPI\Entity' => 'meta_entities'
                )
            ),

        ),
        'configuration' => array(
            'udms_meta' => array(
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'driver' => 'udms_meta',
                'generate_proxies' => true,
                'proxy_dir' => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\Proxy',
                'filters' => array()
            ),

        ),
        'entitymanager' => array(
            'udms_meta' => array(
                'connection' => 'udms_meta',
                'configuration' => 'udms_meta'
            ),

        ),
        'eventmanager' => array(
            'udms_meta' => array(),

        ),
        'sql_logger_collector' => array(
            'udms_meta' => array(),

        ),
        'entity_resolver' => array(
            'udms_meta' => array(),

        ),
        'authentication' => array(
            'udms_meta' => array(
                'objectManager' => 'doctrine.entitymanager.udms_meta'
            ),

        )
    ),
    'service_manager' => array(
        'factories' => array(
            'doctrine.authenticationadapter.udms_meta' => new DoctrineModule\Service\Authentication\AdapterFactory('udms_meta'),
            'doctrine.authenticationstorage.udms_meta' => new DoctrineModule\Service\Authentication\StorageFactory('udms_meta'),
            'doctrine.authenticationservice.udms_meta' => new DoctrineModule\Service\Authentication\AuthenticationServiceFactory('udms_meta'),
            'doctrine.connection.udms_meta' => new DoctrineORMModule\Service\DBALConnectionFactory('udms_meta'),
            'doctrine.configuration.udms_meta' => new DoctrineORMModule\Service\ConfigurationFactory('udms_meta'),
            'doctrine.entitymanager.udms_meta' => new DoctrineORMModule\Service\EntityManagerFactory('udms_meta'),
            'doctrine.driver.udms_meta' => new DoctrineModule\Service\DriverFactory('udms_meta'),
            'doctrine.eventmanager.udms_meta' => new DoctrineModule\Service\EventManagerFactory('udms_meta'),
            'doctrine.entity_resolver.udms_meta' => new DoctrineORMModule\Service\EntityResolverFactory('udms_meta'),
            'doctrine.sql_logger_collector.udms_meta' => new DoctrineORMModule\Service\SQLLoggerCollectorFactory('udms_meta'),
            'doctrine.mapping_collector.udms_meta' => function (Zend\ServiceManager\ServiceLocatorInterface $sl)
            {
                $em = $sl->get('doctrine.entitymanager.udms_meta');

                return new DoctrineORMModule\Collector\MappingCollector($em->getMetadataFactory(), 'udms_meta_mappings');
            },
            'DoctrineORMModule\Form\Annotation\AnnotationBuilder' => function (Zend\ServiceManager\ServiceLocatorInterface $sl)
            {
                return new DoctrineORMModule\Form\Annotation\AnnotationBuilder($sl->get('doctrine.entitymanager.udms_meta'));
            },

        )
    )
);