<?php

use Doctrine\Tests\Common\Annotations\Fixtures\Annotation\Route;

use Doctrine\ORM\Query\Parameter;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;

use Data\Controller\DirController;

class DirControllerTest extends PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {
        $this->_controller = new DirController();
    }

    public function provideTestIndexAction()
    {
        return array(
            array(array('dirLocation' => 'test'), 0),
            array(array('dirLocation' => 'test'), 1),
            array(array(), 1)
        );
    }

    /**
     * @dataProvider provideTestIndexAction
     */
    public function testIndexAction($post, $branchId)
    {
        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $branchMock = $this->getMock('RoleAPI\Entity\Branch', array('getLocation', 'getName', 'getBranchId','getTrash'));

        $repo = $this->getMock('Repository', array('find'));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($branchMock));

        $em = $this->getMock('ObjectManager', array('getRepository', 'getBranchId', 'getChildrenEntities'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));
        $em->expects($this->any())
            ->method('getChildrenEntities')
            ->will($this->returnValue(array($branchMock)));
        $em->expects($this->any())
            ->method('getBranchId')
            ->will($this->returnValue($branchId));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }

    public function testThrowException()
    {
        $branchMock = $this->getMock('RoleAPI\Entity\Branch', array('getName', 'getLocation', 'getTrash'));

        $em = $this->getMock('ObjectManager',array('getBranchId', 'getChildrenEntities'));
        $em->expects($this->any())
            ->method('getBranchId')
            ->will($this->throwException(new \InvalidArgumentException()));

        $em->expects($this->any())
            ->method('getChildrenEntities')
            ->will($this->returnValue((array($branchMock))));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));


        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }

    public function tearDown()
    {

    }
}