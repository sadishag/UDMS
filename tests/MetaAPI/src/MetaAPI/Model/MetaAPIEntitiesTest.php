<?php
namespace MetaAPI\Entity;
class MetaAPIEntitiesTest extends \PHPUnit_Framework_TestCase {

    public function setUp()
    {

    }
    public function tearDown()
    {

    }

    public function testMetaDataTypesEntity(){
        $metaDataTypes =new MetaDataTypes();
        $Name="PDF";
        $Mediatyp="pdf";
        $metaDataTypes->setName($Name);
        $metaDataTypes->setMediatyp($Mediatyp);
        $this->assertEquals($Name, $metaDataTypes->getName());
        $this->assertEquals($Mediatyp,$metaDataTypes->getMediatyp());
        $metaDataTypes->getId();$metaDataTypes->getId();
        $metaDataTypes->getSubtyp();$metaDataTypes->setSubtyp('pdf');
        $metaDataTypes->getMaxLength();$metaDataTypes->setMaxLength(1);
        $metaDataTypes->getMaxSize();$metaDataTypes->setMaxSize(1);
        $metaDataTypes->getInteractive();$metaDataTypes->setInteractive(1);
        $metaDataTypes->getResourceUrl();$metaDataTypes->setResourceUrl('/location');
    }

    public function testFileEntity(){
        $File =new File();
        $Name="PDF";
        $VerAmount="3";
        $File->setName($Name);
        $File->setVerAmount($VerAmount);
        $this->assertEquals($Name, $File->getName());
        $this->assertEquals($VerAmount,$File->getVerAmount());
        $File->getFileId();
        $File->getType();$File->setType('pdf');
        $File->getBranch();$File->setBranch(123);
        $File->getAuthor();$File->setAuthor(1);
        $File->getBackup();$File->setBackup(1);
        $File->getVision();$File->setVision(1);
        $File->getReadOnly();$File->setReadOnly(1);
        $File->getKeyWords();$File->setKeyWords('test');
        $File->setChecked(true);
        $File->__toString();
    }
}