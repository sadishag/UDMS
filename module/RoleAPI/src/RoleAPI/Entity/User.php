<?php

namespace RoleAPI\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * Users
 *
 * @ORM\Table(name="User")
 *
 * @property int $UserId
 * @property string $Login
 * @property string $FirstName
 * @property string $LastName
 * @property string $Email
 * @property string $Password
 * @property int $Temp
 * @property string $Key
 * @property int $StyleSetting
 */

class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */

    protected $UserId;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $Login;

    /**
     * @ORM\Column(type="string")
     */

    protected $Password;

    /**
     * @ORM\Column(type="string")
     */

    protected $FirstName;

    /**
     * @ORM\Column(type="string")
     */

    protected $LastName;

    /**
     * @ORM\Column(type="string", unique=true)
     */

    protected $Email;

    /**
     * @ORM\Column(type="integer")
     */
    protected $Temp;

    /**
     * @ORM\Column(type="string", nullable = true, columnDefinition="CHAR(8)" )
     */
    protected $Key;

    /**
     * @return the $Key
     */
    public function getKey()
    {
        return $this->Key;
    }

	/**
     * @param number $Key
     */
    public function setKey($Key)
    {
        $this->Key = $Key;
    }

	/**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $StyleSettings;

    /**
     *
     * @return the $UserId
     */
    public function getUserId()
    {
        return $this->UserId;
    }

    /**
     *
     * @return the $UserId
     */
    public function getId()
    {
        return $this->UserId;
    }

    /**
     *
     * @return the $Login
     */
    public function getLogin()
    {
        return $this->Login;
    }

    /**
     *
     * @return the $Name
     */
    public function getFirstName()
    {
        return $this->FirstName;
    }

    /**
     *
     * @return the $Lastname
     */
    public function getLastName()
    {
        return $this->LastName;
    }

    /**
     *
     * @return the $Email
     */
    public function getEmail()
    {
        return $this->Email;
    }

    /**
     *
     * @return the $Password
     */
    public function getPassword()
    {
        return $this->Password;
    }

    /**
     *
     * @return Temp status
     */
    public function getTemp()
    {
        return $this->Temp;
    }

    /**
     *
     * @param string $Login
     */
    public function setLogin($Login)
    {
        $this->Login = $Login;
    }

    /**
     *
     * @param string $Name
     */
    public function setFirstName($FirstName)
    {
        $this->FirstName = $FirstName;
    }

    /**
     *
     * @param string $Lastname
     */
    public function setLastName($LastName)
    {
        $this->LastName = $LastName;
    }

    /**
     *
     * @param string $Email
     */
    public function setEmail($Email)
    {
        $this->Email = $Email;
    }

    /**
     *
     * @param string $Password
     */
    public function setPassword($Password)
    {
        $this->Password = $Password;
    }

    /**
     *
     * @param integer $Temp
     */
    public function setTemp($Temp)
    {
        $this->Temp = $Temp;
    }
	/**
     * @return the $StyleSettings
     */
    public function getStyleSettings()
    {
        return $this->StyleSettings;
    }

	/**
     * @param field_type $StyleSettings
     */
    public function setStyleSettings($StyleSettings)
    {
        $this->StyleSettings = $StyleSettings;
    }


}