<?php

namespace DataAPI\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Zend\Form\Annotation; // !!!! Absolutely neccessary
/**
 * @ORM\Entity
 * FileVersion
 *
 * @ORM\Table(name="FileVersion", options={"engine"="MyISAM"})
 *
 * @property int $Id
 * @property int $FileId
 * @property int $VerNr
 * @property int $Time
 * @property int $Editor
 * @property int $Owner
 * @property blob $Content
 * @property varchar $Search
 * @property string $Hash
 */

class FileVersion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */

    protected $Id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $FileId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $VerNr;

    /**
     * @ORM\Column(type="integer")
     */

    protected $Time;

    /**
     * @ORM\Column(type="integer")
     */

    protected $Editor;
    /**
     * @ORM\Column(type="integer")
     */

    protected $Owner;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */

    protected $Content;

    /**
     * @ORM\Column(type="text", nullable=true)
     */

    protected $Search;

    /**
     * @ORM\Column(type="string", nullable=true, columnDefinition="CHAR(64)")
     */
    protected $Hash;

    /**
     *
     * @return the $Hash
     */
    public function getHash()
    {
        return $this->Hash;
    }

    /**
     *
     * @return the $Search
     */
    public function getSearch()
    {
        return $this->Search;
    }

    /**
     *
     * @param varchar $Search
     */
    public function setSearch($Search)
    {
        $this->Search = $Search;
    }

    /**
     *
     * @param field_type $Hash
     */
    public function setHash($Hash)
    {
        $this->Hash = $Hash;
    }

    /**
     *
     * @return the $Id
     */
    public function getId()
    {
        return $this->Id;
    }

    /**
     *
     * @return the $FileId
     */
    public function getFileId()
    {
        return $this->FileId;
    }

    /**
     *
     * @return the $VerNr
     */
    public function getVerNr()
    {
        return $this->VerNr;
    }

    /**
     *
     * @return the $Time
     */
    public function getTime()
    {
        return $this->Time;
    }

    /**
     *
     * @return the $Editor
     */
    public function getEditor()
    {
        return $this->Editor;
    }

    /**
     *
     * @return the $Owner
     */
    public function getOwner()
    {
        return $this->Owner;
    }

    /**
     *
     * @return the $Content
     */
    public function getContent()
    {
        if (isset($this->Search)) {
            $content = $this->Search;
        } else {
            $content = $this->Content;
            if (is_resource($content)) {
                $content = stream_get_contents($content);
            }
        }
        return $content;
    }

    /**
     *
     * @param field_type $FileId
     */
    public function setFileId($FileId)
    {
        $this->FileId = $FileId;
    }

    /**
     *
     * @param field_type $VerNr
     */
    public function setVerNr($VerNr)
    {
        $this->VerNr = $VerNr;
    }

    /**
     *
     * @param number $Time
     */
    public function setTime($Time)
    {
        $this->Time = $Time;
    }

    /**
     *
     * @param number $Editor
     */
    public function setEditor($Editor)
    {
        $this->Editor = $Editor;
    }

    /**
     *
     * @param number $Owner
     */
    public function setOwner($Owner)
    {
        $this->Owner = $Owner;
    }

    /**
     *
     * @param blob $Content
     */
    public function setContent($Content)
    {
        $this->Content = $Content;
    }

    /**
     *
     * @param number $Id
     */
    public function setId($Id)
    {
        $this->Id = $Id;
    }

    public function getDataArray()
    {

        return array('Id' => $this->Id, 'FileId' => $this->FileId, 'VerNr' => $this->VerNr, 'Time' => $this->Time,
        'Editor' => $this->Editor, 'Owner' => $this->Owner, 'Content' => $this->Content);
    }

    public function setDataArray($dataArray)
    {
        if (isset($dataArray['Id'])) {
            $this->setId($dataArray['Id']);
        }
        if (isset($dataArray['FileId'])) {
            $this->setFileId($dataArray['FileId']);
        }
        if (isset($dataArray['VerNr'])) {
            $this->setVerNr($dataArray['VerNr']);
        }
        if (isset($dataArray['Time'])) {
            $this->setTime($dataArray['Time']);
        }
        if (isset($dataArray['Editor'])) {
            $this->setEditor($dataArray['Editor']);
        }
        if (isset($dataArray['Owner'])) {
            $this->setOwner($dataArray['Owner']);
        }
        if (isset($dataArray['Content'])) {
            $this->setContent($dataArray['Content']);
        }
    }

}
