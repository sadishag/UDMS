<?php
namespace Data\Controller;

use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;

use Doctrine\ORM\EntityManager;
/**
 * gibt alle kinder eines ordners aus in einen assoziativen array mit name und location
 *
 * @author r.starke
 *
 */
class DirController extends AbstractActionController
{

    /**
     * gibt alle kinder eines ordners aus in einen assoziativen array mit name und location
     * @return array mit den feldern dir und childs
     *
     */
    public function indexAction()
    {
        $location = 'dir://';
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $this->getRequest()->getPost();
            if (isset($post['dirLocation'])) {
                $location = $post['dirLocation'];
            }
        }
        // find out location
        $api = $this->getServiceLocator()->get('RoleAPI');

        $em = $this->getServiceLocator()->get('ObjectManager');
        try{
        $branchId = $api->getBranchId($location);
        }catch(\Exception $e){
            $dir['location'] = $location;
            $dir['name'] = 'Pfad existiert nicht';
            $dir['id'] = -1;
        }
        if (isset($branchId)) {
            if($branchId === 0){
                $dir['location'] = 'dir://';
                $dir['name'] = 'Basisordner';
                $dir['id'] = 0;
            } else {
                $branch = $em->getRepository('RoleAPI\Entity\BranchName')->find($branchId);
                $dir['location'] = $branch->getLocation();
                $dir['name'] = $branch->getName();
                $dir['id'] = $branch->getBranchId();
            }
        }
        // find out childs
        $response = $api->getChildrenEntities($location);
        $childs = array();
        foreach ($response as $child) {
            if($child->getTrash() !== 1){
                $childs[] = array('name' => $child->getName(), 'location' => $child->getLocation());
            }
        }

        return new JsonModel(array('dir' => $dir, 'childs' => $childs));
    }
}

