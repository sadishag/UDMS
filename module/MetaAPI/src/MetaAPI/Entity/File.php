<?php

namespace MetaAPI\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Zend\Form\Annotation; // !!!! Absolutely neccessary
/**
 * @ORM\Entity
 * File
 *
 * @ORM\Table(name="File")
 *
 * @property string $Name
 * @property int $VerAmount
 * @property string $Type
 * @property int $Branch
 * @property int $Author
 * @property int $Backup
 * @property blob $Vision
 * @property int $ReadOnly
 * @property string $Checked
 * @property string $KeyWords
 */

class File
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */

    protected $FileId;

    /**
     * @ORM\Column(type="string")
     */
    protected $Name;

    /**
     * @ORM\Column(type="integer")
     */

    protected $VerAmount;

    /**
     * @ORM\Column(type="string")
     */

    protected $Type;
    /**
     * @ORM\Column(type="integer")
     */

    protected $Branch;

    /**
     * @ORM\Column(type="integer")
     */

    protected $Author;

    /**
     * @ORM\Column(type="integer")
     */

    protected $Backup;

    /**
     * @ORM\Column(type="blob", nullable=true)
     */

    protected $Vision;

    /**
     * @ORM\Column(type="integer")
     */

    protected $ReadOnly;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */

    protected $Checked;

    /**
     * @ORM\Column(type="string", nullable = true)
     */

    protected $KeyWords;

    /**
     * @return the $Checked
     */
    public function getChecked()
    {
        return $this->Checked;
    }

	/**
     * @param string $Checked
     */
    public function setChecked($Checked)
    {
        $this->Checked = $Checked;
    }

	/**
     * @return the $KeyWords
     */
    public function getKeyWords()
    {
        return $this->KeyWords;
    }

	/**
     * @param string $KeyWords
     */
    public function setKeyWords($KeyWords)
    {
        $this->KeyWords = $KeyWords;
    }
    /**
     *
     * @return the $ReadOnly
     */

	public function getReadOnly()
    {
        return $this->ReadOnly;
    }

    /**
     *
     * @param number $ReadOnly
     */
    public function setReadOnly($ReadOnly)
    {
        $this->ReadOnly = $ReadOnly;
    }

    /**
     *
     * @return the $Vision
     */
    public function getVision()
    {
        return $this->Vision;
    }

    /**
     *
     * @param blob $Vision
     */
    public function setVision($Vision)
    {
        $this->Vision = $Vision;
    }

    /**
     *
     * @return the $Backup
     */
    public function getBackup()
    {
        return $this->Backup;
    }

    /**
     *
     * @param number $Backup
     */
    public function setBackup($Backup)
    {
        $this->Backup = $Backup;
    }

    /**
     *
     * @return the $FileId
     */
    public function getFileId()
    {
        return $this->FileId;
    }

    /**
     *
     * @return the $Name
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     *
     * @return the $VerAmount
     */
    public function getVerAmount()
    {
        return $this->VerAmount;
    }

    /**
     *
     * @return the $Type
     */
    public function getType()
    {
        return $this->Type;
    }

    /**
     *
     * @return the $Branch
     */
    public function getBranch()
    {
        return $this->Branch;
    }

    /**
     *
     * @return the $Author
     */
    public function getAuthor()
    {
        return $this->Author;
    }

    /**
     *
     * @param string $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

    /**
     *
     * @param number $VerAmount
     */
    public function setVerAmount($VerAmount)
    {
        $this->VerAmount = $VerAmount;
    }

    /**
     *
     * @param string $Type
     */
    public function setType($Type)
    {
        $this->Type = $Type;
    }

    /**
     *
     * @param number $Branch
     */
    public function setBranch($Branch)
    {
        $this->Branch = $Branch;
    }

    /**
     *
     * @param number $Author
     */
    public function setAuthor($Author)
    {
        $this->Author = $Author;
    }

    public function __toString()
    {
        return (string) $this->getFileId();
    }

}
