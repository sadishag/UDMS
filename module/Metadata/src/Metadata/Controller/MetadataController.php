<?php
/**
 * MetaDataController.php
 *
 * @package    MetaData\Controller
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Mathias Piehl <mathias.piehl@unister.de>
 */

namespace Metadata\Controller;

use MetaAPI\Model\MetaAPIMain;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Console\Response;
use Doctrine\ORM\EntityManager;

/**
 * Controll unit for view\MetaData\index
 *
 * @copyright Copyright (c) 2012 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Mathias Piehl <mathias.piehl@unister.de>
 */
class MetadataController extends CreateController
{
	/**
	 * Function for controlling the show.phtml.
	 * Provides interface for managing the metadata.
	 *
	 * @return mixed $returnArray
	 *         Objekte, Arrays und Integers
	 *
	 */
	public function showAction()
    {
    	/**
    	 * function for debugging
    	 *
    	 * @return mixed $returnArray
    	 *         Objekte, Arrays und Integers
    	 *
    	 */
        /*/
    	function show($var, $info = '')
    	{
    		echo '<pre>'. $info . ': '; echo print_r($var); echo '</pre>'; echo '</br>';
    	}
        //*/

    	$em = $this->getServiceLocator()->get('ObjectManager');
    	$postvar = $this->getRequest()->getPost();
    	//show($postvar, 'postvar');

    	$MetaTypeRepo = $em->getRepository('MetaAPI\Entity\MetaDataTypes');
    	$metaTypeData = $MetaTypeRepo->findBy(array());
    	
    	$standardMetaTypes = array();
    	foreach ($metaTypeData as $key => $metaInfo) {
    		if (substr($metaInfo->getName(), 0, 1) === '!') {
    			$standardMetaTypes[] = $metaInfo;
    		}
    	}
    	 
    	$definedMetaTypes = array();
    	foreach ($metaTypeData as $key => $metaInfo) {
    		if (substr($metaInfo->getName(), 0, 1) !== '!') {
    			$definedMetaTypes[] = $metaInfo;
    		}
    	}
    	 
    	$returnArray['standardmetatypes'] = $standardMetaTypes;
    	$returnArray['definedmetatypes'] = $definedMetaTypes;
    	
    	if (isset($postvar['selectedTypeId'])) {
    	    $MetaTypeRepo = $em->getRepository('MetaAPI\Entity\MetaDataTypes');
    		$selectedMetaType = $MetaTypeRepo->findOneBy(array('Id' => $postvar['selectedTypeId']));
    	    $metaData = $em->getRepository('MetaAPI\Entity\File')->findBy(array('Type' => $selectedMetaType->getSubtyp()));
    	    if (empty($metaData)) {
    	    	$returnArray['error'] = 'Keine Dateien vom Typ "' . $selectedMetaType->getName() . '" vorhanden.'; 
    	    }
            else {
    	        $repoUser = $em->getRepository('RoleAPI\Entity\User');
    	        $repoBranch = $em->getRepository('RoleAPI\Entity\BranchName');

    	        $metaArray = array();
    	        foreach ($metaData as $key => $value){
    	            $metaArrayAuthor = $repoUser->findOneBy(array('UserId' => $value->getAuthor()));
    	            $metaArrayBranch = $repoBranch->findOneBy(array('BranchId' => $value->getBranch()));
    	            $metaArray[] = array('FileId'     => $value->getFileId(),
    	        		                 'FileName'   => $value->getName(),
    				                     'AuthorId'   => $value->getAuthor(),
    				                     'AuthorName' => $metaArrayAuthor->getLogin(),
    				                     'VerAmount'  => $value->getVerAmount(),
    				                     'BranchId'   => $value->getBranch(),
    				                     'BranchName' => $metaArrayBranch->getName(),
    	        		                 'BackUpLevel'=> $value->getBackup(),
    	        		                 'ReadOnly'   => $value->getReadOnly(),
    				         );
    	        }
    	        $returnArray['allmetadata'] = $metaArray;
    	        $returnArray['selectedmetatype'] = $selectedMetaType;
            }
    	}
    		    	
    	if (isset($postvar['metaDataSave'])) {
    		
    	    foreach ($metaTypeData as $key => $value) {
    	        $metaTypeNames[] = $value->getName();
    	    }

    	    for ($i = 0; $i < $postvar['sizeofarray']; $i++) {
    	    	$change = false;
    	    	$metaTypeData = $MetaTypeRepo->findOneBy(array('Id' => $postvar['metaTypeId'.$i]));
    	    	if (empty($metaTypeData)) continue;

    	    	$trimmedPostVar= str_replace(' ', '', $postvar['NameInput'.$i]);
    	    	if ($trimmedPostVar !== $postvar['hiddenName'.$i]) {
    	            if (in_array($trimmedPostVar, $metaTypeNames, true)){
    	                $returnArray['error'] = 'Name schon vergeben!'; 
    	                break;
    	            }
    	            if (empty($trimmedPostVar)){
    	            	$returnArray['error'] = 'Bitte geben Sie einen Namen an.'; 
    	            	break;
    	            }
    	            $metaTypeData->setName($trimmedPostVar); 
    	            $change = true; 
    	    	}

    	    	if ($postvar['mediaType'.$i] !== '0') {
    	    		$metaTypeData->setSubtyp($postvar['mediaType'.$i]); 
    	    		$change = true;
    	    	}

//     	        $trimmedPostVar= str_replace(' ', '', $postvar['MediaTypeInput'.$i]);
//     	    	if ($trimmedPostVar !== $postvar['hiddenMediaType'.$i]) {
//     	            if (empty($trimmedPostVar)){
//     	            	$returnArray['error'] = 'Bitte geben Sie einen Namen an.'; return $returnArray;
//     	            }
//     	            $metaTypeData->setMediatyp($trimmedPostVar); $change = true;
//     	    	}

//     	    	$trimmedPostVar= str_replace(' ', '', $postvar['SubTypeInput'.$i]);
//     	    	if ($trimmedPostVar !== $postvar['hiddenSubType'.$i]) {
//     	    		if (empty($trimmedPostVar)){
//     	    			$returnArray['error'] = 'Bitte geben Sie einen Namen an.'; return $returnArray;
//     	    		}
//     	    		$metaTypeData->setSubtyp($trimmedPostVar); $change = true;
//     	    	}

    	        if ($postvar['MaxLengthInput'.$i] != $postvar['hiddenMaxLength' . $i]) {
    	            if (!empty($postvar['MaxLengthInput'.$i])) $metaTypeData->setMaxLength($postvar['MaxLengthInput'.$i]);
    	        	else $metaTypeData->setMaxLength('0'); 
    	        	$change = true;
    	        }

    	        if ($postvar['MaxSizeInput'.$i] != $postvar['hiddenMaxSize'.$i]) {
    	            if (!empty($postvar['MaxSizeInput'.$i])) $metaTypeData->setMaxSize($postvar['MaxSizeInput'.$i]);
    	            else $metaTypeData->setMaxSize('0'); 
    	            $change = true;
    	        }

    	        if ($postvar['Interactive'.$i] != $metaTypeData->getInteractive()){
    	        	$metaTypeData->setInteractive($postvar['Interactive'.$i]); 
    	        	$change = true;
    	        }
    	        
    	        $trimmedPostVar= str_replace(' ', '', $postvar['ResourceUrlInput'.$i]);
    	        if ($trimmedPostVar !== $metaTypeData->getResourceUrl()) {
    	            $metaTypeData->setResourceUrl($trimmedPostVar); 
    	            $change = true;
    	        }
                
    	        if ($change) {
    	            $returnArray['success'] = 'Wurde geändert';
    	            $em->persist($metaTypeData);
    	            $em->flush();
    	        }
    	    }
        }
        return $returnArray;
    }
}
