<?php
namespace User\Form;
use Zend\Form\Form;

class UserForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct($name);

        $this->setAttribute('method', 'POST');
        $this->setAttribute('name', 'UserForm');

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'email',
            ),
            'options' => array(
                'label' => 'Email: ',
            )
        ));

        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'autocomplete' => 'off'
            ),
            'options' => array(
                'label' => 'Password: '
            ),
        ));

        $this->add(array(
            'name' => 'password2',
            'attributes' => array(
                'type' => 'password',
            ),
            'options' => array(
                'label' => 'Password bestätigen: '
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Daten ändern'
            ),
        ));
    }
}