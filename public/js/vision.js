$(document).ready(function() {
	$('#savevision').click(function() {
		var a = 0;
		var user = $('#userid').val();
		var group = $('#groupid').val();
		var role = $('#roleid').val();
		if (user !== '-- Benutzer --') {
			a++;
		}
		if (group !== '-- Gruppen --') {
			a++;
		}
		if (role !== '-- Rollen --') {
			a++;
		}
		if (a > 1) {
			globalNotification({
				warning : 'Bitte nur eine Änderung vornehmen'
			});
		} else {
			if (user !== '-- Benutzer --') {
				var b = $('#userid');
			}
			if (group !== '-- Gruppen --') {
				var b = $('#groupid');
			}
			if (role !== '-- Rollen --') {
				var b = $('#roleid');
			}
			if (a) {
				var id = b.val();
				var type = b.attr('name');
				var noticeid = $('#selectForm').attr('name');
				globalAjax('../addVision', {
					type : 'POST',
					data : {
						id : id,
						type : type,
						noticeid : noticeid
					}
				});
			}
		}
	})
})
$(document).ready(
		function() {
			$('[title*=" entfernen"]').click(
					function() {
						var noticeid = $('#selectForm').attr('name'), id = ($(
								this).parent().parent().children('#hidden')
								.html()), type = $(this).get(0).id;
						globalAjax('../deleteVision', {
							type : 'POST',
							data : {
								id : id,
								type : type,
								noticeid : noticeid
							}
						});
					})
		})
$(document).ajaxStop(function() {
	window.location.reload();
});
$(document).ready(function() {
	$('.delete-16').tooltip();
	$('.edit-16').tooltip();
	$('.save-16').tooltip();
})
$(document).ready(
		function() {
			$('.edit-16').click(
					function() {
						var id = $(this).parent().parent().children('#hidden')
								.html(), type = $(this).get(0).id, trid = $(
								this).parents('tr').attr('id');
						$('[id*="hiddenrole"]').hide();
						$('#' + type + 'hiddenrole' + trid).fadeIn('slow');
						$('[data-original-title*="schließen"]').click(
								function() {
									$('#' + type + 'hiddenrole' + trid)
											.fadeOut('normal');
								})
					})
		})
$(document).ready(
		function() {
			$('.save-16').click(
					function() {
						var selectedvalue = $(this).parent()
								.children('#roleid').val(), noticeid = $(
								'#selectForm').attr('name'), type = $(this)
								.get(0).id;
						ugrid = $(this).parent().attr('id');
						globalAjax('../saveVisionRole', {
							type : 'POST',
							data : {
								role : selectedvalue,
								noticeid : noticeid,
								type : type,
								ugrid : ugrid
							}
						});
					})
		})
