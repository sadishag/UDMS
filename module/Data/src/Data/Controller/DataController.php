<?php

/**
* DataController.php
*
* @package Data\Controller
* @copyright Copyright (c) 2013 Unister GmbH
* @author Unister GmbH <entwicklung@unister.de>
* @author Thomas Kröning <thomas.kroening@unister.de>
* @author Roland Starke <roland.starke@unister.de>
*/
namespace Data\Controller;

use Notice\Service\NoticeActionValidator;
use RoleAPI\Entity\Roles;

use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Http\Response;

use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\EntityManager;

/**
 * Class of the DataController
 *
 * Deal with the Data and File
 *
 * @package Data\Controller
 * @copyright Copyright (c) 2013 Unister GmbH
 */
class DataController extends AbstractActionController
{

    /**
     * Search all user to set the owner.
     *
     * @return ViewModel as array
     */
    public function showAction()
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $userRepo = $em->getRepository('RoleAPI\Entity\User');
        $users = $userRepo->findAll();
        $currentUser = $userRepo->find($_SESSION['Userid']);
        return new ViewModel(array('users' => $users, 'currentUser' => $currentUser));
    }

    /**
     * Deal with the versioning of the data and
     * edit the content, owner, name of the file.
     *
     * @return JsonModel $response as array -> error/success/warning
     */
    public function editAction()
    {
        $validator = new NoticeActionValidator($this->getServiceLocator());
        $em = $this->getServiceLocator()->get('ObjectManager');
        $api = $this->serviceLocator->get('RoleAPI');
        $time = time();
        $editor = $_SESSION['Userid'];
        $postvar = $this->getRequest()->getPost();
        $fileId = $postvar['id'];
        $location = $postvar['location'];
        $readOnly = $postvar['readOnly'];
        $checked = $postvar['checked'];
        $file = $this->getRequest()->getFiles();

        $fileEntity = $em->getRepository('MetaAPI\Entity\File')->find($fileId);

        if ($fileEntity->getReadOnly() === 0) {
            if ($fileEntity->getChecked() === null || $fileEntity->getChecked() === $editor) {

                $fileVerAmount = $fileEntity->getVerAmount();
                $fileVerRepo = $em->getRepository('DataAPI\Entity\FileVersion');
                $verEntityOld = $fileVerRepo->findOneBy(array('FileId' => $fileId, 'VerNr' => $fileVerAmount));

                $verEntity = new \DataAPI\Entity\FileVersion();

                if (isset($postvar['name']) && $postvar['name'] !== $fileEntity->getName()) {
                    $branch = $em->getRepository('RoleAPI\Entity\BranchName')->findOneBy(
                    array('BranchId' => $fileEntity->getBranch()));

                    $fileEntity->setName($postvar['name']);
                    $aenderung[] = 'Name';
                    $branch->setName($postvar['name']);
                    $em->persist($branch);
                }

                if (isset($location)) {
                    $branch = $em->getRepository('RoleAPI\Entity\BranchName')->findOneBy(
                    array('BranchId' => $fileEntity->getBranch()));
                    if ($location !== $api->getParent($branch->getLocation())) {
                        $newlocation = $this->newLocation($fileEntity, $api, $location);
                        $branch->setLocation($newlocation);
                        $em->persist($branch);
                        $aenderung[] = 'Location';
                    }
                }

                if (isset($postvar['owner']) && !empty($postvar['owner'])) {
                    $ownerId = $api->getUserId($postvar['owner']);
                    if ($ownerId !== $verEntityOld->getOwner()) {
                        $verEntity->setOwner($ownerId);
                        $aenderung[] = 'Besitzer';
                    } else {
                        $verEntity->setOwner($verEntityOld->getOwner());
                    }
                } else {
                    $verEntity->setOwner($verEntityOld->getOwner());
                }
                $oldContent = $verEntityOld->getContent();
                if ($this->isUpload($file)) {
                    $content = file_get_contents($file['file']['tmp_name']);
                    if ($content !== $oldContent) {
                        $content = file_get_contents($file['file']['tmp_name']);
                        $this->setContent($fileEntity, $verEntity, $content);
                        $aenderung[] = 'Inhalt';
                    } else {
                        $this->setContent($fileEntity, $verEntity, $content);
                    }
                } else {
                    $this->setContent($fileEntity, $verEntity, $oldContent);
                }

                if ($readOnly === 'true' || $readOnly === '1' || $readOnly === 'on') {
                    $fileEntity->setReadOnly(1);
                    if (!is_dir(getcwd() . '/data/readOnly/')) {
                        mkdir(getcwd() . '/data/readOnly/');
                    }
                    $destination = getcwd() . '/data/readOnly/' . $time . $postvar['name'];
                    file_put_contents($destination, $verEntity->getContent());
                    chmod($destination, 0444);
                    $aenderung[] = 'ReadOnly';
                }

                if (isset($aenderung)) {
                    if ($validator->validate($_SESSION['Userid'], $fileId, 'update')) {
                        $fileEntity->setVerAmount($fileVerAmount + 1);
                        $em->persist($fileEntity);
                        $em->flush();
                        $verEntity->setFileId($fileId);
                        $verEntity->setVerNr($fileVerAmount + 1);
                        $verEntity->setTime($time);
                        $verEntity->setEditor($editor);
                        $verEntity->setHash(hash('sha256', $verEntity->getTime() . $verEntity->getContent()));

                        $em = $this->getServiceLocator()->get('ObjectManager');
                        $em->persist($verEntity, $fileEntity->getBackup());
                        $em->flush();

                        $response['success'] = implode($aenderung, ', ') . ' erfolgreich geändert';
                    } else {
                        $response['error'] = 'Keine Berechtigung';
                    }
                } else {
                    $response['warning'] = 'Keine änderungen!';
                }
            } else {
                $userRepo = $em->getRepository('RoleAPI\Entity\User')->findOneBy(
                array('UserId' => $fileEntity->getChecked()));
                $user[] = $userRepo->getFirstName();
                $user[] = $userRepo->getLastName();
                $user[] = $userRepo->getLogin();
                $response['warning'] = 'Datei wurde aus gechecked von ' . $user[2] . ' [' . $user[0] . ' ' .
                 $user[1] . ']';
            }
        } else {
            $response['warning'] = 'Datei darf nicht geändert werden!';
        }

        return new JsonModel($response);
    }

    public function checkedAction()
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $api = $this->serviceLocator->get('RoleAPI');
        $editor = $_SESSION['Userid'];
        $postvar = $this->getRequest()->getPost();
        $fileLoc = $postvar['location'];

        $branch = $em->getRepository('RoleAPI\Entity\BranchName')->findOneBy(array('Location' => $fileLoc));
        $fileEntity = $em->getRepository('MetaAPI\Entity\File')->findOneBy(
        array('Branch' => $branch->getBranchId()));

        if ($fileEntity->getChecked() === null) {
            $fileEntity->setChecked($editor);
            $response['success'] = 'Datei wurde aus gechecked!';
        } else if ($fileEntity->getChecked() !== null && ($fileEntity->getChecked() === $editor || $api->isAllowed($editor, $fileLoc, 'isadmin'))) {
            $fileEntity->setChecked(null);
            $response['success'] = 'Datei wurde ein gechecked!';
        } else {
            $userRepo = $em->getRepository('RoleAPI\Entity\User')->findOneBy(
            array('UserId' => $fileEntity->getChecked()));
            $user[] = $userRepo->getFirstName();
            $user[] = $userRepo->getLastName();
            $user[] = $userRepo->getLogin();
            $response['warning'] = 'Datei wurde aus gechecked von ' . $user[2] . ' [' . $user[0] . ' ' .
            $user[1] . ']';
        }
        $em->persist($fileEntity);
        $em->flush();
        return new JsonModel($response);
    }

    /**
     * Save the user file into the filesystem
     *
     * @return JsonModel $returnResponse as array -> error/success/warning
     */
    public function uploadAction()
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $api = $this->getServiceLocator()->get('RoleAPI');

        $editor = $_SESSION['Userid'];
        $postvar = $this->getRequest()->getPost();

        $time = time();
        $keyWords = isset($postvar['keywords']) ? $postvar['keywords'] : null;
        $backupLevel = $postvar['backupLevel'] - 1;
        $location = $postvar['location'];
        $fileName = $postvar['name'];
        $owner = $postvar['owner'];
        $readOnly = $postvar['readOnly'];
        $ownerId = $api->getUserId($owner);
        $file = $this->getRequest()->getFiles();
        $type = $file['file']['type'];

        $fileEntity = new \MetaAPI\Entity\File();
        $branch = new \RoleAPI\Entity\BranchName();
        $verEntity = new \DataAPI\Entity\FileVersion();

        $vergleichName = array();
        $childrenLocation = $api->getChildrenEntities($location);
        foreach ($childrenLocation as $vergleich) {
            $vergleichName[] = $vergleich->getName();
        }

        if ($this->isUpload($file)) {
            if (!(in_array($fileName, $vergleichName))) {

                $content = file_get_contents($file['file']['tmp_name']);

                $pathinfo = pathinfo($fileName);

                if (isset($pathinfo['extension'])) {
                    $ext = strtolower($pathinfo['extension']);
                    $typeExt = $this->typeFile($ext);
                    if ($pathinfo['filename']) {
                        if (isset($typeExt)) {
                            $helplocation = count($childrenLocation);
                            $helplocation++;
                            if ($location === 'dir://') {
                                $location = str_replace('dir', $typeExt, $location);
                                $newlocation = $location . 'A' . $helplocation;
                            } else {
                                $location = str_replace('dir', $typeExt, $location);
                                $newlocation = $location . ':A' . $helplocation;
                            }
                            if ($readOnly === 'true' || $readOnly === '1' || $readOnly === 'on') {
                                $fileEntity->setReadOnly(1);
                                if (!is_dir(getcwd() . '/data/readOnly/')) {
                                    mkdir(getcwd() . '/data/readOnly/');
                                }
                                $destination = getcwd() . '/data/readOnly/' . $time . $fileName;
                                move_uploaded_file($file['file']['tmp_name'], $destination);
                                chmod($destination, 0444);
                            } else {
                                $fileEntity->setReadOnly(0);
                            }

                            $branch->setName($fileName);
                            $branch->setLocation($newlocation);
                            $em->persist($branch);
                            $em->flush();

                            $branchId = $branch->getBranchId();
                            $fileEntity->setName($fileName);
                            $fileEntity->setVerAmount(1);
                            $fileEntity->setBranch($branchId);
                            $fileEntity->setAuthor($editor);
                            $fileEntity->setType($type);
                            $fileEntity->setBackup($backupLevel);
                            $fileEntity->setKeyWords($keyWords);
                            $em->persist($fileEntity);
                            $em->flush();
                            $verEntity->setFileId($fileEntity->getFileId());

                            $verEntity->setVerNr($fileEntity->getVerAmount());
                            $verEntity->setTime($time);
                            $verEntity->setEditor($editor);
                            $verEntity->setOwner($ownerId);
                            $this->setContent($fileEntity, $verEntity, $content);
                            $verEntity->setHash(hash('sha256', $verEntity->getTime() . $content));
                            $em->persist($verEntity, $backupLevel);
                            $em->flush();
                            $returnResponse['success'] = 'Upload erfolgreich';
                        } else {
                            $returnResponse['warning'] = 'Diese ist keine gültige Datei!';
                        }
                    } else {
                        $returnResponse['warning'] = 'Bitte einen Dateinamen eingeben!';
                    }
                } else {
                    $returnResponse['warning'] = 'Bitte an die Datei einen Dateityp setzten !';
                }
            } else {
                $returnResponse['warning'] = 'Upload fehlgeschlagen Datei schon vorhanden';
            }
        } else {
            $returnResponse['error'] = 'Upload fehlgeschlagen';
        }

        return new JsonModel($returnResponse);
    }

    /**
     * Analyzed the uploaded data whether these are ok.
     *
     * @return true/false
     */
    public function isUpload($file)
    {
        return is_uploaded_file($file['file']['tmp_name']);
    }

    /**
     * Deal with the downloadable data.
     * Give the content of the file so that
     * the user these can download.
     */
    public function downloadAction()
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $gettvar = $this->getRequest()->getQuery();
        $fileLoc = $gettvar['location'];

        $branchRepo = $em->getRepository('RoleAPI\Entity\BranchName');
        $branch = $branchRepo->findOneBy(array('Location' => $fileLoc));
        $branchId = $branch->getBranchId();

        $fileRepo = $em->getRepository('MetaAPI\Entity\File');
        $file = $fileRepo->findOneBy(array('Branch' => $branchId));
        $fileId = $file->getFileId();
        $fileVerAmount = $file->getVerAmount();

        $fileVerRepo = $em->getRepository('DataAPI\Entity\FileVersion');
        $fileVer = $fileVerRepo->findOneBy(array('FileId' => $fileId, 'VerNr' => $fileVerAmount));

        $fileContent = $fileVer->getContent();

        if ($fileVer->getHash() === hash('sha256', $fileVer->getTime() . $fileContent)) {
            header('Content-Description: File Transfer');
            header('Content-Type: ' . $file->getType());
            header('Content-Disposition: attachment; filename=' . $file->getName());
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . strlen($fileContent));
            ob_clean();
            flush();
            echo $fileContent;
        } else {
            $response = new Response();
            $response->setContent('Hash des Files stimmt nicht überein');

            return $response;
        }
        throw new \RuntimeException();
    }

    /**
     * Delete temporary the data.
     * Set into the database an entry in the column trash.
     *
     * @return $returnResponse error/success/warning
     */
    public function deleteAction()
    {
        $api = $this->getServiceLocator()->get('RoleAPI');
        $em = $this->getServiceLocator()->get('ObjectManager');
        $editor = $_SESSION['Userid'];
        $time = time();
        $postvar = $this->getRequest()->getPost();
        $location = $postvar['location'];

        $branch = $em->getRepository('RoleAPI\Entity\BranchName')->findOneBy(array('Location' => $location));

        $dir = strpos($branch->getLocation(), 'dir');
        if ($dir !== false) {
            // $dirChilds = $this->deleteChilds($location, $api);
            $returnResponse['information'] = 'Ordner können nocht nicht gelöscht werden';
        } else {
            $trashNameBranch = 'trash.' . $branch->getName();
            $branchId = $branch->getBranchId();

            $fileEntity = $em->getRepository('MetaAPI\Entity\File')->findOneBy(array('Branch' => $branchId));
            $trashNameFile = 'tash.' . $fileEntity->getName();

            $fileEntity->setName($trashNameFile);
            $em->persist($fileEntity);

            $trashInfo[] = $editor;
            $trashInfo[] = $time;
            $trashInfo = json_encode($trashInfo);
            $branch->setName($trashNameBranch);
            $branch->setTrash(1);
            $branch->setTrashInfo($trashInfo);
            $em->persist($branch);
            $em->flush();
            $returnResponse['success'] = 'Erfolgreich gelöscht';
        }
        return new JsonModel($returnResponse);
    }

    /**
     * Search all childs of the parent folder
     *
     * @return $return as array -> the location
     */
    public function deleteChilds($location, $api)
    {
        $return = array();
        $children = $api->getChildrenEntities($location);

        foreach ($children as $child) {
            if ($child) {
                $return[$child->getLocation()] = $this->deleteChilds($child->getLocation(), $api);
            }
        }
        return $return;
    }

    /**
     * Find a exist location an count up for the uploaded file
     *
     * @return $newlocation as string
     */
    public function newLocation($fileEntity, $api, $location)
    {
        $fileName = $fileEntity->getName();
        $pathinfo = pathinfo($fileName);
        $ext = strtolower($pathinfo['extension']);
        $type = $this->typeFile($ext);
        if (isset($type)) {
            $childrenLocation = $api->getChildrenEntities($location);
            $helplocation = count($childrenLocation);
            $helplocation++;
            if ($location === 'dir://') {
                $location = str_replace('dir', $type, $location);
                $newlocation = $location . 'A' . $helplocation;
            } else {
                $location = str_replace('dir', $type, $location);
                $newlocation = $location . ':A' . $helplocation;
            }
            return $newlocation;
        }
    }

    public function setContent($fileEntity, $fileVersionEntity, $content)
    {
        if (strpos($fileEntity->getType(), 'text') === 0) {
            $fileVersionEntity->setSearch($content);
        } else {
            $fileVersionEntity->setContent($content);
        }
    }

    /**
     * Find the type of a given extension and return
     * the type for the branch identification
     *
     * @param string $ext
     *
     * @return string $key
     */
    public function typeFile($ext)
    {
        $types['doc'] = array('txt', 'me', 'new', 'nfo', 'sdf', 'pmx', 'doc', 'dot', 'cpd', 'odt', 'docx', 'log',
        'bbs', 'rtf', 'cfg', 'wps', 'wri', 'awd', 'wpd');

        $types['notice'] = array('notice');

        $types['image'] = array('bmp', 'mng', 'wmf', 'ufo', 'rle', 'pct', 'tga', 'pcx', 'pic', 'eps', 'lbm', 'fif',
        'fpx', 'img', 'gif', 'cdr', 'jpg', 'jpeg', 'psp', 'psd', 'tif', 'vsd', 'wmf', 'png');

        $types['archive'] = array('zip', 'dwc', 'lha', 'zoo', 'lzs', 'lif', 'hyp', 'diz', 'rar', 'tar', 'tar.gz',
        'gz', '7z', 'ace', 'arc', 'arj');

        $types['excel'] = array('xls', 'csv', 'xlc', 'xlsx', 'xlt', 'frm', 'xlt', 'wks', 'ods', 'ots', 'stc',
        'sxc', 'uos');

        $types['pdf'] = array('pdf');

        $types['pres'] = array('cmx', 'ppt', 'pps', 'odp', 'pptx');

        $types['code'] = array('php', 'java', 'prg', 'ini', 'sql', 'xlm', 'xla', 'inf', 'mac', 'asm', 'asp', 'bas',
        'c', 'cpp', 'bat', 'bin', 'sh', 'phtml', 'css', 'htm', 'html', 'stm', 'xml');

        $types['exec'] = array('exe', 'com');

        $types['media'] = array('wav', 'wma', 'midi', 'rmi', 'vob', 'voc', 'vdo', 'ogg', 'snd', 'dvi', 'mp3',
        'mpg', 'mpeg', 'au', 'avi', 'wmv', 'mov');

        $types['mail'] = array('dcx', 'cpe', 'eml');

        foreach ($types as $key => $type) {
            if (in_array($ext, $type)) {
                return $key;
            }
        }
    }
}
