<?php

namespace DataAPI\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Zend\Form\Annotation;
/**
 * @ORM\Entity
 * FileSigner
 *
 * @ORM\Table(name="FileSigner")
 *
 * @property int $Id
 * @property int $FileId
 * @property int $UserId
 * @property int $Signer
 * @property bool $Signed
 * @property varchar $Comment
 * @property int $RequestTime
 * @property int $ConfirmedTime
 *
 */

class FileSigner
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */

    protected $Id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $FileId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $UserId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $Signer;

    /**
     * @ORM\Column(type="boolean", options={"default" = false} )
     */
    protected $Signed;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $Comment;

    /**
     * @ORM\Column(type="integer")
     */
    protected $RequestTime;

    /**
     * @ORM\Column(type="integer")
     */
    protected $ConfirmedTime;
	/**
     * @return the $Signer
     */
    public function getSigner()
    {
        return $this->Signer;
    }

	/**
     * @param number $Signer
     */
    public function setSigner($Signer)
    {
        $this->Signer = $Signer;
    }

	/**
     * @return the $Id
     */
    public function getId()
    {
        return $this->Id;
    }

	/**
     * @return the $FileId
     */
    public function getFileId()
    {
        return $this->FileId;
    }

	/**
     * @return the $UserId
     */
    public function getUserId()
    {
        return $this->UserId;
    }

	/**
     * @return the $Signed
     */
    public function getSigned()
    {
        return $this->Signed;
    }

	/**
     * @return the $Comment
     */
    public function getComment()
    {
        return $this->Comment;
    }

	/**
     * @return the $RequestTime
     */
    public function getRequestTime()
    {
        return $this->RequestTime;
    }

	/**
     * @return the $ConfirmedTime
     */
    public function getConfirmedTime()
    {
        return $this->ConfirmedTime;
    }

	/**
     * @param number $FileId
     */
    public function setFileId($FileId)
    {
        $this->FileId = $FileId;
    }

	/**
     * @param number $UserId
     */
    public function setUserId($UserId)
    {
        $this->UserId = $UserId;
    }

	/**
     * @param field_type $Signed
     */
    public function setSigned($Signed)
    {
        $this->Signed = $Signed;
    }

	/**
     * @param varchar $Comment
     */
    public function setComment($Comment)
    {
        $this->Comment = $Comment;
    }

	/**
     * @param number $RequestTime
     */
    public function setRequestTime($RequestTime)
    {
        $this->RequestTime = $RequestTime;
    }

	/**
     * @param number $ConfirmedTime
     */
    public function setConfirmedTime($ConfirmedTime)
    {
        $this->ConfirmedTime = $ConfirmedTime;
    }

}