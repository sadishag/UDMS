<?php
namespace Notice\Service;

class NoticeActionValidatorTest extends \PHPUnit_Framework_TestCase

{
    protected $_controller;

    public function provideTestvalidate()
    {
        $array = array('user' => array(1 => 1), 'group' => array(), 'role' => array());
        $stream = fopen('php://memory','r+');
        fwrite($stream, base64_encode(serialize($array)));
        rewind($stream);

        $array2 = array('group' => array(1 => 1), 'user' => array(), 'role' => array());
        $stream2 = fopen('php://memory','r+');
        fwrite($stream2, base64_encode(serialize($array2)));
        rewind($stream2);

        $array3 = array('role' => array(1 => 1), 'user' => array(), 'group' => array());
        $stream3 = fopen('php://memory','r+');
        fwrite($stream3, base64_encode(serialize($array3)));
        rewind($stream3);

        return array(
            array('text/notice', $stream),
            array('text/notice', $stream2),
            array('text/notice', $stream3),
            array('text/notice', ''),
            array('pdf', '')
        );
    }

    /**
     * @dataProvider provideTestvalidate
     */
    public function testValidate($type, $vision)
    {
        $rightKey = array('read' => true);
        $stream = fopen('php://memory','r+');
        fwrite($stream, base64_encode(serialize($rightKey)));
        rewind($stream);

        $fileMock = $this->getMock('MetaAPI\Entity\File', array('getVision', 'getAuthor', 'getType', 'getRightKey'));
        $fileMock->expects($this->any())
            ->method('getVision')
            ->will($this->returnValue($vision));
        $fileMock->expects($this->any())
            ->method('getAuthor')
            ->will($this->returnValue(1));
        $fileMock->expects($this->any())
            ->method('getType')
            ->will($this->returnValue($type));
        $fileMock->expects($this->any())
            ->method('getRightKey')
            ->will($this->returnValue($stream));

        $groupMock = $this->getMock('RoleAPI\Entity\Groups', array('getGroupId', 'getRoleId'));
        $groupMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));
        $groupMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));

        $repo = $this->getMock('Repository', array('findOneBy', 'findBy'));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($fileMock));
        $repo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($groupMock)));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller = new NoticeActionValidator($smMock);

        $_SESSION['Userid'] = 1;

        $this->_controller->validate(1, 1, 'read');
    }
}