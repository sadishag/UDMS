<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view')),

    'router' => array(
        'routes' => array(
            'groupadmin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/groupadmin/:groupid[/:action][/id/:id][/page/:page]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                        'defaults' => array(
                            'controller' => 'GroupAdmin\Controller\Index', 'action' => 'index', 'page' => 1
                        )
                )
            )
        ),


    ),


    'controllers' => array(
        'invokables' => array(
            'GroupAdmin\Controller\Index' => 'GroupAdmin\Controller\IndexController',
            'GroupAdmin\Controller\Group'=> 'GroupAdmin\Controller\GroupController'
             )
    )
    );
