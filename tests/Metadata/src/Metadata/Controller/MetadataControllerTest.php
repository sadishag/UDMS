<?php
namespace Metadata\Controller;


use Metadata\Controller\MetadataController;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;


class MetadataControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {
    	$this->_controller = new MetadataController();
    }
    public function tearDown()
    {
    }

    public function testshowActionStandardTypes()
    {
        $postvar = array('metaDataSave' => '1', 'selectedTypeId' => '1','sizeofarray'=>'2', 'MaxLengthInput0' => '1', 'hiddenMaxLength0' => '2', 'Interactive' == '0', 'NameInput0' => 'Name', 'MaxSizeInput0' => '1', 'hiddenMaxSize' => '2');
        $this->_controller->getRequest()->setMethod('post')->setPost(new Parameters($postvar));

        $albumMock = $this->getMock('MetaAPI\Entity\MetaDataTypes', array('getReadOnly','getBackup','getVerAmount','getLogin','getFileId','getName', 'getMediatyp', 'getSubtyp', 'getMaxLength','getBranch', 'getMaxSize', 'getInteractive', 'getResourceUrl','getAuthor'));
    	$albumMock->expects($this->any())
    	->method('getName')
    	->will($this->returnValue('!Typname'));

    	$repo = $this->getMock('Repository', array('findby', 'findoneby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue(array($albumMock,$albumMock,$albumMock)));
    	$repo->expects($this->any())
    	->method('findOneBy')
    	->will($this->returnValue($albumMock));

    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'));
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));

    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValue($em));

    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->showAction();

    }

    public function testshowDefinedTypes()
    {

        $albumMock = $this->getMock('MetaAPI\Entity\MetaDataTypes', array('getName', 'getMediatyp', 'getSubtyp', 'getMaxLength', 'getMaxSize', 'getInteractive', 'getResourceUrl'));
        $albumMock->expects($this->any())
        ->method('getName')
        ->will($this->returnValue('Typname'));

        $repo = $this->getMock('Repository', array('findby', 'findoneby'));
        $repo->expects($this->any())
        ->method('findby')
        ->will($this->returnValue(array($albumMock,$albumMock,$albumMock)));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository'));
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->showAction();

    }

    public function testshowActionMetaDataSave()
    {
        $postvar = array('metaDataSave' => '1','sizeofarray'=>'2', 'metaTypeId0' => '1', 'MaxLengthInput0' => '1', 'hiddenMaxLength0' => '2', 'Interactive' == '0', 'NameInput0' => 'NewName', 'hiddenName0' => 'Name', 'MaxSizeInput0' => '1', 'hiddenMaxSize' => '2');
        $this->_controller->getRequest()->setMethod('post')->setPost(new Parameters($postvar));
        
        $albumMock = $this->getMock('MetaAPI\Entity\MetaDataTypes', array('getReadOnly','getBackup','getVerAmount','getLogin','getFileId','getName', 'getMediatyp', 'getSubtyp', 'getMaxLength','getBranch', 'getMaxSize', 'getInteractive', 'getResourceUrl','getAuthor'));
        $albumMock->expects($this->any())
        ->method('getName')
        ->will($this->returnValue('NewName'));
        
        $repo = $this->getMock('Repository', array('findby', 'findoneby'));
        $repo->expects($this->any())
        ->method('findby')
        ->will($this->returnValue(array($albumMock)));
        $repo->expects($this->any())
        ->method('findoneBy')
        ->will($this->returnValue($albumMock));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'));
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->showAction();

    }
    
    public function testshowActionMetaDataSaveRenamedType()
    {
    	$postvar = array('metaDataSave' => '1','sizeofarray'=>'2', 'metaTypeId0' => '1', 'MaxLengthInput0' => '1', 'hiddenMaxLength0' => '2', 'Interactive' == '0', 'NameInput0' => 'NewName', 'hiddenName0' => 'Name', 'MaxSizeInput0' => '1', 'hiddenMaxSize' => '2');
    	$this->_controller->getRequest()->setMethod('post')->setPost(new Parameters($postvar));
    
    	$albumMock = $this->getMock('MetaAPI\Entity\MetaDataTypes', array('getReadOnly','getBackup','getVerAmount','getLogin','getFileId','getName', 'getMediatyp', 'getSubtyp', 'getMaxLength','getBranch', 'getMaxSize', 'getInteractive', 'getResourceUrl','getAuthor'));
    	$albumMock->expects($this->any())
    	->method('getName')
    	->will($this->returnValue('Name'));
    
    	$repo = $this->getMock('Repository', array('findby', 'findoneby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue(array($albumMock)));
    	$repo->expects($this->any())
    	->method('findoneBy')
    	->will($this->returnValue($albumMock));
    
    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'));
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));
    
    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValue($em));
    
    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->showAction();
    
    }
    public function testshowActionSelectedType()
    {
        $postvar = array('selectedTypeId' => '1','sizeofarray'=>'2', 'MaxLengthInput0' => '1', 'hiddenMaxLength0' => '2', 'Interactive' == '0', 'NameInput0' => 'Name');
        $this->_controller->getRequest()->setMethod('post')->setPost(new Parameters($postvar));

        $albumMock = $this->getMock('MetaAPI\Entity\MetaDataTypes', array('getReadOnly','getBackup','getVerAmount','getLogin','getFileId','getName', 'getMediatyp', 'getSubtyp', 'getMaxLength','getBranch', 'getMaxSize', 'getInteractive', 'getResourceUrl','getAuthor'));
        $albumMock->expects($this->any())
        ->method('getName')
        ->will($this->returnValue('!Typname'));

        $repo = $this->getMock('Repository', array('findby', 'findoneby'));
        $repo->expects($this->any())
        ->method('findby')
        ->will($this->returnValue(array()));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'));
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->showAction();

    }
    public function testshowActionNoSaveNoSelect()
    {
    $postvar = array();

    $albumMock = $this->getMock('MetaAPI\Entity\MetaDataTypes', array('getReadOnly','getBackup','getVerAmount','getLogin','getFileId','getName', 'getMediatyp', 'getSubtyp', 'getMaxLength','getBranch', 'getMaxSize', 'getInteractive', 'getResourceUrl','getAuthor'));
    $albumMock->expects($this->any())
    ->method('getName')
    ->will($this->returnValue('!Typname'));

    $repo = $this->getMock('Repository', array('findby', 'findoneby'));
    $repo->expects($this->any())
    ->method('findby')
    ->will($this->returnValue(array($albumMock,$albumMock,$albumMock)));
    $repo->expects($this->any())
    ->method('findOneBy')
    ->will($this->returnValue($albumMock));

    $em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'));
    $em->expects($this->any())
    ->method('getRepository')
    ->will($this->returnValue($repo));

    $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    $smMock->expects($this->any())
    ->method('get')
    ->will($this->returnValue($em));

    $this->_controller->setServiceLocator($smMock);
    $this->_controller->showAction();

    }

    public function testshowActionMetaDataSaveInteractiveCheck()
    {
        $postvar = array('metaDataSave' => '1','sizeofarray'=>'2', 'MaxLengthInput0' => '1', 'hiddenMaxLength0' => '2', 'Interactive0' => '0', 'NameInput0' => 'Name', 'MaxSizeInput0' => '1', 'hiddenMaxSize' => '2');
        $this->_controller->getRequest()->setMethod('post')->setPost(new Parameters($postvar));

        $albumMock = $this->getMock('MetaAPI\Entity\MetaDataTypes', array('getReadOnly','getBackup','getVerAmount','getLogin','getFileId','getName', 'getMediatyp', 'getSubtyp', 'getMaxLength','getBranch', 'getMaxSize', 'getInteractive', 'getResourceUrl','getAuthor'));
        $albumMock->expects($this->any())
        ->method('getInteractive')
        ->will($this->returnValue('1'));
        $albumMock->expects($this->any())
        ->method('getSubtyp')
        ->will($this->returnValue(''));

        $repo = $this->getMock('Repository', array('findby', 'findoneby'));
        $repo->expects($this->any())
        ->method('findby')
        ->will($this->returnValue(array($albumMock,$albumMock,$albumMock)));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'));
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->showAction();

    }
    public function testshowActionMetaDataSaveInputMaxValuesAreEmptyButChanged()
    {
    	$postvar = array('metaDataSave' => '1','sizeofarray'=>'2', 'metaTypeId0' => '1', 'MaxLengthInput0' => '0', 'hiddenMaxLength0' => '1', 'Interactive' == '0', 'NameInput0' => 'NewName', 'hiddenName0' => 'Name', 'MaxSizeInput0' => '', 'hiddenMaxSize0' => '1');
    	$this->_controller->getRequest()->setMethod('post')->setPost(new Parameters($postvar));
    
    	$albumMock = $this->getMock('MetaAPI\Entity\MetaDataTypes', array('getReadOnly','getBackup','getVerAmount','getLogin','getFileId','getName', 'getMediatyp', 'getSubtyp', 'getMaxLength','getBranch', 'getMaxSize', 'getInteractive', 'getResourceUrl','getAuthor'));
    	$albumMock->expects($this->any())
    	->method('getName')
    	->will($this->returnValue('Name'));
    
    	$repo = $this->getMock('Repository', array('findby', 'findoneby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue(array($albumMock)));
    	$repo->expects($this->any())
    	->method('findoneBy')
    	->will($this->returnValue($albumMock));
    
    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'));
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));
    
    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValue($em));
    
    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->showAction();
    
    }
}