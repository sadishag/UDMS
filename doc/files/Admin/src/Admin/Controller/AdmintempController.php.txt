<?php
namespace Admin\Controller;
/**
 * AdmintempController.php
 *
 * @package    Admin\Controller
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Sebastian Krebs <sebastian.krebs@unister.de>
 */
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Controller for view\Admin\admintemp
 *
 * @copyright Copyright (c) 2012 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Sebastian Krebs <sebastian.krebs@unister.de>
 *
 */

class AdmintempController extends AbstractActionController
{
    /**
     * Function to Controlle the index.phtml
     * read all temporäre user
     * Give a Admin the Option to Check the temp. User
     * @return mixed array $returnarray
     *     strings
     *     arrays with Objects
     */

    public function indexAction()
    {

        $userid = $_SESSION['Userid'];
        $em = $this->getServiceLocator()->get('ObjectManager');
        $api = $this->serviceLocator->get('RoleAPI');
        $ressource = 0;
        $action = 'isadmin';
        $returnarray = array();
        if ($api->isAllowed($userid, $ressource, $action)) {

            $postvar = $this->getRequest()->getPost();
            if (isset($postvar['sizeofpost'])) {
                for ($i = 0; $i < $postvar['sizeofpost']; $i++) {
                    if (isset($postvar['tempchange' . $i]) && $postvar['tempchange' . $i] == 1) {
                        $newTempUser = $em->getRepository('RoleAPI\Entity\User')->findOneBy(
                        array('UserId' => $postvar['user' . $i]));
                        $helptemp = $newTempUser->getTemp();
                        $newTempUser->setTemp($helptemp - 2);
                        $em->persist($newTempUser);
                        $em->flush();
                        $returnarray['success'] = 'Änderrung wurde erfolgreich übernommen';

                    }
                    if (isset($postvar['tempchange' . $i]) && $postvar['tempchange' . $i] == 2) {
                        $newTempUser = $em->getRepository('RoleAPI\Entity\User')->findOneBy(
                        array('UserId' => $postvar['user' . $i]));
                        $newTempUserRole = $em->getRepository('RoleAPI\Entity\Roles')->findOneBy(
                        array('UserId' => $postvar['user' . $i],
                              'GroupId' => 0));
                        $em->remove($newTempUserRole);
                        $em->remove($newTempUser);
                        $em->flush();
                        $returnarray['success'] = 'User wurde erfolgreich gelöscht';

                    }
                    if (isset($postvar['tempchange' . $i]) && $postvar['tempchange' . $i] == 3) {
                        $newTempUser = $em->getRepository('RoleAPI\Entity\User')->findOneBy(
                        array('UserId' => $postvar['user' . $i]));
                        $returnarray['userinformation'] = $newTempUser;
                        $returnarray['hashvalue'] = sha1($newTempUser->getEmail() . $newTempUser->getLogin());
                        $returnarray['information'] = 'Code wurde erstellt';

                    }

                }
            }

            $tempUser1 = $em->getRepository('RoleAPI\Entity\User')->findby(array('Temp' => 1));
            $tempUser2 = $em->getRepository('RoleAPI\Entity\User')->findby(array('Temp' => 2));
            $tempUser3 = $em->getRepository('RoleAPI\Entity\User')->findby(array('Temp' => 3));

            if (empty($tempUser1) && empty($tempUser2) && empty($tempUser3)) {

                $returnarray['information'] = 'Keine Temporaeren User vorhanden';
            } else {
                foreach ($tempUser1 as $user1) {
                    $returnarray['tempuser'][] = $user1;
                }
                foreach ($tempUser2 as $user2) {
                    $returnarray['tempuser'][] = $user2;
                }
                foreach ($tempUser3 as $user3) {
                    $returnarray['tempuser'][] = $user3;
                }

            }



        } else {
            return $this->redirect()->toUrl('/Angemeldet');
        }
        return $returnarray;
    }
}
