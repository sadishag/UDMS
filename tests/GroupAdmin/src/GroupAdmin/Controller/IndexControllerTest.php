<?php

namespace GroupAdmin\Controller;

use GroupAdmin\Controller\IndexController;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;

class IndexControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {

        $this->_controller = new IndexController();

    }

    public function tearDown()
    {

    }

    public function testindexAction()
    {

        $post = array('userFirst' => '123123', 'userLast' => '354345', 'userglobalsearch' => 'test',
        'rolesave' => 'sadd', 'groupSearch' => 'asdasd', 'grupperolesichern' => 'asdasd');
        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $groupNameMock = $this->getMock('RoleAPI\Entity\GroupName', array('getName', 'getFirstName', 'getLastName', 'getUserId'));
        $groupNameMock->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('test'));
        $groupNameMock->expects($this->any())
        ->method('getFirstName')
        ->will($this->returnValue('test'));
        $groupNameMock->expects($this->any())
        ->method('getLastName')
        ->will($this->returnValue('test'));
        $groupNameMock->expects($this->any())
        ->method('getUserId')
        ->will($this->returnValue('1'));

        $groupMock = $this->getMock('RoleAPI\Entity\Groups');

        $userMock = $this->getMock('RoleAPI\Entity\User');


        $repo = $this->getMock('Repository', array('findAll', 'findOneBy', 'find', 'findBy'));
        $repo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($groupMock, $groupMock)));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($groupNameMock));
        $repo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue($userMock));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($userMock));

        $apiMock = $this->getMock('RoleAPI', array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValueMap($mapmock));

        $route = new RouteMatch(array('id' => '1', 'groupid' => '2'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();

    }

    public function testAddAction()
    {
        $post = array('userFirst' => '123123', 'userLast' => '354345', 'userglobalsearch' => 'test',
        'rolesave' => 'sadd', 'groupSearch' => 'asdasd', 'grupperolesichern' => 'asdasd');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $groupNameMock = $this->getMock('RoleAPI\Entity\GroupName', array('getName'));
        $groupNameMock->expects($this->any())
        ->method('getName')
        ->will($this->returnValue('test'));

        $groupMock = $this->getMock('RoleAPI\Entity\Groups');

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy', 'find', 'findBy'));
        $repo->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue($groupMock));
        $repo->expects($this->any())
        ->method('find')
        ->will($this->returnValue($groupNameMock));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $route = new RouteMatch(array('id' => '1'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->addAction();
    }

    public function testAddActionOhnePost()
    {
        $groupNameMock = $this->getMock('RoleAPI\Entity\GroupName', array('getName'));
        $groupNameMock->expects($this->any())
        ->method('getName')
        ->will($this->returnValue('test'));

        $groupMock = $this->getMock('RoleAPI\Entity\Groups');

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('find', 'findBy'));
        $repo->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue($groupMock));
        $repo->expects($this->any())
        ->method('find')
        ->will($this->returnValue($groupNameMock));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository'));
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $route = new RouteMatch(array('id' => '1'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->addAction();
    }

    public function testAddUserToGroupAction()
    {
        $post = array('userFirst' => '123123', 'userLast' => '354345', 'userglobalsearch' => 'test',
        'rolesave' => 'sadd', 'groupSearch' => 'asdasd', 'grupperolesichern' => 'asdasd');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $groupNameMock = $this->getMock('RoleAPI\Entity\GroupName', array('getGroupName'));
        $groupNameMock->expects($this->any())
        ->method('getName')
        ->will($this->returnValue('test'));

        $groupMock = $this->getMock('RoleAPI\Entity\Groups', array('getUserId', 'getGroupId', 'getTemp', 'getHash'));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy', 'find', 'findBy'));
        $repo->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue($groupMock));
        $repo->expects($this->any())
        ->method('find')
        ->will($this->returnValue($groupNameMock));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $route = new RouteMatch(array('id' => '1', 'groupid' => '1'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->addUserToGroupAction();
    }

    public function testAddUserToGroupActionOhneUser()
    {
        $post = array('userFirst' => '123123', 'userLast' => '354345', 'userglobalsearch' => 'test',
        'rolesave' => 'sadd', 'groupSearch' => 'asdasd', 'grupperolesichern' => 'asdasd');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $groupNameMock = $this->getMock('RoleAPI\Entity\GroupName', array('getGroupName'));
        $groupNameMock->expects($this->any())
        ->method('getName')
        ->will($this->returnValue('test'));

        $groupMock = $this->getMock('RoleAPI\Entity\Groups', array('getUserId', 'getGroupId', 'getTemp', 'getHash'));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy', 'find', 'findBy'));
        $repo->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue(false));
        $repo->expects($this->any())
        ->method('find')
        ->will($this->returnValue($groupNameMock));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $route = new RouteMatch(array('id' => '1', 'groupid' => '1'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->addUserToGroupAction();
    }
}
