<?php

/**
 * NoticeupdateController.php
 *
 * @package    Notice\Controller
 * @copyright  Copyright (c) 2013 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Thomas Kröning <thomas.kroening@unister.de>
 * @author     Sebastian Fritze <sebastian.fritze@unister.de>
 */
namespace Notice\Controller;

use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Notice\Controller\NoticeController;

class NoticeupdateController extends NoticeController
{

    /**
     * Action to show all of your Note
     *
     * @return notizen as array
     */
    public function showAction()
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $userid = $_SESSION['Userid'];

        $notices = $em->getRepository('MetaAPI\Entity\File')->findBy(array('Type' => 'text/notice'));
        $fileVersionRepo = $em->getRepository('DataAPI\Entity\FileVersion');
        $notizen = array();
        $branchRepo = $em->getRepository('RoleAPI\Entity\BranchName');

        foreach ($notices as $notice) {
            $newestNoticeVersion = $fileVersionRepo->findOneBy(
            array('FileId' => $notice->getFileId(), 'VerNr' => $notice->getVerAmount()));

            $trash = $branchRepo->findOneBy(array('BranchId' => $notice->getBranch()))
                ->getTrash();

            if ($newestNoticeVersion->getOwner() === $userid && $trash === null) {
                $notizen[] = $notice;
            }
        }
        $visibles = $this->getVisibles($userid, $notizen);

        return array('notizen' => $notizen, 'visible' => $visibles);
    }

    /**
     * gets for the user visible notices
     *
     * @param int $userid
     * @param array $notizen
     * @return array $returnarray
     */
    public function getVisibles($userid, $notizen)
    {
        $returnarray = array();
        $noteIds = array();

        $em = $this->getServiceLocator()->get('ObjectManager');

        $byUser = $this->getVisiblesByUser($userid);
        $byGroup = $this->getVisiblesByGroup($userid);
        $byRole = $this->getVisiblesByRole($userid);

        $metaIds = array_unique(array_merge($byUser, $byGroup, $byRole));
        sort($metaIds);

        foreach ($notizen as $notiz) {
            $noteId = $notiz->getFileId();

            if (in_array($noteId, $metaIds)) {
                $index = array_keys($metaIds, $noteId);
                unset($metaIds[$index[0]]);
            }
        }

        foreach ($metaIds as $metaId) {
            $noteData = $em->getRepository('MetaAPI\Entity\File')->findOneBy(array('FileId' => $metaId));
            $returnarray[] = $noteData;
        }
        return $returnarray;
    }

    /**
     * gets the metaIds of visible notices by the users userId
     *
     * @param int $userid
     * @return array $returnarray
     */
    public function getVisiblesByUser($userid)
    {
        $returnarray = array();

        $em = $this->getServiceLocator()->get('ObjectManager');
        $metaDatas = $em->getRepository('MetaAPI\Entity\File')->findAll();

        foreach ($metaDatas as $metaData) {
            if (is_resource($metaData->getVision())) {
                $stringContent = stream_get_contents($metaData->getVision());
                rewind($metaData->getVision());
                $vision = unserialize(base64_decode($stringContent));
                $keys = array_keys($vision['user']);
                if (!empty($vision) && in_array($userid, $keys)) {
                    $returnarray[] = $metaData->getFileId();
                }
            }
        }

        return $returnarray;
    }

    /**
     * gets the metaIds of visible notices by the groups the user is in
     *
     * @param int $userid
     * @return array $returnarray
     */
    public function getVisiblesByGroup($userid)
    {
        $returnarray = array();
        $userGroups = array();

        $em = $this->getServiceLocator()->get('ObjectManager');
        $metaDatas = $em->getRepository('MetaAPI\Entity\File')->findAll();

        $groupIds = $em->getRepository('RoleAPI\Entity\Groups')->findBy(array('UserId' => $userid));

        foreach ($groupIds as $groupId) {
            $userGroups[] = $groupId->getGroupId();
        }

        $userGroups = array_unique($userGroups);
        foreach ($userGroups as $userGroup) {
            foreach ($metaDatas as $metaData) {
                if (is_resource($metaData->getVision())) {
                    $stringContent = stream_get_contents($metaData->getVision());
                    rewind($metaData->getVision());
                    $vision = unserialize(base64_decode($stringContent));
                    $keys = array_keys($vision['group']);
                    if (!empty($vision) && in_array($userid, $keys)) {
                        $returnarray[] = $metaData->getFileId();
                    }
                }
            }
        }

        return $returnarray;
    }

    /**
     * gets the metaIds of visible notices by the users globle roles
     *
     * @param int $userid
     * @return array $returnarray
     */
    public function getVisiblesByRole($userid)
    {
        $returnarray = array();
        $roleIds = array();
        $em = $this->getServiceLocator()->get('ObjectManager');
        $metaDatas = $em->getRepository('MetaAPI\Entity\File')->findAll();

        $globalRoles = $em->getRepository('RoleAPI\Entity\Roles')->findBy(
        array('UserId' => $userid, 'GroupId' => 0));

        foreach ($globalRoles as $role) {
            $roleIds[] = $role->getRoleId();
        }

        foreach ($metaDatas as $metaData) {
            if (is_resource($metaData->getVision())) {
                $stringContent = stream_get_contents($metaData->getVision());
                rewind($metaData->getVision());
                $vision = unserialize(base64_decode($stringContent));

                if (array_intersect(array_keys($vision['role']), $roleIds)) {
                    $returnarray[] = $metaData->getFileId();
                }
            }
        }
        return $returnarray;
    }
}