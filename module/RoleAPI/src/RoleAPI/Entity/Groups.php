<?php

namespace RoleAPI\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 @ORM\Entity
 * Groups
 *
 * @ORM\Table(name="Groups")
 * @property int $Id
 * @property int $UserId
 * @property int $GroupId
 * @property int $Temp
 * @property string $Hash
 */





class Groups
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */

    protected $Id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $UserId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */

    protected $GroupId;

    /**
     * @ORM\Column(type="integer")
     */

    protected $Temp;

    /**
     *
     *@ORM\Column(type="string")
     */

    protected $Hash;
	/**
     * @return the $Hash
     */
    public function getHash()
    {
        return $this->Hash;
    }

	/**
     * @param string $Hash
     */
    public function setHash($Hash)
    {
        $this->Hash = $Hash;
    }

	/**
     * @return the id
     */
    public function getId()
    {
        return $this->Id;
    }

	/**
     * @return the $UserId
     */
    public function getUserId()
    {
        return $this->UserId;
    }

	/**
     * @return the $GroupId
     */
    public function getGroupId()
    {
        return $this->GroupId;
    }

	/**
     * @return the $Temp
     */
    public function getTemp()
    {
        return $this->Temp;
    }

	/**
     * @param number $UserId
     */
    public function setUserId($UserId)
    {
        $this->UserId = $UserId;
    }

	/**
     * @param number $GroupId
     */
    public function setGroupId($GroupId)
    {
        $this->GroupId = $GroupId;
    }

	/**
     * @param number $Temp
     */
    public function setTemp($Temp)
    {
        $this->Temp = $Temp;
    }


}