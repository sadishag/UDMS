<?php
namespace User\Controller;
/**
 * TempController.php
 *
 * @package    User\Controller
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Sebastian Krebs <sebastian.krebs@unister.de>
 */
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Controller for view\User\Temp
 *
 * @copyright Copyright (c) 2012 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Sebastian Krebs <sebastian.krebs@unister.de>
 *
 */


class TempController extends AbstractActionController
{
    /**
     * Function to Controlle the templogin.phtml
     * Create a Checkkey
     * Check the Inputkey with the Checkkey
     * if Inputkey like the Checkkey
     * Set the Email from this User of True
     * @return mixed array $returnarray
     *     strings
     */

    public function temploginAction()
    {
        $tempid = $_SESSION['TempUserid'];
        $em = $this->getServiceLocator()->get('ObjectManager');
        $tempuser = $em->getRepository('RoleAPI\Entity\User')->findOneBy(array('UserId' => $tempid));
        $returnarray = array();
        $postvar = $this->getRequest()->getPost();
        if ($tempuser->getTemp() == 3 || $tempuser->getTemp() == 1) {

            $returnarray['user'] = $tempuser;
            if (isset($postvar['codecheck'])) {
                $hashvalue = sha1($tempuser->getEmail() . $tempuser->getLogin());
                if ($hashvalue === $postvar['newHash']) {
                    $tempold = $tempuser->getTemp();
                    $tempuser->setTemp($tempold - 1);
                    $em->persist($tempuser);
                    $em->flush();
                    $returnarray['success'] = 'Email wurde bestätigt';
                    $returnarray['myLittlehelper'] = 'helpme';

                } else {
                    $returnarray['error'] = 'Code war falsch';
                }

            }

        } else {
             return $this->redirect()->toUrl('/');
        }
        return $returnarray;
    }
}