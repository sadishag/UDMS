<?php
$time1 = microtime(true);
error_reporting(E_ALL);
$lines = file(__DIR__ . '/dbfill.conf');
$dbuser = trim($lines[5]);
$dbpw = trim($lines[6]);
$dbaddress = trim($lines[7]);
$dbname = trim($lines[8]);
$amount = (int) trim($lines[0]);

$sqlipath=__DIR__.'/../../data/sqlite/'.$dbname;
$dblink='sqlite:'.$sqlipath;
$dbc = new PDO($dblink);
require_once __DIR__ . '/Faker-master/src/autoload.php';
$faker = Faker\Factory::create();
$dbc->query('TRUNCATE User');
$dbc->query('TRUNCATE Roles');
$dbc->query('TRUNCATE RoleName');
$dbc->query('TRUNCATE Groups');
$dbc->query('TRUNCATE GroupName');
$dbc->query('TRUNCATE BranchName');

$sql = "Insert into User(Login,Password,FirstName,LastName,Email,Temp)Values";
$userArray = array();
$fnameArray = array();
$lnameArray = array();
$emailArray = array();
echo "creating " . $amount . " Users." . PHP_EOL;
for ($i = 0; $i < $amount; $i++) {
    while (true) {
        $usname = str_replace("'", "_", $faker->userName);
        if (!(in_array($usname, $userArray))) {
            $userArray[] = $usname;
            break;
        }

    }
    $fname = str_replace("'", "_", $faker->firstName);
    $fnameArray[] = $fname;

    $lname = str_replace("'", "_", $faker->lastName);
    $lnameArray[] = $lname;
    while (true) {
        $email = str_replace("'", "_", $faker->email);
        if (!(in_array($email, $emailArray))) {
            $emailArray[] = $email;
            break;
        }

    }
    if (($i) % ((int) ($amount / 10)) === 0) {
        echo 100 * $i / $amount . "%" . PHP_EOL;
    }
}
echo "100%".PHP_EOL;
$valuesArray = array();
for ($i = 0; $i < $amount; $i++) {
    if ($i + 1 == $amount) {
        $valuesArray[] = $sql."('" .
         implode("','", array($userArray[$i], $faker->sha1, $fnameArray[$i], $lnameArray[$i], $emailArray[$i],"0"))
         ."');";
    } else {
        $valuesArray[] = $sql."('" .
         implode("','", array($userArray[$i], $faker->sha1, $fnameArray[$i], $lnameArray[$i], $emailArray[$i],"0"))
         ."');";
    }
}
foreach($valuesArray as $v){
    system("sqlite3 ".$sqlipath." \"".$v."\"");
}
die(system("sqlite3 ".$sqlipath." \"Select * from User\""));
$esql = implode("", $valuesArray);
// $res = $dbc->query($esql);
system("sqlite3 ".$sqlipath." \"".$esql."\"");

echo "finished creating users" . PHP_EOL;die();
$sql = "Insert into BranchName(Name,Location)Values";
$paren = array('A1', 'A2', 'A3', 'A4', 'A5', 'A6');
$child = array('B1', 'B2', 'B3');
$schild = array('C1', 'C2', 'C3');
$tchild = array('D1', 'D2', 'D3');
$path = array();
echo "creating Branches" . PHP_EOL;
foreach ($paren as $f) {

    $path[] = $f;

}
foreach ($paren as $f) {
    foreach ($child as $ff) {

        $path[] = $f . ":" . $ff;

    }
}
foreach ($paren as $f) {
    foreach ($child as $ff) {
        foreach ($schild as $fff) {

            $path[] = $f . ":" . $ff . ":" . $fff;

        }
    }
}
foreach ($paren as $f) {
    foreach ($child as $ff) {
        foreach ($schild as $fff) {
            foreach ($tchild as $ffff) {
                $path[] = $f . ":" . $ff . ":" . $fff . ":" . $ffff;
            }
        }
    }
}
$count = sizeof($path);
$bnameArray = array();
for ($i = 0; $i < $count; $i++) {

    $bname = str_replace("'", "_", $faker->company);

    $bnameArray[] = $bname;

}
$valuesArray = array();
for ($i = 0; $i < $count; $i++) {
    $valuesArray[] = "('" . $bnameArray[$i] . "','" . "dir://" . $path[$i] . "')";
}
$esql = $sql . implode(",", $valuesArray) . ";";

$res = $dbc->query($esql);

echo "finished creating Branches" . PHP_EOL;

$sql = "Insert into GroupName(Name,BranchId)Value";
$a = 0;
echo "creating 50 Groups" . PHP_EOL;
$valuesArray = array();
for ($i = 0; $i < 50; $i++) {
    $valuesArray[] = "('" . str_replace("'", "_", $faker->catchPhrase) . "','" . rand(1, $count) . "')";
}
$esql = $sql . implode(",", $valuesArray) . ";";
$res = $dbc->query($esql);

echo "finished creating Groups" . PHP_EOL;

$ugamount = $amount * 4;
$sql = "Insert into Groups(UserId,GroupId,Temp,Hash)Values";
$a = 0;
echo "setting up User Group Relations : " . $ugamount . " Relations to be set" . PHP_EOL;
$uArray = array();
$gArray = array();
$valuesArray = array();
for ($i = 0; $i < $ugamount; $i++) {
    $uArray[$i] = rand(1, $amount);
    $gArray[$i] = rand(1, 45);
    $valuesArray[] = "('" . $uArray[$i] . "','" . $gArray[$i] . "','" . "0" . "','" . $faker->sha1 . "')";
}
$i=$ugamount;
$uArray[]=1;
$gArray[]=1;
$valuesArray[] = "('" . $uArray[$i] . "','" . $gArray[$i] . "','" . "0" . "','" . $faker->sha1 . "')";
$i++;
$uArray[]=1;
$gArray[]=2;
$valuesArray[] = "('" . $uArray[$i] . "','" . $gArray[$i] . "','" . "0" . "','" . $faker->sha1 . "')";
$ugamount+=2;
$esql = $sql . implode(",", $valuesArray) . ";";

$res = $dbc->query($esql);

echo "finished creating User Group Relations" . PHP_EOL;
$sql = "Insert into RoleName(Name,RightKey)Values(";
$roles = array(
'ohne Rolle'=>base64_encode(serialize(array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'roleadministration' => false,'groupadministration' => false,'createuser'=>false,'isadmin'=>false))),
'!Admin' => base64_encode(serialize(array('create' => true, 'read' => true, 'update' => true, 'delete' => true, 'roleadministration' => true,'groupadministration' => true,'createuser'=>true,'isadmin'=>true))),
'!Keine Rolle' => base64_encode(serialize(array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'roleadministration' => false,'groupadministration' => false,'createuser'=>false,'isadmin'=>false))),
'!Member' => base64_encode(serialize(array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'roleadministration' => false,'groupadministration' => false,'createuser'=>false,'isadmin'=>false))),
'!Guest' => base64_encode(serialize(array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'roleadministration' => false,'groupadministration' => false,'createuser'=>false,'isadmin'=>false))),
'!Moderator' => base64_encode(serialize(array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'roleadministration' => false,'groupadministration' => false,'createuser'=>false,'isadmin'=>false))),
'!Superman' => base64_encode(serialize(array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'roleadministration' => false,'groupadministration' => false,'createuser'=>false,'isadmin'=>false))),
'#kein Zugriff'=>base64_encode(serialize(array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'roleadministration' => false,'groupadministration' => false,'createuser'=>false,'isadmin'=>false))),
'#Lese Recht'=> base64_encode(serialize(array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'roleadministration' => false,'groupadministration' => false,'createuser'=>false,'isadmin'=>false))),
'#Editier Recht'=>  base64_encode(serialize(array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'roleadministration' => false,'groupadministration' => false,'createuser'=>false,'isadmin'=>false))),
'#Erstell und Editier Recht'=> base64_encode(serialize(array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'roleadministration' => false,'groupadministration' => false,'createuser'=>false,'isadmin'=>false))),
'#Erstell, Editier und Entfernen Recht'=> base64_encode(serialize(array('create' => false, 'read' => false, 'update' => false, 'delete' => false, 'roleadministration' => false,'groupadministration' => false,'createuser'=>false,'isadmin'=>false))),
);
echo "setting up Roles" . PHP_EOL;
foreach ($roles as $rkey => $rvalue) {
    $esql = $sql . "'" . $rkey . "'" . "," . "'" . $rvalue . "'" . ");";
    $res = $dbc->query($esql);
}
echo "finished setting up Roles" . PHP_EOL;
$gamount = 50;
$uamount = (int) ($amount / 1.5);
$samount = $amount * 2;
$sql = "Insert into Roles(UserId,GroupId,RoleId)Values";
echo "setting up User Role Relations" . PHP_EOL;
$valuesArray = array();
for ($i = 1; $i < $uamount; $i++) {
    $valuesArray[] = "('" . $i . "','" . "0" . "','" . rand(4, 7) . "')";
}
echo "setting up Group Role Relations" . PHP_EOL;
for ($i = 1; $i <= $gamount; $i++) {
    $valuesArray[] = "('" . "0" . "','" . $i . "','" . ($i % 5 + 3) . "')";
}
echo "setting up User in Group Role Relations" . PHP_EOL;
for ($i = 1; $i <= $samount; $i++) {
    $val=rand(4, 7);
    if($uArray[$i]===1){
        $val=2;
    }
    $valuesArray[] = "('" . $uArray[$i] . "','" . $gArray[$i] . "','" . $val . "')";
}
$val = 2;
$valuesArray[] = "('" . $uArray[$ugamount-1] . "','" . $gArray[$ugamount-1] . "','" . $val . "')";
$valuesArray[] = "('" . $uArray[$ugamount-2] . "','" . $gArray[$ugamount-2] . "','" . $val . "')";
$esql = $sql . implode(",", $valuesArray) . ";";

$res = $dbc->query($esql);

echo "creating sample users" . PHP_EOL;
$esql = "UPDATE User Set Login='Admin',Password='12345',FirstName='Max',LastName='Mustermann' where UserId=1;";
$res = $dbc->query($esql);
$esql = "Update Roles set RoleId=2 where Id=1;";
$res = $dbc->query($esql);
$esql = "UPDATE User Set Login='testuser',Password='54321',FirstName='Meer',LastName='Schwein' where UserId=2;";
$res = $dbc->query($esql);
$esql = "Update Roles set RoleId=3 where Id=2;";
$dbc->query($esql);

$time2 = microtime(true);
echo "Done in " . ($time2 - $time1) . " seconds.";