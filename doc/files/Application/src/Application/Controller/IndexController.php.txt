<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        $api = $this->getServiceLocator()->get('RoleAPI');
        $rights['groupadministration'] = $api->isAllowed($_SESSION['Userid'], 0, 'groupadministration');
        $rights['roleadministration'] = $api->isAllowed($_SESSION['Userid'], 0, 'roleadministration');
        $rights['createuser'] = $api->isAllowed($_SESSION['Userid'], 0, 'createuser');
        $rights['isadmin'] = $api->isAllowed($_SESSION['Userid'], 0, 'isadmin');

        return new ViewModel(array('rights' => $rights));
    }

}

