<?php

namespace Data\Model;

use DataAPI\Entity\FileSigner;
use Zend\View\Model\JsonModel;

class SignerModel
{
    private $_sm;

    public function setServiveLocator($sm)
    {
        $this->_sm = $sm;
    }

    public function showSignFile()
    {
        $em = $this->_sm->get('ObjectManager');
        $signRepo = $em->getRepository('DataAPI\Entity\FileSigner');
        $userRepo = $em->getRepository('RoleAPI\Entity\User');
        $fileRepo = $em->getRepository('MetaAPI\Entity\File');

        $signers = $signRepo->findBy(array('UserId' => $_SESSION['Userid'], 'Signed' => false));
        foreach ($signers as $sign) {
            $sign->name = $this->getFileName($sign, $fileRepo);
            $sign->user = $this->getUserName($sign, $userRepo);
            $sign->request = $this->getTime($sign, $signRepo);
        }

        return $signers;
    }

    public function getFileName($sign, $fileRepo)
    {
        $fileInfo = $fileRepo->findOneBy(array('FileId' => $sign->getFileId()))
            ->getName();
        return $fileInfo;
    }

    public function getUserName($sign, $userRepo)
    {
        $userName = $userRepo->findOneBy(array('UserId' => $sign->getUserId()));
        $first = $userName->getFirstName();
        $last = $userName->getLastName();
        $userName = $first . ' ' . $last;
        return $userName;
    }

    public function getTime($sign, $signRepo)
    {
        $fileInfo = $signRepo->findOneBy(array('FileId' => $sign->getFileId()))
            ->getRequestTime();
        $request = date('H:i:s \U\h\r d.m.y', $fileInfo);
        return $request;
    }

    public function isSignFile()
    {
        $em = $this->_sm->get('ObjectManager');
        $signRepo = $em->getRepository('DataAPI\Entity\FileSigner');
        $sign = $signRepo->findBy(array('UserId' => $_SESSION['Userid'], 'Signed' => false));

        return empty($sign);
    }

    public function add($signer, $fileId)
    {
        $em = $this->_sm->get('ObjectManager');

        $signerFile = new FileSigner();
        $signerFile->setFileId($fileId);
        $signerFile->setUserId($_SESSION['Userid']);
        $signerFile->setSigner($signer);
        $signerFile->setRequestTime(time());
        $em->persist($signerFile);
        $em->flush();
    }

    public function signAccept($id, $key, $comment = null)
    {
        $em = $this->_sm->get('ObjectManager');

        $signerFile = $em->getRepository('DataAPI\Entity\FileSigner')->findOneBy(array('Id' => $id));
        if ($_SESSION['Userid'] === $signerFile->getSigner()) {
            $userKey = $em->getRepository('RoleAPI\Entity\User')
                ->findOneBy(array('UserId' => $_SESSION['Userid']))
                ->getKey();
            if ($key === $userKey) {
                if ($comment) {
                    $signerFile->setComment($comment);
                }
                $signerFile->setSigned(true);
                $signerFile->setConfirmedTime(time());
                $em->persist($signerFile);
                $em->flush();
            } else {
                throw new \Exception('Falscher Key!');
            }
        } else {
            throw new \Exception('geht nicht');
        }
    }

    public function signReject($id, $key, $comment = null)
    {
        $em = $this->_sm->get('ObjectManager');
        $signerFile = $em->getRepository('DataAPI\Entity\FileSigner')->findOneBy(array('Id' => $id));
        if ($_SESSION['Userid'] === $signerFile->getSigner()) {
            $userKey = $em->getRepository('RoleAPI\Entity\User')
                ->findOneBy(array('UserId' => $_SESSION['Userid']))
                ->getKey();
            if ($key === $userKey) {
                if ($comment) {
                    $signerFile->setComment($comment);
                }
                $signerFile->setSigned(false);
                $signerFile->setConfirmedTime(time());
                $em->persist($signerFile);
                $em->flush();
            } else {
                throw new \Exception('Falscher Key!');
            }
        } else {
            throw new \Exception('geht nicht');
        }
    }
}