<?php
namespace Notice\Controller;

use Zend\Stdlib\Parameters;

class NoticeupdateControllerTest extends \PHPUnit_Framework_TestCase

{
    protected $_controller;

    public function setUp()
    {
        $this->_controller = new NoticeupdateController();

    }

    public function tearDown()
    {
    }

    public function testShowAction()
    {
        $_SESSION['Userid'] = 1;

        $functionMock = $this->getMock('Notice\Controller\NoticeupdateController', array('getVisibles'));

        $fileMock = $this->getMock('MetaAPI\Entity\File', array('getFileId', 'getOwner', 'getTrash'));
        $fileMock->expects($this->any())
            ->method('getOwner')
            ->will($this->returnValue(1));

        $repo = $this->getMock('Repository', array('findBy', 'findOneBy'));
        $repo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($fileMock, $fileMock)));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($fileMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $functionMock->setServiceLocator($smMock);
        $functionMock->showAction();
    }

    public function testGetVisibles()
    {
        $_SESSION['Userid'] = 1;

        $noteMock = $this->getMock('MetaAPI\Entity\File', array('getFileId'));
        $noteMock->expects($this->any())
            ->method('getFileId')
            ->will($this->returnValue(1));

        $notizen = array($noteMock, $noteMock);

        $functionMock = $this->getMock('Notice\Controller\NoticeupdateController', array('getVisiblesByUser', 'getVisiblesByGroup', 'getVisiblesByRole'));
        $functionMock->expects($this->any())
            ->method('getVisiblesByUser')
            ->will($this->returnValue(array(1)));
        $functionMock->expects($this->any())
            ->method('getVisiblesByGroup')
            ->will($this->returnValue(array(2)));
        $functionMock->expects($this->any())
            ->method('getVisiblesByRole')
            ->will($this->returnValue(array(3)));

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($noteMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $functionMock->setServiceLocator($smMock);
        $functionMock->getVisibles(1, $notizen);
    }

    public function testGetvisiblesByUser()
    {
        $array = array('user' => array(1 => 1));
        $stream = fopen('php://memory','r+');
        fwrite($stream, base64_encode(serialize($array)));
        rewind($stream);

        $fileMock = $this->getMock('MetaAPI\Entity\File', array('getVision'));
        $fileMock->expects($this->any())
            ->method('getVision')
            ->will($this->returnValue($stream));

        $repo = $this->getMock('Repository', array('findAll'));
        $repo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($fileMock, $fileMock)));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->getVisiblesByUser(1);
    }

    public function testGetvisiblesByGroup()
    {
        $array = array('group' => array(1 => 1));
        $stream = fopen('php://memory','r+');
        fwrite($stream, base64_encode(serialize($array)));
        rewind($stream);

        $fileMock = $this->getMock('MetaAPI\Entity\File', array('getVision'));
        $fileMock->expects($this->any())
            ->method('getVision')
            ->will($this->returnValue($stream));

        $groupMock = $this->getMock('RoleAPI\Entity\Groups', array('getGroupId'));
        $groupMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));

        $repo = $this->getMock('Repository', array('findAll', 'findBy'));
        $repo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($fileMock, $fileMock)));
        $repo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($groupMock)));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->getVisiblesByGroup(1);
    }

    public function testGetvisiblesByRole()
    {
        $array = array('role' => array(1 => 1));
        $stream = fopen('php://memory','r+');
        fwrite($stream, base64_encode(serialize($array)));
        rewind($stream);

        $fileMock = $this->getMock('MetaAPI\Entity\File', array('getVision'));
        $fileMock->expects($this->any())
            ->method('getVision')
            ->will($this->returnValue($stream));

        $roleMock = $this->getMock('RoleAPI\Entity\Roles', array('getRoleId'));
        $roleMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));

        $repo = $this->getMock('Repository', array('findAll', 'findBy'));
        $repo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($fileMock, $fileMock)));
        $repo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($roleMock)));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->getVisiblesByRole(1);
    }
}
