<?php
namespace ObjectManager;
class ModuleTest extends \PHPUnit_Framework_TestCase
{
    protected $_sm;
    public function setUp(){
        $em = $this->getMock('Doctrine\ORM\EntityManager',null);
        $this->_sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $this->_sm->expects($this->any())
        ->method('get')
        ->will($this->returnValue($em));
    }
    public function testModule(){
        $module=new Module();
        $module->getAutoloaderConfig();
        $module->getConfig();
        $testfunc=$module->getServiceConfig();
        call_user_func($testfunc['factories']['ObjectManager'],$this->_sm);
    }
    public function tearDown(){

    }
}