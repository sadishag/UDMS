<?php
namespace Admin\Controller;

use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Parameters;

class RemovedataControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {
        $this->_controller = new RemovedataController();
    }

    public function provideTestIndexAction()
    {
        $stream = fopen('php://memory', 'r+');
        fwrite($stream, base64_encode(serialize('test')));
        rewind($stream);

        $mock = $this->getMock('RoleAPI\Entity\BranchName', array('getTrashInfo'));
        $mock->expects($this->any())
            ->method('getTrashInfo')
            ->will($this->returnValue($stream));

        return array(
            array(true, array($mock)),
            array(false, array())
        );
    }

    /**
     * @dataProvider provideTestIndexAction
     */
    public function testIndexAction($allow, $trash)
    {
        $_SESSION['Userid'] = 1;

        $post = array('sizeofarray' => 4, 'Aktion1' => 1);

        $mock = $this->getMock('RoleAPI\Entity\BranchName', array('getLocation', 'getFileId', 'getTrashInfo', 'getFirstName', 'getLastName'));

        $repo = $this->getMock('Repository', array('findBy', 'findOneBy'));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($mock));
        $repo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue($trash));


        $em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'isAllowed', 'getChildrenEntities', 'remove', 'flush'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));
        $em->expects($this->any())
            ->method('isAllowed')
            ->will($this->returnValue($allow));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $mvcEvent = new MvcEvent();
        $mvcEvent->setResponse(new Response());
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->indexAction();
    }
}