<?php
namespace Notice\Controller;
/**
 * NoticeController.php
 *
 * @package    Notice\Controller
 * @copyright  Copyright (c) 2013 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Sebastian Krebs <sebastian.krebs@unister.de>
 * @author     Sebastian Fritze <sebastian.fritze@unister.de>
 */

use RoleAPI\Entity\Roles;
use RoleAPI\Entity\Groups;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class NoticeController extends AbstractActionController
{

    /**
     * Action to create a Note
     * Call the Function:saveNewNotes()
     *
     * @return mixed $returnArray
     *         int
     */
    public function createAction()
    {

        $em = $this->getServiceLocator()->get('ObjectManager');
        $owner = $em->getRepository('RoleAPI\Entity\User')->findAll();

        $userid = $_SESSION['Userid'];

        $returnArray['userid'] = $userid;
        $returnArray['owner'] = $owner;
        $returnArray['currentUser'] = $em->getRepository('RoleAPI\Entity\User')->find($userid);

        return $returnArray;
    }

    /**
     * action to set visibility and rights for notes
     *
     * @return array $returnarray
     */
    public function visionAction()
    {
        $noticeId = (int) $this->params('noticeid', false);

        $em = $this->getServiceLocator()->get('ObjectManager');
        $noteMeta = $em->getRepository('MetaAPI\Entity\File')->findOneBy(array('FileId' => $noticeId));
        if (empty($noteMeta)) {
            return $this->redirect()->toUrl('/notice');
        }
        $noteName = $noteMeta->getName();
        /*
        $noteMetaId = $noteMeta->getMetaId();
        $metaData = $em->getRepository('MetaAPI\Entity\MetaData')->find($noteMetaId);
        if (null === $metaData) {
            return $this->redirect()->toUrl('/notice');
        }*/

        $userid = $_SESSION['Userid'];

        $returnarray['alluser'] = $em->getRepository('RoleAPI\Entity\User')->findAll();
        $returnarray['allgroups'] = $em->getRepository('RoleAPI\Entity\GroupName')->findAll();
        $returnarray['allroles'] = $em->getRepository('RoleAPI\Entity\RoleName')->findAll();

        $returnarray['selectroles'] = array_filter($returnarray['allroles'],
        function ($var)
        {
            return false !== strpos($var->getName(), '#');
        });

        $returnarray['allroles'] = array_filter($returnarray['allroles'], function($var) {
            return false !== strpos($var->getName(), '!');
        });

        $returnarray['noticeid'] = $noticeId;
        $returnarray['notename'] = $noteName;

        $returnarray['vision'] = $this->hasVision($noticeId);
        $returnarray['vision'] = $this->checkCurrentRole($returnarray['vision'], $noticeId);
        return $returnarray;
    }

    /**
    * checks for a given notice the roles of listest users
    *
    * @param array $visionarray
    * @param int $noticeid
    * @return array $visionarray
    */
    public function checkCurrentRole($visionarray, $noticeid)
    {
            $em = $this->getServiceLocator()->get('ObjectManager');
            $metaData = $em->getRepository('MetaAPI\Entity\File')->find($noticeid);
            $rolesArray = array();

        if (is_resource($metaData->getVision())) {
            $stringContent = stream_get_contents($metaData->getVision());
            rewind($metaData->getVision());
            $rolesArray = unserialize(base64_decode($stringContent));
        }

        foreach ($rolesArray as $type => $arr) {
            foreach ($arr as $id => $role) {
                if ($role !== 0) {
                    $rolename = $em->getRepository('RoleAPI\Entity\RoleName')
                        ->findOneBy(array('RoleId' => $role))
                        ->getName();
                } else {
                    $rolename = 'Noch keine Rolle gesetzt';
                }

                $rolename = str_replace('#', '', $rolename);
                $visionarray[$type][$id]['role'] = $rolename;

            }
        }

        return $visionarray;
    }

    /**
    * gets the data for updating a role for a set user
    *
    * @return \Zend\View\Model\JsonModel
    */
    public function saveVisionRoleAction()
    {
        $postvar = $this->getRequest()->getPost();

        if (false === strpos($postvar['role'], '-- Rolle --')) {
            $this->saveVision($postvar['ugrid'], $postvar['type'], $postvar['noticeid'], $postvar['role'], true);
        }

        return new JsonModel(array('success' => 'Rolle geändert'));
    }

    /**
    * gets the data fpr saving a new user as visible
    *
    * @return \Zend\View\Model\JsonModel
    */
    public function addVisionAction()
    {
        $postvar = $this->getRequest()->getPost();
        $this->saveVision($postvar['id'], $postvar['type'], $postvar['noticeid']);

        return new JsonModel(array('success' => 'Hinzugefügt'));
    }

    /**
    * saves the given data in database
    *
    * @param int $id
    * @param string $type
    * @param int $noticeid
    * @param int $role
    * @return
    */
    public function saveVision($id, $type, $noticeid, $role = 8)
    {

        $em = $this->getServiceLocator()->get('ObjectManager');
        $metaData = $em->getRepository('MetaAPI\Entity\File')->find($noticeid);

        if (is_resource($metaData->getVision())) {
            $stringContent = stream_get_contents($metaData->getVision());
            rewind($metaData->getVision());
            $content = unserialize(base64_decode($stringContent));
        }

        if (empty($content)) {
            $content = array('user' => array(), 'group' => array(), 'role' => array());

        }
        $content[$type][$id] = $role;

        ksort($content[$type]);

        $metaData->setVision(base64_encode(serialize($content)));
        $em->persist($metaData);
        $em->flush();


        return;
    }

    /**
    * gets an array of all users, groups and roles entitled to see the notice
    *
    * @param int $id
    * @return array $return
    */
    public function hasVision($id)
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $metaData = $em->getRepository('MetaAPI\Entity\File')->find($id);

        $return = null;

        if (is_resource($metaData->getVision())) {
            $stringContent = stream_get_contents($metaData->getVision());
            rewind($metaData->getVision());
            $vision = unserialize(base64_decode($stringContent));

                if (!empty($vision['user'])) {
                    foreach ($vision['user'] as $user => $roles) {
                        $userarray[$user]['name'] = $em->getRepository('RoleAPI\Entity\User')
                            ->findOneBy(array('UserId' => $user))
                            ->getLogin();
                    }
                } else {
                    $userarray = array();
                }

            if (!empty($vision['group'])) {
                foreach ($vision['group'] as $group => $roles) {
                    $grouparray[$group]['name'] = $em->getRepository('RoleAPI\Entity\GroupName')
                        ->findOneBy(array('GroupId' => $group))
                        ->getName();
                }
            } else {
                $grouparray = array();
            }

            if (!empty($vision['role'])) {
                foreach ($vision['role'] as $role => $roles) {
                    $rolearray[$role]['name'] = str_replace('!', '',
                    $em->getRepository('RoleAPI\Entity\RoleName')
                        ->findOneBy(array('RoleId' => $role))
                        ->getName());
                }
            } else {
                $rolearray = array();
            }

            $return['user'] = $userarray;
            $return['group'] = $grouparray;
            $return['role'] = $rolearray;
        }

        return $return;
    }

    /**
    * deletes a user from the list of user set for the notice
    *
    * @return \Zend\View\Model\JsonModel
    */
    public function deleteVisionAction()
    {
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $em = $this->getServiceLocator()->get('ObjectManager');

            $metaData = $em->getRepository('MetaAPI\Entity\File')->find($post['noticeid']);

            if (is_resource($metaData->getVision())) {
                $stringContent = stream_get_contents($metaData->getVision());
                rewind($metaData->getVision());
                $vision = unserialize(base64_decode($stringContent));

                unset($vision[$post['type']][$post['id']]);
            }

            $metaData->setVision(base64_encode(serialize($vision)));
            $em->persist($metaData);
            $em->flush();
        }
        return new JsonModel();
    }
}
