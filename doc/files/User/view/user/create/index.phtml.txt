<?php if(!isset($this->newUser)){?>
<div id="wrapper">
    <section id="content">
        <h1>User erstellen</h1>
        <form action="" method="post" id="usercreate" class="floatLeft">
            <input value="<?php echo $this->First?>" type="text" name="userFirst" id="userfirst" autocomplete="off"
                placeholder="Vorname" required><br> <input value="<?php echo $this->Last?>" type="text"
                name="userLast" id="userlast" autocomplete="off" placeholder="Nachname" required><br> <input
                value="<?php echo $this->Email?>" type="email" name="email" id="emailid" autocomplete="off"
                placeholder="Email" required><br> <input value="<?php echo $this->pass1;?>" type="password" name="pass1"
                id="pw1id" autocomplete="off" placeholder="Passwort" required><br> <input
                value="<?php echo $this->pass2?>" type="password" name="pass2" id="pw2id" autocomplete="off"
                placeholder="Passwort bestätigen" required><br> <input type="submit" name="save" id="globalFilter"
                value="Speichern" class="vAlignMiddle">

        </form>


    </section>
</div>
<?php }else {?>

<h1>Daten des Neuen Nutzers</h1>
<table>
    <tr>
        <th>Daten</th>
        <th></th>
    </tr>
    <tr>
        <td>Login</td>
        <td><?php echo $this->newUser->getLogin(); ?></td>

    </tr>
    <tr>
        <td>Vorname</td>
        <td><?php echo $this->newUser->getFirstName(); ?></td>
    </tr>
    <tr>
        <td>Nachname</td>
        <td><?php echo $this->newUser->getLastName(); ?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?php echo $this->newUser->getEmail(); ?></td>
    </tr>
    <tfoot>


</table>
 <div>BestätigungsHash: <?php echo $this->hashvalue;?></div>
 <a class="btn btn-success btn-large" href="/Angemeldet">Zurück zum Hauptmenu</a>

<?php }?>

<div id="slider">
    <div>
        <?php
            include 'help.html';
        ?>
    </div>
    <p id="sliderButton"><span>Hilfe!</span></p>
</div>
<script>
<?php
if (isset($this->error)) {
    ?>
    globalNotification({error: '<?php echo $this->error ?>'});
    <?php
}
?>
</script>
<script>
<?php
if (isset($this->success)) {
    ?>
    globalNotification({success: '<?php echo $this->success ?>'});
    <?php
}
?>
</script>
