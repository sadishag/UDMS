<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'container_entities' => array(
                'class' => '\Doctrine\ORM\Mapping\Driver\AnnotationDriver', 'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/DataAPI/Entity'
                )
            ),
            'udms_container' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\DriverChain',
                'drivers' => array(
                    'DataAPI\Entity' => 'container_entities'
                )
            ),
            'udms_sqli_container' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\DriverChain',
                'drivers' => array(
                    'DataAPI\Entity' => 'container_entities'
                )
            )
        ),
        'configuration' => array(
            'udms_container' => array(
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'driver' => 'udms_container',
                'generate_proxies' => true,
                'proxy_dir' => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\Proxy',
                'filters' => array()
            ),
            'udms_sqli_container' => array(
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'driver' => 'udms_sqli_container',
                'generate_proxies' => true,
                'proxy_dir' => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\Proxy',
                'filters' => array()
            )
        ),
        'entitymanager' => array(
            'udms_container' => array(
                'connection' => 'udms_container',
                'configuration' => 'udms_container'
            ),
            'udms_sqli_container' => array(
                'connection' => 'udms_sqli_container',
                'configuration' => 'udms_sqli_container'
            )
        ),
        'eventmanager' => array(
            'udms_container' => array(),
            'udms_sqli_container' => array()
        ),
        'sql_logger_collector' => array(
            'udms_container' => array(),
            'udms_sqli_container' => array()
        ),
        'entity_resolver' => array(
            'udms_container' => array(),
            'udms_sqli_container' => array()
        ),
        'authentication' => array(
            'udms_container' => array(
                'objectManager' => 'doctrine.entitymanager.udms_container'
            ),
            'udms_sqli_container' => array(
                'objectManager' => 'doctrine.entitymanager.udms_sqli_container'
            )
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'doctrine.authenticationadapter.udms_container' => new DoctrineModule\Service\Authentication\AdapterFactory('udms_container'),
            'doctrine.authenticationstorage.udms_container' => new DoctrineModule\Service\Authentication\StorageFactory('udms_container'),
            'doctrine.authenticationservice.udms_container' => new DoctrineModule\Service\Authentication\AuthenticationServiceFactory('udms_container'),
            'doctrine.connection.udms_container' => new DoctrineORMModule\Service\DBALConnectionFactory('udms_container'),
            'doctrine.configuration.udms_container' => new DoctrineORMModule\Service\ConfigurationFactory('udms_container'),
            'doctrine.entitymanager.udms_container' => new DoctrineORMModule\Service\EntityManagerFactory('udms_container'),
            'doctrine.driver.udms_container' => new DoctrineModule\Service\DriverFactory('udms_container'),
            'doctrine.eventmanager.udms_container' => new DoctrineModule\Service\EventManagerFactory('udms_container'),
            'doctrine.entity_resolver.udms_container' => new DoctrineORMModule\Service\EntityResolverFactory('udms_container'),
            'doctrine.sql_logger_collector.udms_container' => new DoctrineORMModule\Service\SQLLoggerCollectorFactory('udms_container'),
            'doctrine.mapping_collector.udms_container' => function (Zend\ServiceManager\ServiceLocatorInterface $sl)
            {
                $em = $sl->get('doctrine.entitymanager.udms_container');

                return new DoctrineORMModule\Collector\MappingCollector($em->getMetadataFactory(), 'udms_container_mappings');
            },
            'DoctrineORMModule\Form\Annotation\AnnotationBuilder' => function (Zend\ServiceManager\ServiceLocatorInterface $sl)
            {
                return new DoctrineORMModule\Form\Annotation\AnnotationBuilder($sl->get('doctrine.entitymanager.udms_container'));
            },
            'doctrine.authenticationadapter.udms_sqli_container' => new DoctrineModule\Service\Authentication\AdapterFactory('udms_sqli_container'),
            'doctrine.authenticationstorage.udms_sqli_container' => new DoctrineModule\Service\Authentication\StorageFactory('udms_sqli_container'),
            'doctrine.authenticationservice.udms_sqli_container' => new DoctrineModule\Service\Authentication\AuthenticationServiceFactory('udms_sqli_container'),
            'doctrine.connection.udms_sqli_container' => new DoctrineORMModule\Service\DBALConnectionFactory('udms_sqli_container'),
            'doctrine.configuration.udms_sqli_container' => new DoctrineORMModule\Service\ConfigurationFactory('udms_sqli_container'),
            'doctrine.entitymanager.udms_sqli_container' => new DoctrineORMModule\Service\EntityManagerFactory('udms_sqli_container'),
            'doctrine.driver.udms_sqli_container' => new DoctrineModule\Service\DriverFactory('udms_sqli_container'),
            'doctrine.eventmanager.udms_sqli_container' => new DoctrineModule\Service\EventManagerFactory('udms_sqli_container'),
            'doctrine.entity_resolver.udms_sqli_container' => new DoctrineORMModule\Service\EntityResolverFactory('udms_sqli_container'),
            'doctrine.sql_logger_collector.udms_sqli_container' => new DoctrineORMModule\Service\SQLLoggerCollectorFactory('udms_sqli_container'),
            'doctrine.mapping_collector.udms_sqli_container' => function (Zend\ServiceManager\ServiceLocatorInterface $sl)
            {
                $em = $sl->get('doctrine.entitymanager.udms_sqli_container');

                return new DoctrineORMModule\Collector\MappingCollector($em->getMetadataFactory(), 'udms_sqli_container_mappings');
            }
        )
    )


);