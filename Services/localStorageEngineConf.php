<?php
//min priority === count of elements. all priorities set and unique
return array(
    '1' => array(
        'name' => 'doctrine.entitymanager.udms_role',
        'type' => 'doctrine',
        'namespace' => 'RoleAPI'
    ),
    '2' => array(
        'name' => 'doctrine.entitymanager.udms_meta',
        'type' => 'doctrine',
        'namespace' => 'MetaAPI'
    ),
    '3' => array(
        'name' => 'doctrine.entitymanager.udms_container',
        'type' => 'doctrine',
        'namespace' => 'DataAPI'
    ),
    '4' => array(
        'name' => 'doctrine.entitymanager.udms_sqli_container',
        'type' => 'sqli',
        'namespace' => 'DataAPI'
    ),
    '5' => array(
        'name' => getcwd() . '/data/',
        'type' => 'FileManager',
        'namespace' => 'DataAPI'
    )
);