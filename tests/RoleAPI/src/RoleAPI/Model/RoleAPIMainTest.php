<?php
namespace RoleAPI\Model;
use RoleAPI\Model\RoleAPIMain;
class RoleAPIMainTest extends \PHPUnit_Framework_TestCase
{
    protected $_api;

    public function setUp()
    {
        if (!$this->_api) {

            $branchMock = $this->getMock('Entity', array('getBranchId', 'getLocation'));
            $branchMock->expects($this->any())
                ->method('getBranchId')
                ->will($this->returnValue(1));
            $branchMock->expects($this->any())
                ->method('getLocation')
                ->will($this->returnValue('dir://A1'));

            $branchrepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
            $branchrepo->expects($this->any())
                ->method('find')
                ->will($this->returnValue($branchMock));
            $branchrepo->expects($this->any())
                ->method('findOneBy')
                ->will($this->returnValue($branchMock));
            $branchrepo->expects($this->any())
                ->method('findAll')
                ->will($this->returnValue(array($branchMock, $branchMock)));
            $key = 'YTo0OntzOjY6ImNyZWF0ZSI7YjowO3M6NDoicmVhZCI7YjoxO3M6NjoidXBkYXRlIjtiOjE7czo2OiJkZWxldGUiO2I6MDt9';

            $file = fopen(__DIR__ . '/mockkey', 'w');
            fwrite($file, $key);
            $file = fopen(__DIR__ . '/mockkey', 'a');

            $roleNameMock = $this->getMock('Entity',
            array('getRoleId', 'getRightKey', 'getRoleName', 'setRightKey', 'setRoleName'));
            $roleNameMock->expects($this->any())
                ->method('getRoleId')
                ->will($this->returnValue(1));
            $roleNameMock->expects($this->any())
                ->method('getRoleName')
                ->will($this->returnValue('dir://A1'));
            $roleNameMock->expects($this->any())
                ->method('getRightKey')
                ->will($this->returnValue($file));

            $roleNameMock2 = $this->getMock('Entity',
            array('getRoleId', 'getRightKey', 'getRoleName', 'setRightKey', 'setRoleName'));
            $roleNameMock2->expects($this->any())
                ->method('getRoleId')
                ->will($this->returnValue(1));
            $roleNameMock2->expects($this->any())
                ->method('getRoleName')
                ->will($this->returnValue('dir://A1'));
            $roleNameMock2->expects($this->any())
                ->method('getRightKey')
                ->will($this->returnValue($key));

            $roleNamevaluesmap = array(array(1, $roleNameMock2), array(2, $roleNameMock));
            $roleNameMap = array(array(array('Name' => 'UserRole:1'), null),
            array(array('Name' => 'UserRole:1:1'), $roleNameMock));

            $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
            $roleNamerepo->expects($this->any())
                ->method('find')
                ->will($this->returnValueMap($roleNamevaluesmap));
            $roleNamerepo->expects($this->any())
                ->method('findOneBy')
                ->will($this->returnValueMap($roleNameMap));
            $roleNamerepo->expects($this->any())
                ->method('findAll')
                ->will($this->returnValue(array($roleNameMock, $roleNameMock)));

            $userMock = $this->getMock('Entity', array('getUserId', 'getLogin'));
            $userMock->expects($this->any())
                ->method('getUserId')
                ->will($this->returnValue(1));
            $userMock->expects($this->any())
                ->method('getLogin')
                ->will($this->returnValue('Admin'));

            $userrepo = $this->getMock('Repository', array('find', 'findOneBy'));
            $userrepo->expects($this->any())
                ->method('find')
                ->will($this->returnValue($userMock));
            $userrepo->expects($this->any())
                ->method('findOneBy')
                ->will($this->returnValue($userMock));

            $rolesMock = $this->getMock('Entity',
            array('getRoleId', 'getUserId', 'getGroupId', 'setRoleId', 'setUserId', 'setGroupId'));
            $rolesMock->expects($this->any())
                ->method('getUserId')
                ->will($this->returnValue(1));
            $rolesMock->expects($this->any())
                ->method('getRoleId')
                ->will($this->returnValue(1));
            $rolesMock->expects($this->any())
                ->method('getGroupId')
                ->will($this->returnValue(1));

            $adroleMock = $this->getMock('Entity',
            array('getRoleId', 'getUserId', 'getGroupId', 'setRoleId', 'setUserId', 'setGroupId'));
            $adroleMock->expects($this->any())
                ->method('getRoleId')
                ->will($this->returnValue(3));

            $rolesValueMap = array(array(array('UserId' => 1, 'GroupId' => 0, 'RoleId' => 1), null),
            array(array('UserId' => 1, 'GroupId' => 1, 'RoleId' => 1), null),
            array(array('UserId' => 1, 'GroupId' => 0), array($rolesMock, $rolesMock)),
            array(array('RoleId' => 1), array($rolesMock, $rolesMock)),
            array(array('GroupId' => 1), array($rolesMock, $rolesMock)),
            array(array('GroupId' => 1, 'UserId' => 0), array($rolesMock, $rolesMock)),
            array(array('GroupId' => 1, 'UserId' => 1), array($rolesMock, $rolesMock)),
            array(array('GroupId' => 3, 'UserId' => 3), array($adroleMock)));

            $rolesrepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
            $rolesrepo->expects($this->any())
                ->method('find')
                ->will($this->returnValue($rolesMock));
            $rolesrepo->expects($this->any())
                ->method('findOneBy')
                ->will($this->returnValue($rolesMock));
            $rolesrepo->expects($this->any())
                ->method('findBy')
                ->will($this->returnValueMap($rolesValueMap));

            $groupMock = $this->getMock('Entity', array('getUserId', 'getGroupId'));
            $groupMock->expects($this->any())
                ->method('getUserId')
                ->will($this->returnValue(1));
            $groupMock->expects($this->any())
                ->method('getGroupId')
                ->will($this->returnValue(1));

            $groupvaluesMap = array(array(array('GroupId' => 1), array($groupMock, $groupMock)),
            array(array('UserId' => 1), array($groupMock, $groupMock)));

            $grouprepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
            $grouprepo->expects($this->any())
                ->method('find')
                ->will($this->returnValue($groupMock));
            $grouprepo->expects($this->any())
                ->method('findOneBy')
                ->will($this->returnValue($groupMock));
            $grouprepo->expects($this->any())
                ->method('findBy')
                ->will($this->returnValueMap($groupvaluesMap));

            $groupNameMock = $this->getMock('Entity', array('getBranchId', 'getGroupId'));
            $groupNameMock->expects($this->any())
                ->method('getBranchId')
                ->will($this->returnValue(1));
            $groupNameMock->expects($this->any())
                ->method('getGroupId')
                ->will($this->returnValue(1));

            $groupNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
            $groupNamerepo->expects($this->any())
                ->method('find')
                ->will($this->returnValue($groupNameMock));
            $groupNamerepo->expects($this->any())
                ->method('findOneBy')
                ->will($this->returnValue($groupNameMock));
            $groupNamerepo->expects($this->any())
                ->method('findAll')
                ->will($this->returnValue(array($groupNameMock, $groupNameMock)));

            $map = array(array('RoleAPI\Entity\User', $userrepo), array('RoleAPI\Entity\BranchName', $branchrepo),
            array('RoleAPI\Entity\RoleName', $roleNamerepo), array('RoleAPI\Entity\Roles', $rolesrepo),
            array('RoleAPI\Entity\Groups', $grouprepo), array('RoleAPI\Entity\GroupName', $groupNamerepo));

            $em = $this->getMock('Doctrine\ORM\EntityManager',
            array('getRepository', 'flush', 'persist', 'remove'), array(), '', false);
            $em->expects($this->any())
                ->method('getRepository')
                ->will($this->returnValueMap($map));
            $sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
            $sm->expects($this->any())
                ->method('get')
                ->will($this->returnValue($em));

            $this->_api = new RoleAPIMain($sm);
        }
    }

    public function testHandleLocationParameter()
    {

        $branchId = 1;
        $branchLocation = "dir://A1";

        $return = $this->_api->handleLocationParameter($branchId, true);
        $this->assertEquals($branchLocation, $return);
        $return = $this->_api->handleLocationParameter($branchId, false);
        $this->assertEquals($branchId, $return);
        $return = $this->_api->handleLocationParameter($branchLocation, true);
        $this->assertEquals($branchLocation, $return);
        $return = $this->_api->handleLocationParameter($branchLocation, false);
        $this->assertEquals($branchId, $return);
        $this->assertEquals(0, $this->_api->getBranchId('dir://'));

        $this->assertEquals('dir://', $this->_api->getBranchLocation(0));

        $this->assertEquals('dir://', $this->_api->handleStringLocationParam($branchLocation . ':..'));
        $this->assertEquals('dir://A1:B2', $this->_api->handleStringLocationParam('dir://A1:B2'));
    }

    public function testHandleLocationParameterException()
    {

        $this->setExpectedException('Exception');
        $return = $this->_api->handleLocationParameter(true, false);
    }

    public function testHandleUserParameter()
    {
        $userId = 1;
        $userLogin = "Admin";
        $this->assertEquals($userLogin, $this->_api->getUserLogin($userId));
        $this->assertEquals($userId, $this->_api->getUserId($userLogin));

        $return = $this->_api->handleUserParameter($userId, true);
        $this->assertEquals($userLogin, $return);
        $return = $this->_api->handleUserParameter($userId, false);
        $this->assertEquals($userId, $return);
        $return = $this->_api->handleUserParameter($userLogin, true);
        $this->assertEquals($userLogin, $return);
        $return = $this->_api->handleUserParameter($userLogin, false);
        $this->assertEquals($userId, $return);

    }

    public function testHandleUserParameterException()
    {

        $this->setExpectedException('Exception');
        $return = $this->_api->handleUserParameter(true, false);
    }

    public function testHandleActionParameter()
    {
        $return = $this->_api->handleActionParameter('create');
        $this->assertEquals('create', $return);
    }

    public function testHandleActionParameterException()
    {
        $this->setExpectedException('Exception');
        $return = $this->_api->handleActionParameter(true);
    }

    public function testHandlePermArrayParameter()
    {
        $perm = array('create' => true, 'update' => false);
        $return = $this->_api->handlePermArrayParameter($perm);
        $this->assertEquals(true, $return['create']);
        $this->assertEquals(false, $return['read']);
        $this->assertEquals(false, $return['update']);
    }

    public function testHandlePermArrayParameterExceptionInvalidKey()
    {
        $perm = array('kill' => false);
        $this->setExpectedException('Exception');
        $this->_api->handlePermArrayParameter($perm);
    }

    public function testHandlePermArrayParameterExceptionInvalidValue()
    {
        $perm = array('create' => 'true');
        $this->setExpectedException('Exception');
        $this->_api->handlePermArrayParameter($perm);
    }

    public function testgetChildrenX()
    {
        $location = 'dir://';
        $this->_api->getChildrenEntities($location);
        $this->_api->getChildrenIds($location);
        $this->_api->getChildrenLocations($location);
    }

    public function testisBranchChildOf()
    {
        $location = 'dir://';
        $plocation = 'dir://A1';
        $this->_api->isBranchChildOf($location, $plocation);

        $this->_api->isBranchChildOf($plocation, $location);
    }

    public function testcreateUserSpecificRole()
    {
        $roleName = 'test';
        $perm = array('create' => true);
        $this->_api->createUserSpecificRole($roleName, $perm);
    }

    public function testAlterUserRights()
    {
        $userId = 1;

        $perm = $perm = array('create' => true);
        $groupId = 1;
        $this->_api->alterUserRights($userId, $groupId, $perm);
        $groupId = 0;
        $this->_api->alterUserRights($userId, $groupId, $perm);
    }

    public function testdeleteRole()
    {
        $roleId = 0;
        $this->_api->deleteRole($roleId);
        $roleId = 1;
        $this->_api->deleteRole($roleId);
    }

    public function testdeleteGroup()
    {
        $groupId = 0;
        $this->_api->deleteGroup($groupId);
        $groupId = 1;
        $this->_api->deleteGroup($groupId);
    }

    public function testsaveRightArray()
    {
        $roleId = 1;
        $perm = array('create' => true);
        $this->_api->saveRightArray($roleId, $perm);
    }

    public function testgetRightKeys()
    {
        $this->_api->getRightKeys();
    }

    public function testgetNonDoubleRoles()
    {
        $userId = 1;
        $branchId = 1;
        $this->_api->getNonDoubleRoles($userId, $branchId);
    }

    public function testisAllowed()
    {
        $user = 1;
        $ressource = 1;
        $action = 'create';
        $this->_api->isAllowed($user, $ressource, $action);
        $action = 'read';
        $this->_api->isAllowed($user, $ressource, $action);
    }

    public function testgetRightArrayByRoleIdStreamTest()
    {
        $roleId = 2;
        $this->_api->getRightArrayByRoleId($roleId);
        $this->_api->getRightArrayByRoleId($roleId);
        $this->_api->getRightArrayByRoleId($roleId);
    }

    public function testisAllowedForGroup()
    {

        $roleNameMock = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getRoleName', 'setRightKey', 'setRoleName'));
        $roleNameMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock->expects($this->any())
            ->method('getRoleName')
            ->will($this->returnValue('dir://A1'));
        $roleNameMock->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));

        $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
        $roleNamerepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($roleNameMock, $roleNameMock)));

        $rolesMock = $this->getMock('Entity',
        array('getRoleId', 'getUserId', 'getGroupId', 'setRoleId', 'setUserId', 'setGroupId'));
        $rolesMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));

        $rolesrepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $rolesrepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($rolesMock, $rolesMock)));

        $map = array(array('RoleAPI\Entity\RoleName', $roleNamerepo), array('RoleAPI\Entity\Roles', $rolesrepo));

        $em = $this->getMock('Doctrine\ORM\EntityManager', array('getRepository', 'flush', 'persist', 'remove'),
        array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($map));
        $sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_api = new RoleAPIMain($sm);

        $user = 3;
        $group = 3;
        $action = 'isadmin';
        $this->_api->isAllowedForGroup($user, $group, $action);

    }

    public function testisAllowedForGroupFalse()
    {
        $roleNameMock = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getRoleName', 'setRightKey', 'setRoleName'));
        $roleNameMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock->expects($this->any())
            ->method('getRoleName')
            ->will($this->returnValue('dir://A1'));
        $roleNameMock->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MDtzOjQ6InJlYWQiO2I6MDtzOjY6InVwZGF0ZSI7YjowO3M6NjoiZGVsZXRlIjtiOjA7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjA7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MDtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjA7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjowO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MDtzOjc6ImlzYWRtaW4iO2I6MDt9'));

        $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
        $roleNamerepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($roleNameMock, $roleNameMock)));

        $rolesMock = $this->getMock('Entity',
        array('getRoleId', 'getUserId', 'getGroupId', 'setRoleId', 'setUserId', 'setGroupId'));
        $rolesMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));

        $rolesrepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $rolesrepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($rolesMock, $rolesMock)));

        $map = array(array('RoleAPI\Entity\RoleName', $roleNamerepo), array('RoleAPI\Entity\Roles', $rolesrepo));

        $em = $this->getMock('Doctrine\ORM\EntityManager', array('getRepository', 'flush', 'persist', 'remove'),
        array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($map));
        $sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_api = new RoleAPIMain($sm);

        $user = 3;
        $group = 3;
        $action = 'isadmin';
        $this->_api->isAllowedForGroup($user, $group, $action);
    }

    public function testdesignateNewAdmin()
    {
        $roleNameMock = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getRoleName', 'setRightKey', 'setRoleName'));
        $roleNameMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock->expects($this->any())
            ->method('getRoleName')
            ->will($this->returnValue('dir://A1'));
        $roleNameMock->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YTo4OntzOjY6ImNyZWF0ZSI7YjoxO3M6NDoicmVhZCI7YjoxO3M6NjoidXBkYXRlIjtiOjE7czo2OiJkZWxldGUiO2I6MTtzOjE4OiJyb2xlYWRtaW5pc3RyYXRpb24iO2I6MTtzOjE5OiJncm91cGFkbWluaXN0cmF0aW9uIjtiOjE7czoxMDoiY3JlYXRldXNlciI7YjoxO3M6NzoiaXNhZG1pbiI7YjoxO30='));

        $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
        $roleNamerepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($roleNameMock, $roleNameMock)));

        $rolesMock = $this->getMock('Entity',
        array('getId', 'getRoleId', 'getUserId', 'getGroupId', 'setRoleId', 'setUserId', 'setGroupId'));
        $rolesMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $rolesrepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $rolesrepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($rolesMock, $rolesMock)));

        $groupMock = $this->getMock('Entity', array('getUserId', 'getGroupId'));
        $groupMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $groupMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));

        $grouprepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $grouprepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($groupMock));
        $grouprepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($groupMock));
        $grouprepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($groupMock, $groupMock)));

        $map = array(array('RoleAPI\Entity\RoleName', $roleNamerepo), array('RoleAPI\Entity\Groups', $grouprepo),
        array('RoleAPI\Entity\Roles', $rolesrepo));

        $em = $this->getMock('Doctrine\ORM\EntityManager', array('getRepository', 'flush', 'persist', 'remove'),
        array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($map));

        $sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_api = new RoleAPIMain($sm);
        $user = 1;
        $newRole = 1;
        $this->_api->designateAdmin($user, $newRole);
    }

public function testgetUserInGroupStandardRole()
    {
        $roleNameMock = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('!dir://A1'));
        $roleNameMock->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock2 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock2->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock2->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('!dir://A1'));
        $roleNameMock2->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock3 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock3->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock3->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('!dir://A1'));
        $roleNameMock3->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));

        $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
        $roleNamerepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($roleNameMock, $roleNameMock2, $roleNameMock3)));

        $rolesMock = $this->getMock('Entity',
        array('getId', 'getRoleId', 'getUserId', 'getGroupId', 'setRoleId', 'setUserId', 'setGroupId'));
        $rolesMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $rolesrepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $rolesrepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($rolesMock, $rolesMock)));

        $groupMock = $this->getMock('Entity', array('getUserId', 'getGroupId'));
        $groupMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $groupMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));

        $grouprepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $grouprepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($groupMock));
        $grouprepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($groupMock));
        $grouprepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($groupMock, $groupMock)));

        $map = array(array('RoleAPI\Entity\RoleName', $roleNamerepo), array('RoleAPI\Entity\Groups', $grouprepo),
        array('RoleAPI\Entity\Roles', $rolesrepo));

        $em = $this->getMock('Doctrine\ORM\EntityManager', array('getRepository', 'flush', 'persist', 'remove'),
        array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($map));

        $sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_api = new RoleAPIMain($sm);

        $user = 1;
        $group = 1;

        $this->_api->getUserInGroupStandardRole($user, $group);
    }

    public function testgetUserInGroupnoStandardRole()
    {
        $roleNameMock = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('!dir://A1'));
        $roleNameMock->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock2 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock2->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock2->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('!dir://A1'));
        $roleNameMock2->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock3 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock3->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock3->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('!dir://A1'));
        $roleNameMock3->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));

        $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
        $roleNamerepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($roleNameMock, $roleNameMock)));

        $rolesMock = $this->getMock('Entity',
        array('getId', 'getRoleId', 'getUserId', 'getGroupId', 'setRoleId', 'setUserId', 'setGroupId'));
        $rolesMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $rolesrepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $rolesrepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(null));

        $groupMock = $this->getMock('Entity', array('getUserId', 'getGroupId'));
        $groupMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $groupMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));

        $grouprepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $grouprepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($groupMock));
        $grouprepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($groupMock));
        $grouprepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($groupMock, $groupMock)));

        $map = array(array('RoleAPI\Entity\RoleName', $roleNamerepo), array('RoleAPI\Entity\Groups', $grouprepo),
        array('RoleAPI\Entity\Roles', $rolesrepo));

        $em = $this->getMock('Doctrine\ORM\EntityManager', array('getRepository', 'flush', 'persist', 'remove'),
        array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($map));

        $sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_api = new RoleAPIMain($sm);

        $user = 1;
        $group = 1;

        $this->_api->getUserInGroupStandardRole($user, $group);
    }

    public function testgetUsernotInGroupStandardRole()
    {
        $roleNameMock = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('!dir://A1'));
        $roleNameMock->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock2 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock2->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock2->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('!dir://A1'));
        $roleNameMock2->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock3 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock3->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock3->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('!dir://A1'));
        $roleNameMock3->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));

        $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
        $roleNamerepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($roleNameMock, $roleNameMock)));

        $rolesMock = $this->getMock('Entity',
        array('getId', 'getRoleId', 'getUserId', 'getGroupId', 'setRoleId', 'setUserId', 'setGroupId'));
        $rolesMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $rolesrepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $rolesrepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(null));

        $groupMock = $this->getMock('Entity', array('getUserId', 'getGroupId'));
        $groupMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $groupMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));

        $grouprepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $grouprepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue(null));
        $grouprepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue(null));
        $grouprepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(null));

        $map = array(array('RoleAPI\Entity\RoleName', $roleNamerepo), array('RoleAPI\Entity\Groups', $grouprepo),
        array('RoleAPI\Entity\Roles', $rolesrepo));

        $em = $this->getMock('Doctrine\ORM\EntityManager', array('getRepository', 'flush', 'persist', 'remove'),
        array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($map));

        $sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_api = new RoleAPIMain($sm);

        $user = 1;
        $group = 1;
        $this->setExpectedException('Exception');
        $this->_api->getUserInGroupStandardRole($user, $group);
    }
    public function testgetUserInGroupCrudRole()
    {
        $roleNameMock = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('#dir://A1'));
        $roleNameMock->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock2 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock2->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock2->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('#dir://A1'));
        $roleNameMock2->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock3 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock3->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock3->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('#dir://A1'));
        $roleNameMock3->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));

        $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
        $roleNamerepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($roleNameMock, $roleNameMock2, $roleNameMock3)));

        $rolesMock = $this->getMock('Entity',
        array('getId', 'getRoleId', 'getUserId', 'getGroupId', 'setRoleId', 'setUserId', 'setGroupId'));
        $rolesMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $rolesrepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $rolesrepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($rolesMock, $rolesMock)));

        $groupMock = $this->getMock('Entity', array('getUserId', 'getGroupId'));
        $groupMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $groupMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));

        $grouprepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $grouprepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($groupMock));
        $grouprepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($groupMock));
        $grouprepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($groupMock, $groupMock)));

        $map = array(array('RoleAPI\Entity\RoleName', $roleNamerepo), array('RoleAPI\Entity\Groups', $grouprepo),
        array('RoleAPI\Entity\Roles', $rolesrepo));

        $em = $this->getMock('Doctrine\ORM\EntityManager', array('getRepository', 'flush', 'persist', 'remove'),
        array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($map));

        $sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_api = new RoleAPIMain($sm);

        $user = 1;
        $group = 1;

        $this->_api->getUserInGroupCrudRole($user, $group);
    }

    public function testgetUserInGroupnoCrudRole()
    {
        $roleNameMock = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('#dir://A1'));
        $roleNameMock->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock2 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock2->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock2->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('#dir://A1'));
        $roleNameMock2->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock3 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock3->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock3->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('#dir://A1'));
        $roleNameMock3->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));

        $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
        $roleNamerepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($roleNameMock, $roleNameMock)));

        $rolesMock = $this->getMock('Entity',
        array('getId', 'getRoleId', 'getUserId', 'getGroupId', 'setRoleId', 'setUserId', 'setGroupId'));
        $rolesMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $rolesrepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $rolesrepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(null));

        $groupMock = $this->getMock('Entity', array('getUserId', 'getGroupId'));
        $groupMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $groupMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));

        $grouprepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $grouprepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($groupMock));
        $grouprepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($groupMock));
        $grouprepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($groupMock, $groupMock)));

        $map = array(array('RoleAPI\Entity\RoleName', $roleNamerepo), array('RoleAPI\Entity\Groups', $grouprepo),
        array('RoleAPI\Entity\Roles', $rolesrepo));

        $em = $this->getMock('Doctrine\ORM\EntityManager', array('getRepository', 'flush', 'persist', 'remove'),
        array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($map));

        $sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_api = new RoleAPIMain($sm);

        $user = 1;
        $group = 1;

        $this->_api->getUserInGroupCrudRole($user, $group);
    }

    public function testgetUsernotInGroupCrudRole()
    {
        $roleNameMock = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('#dir://A1'));
        $roleNameMock->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock2 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock2->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock2->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('#dir://A1'));
        $roleNameMock2->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));
        $roleNameMock3 = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock3->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock3->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('#dir://A1'));
        $roleNameMock3->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));

        $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
        $roleNamerepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($roleNameMock, $roleNameMock)));

        $rolesMock = $this->getMock('Entity',
        array('getId', 'getRoleId', 'getUserId', 'getGroupId', 'setRoleId', 'setUserId', 'setGroupId'));
        $rolesMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));
        $rolesMock->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $rolesrepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $rolesrepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($rolesMock));
        $rolesrepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(null));

        $groupMock = $this->getMock('Entity', array('getUserId', 'getGroupId'));
        $groupMock->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(1));
        $groupMock->expects($this->any())
            ->method('getGroupId')
            ->will($this->returnValue(1));

        $grouprepo = $this->getMock('Repository', array('find', 'findOneBy', 'findBy'));
        $grouprepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue(null));
        $grouprepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue(null));
        $grouprepo->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(null));

        $map = array(array('RoleAPI\Entity\RoleName', $roleNamerepo), array('RoleAPI\Entity\Groups', $grouprepo),
        array('RoleAPI\Entity\Roles', $rolesrepo));

        $em = $this->getMock('Doctrine\ORM\EntityManager', array('getRepository', 'flush', 'persist', 'remove'),
        array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($map));

        $sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_api = new RoleAPIMain($sm);

        $user = 1;
        $group = 1;
        $this->setExpectedException('Exception');
        $this->_api->getUserInGroupCrudRole($user, $group);
    }

    public function testgetCRUDroles()
    {
        $roleNameMock = $this->getMock('Entity',
        array('getRoleId', 'getRightKey', 'getName', 'setRightKey', 'setRoleName'));
        $roleNameMock->expects($this->any())
            ->method('getRoleId')
            ->will($this->returnValue(1));
        $roleNameMock->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('#dir://A1'));
        $roleNameMock->expects($this->any())
            ->method('getRightKey')
            ->will(
        $this->returnValue(
        'YToxMDp7czo2OiJjcmVhdGUiO2I6MTtzOjQ6InJlYWQiO2I6MTtzOjY6InVwZGF0ZSI7YjoxO3M6NjoiZGVsZXRlIjtiOjE7czoxODoicm9sZWFkbWluaXN0cmF0aW9uIjtiOjE7czoxNDoiYWRtaW5ncm91cHZpZXciO2I6MTtzOjE2OiJhZG1pbmdyb3VwY3JlYXRlIjtiOjE7czoxNjoiYWRtaW5ncm91cGRlbGV0ZSI7YjoxO3M6MTY6ImFkbWluZ3JvdXBjaGFuZ2UiO2I6MTtzOjc6ImlzYWRtaW4iO2I6MTt9'));

        $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
        $roleNamerepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($roleNameMock));
        $roleNamerepo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($roleNameMock, $roleNameMock)));

        $em = $this->getMock('Doctrine\ORM\EntityManager', array('getRepository', 'flush', 'persist', 'remove'),
        array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($roleNamerepo));

        $sm = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));
        $this->_api=new RoleAPIMain($sm);
        $this->_api->getCrudRoleArrays();
    }

    function testGetNonexistentBranch(){

        $roleNameMock = $this->getMock('Entity',
        array('getBranchId', 'getLocation'));
        $roleNamerepo = $this->getMock('Repository', array('find', 'findOneBy', 'findAll'));
        $roleNamerepo->expects($this->any())
        ->method('find')
        ->will($this->returnValue(false));
        $roleNamerepo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue(false));
        $em = $this->getMock('Doctrine\ORM\EntityManager', array('getRepository', 'flush', 'persist', 'remove'),
        array(), '', false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($roleNamerepo));
        $sm = $this->getMock('sm',array('get'));
        $sm->expects($this->any())
        ->method('get')
        ->will($this->returnValue($em));

        $this->_api=new RoleAPIMain($sm);
        $this->_api->getBranchLocation(1);
        $this->_api->getBranchId('dir://A1');

    }
    public function tearDown()
    {
        unlink(__DIR__ . '/mockkey');
    }
}