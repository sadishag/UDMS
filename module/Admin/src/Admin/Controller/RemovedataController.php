<?php
namespace Admin\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class RemovedataController extends AbstractActionController
{

    public function indexAction()
    {
        $userid = $_SESSION['Userid'];
        $em = $this->getServiceLocator()->get('ObjectManager');
        $api = $this->serviceLocator->get('RoleAPI');
        $ressource = 0;
        $action = 'isadmin';
        $returnarray = array();
        if ($api->isAllowed($userid, $ressource, $action)) {
            $postvar = $this->getRequest()->getPost();
            if (isset($postvar['sizeofarray'])) {
                for ($i = 0; $i < $postvar['sizeofarray']; $i++) {
                    if ($postvar['Aktion' . $i] != 'null') {
                        if ($postvar['Aktion' . $i] == 1) {
                            $deleteValue = $postvar['ID' . $i];
                            $this->deletedata($deleteValue, $em, $api);
                        } else {
                            $this->recoverdata($postvar);
                        }
                    }

                }
            }
            $trashdata = $em->getRepository('RoleAPI\Entity\BranchName')->findBy(array('Trash' => 1));
            if (!empty($trashdata)) {
                $returnarray['trashdata'] = $trashdata;
                foreach ($trashdata as $data) {

                    $helpme = json_decode(stream_get_contents($data->getTrashInfo()));
                    $deleteuser = $em->getRepository('RoleAPI\Entity\User')->findoneBy(
                    array('UserId' => $helpme[0]));
                    $deletusername = $deleteuser->getFirstName() . ' ' . $deleteuser->getLastName();
                    $deletdate = date("Y-m-d H:i:s", $helpme[1]);
                    $returnarray['deleteuser'][$data->getBranchId()] = $deletusername;
                    $returnarray['deletedata'][$data->getBranchId()] = $deletdate;
                }

            } else {
                $returnarray['warning'] = 'Keine Daten im Papierkorb';
            }

        } else {
            return $this->redirect()->toUrl('/Angemeldet');
        }
        return $returnarray;

    }

    public function deletedata($postvar, $em, $api)
    {
        var_dump($postvar);
        $branchObjekt = $em->getRepository('RoleAPI\Entity\BranchName')->findOneBy(array('BranchId' => $postvar));
        $fileObject = $em->getRepository('MetaAPI\Entity\File')->findOneBy(array('Branch' => $postvar));

        if (!empty($fileObject)) {
            $fileContent = $em->getRepository('DataAPI\Entity\FileVersion')->findOneBy(
            array('FileId' => $fileObject->getFileId()));
            $em->remove($fileContent);
            $em->remove($fileObject);
            $em->remove($branchObjekt);
            $em->flush();
            echo 'Gelöscht';
        } else {
            $childs = $api->getChildrenEntities($branchObjekt->getLocation());
            $arrayofallFolder = ($this->test($childs, $api, $em));
            var_dump($arrayofallFolder[21][69][213]);
            //$this->deletearray($arrayofallFolder, $em);
        }

    }

    public function recoverdata($postvar)
    {

    }

    public function test($childs, $api, $em)
    {
        $helptest = array();

        $helpchild = array();
        foreach ($childs as $singlechild) {
            $helpchild = $api->getChildrenEntities($singlechild->getLocation());
            $helptest[$singlechild->getId()] = $this->test($helpchild, $api, $em);
        }
        return $helptest;
    }

    public function deletearray($arrayofallFolder, $em)
    {
        if (is_array($arrayofallFolder)) {
            foreach ($arrayofallFolder as $key => $value) {
                $fileObject = $em->getRepository('MetaAPI\Entity\File')->findOneBy(array('Branch' => $key));
                if (!empty($fileObject)) {
                    $branchObjekt = $em->getRepository('RoleAPI\Entity\BranchName')->findOneBy(
                    array('BranchId' => $key));
                    $fileContent = $em->getRepository('DataAPI\Entity\FileVersion')->findOneBy(
                    array('FileId' => $fileObject->getFileId()));
                    $fileContent = $em->getRepository('DataAPI\Entity\FileVersion')->findOneBy(
                    array('FileId' => $fileObject->getFileId()));
                    $em->remove($fileContent);
                    $em->remove($fileObject);
                    $em->remove($branchObjekt);
                    $em->flush();
                } else {
                    if (!empty($value)) {
                        $this->deletearray($value, $em);
                    } else {
                        $branchObjekt = $em->getRepository('RoleAPI\Entity\BranchName')->findOneBy(
                        array('BranchId' => $key));
                        $em->remove($branchObjekt);
                        $em->flush();
                    }
                }
            }
        }
    }
}