<div id="wrapper">
 <section id="content">
  <h2>Rollen vergabe </h2><br>

     <form action="" method="post" id="createrole" class="clear">
         <input type="search" name="rolenname" id="rolenameid" autocomplete="on" placeholder="Rollen Name">
         <input type="submit" name="createrole" id="createroleid" value="Rolle erstellen" class="vAlignMiddle">
     </form>


     <form action="" method="post" id="changerole" class="floatLeft">

         <select name="rolechange" id="rolechangeid" class="vAlignMiddle">
                    <option selected="selected" value="0">-- Rollen Anzeigen --</option>
                <?php
                    foreach ($this->RoleName as $key=>$value)
                    {
                ?>
                    <option value="<?php echo $key; ?>">
                        <?php echo $value; ?>
                    </option>
                <?php
                    }
                ?>

                </select>
                   <input type="submit" name="changerole" id="changeroleid" value="Rolle bearbeiten" class="vAlignMiddle">
                   <input type="submit" name="deleterole" id="deleteroleid" value="Rolle Löschen" class="vAlignMiddle">
                </form>
<?php if ($this->rolerightarray)
{?>
<h3 class="clear"><?php echo $this->RoleName[$this->rolename->getRoleId()];?></h3>
<?php $i=0;?>
 <form action="" method="post" id="rolechangetable">
  <input type="hidden" name="activrole" id="activroleid"value="<?php echo $this->rolename->getRoleId();?>">
           <table id="roletable" class="clear">
            <thead>
                <tr>
                    <th>RechteName</th>
                    <th>Recht-gesetzt</th>

                </tr>
            </thead>
            <tbody>

                <?php
                foreach ($this->rolerightarray as $key => $value) {
                    ?>
                    <tr>
                    <td><?php echo $key;?></td>
                    <td><input type="checkbox" name="<?php echo 'checkbox'.++$i;?>" <?php echo $value? 'checked' : '' ?>> </td>
                </tr>
                <?php }?>
         </tbody>
         <tfoot>
                <tr>

                    <td></td>
                    <td ><input type="submit" value="speichern" name="rolechangesave" id="rolechangesaveid"></td>
                </tr>
            </tfoot>

        </table>
        </form>
<?php }?>

   </section>
</div>

<div id="slider">
    <div>
        <?php
            include 'help.html';
        ?>
    </div>
    <p id="sliderButton"><span>Hilfe!</span></p>
</div>


<script>
<?php
if(isset($this->error)){
    ?>
    globalNotification({error: '<?php echo $this->error ?>'});
    <?php
}
?>
</script>
<script>
<?php
if(isset($this->success)){
    ?>
    globalNotification({success: '<?php echo $this->success ?>'});
    <?php
}
?>
</script>
