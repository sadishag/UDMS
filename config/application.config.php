<?php
return array(
    'modules' => array(
        'Application',
        'DoctrineModule',
        'DoctrineORMModule',
        'Auth',
        'Admin',
        'GroupAdmin',
        'User',
        'RoleAPI',
        'Notice',
        'Groupchange',
        'Metadata',
        'MetaAPI',
        'Data',
        'ObjectManager',
        'DataAPI',
    ),
    'module_listener_options' => array(
        'module_paths' => array(
            './module',
            './vendor',
        ),
        'config_glob_paths' => array('config/autoload/{,*.}{global,local}.php'),
    ),
);
