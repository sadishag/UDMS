Member = {};
Member.data = [];
Member.updateAndDisplayData = function(members){
	for(var i = 0; members.length > i; i++){
		var member = members[i];

		
		var $row = $('<tr></tr>');
		$row.data('id', member.id);
		$row.data('name', member.name);
		
		var $name = $('<td></td>');
		$name.text(member.name);
		
		var $actions = $('<td></td>');
		
		var $delete = $('<span class="delete-16" title="aus Gruppe entfernen"></span>');
		$delete.click(function(){
				Member.delete($(this).parent().parent().data('id'));				
		});
		var $add = $('<span class="accept-16" title="zu Gruppe Hinzufügen"></span>');
		$add.click(function(){
				Member.add($(this).parent().parent().data('id'));				
		});
		
		$actions.append($add);		
		$actions.append($delete);


		if(member.name.length > 0){
			$row.append($name);
			$row.append($actions);			
		}

		if(typeof(Member.data[member.id]) === 'undefined'){
			//wenn es die gruppe vorher nicht angezeigt war zeige sie an
			$('#memberTableContent').append($row);
		}else{
			//wenn es die gruppe schon angezeigt ist update die zeile
			Member.data[member.id].replaceWith($row);
		}
		
		Member.data[member.id] = $row;

	}
};

Member.handleResponse = function(response){

	//create table if don't exist
	if($('#memberTable').length === 0){
		$('#content').append('<table id="memberTable" class="clear"><thead><tr><th>Name</th><th>Aktionen</th></tr></thead><tbody id="memberTableContent"></tbody></table>');
	}
	
	//create or update lines in table
	if(typeof(response.members) !== 'undefined' && response.members.length > 0){
		Member.updateAndDisplayData(response.members);	
	}
	$('#memberTable').dataTable();
};

Member.show = function(){
	globalAjax('show').success(Member.handleResponse);
};
Member.add = function(id){
	globalAjax('add', {type: 'POST', data:{id: id}}).success(Member.handleResponse);
}
Member.delete = function(id){
	globalAjax('delete', {type: 'POST', data:{id: id}}).success(Member.handleResponse);
}





//autoload temp members
Member.show();


