<?php
namespace Notice\Controller;

use Zend\Feed\PubSubHubbub\Publisher;

use Zend\Http\Response;

use Notice\Controller\NoticeController;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;

class NoticeControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {
        $this->_controller = new NoticeController();
    }

    public function tearDown()
    {
    }

    public function testCreateAction()
    {
        $_SESSION['Userid'] = 1;

        $repo = $this->getMock('Repository', array('findAll', 'find'));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->createAction();
    }

    public function provideTestVisionAction()
    {
        $noteMock = $this->getMock('RoleAPI\Entity\Note',array('getName'));
        $metaDataMock = $this->getMock('RoleAPI\Entity\MetaData');

        return array(
            array($noteMock, $metaDataMock),
            array($noteMock, null),
            array(null, null)
        );
    }

    /**
     * @dataProvider provideTestVisionAction
     */
    public function testVisionAction($noteMock, $metaDataMock)
    {
        $functionMock = $this->getMock('Notice\Controller\NoticeController', array('hasVision','checkCurrentRole'));

        $_SESSION['Userid'] = 1;

        $userMock = $this->getMock('RoleAPI\Entity\User', array('getName'));

        $repo = $this->getMock('Repository', array('findOneBy', 'find', 'findAll'));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($noteMock));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($metaDataMock));
        $repo->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($userMock, $userMock)));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository', 'getCrudRoleArrays'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $route = new RouteMatch(array());
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $mvcEvent->setResponse(new Response());
        $functionMock->setEvent($mvcEvent);
        $functionMock->setServiceLocator($smMock);
        $functionMock->visionAction();
    }

    public function testAddVisionAction()
    {
        $post = array('name' => 'Max Mustermann');

        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $_SESSION['Userid'] = 1;

        $userMock = $this->getMock('RoleAPI\Entity\User', array('getBranch', 'getGroupId','getVision','setVision'));

        $repo = $this->getMock('Repository', array('findOneBy', 'findBy','find'));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($userMock));
        $repo->expects($this->any())
        ->method('find')
        ->will($this->returnValue($userMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository', 'persist', 'flush'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->addVisionAction();
    }

    public function provideTestHasVision()
    {
        return array(
            array(array('user' => array(0 => 0), 'group' => array(0 => 0), 'role' => array(0 => 0))),
            array(array())
        );
    }

    /**
     * @dataProvider provideTestHasVision
     */
    public function testHasVision($array)
    {
        $stream = fopen('php://memory','r+');
        fwrite($stream, base64_encode(serialize($array)));
        rewind($stream);

        $userMock = $this->getMock('RoleAPI\Entity\User', array('getLogin', 'getName'));

        $fileMock = $this->getMock('MetaAPI\Entity\File', array('getVision'));
        $fileMock->expects($this->any())
            ->method('getVision')
            ->will($this->returnValue($stream));

        $repo = $this->getMock('Repository', array('find', 'findOneBy'));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($fileMock));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($userMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->hasVision(1);
    }

    public function testDeleteVisionAction()
    {
        $post = array();

        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $array = array();
        $stream = fopen('php://memory','r+');
        fwrite($stream, base64_encode(serialize($array)));
        rewind($stream);

        $fileMock = $this->getMock('MetaAPI\Entity\File', array('getVision'));
        $fileMock->expects($this->any())
            ->method('getVision')
            ->will($this->returnValue($stream));

        $repo = $this->getMock('Repository', array('find'));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($fileMock));


        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository', 'persist', 'flush'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->deleteVisionAction();
    }

    public function provideTestSaveVisionRoleAction()
    {
        $userMock = $this->getMock('RoleAPI\Entity\User', array('getBranch', 'getGroupId', 'setRoleId','getVision','setVision'));

        return array(
            array($userMock),
            array(null)
        );
    }

    /**
     * @dataProvider provideTestSaveVisionRoleAction
     */
    public function testSaveVisionRoleAction($data)
    {
        $userMock = $this->getMock('RoleAPI\Entity\User', array('getBranch', 'getGroupId', 'setRoleId','getVision','setVision'));

        $repo = $this->getMock('Repository', array('findOneBy','find'));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($userMock));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($userMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository', 'persist', 'flush'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->saveVisionRoleAction();
    }

    public function testChekCurrentRole()
    {
        $array = array('user' => array(1 => 4, 2 => 0));
        $stream = fopen('php://memory','r+');
        fwrite($stream, base64_encode(serialize($array)));
        rewind($stream);

        $fileMock = $this->getMock('MetaAPI\Entity\File', array('getVision'));
        $fileMock->expects($this->any())
            ->method('getVision')
            ->will($this->returnValue($stream));

        $roleNameMock = $this->getMock('RoleAPI\Entity\RoleName', array('getName'));

        $repo = $this->getMock('Repository', array('find', 'findOneBy'));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($fileMock));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($roleNameMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->checkCurrentRole(array(), 1);
    }

    public function testSaveVision()
    {
        $array = array('user' => array(1 => 4, 2 => 0));
        $stream = fopen('php://memory','r+');
        fwrite($stream, base64_encode(serialize($array)));
        rewind($stream);

        $fileMock = $this->getMock('MetaAPI\Entity\File', array('getVision'));
        $fileMock->expects($this->any())
            ->method('getVision')
            ->will($this->returnValue($stream));

        $repo = $this->getMock('Repository', array('find'));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($fileMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository', 'persist', 'flush'));
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->saveVision(1, 'user', 1);
    }
}

