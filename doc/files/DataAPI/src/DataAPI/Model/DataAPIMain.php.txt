<?php
/**
 * DataAPIMain.php Main Class for the DataAPI
 *
 * @package    DataAPI\Model
 * @copyright  Copyright (c) 2013 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Lucas Mikulla <lucas.mikulla@unister.de>
 */

namespace DataAPI\Model;

/**
 * Main Class of the DataAPI
 *
 * @package DataAPI\Model
 * @copyright Copyright (c) 2013 Unister GmbH
 */
class DataAPIMain
{
    /**
     * instance of the ServiceLocator
     *
     * @var ServiceLocator
     */
    protected $_serviceManager;

    /**
     * instance of the EntityManager
     *
     * @var ObjectManager
     */
    protected $_entityManager;

    /**
     * Constructor for getting a doctrine entitymanager instance
     *
     * @param ServiceLocator $sm
     */
    public function __construct($sm)
    {
        $this->_serviceManager = $sm;
        $this->_entityManager = $this->_serviceManager->get('ObjectManager');
    }

}
