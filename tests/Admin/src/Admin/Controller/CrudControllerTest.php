<?php
namespace Admin\Controller;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Admin\Controller\CrudController;
use Zend\Stdlib\Parameters;

class CrudControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp() {
        $this->_controller = new CrudController();
    }

    public function tearDown() {

    }

    public function testIndexAction() {
        $_SESSION='2312';
        $post = array(
                    'userFirst' => '123123',
                    'userLast' => '354345',
                    'userglobalsearch' => 'test',
                    'rolesave' => 'sadd',
                    'groupSearch' => 'asdasd',
                    'grupperolesichern' => 'asdasd'
        );
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy','findby'));
        $repo->expects($this->any())
        ->method('findby')
        ->will($this->returnValue($albumMock));

        $em = $this->getMock('ObjectManager',
                                array("getRepository", "persist", "flush"),
                                array(),
                                "",
                                false
        );
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $apiMock = $this->getMock('RoleAPI',
                                    array("createNewRole", 'getRightArrayByRoleId', 'deleteRole',
                                            'saveRightArray','getCrudRoleArrays'),
                                    array(),
                                    "",
                                    false
        );
        $apiMock->expects($this->any())
        ->method('getCrudRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));


        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }

    public function testIndexActionPostVar() {
        $_SESSION='2312';
        $post = array(
                        'userFirst' => '123123',
                        'hiddenuser2' => 'asda',
                        'role2' => 'adasda',
                        'userLast' => '354345',
                        'userglobalsearch' => 'test',
                        'rolesave' => 'sadd',
                        'groupSearch' => 'asdasd',
                        'groupsave' => 'true',
                        'sizeofarray' => '5',
                        'role1' => '0',
                        'grupperolesichern' => 'asdasd'
        );
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue('asdasda'));

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy','findby'));
        $repo->expects($this->any())
        ->method('findby')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $em = $this->getMock('ObjectManager',
                                array("getRepository", "persist", "flush"),
                                array(),
                                "",
                                false
        );
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $apiMock = $this->getMock('RoleAPI',
                                    array("createNewRole", 'getRightArrayByRoleId', 'deleteRole',
                                            'saveRightArray','getCrudRoleArrays'),
                                    array(),
                                    "",
                                    false
        );
        $apiMock->expects($this->any())
        ->method('getCrudRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));


        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }

    public function testIndexActionPostVarElse() {
        $_SESSION='2312';
        $post = array(
                    'userFirst' => '123123',
                    'hiddenuser2' => 'asda',
                    'role2' => 'adasda',
                    'userLast' => '354345',
                    'userglobalsearch' => 'test',
                    'rolesave' => 'sadd',
                    'groupSearch' => 'asdasd',
                    'groupsave' => 'true',
                    'sizeofarray' => '5',
                    'role1' => '0',
                    'grupperolesichern' => 'asdasd'
        );
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue('asdasda'));

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy','findby'));
        $repo->expects($this->any())
        ->method('findby')
        ->will($this->returnValue($albumMock));


        $em = $this->getMock('ObjectManager',
                                array("getRepository", "persist", "flush"),
                                array(),
                                "",
                                false
        );
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $apiMock = $this->getMock('RoleAPI',
                                    array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray',
                                            'getCrudRoleArrays'),
                                    array(),
                                    "",
                                    false
        );
        $apiMock->expects($this->any())
        ->method('getCrudRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));


        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }

    public function testIndexActionShow() {
        $_SESSION='2312';
        $post = array(
                    'userFirst' => '123123',
                    'group' => 'asdasd',
                    'hiddenuser2' => 'asda',
                    'role2' => 'adasda',
                    'userLast' => '354345',
                    'userglobalsearch' => 'test',
                    'rolesave' => 'sadd',
                    'groupSearch' => 'asdasd',
                    'groupsave' => 'true',
                    'sizeofarray' => '5',
                    'role1' => '0',
                    'grupperolesichern' => 'asdasd'
        );
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue('asdasda'));

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy','findby'));
        $repo->expects($this->any())
        ->method('findby')
        ->will($this->returnValue($albumMock));


        $em = $this->getMock('ObjectManager',
                                array("getRepository", "persist", "flush"),
                                array(),
                                "",
                                false
        );
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $apiMock = $this->getMock('RoleAPI',
                                    array("createNewRole", 'getRightArrayByRoleId', 'deleteRole',
                                            'saveRightArray','getCrudRoleArrays'),
                                    array(),
                                    "",
                                    false
        );
        $apiMock->expects($this->any())
        ->method('getCrudRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));


        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }

    public function testIndexActionShowArray() {
        $_SESSION='2312';
        $post = array(
                    'userFirst' => '123123',
                    'group' => 'asdasd',
                    'hiddenuser2' => 'asda',
                    'role2' => 'adasda',
                    'userLast' => '354345',
                    'userglobalsearch' => 'test',
                    'rolesave' => 'sadd',
                    'groupSearch' => 'asdasd',
                    'groupsave' => 'true',
                    'sizeofarray' => '5',
                    'role1' => '0',
                    'grupperolesichern' => 'asdasd'
        );
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User',
                                        array('getRoleId', 'setRoleId','getGroupId','getUserId')
        );
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue('5'));
        $albumMock->expects($this->any())
        ->method('getUserId')
        ->will($this->returnValue('2'));

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy','findby'));
        $repo->expects($this->any())
        ->method('findby')
        ->will($this->returnValue(array($albumMock,$albumMock,$albumMock)));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));


        $em = $this->getMock('ObjectManager',
                                array("getRepository", "persist", "flush"),
                                array(),
                                "",
                                false
        );
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $apiMock = $this->getMock('RoleAPI',
                                    array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray',
                                            'getCrudRoleArrays','isAllowedForGroup',
                                                'getUserInGroupCrudRole'
                                    ),
                                    array(),
                                    "",
                                    false
        );
        $apiMock->expects($this->any())
        ->method('getCrudRoleArrays')
        ->will($this->returnValue(array('1' => '3', '6' => '3','3' => '3', '5' => '3', '4' => '3','2' => '3',)));
        $apiMock->expects($this->any())
        ->method('getUserInGroupCrudRole')
        ->will($this->returnValue(1));
        $apiMock->expects($this->any())
        ->method('isAllowedForGroup')
        ->will($this->returnValue(1));


        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }
}

