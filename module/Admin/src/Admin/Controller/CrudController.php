<?php
/**
 * CrudController.php
 *
 * @package    Admin\Controller
 * @copyright  Copyright (c) 2013 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Dennis Krüger <dennis.krueger@unister.de>
 */

namespace Admin\Controller;

use Zend\Console\Response;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Doctrine\ORM\EntityManager;

/**
 * Controlleinheit für view\Groupchange\index
 *
 * @copyright Copyright (c) 2012 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Sebastian Krebs <sebastian.krebs@unister.de>
 * @author Mathias Piehl <mathias.piehl@unister.de>
 */

class CrudController extends AbstractActionController
{

    /**
     * Funktion zur Steuerung der index.phtml
     * Sucht alle Gruppen in den der eingeloggte User Gruppadmin ist.
     * Zeigt diese Gruppen.
     * Zeigt nach auswahl der Gruppe alle User und ihre derzeitige Rolle
     * Rollen können verändert werden
     *
     * @return mixed $returnArray
     *         Objekte, Arrays und Integers
     *
     */

    public function indexAction()
    {
        $userid = $_SESSION['Userid'];
        $postvar = $this->getRequest()->getPost();
        $api = $this->serviceLocator->get('RoleAPI');
        $em = $this->getServiceLocator()->get('ObjectManager');
        $rols = $api->getCrudRoleArrays();

        $allgroupwheremember = $em->getRepository('RoleAPI\Entity\Groups')->findBy(array('UserId' => $userid));
        $grouparray = array();
        foreach ($allgroupwheremember as $group) {
            if ($api->isAllowedForGroup($userid, $group->getGroupId(), 'isadmin')) {
                $grouparray[] = $group->getGroupId();
            }
        }
        $grouparray = array_unique($grouparray);

        $grouparrayname = $em->getRepository('RoleAPI\Entity\GroupName')->findall();

        if ($postvar['groupsave']) {
            for ($i = 0; $i < $postvar['sizeofarray']; $i++) {
                if ($postvar['role' . $i] === "0") {
                    $returnArray['warning'] = 'Keine Änderrung vorgenommen, speichern nicht nötig.';
                    continue;
                } elseif (($postvar['role' . $i]) && ($postvar['hiddenuser' . $i])) {
                    $newroleingroup = $em->getRepository('RoleAPI\Entity\Roles')->findOneBy(
                    array('UserId' => $postvar['hiddenuser' . $i], 'GroupId' => $postvar['group']));

                    if ($newroleingroup) {
                        $newroleingroup->setRoleId($postvar['role' . $i]);
                    } else {
                        $newroleingroup = new \RoleAPI\Entity\Roles();
                        $newroleingroup->setRoleId($postvar['role' . $i]);
                        $newroleingroup->setUserId($postvar['hiddenuser' . $i]);
                        $newroleingroup->setGroupId($postvar['group']);
                    }

                    $em->persist($newroleingroup);
                    $em->flush();
                    $returnArray['error'] = 'Rechte wurden geändert. Siehe "Aktuelle Rechte"';
                }
            }
            if (isset($returnArray['error'])) {
                unset($returnArray['warning']);
            }

        }

        if ($postvar['group']) {
            $groupname = $em->getRepository('RoleAPI\Entity\GroupName')->findOneBy(
            array('GroupId' => $postvar['group']));
            $allgroupmember = $em->getRepository('RoleAPI\Entity\Groups')->findBy(
            array('GroupId' => $postvar['group']));

            $roleingrptouser = array();
            foreach ($allgroupmember as $groupmembersolo) {
                $user = $groupmembersolo->getUserId();
                $roleingrptouser[$user] = $api->getUserInGroupCrudRole($user, $postvar['group']);

            }

            $user = array();
            $i = 0;
            foreach ($roleingrptouser as $key => $value) {
                $user[$i] = $em->getRepository('RoleAPI\Entity\User')->findOneBy(array('UserId' => $key));
                $user[$i] = array('UserId' => $user[$i]->getUserId(), 'FirstName' => $user[$i]->getFirstName(),
                            'LastName' => $user[$i]->getLastName(), 'RoleName' => $rols[$value]);
                $i++;
            }

            $returnArray['user'] = $user;
            $returnArray['groupname'] = $groupname;
        }

        $returnArray['role'] = $rols;
        $returnArray['grouparrayname'] = $grouparrayname;
        return $returnArray;
    }
}
