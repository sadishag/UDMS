<script src="/js/vision.js" defer></script>
<h1>Notizrechtbearbeitung - <?php echo $this->notename; ?></h1>
<form action="" method="POST" id="selectForm" name="<?php echo $this->noticeid; ?>">
    <select name="user" id="userid" class="vAlignMiddle">
        <option selected="selected">-- Benutzer --</option>
    <?php
    foreach ($this->alluser as $user)
    {
        ?>
        <option value="<?php echo $user->getUserId();?>" >
        <?php echo $user->getLogin(); ?>
        </option>
        <?php
    }
    ?>
    </select>
    <select name="group" id="groupid" class="vAlignMiddle">
    <option selected="selected">-- Gruppen --</option>
    <?php
    foreach ($this->allgroups as $group)
    {
        ?>
        <option value="<?php echo $group->getGroupId();?>" >
        <?php echo $group->getName(); ?>
        </option>
        <?php
    }
    ?>

    </select>
    <select name="role" id="roleid" class="vAlignMiddle">
    <option selected="selected">-- Rollen --</option>
    <?php
    foreach ($this->allroles as $role)
    {
        ?>
        <option value="<?php echo $role->getRoleId();?>" >
        <?php echo str_replace('!', '', $role->getName()); ?>
        </option>
        <?php
    }
    ?>
    </select>
    <input id="savevision" class="btn btn-success btn-large clear" type="button" value="Speichern">
</form>
<?php if (!empty($this->vision['user'])) { ?>
    <table id="visionHasUser" class="clear">
        <thead>
            <tr>
                <th style="display: none"></th>
                <th>Benutzer</th>
                <th>Aktionen</th>
            </tr>
        </thead>
        <tbody>
        <?php $i=0; ?>
        <?php foreach ($this->vision['user'] as $id => $user) { ?>
            <tr id="<?php echo ++$i; ?>">
                <td id="hidden" style="display:none"><?php echo $id; ?></td>
                <td><?php echo $user['name']; ?></td>
                <td>
                    <span id="user" class="delete-16" title="Benutzer entfernen"></span>
                    <span id="user" class="edit-16" title="Rechte bearbeiten"></span>
                </td>
            </tr>
            <tr style="display: none"></tr>
            <tr id="userhiddenrole<?php echo $i;?>" style="display: none">
                <td>----&gt; momentane Rolle: <?php echo isset($user['role']) ? $user['role'] : 'Noch keine Rolle gesetzt' ?></td>
                <td id='<?php echo $id; ?>'>
                    <select name="role" id="roleid" class="vAlignMiddle">
                    <option selected="selected">-- Rolle --</option>
                    <?php
                    foreach ($this->selectroles as $role)
                    {
                        ?>
                        <option value="<?php echo $role->getRoleId();?>" >
                        <?php echo str_replace('#', '', $role->getName()); ?>
                        </option>
                        <?php } ?>
                    </select>
                    <span title="schließen" id="user" class="delete-16"></span>
                    <span title="speichern" id="user" class="save-16"></span>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>
<?php if (!empty($this->vision['group'])) { ?>
<table id="visionHasGroup" class="clear">
    <thead>
        <tr>
            <th style="display: none"></th>
            <th>Gruppen</th>
            <th>Aktionen</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=0; ?>
    <?php foreach ($this->vision['group'] as $id => $group) { ?>
        <tr id="<?php echo ++$i; ?>">
            <td id="hidden" style="display: none"><?php echo $id; ?></td>
            <td><?php echo $group['name']; ?></td>
            <td>
            <span id="group" class="delete-16" title="Gruppe entfernen"></span>
            <span id="group" class="edit-16" title="Rechte bearbeiten"></span>
            </td>
        </tr>
        <tr style="display: none"></tr>
            <tr id="grouphiddenrole<?php echo $i;?>" style="display: none">
                <td>----&gt; momentane Rolle: <?php echo isset($group['role']) ? $group['role'] : 'Noch keine Rolle gesetzt' ?></td>
                <td id='<?php echo $id; ?>'>
                    <select name="role" id="roleid" class="vAlignMiddle">
                    <option selected="selected">-- Rolle --</option>
                    <?php
                    foreach ($this->selectroles as $role)
                    {
                        ?>
                        <option value="<?php echo $role->getRoleId();?>" >
                        <?php echo str_replace('#', '', $role->getName()); ?>
                        </option>
                        <?php } ?>
                    </select>
                    <span title="schließen" id="group" class="delete-16"></span>
                    <span title="speichern" id="group" class="save-16"></span>
                </td>
            </tr>
    <?php } ?>
    </tbody>
</table>
<?php } ?>
<?php if (!empty($this->vision['role'])) { ?>
<table id="visionHasRole" class="clear">
    <thead>
        <tr>
            <th style="display: none"></th>
            <th>Rolle</th>
            <th>Aktionen</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=0; ?>
    <?php foreach ($this->vision['role'] as $id => $role) { ?>
         <tr id="<?php echo ++$i; ?>">
            <td id="hidden" style="display:none"><?php echo $id; ?></td>
            <td><?php echo $role['name']; ?></td>
            <td>
                <span id="role" class="delete-16" title="Rolle entfernen"></span>
                <span id="role" class="edit-16" title="Rechte bearbeiten"></span>
            </td>
        </tr>
        <tr style="display: none"></tr>
            <tr id="rolehiddenrole<?php echo $i;?>" style="display: none">
                <td>----&gt; momentane Rolle: <?php echo isset($role['role']) ? $role['role'] : 'Noch keine Rolle gesetzt' ?></td>
                <td id='<?php echo $id; ?>'>
                    <select name="role" id="roleid" class="vAlignMiddle">
                    <option selected="selected">-- Rolle --</option>
                    <?php
                    foreach ($this->selectroles as $role)
                    {
                        ?>
                        <option value="<?php echo $role->getRoleId();?>" >
                        <?php echo str_replace('#', '', $role->getName()); ?>
                        </option>
                        <?php } ?>
                    </select>
                    <span title="schließen" id="role" class="delete-16"></span>
                    <span title="speichern" id="role" class="save-16"></span>
                </td>
            </tr>
    <?php } ?>
    </tbody>
</table>
<?php } ?>
<a class="btn btn-success btn-large clear" href="/notice">Zurück zur Übersicht</a>
