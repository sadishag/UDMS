<?php

namespace RoleAPI\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * BranchName
 *
 * @ORM\Table(name="BranchName")
 *
 * @property int $BranchId
 * @property string $Name
 * @property string $Location
 * @property int $Trash
 * @property blob $TrashInfo
 */

class BranchName
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */

    protected $BranchId;

    /**
     * @ORM\Column(type="string")
     */
    protected $Name;

    /**
     * @ORM\Column(type="string")
     */
    protected $Location;

    /**
     * @ORM\Column(type="integer", nullable = true)
     */
    protected $Trash;

    /**
     * @ORM\Column(type="blob", nullable = true)
     */
    protected $TrashInfo;

    /**
     * @return the $TrashInfo
     */
    public function getTrashInfo()
    {
        return $this->TrashInfo;
    }

	/**
     * @param blob $TrashInfo
     */
    public function setTrashInfo($TrashInfo)
    {
        $this->TrashInfo = $TrashInfo;
    }

	/**
     * @return the $Trash
     */
    public function getTrash()
    {
        return $this->Trash;
    }

	/**
     * @param number $Trash
     */
    public function setTrash($Trash)
    {
        $this->Trash = $Trash;
    }

	/**
     *
     * @return the $BranchId
     */
    public function getBranchId()
    {
        return $this->BranchId;
    }

    /**
     *
     * @return the $BranchId
     */
    public function getId()
    {
        return $this->BranchId;
    }

    /**
     *
     * @return the $Name
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     *
     * @return the $Location
     */
    public function getLocation()
    {
        return $this->Location;
    }

    /**
     *
     * @param string $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

    /**
     *
     * @param string $Location
     */
    public function setLocation($Location)
    {
        $this->Location = $Location;
    }

}