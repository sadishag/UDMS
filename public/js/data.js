/* global */
var currentDir = {location: 'dir://'};

function showDirs(response){

	if(typeof response.dir !== 'undefined'){
		currentDir = response.dir;
	}
	$('#data').children('tr').remove();
	$('#data').addClass('dirs');
	
	if(typeof response.childs !== 'undefined'){
		for(var i = 0; i < response.childs.length; i++){
			var type = response.childs[i].location.split('://')[0];				
			
			var $row = $('<tr></tr>');
			$row.data('location', response.childs[i].location);
			$row.data('name', response.childs[i].name);
			
			var $name = $('<td></td>');
			$name.text(response.childs[i].name);
			$name.addClass(type);

			
			$row.click(function(e){
				if(!(e.ctrlKey)){
					$(this).siblings().removeClass('selected');
				}
				$(this).addClass('selected');
			})
			$row.dblclick(function(){
				//if its a dir go in the dir
				if($(this).data('location').split('://')[0] === 'dir'){
					globalAjax('/dir', {type: 'POST', data: {dirLocation:  $(this).data('location')}}).success(showDirs);						
				}else{
					globalAjax('/file', {type: 'POST', data: {dirLocation:  $(this).data('location')}}).success(function(r){
						openFileEditDialog(r).addEventHandler('close', function(){
				    		globalAjax('/dir', {data: {dirLocation : currentDir.location}}).success(showDirs);
				        });
						
					});
				}
			});
			
			
			$row.append($name);
			
			$('#data').append($row);
			
		}
	}
}


function showTree(){
	$('#tree').jstree({
	    plugins : ["themes", "json_data", "ui", "types"],
		json_data: {
			ajax: {
				url: '/dir',
				type: 'post',
				data: function(n){
					return {
						dirLocation : n.attr ? n.attr('id') : 'dir://',
					}
				},
				dataFilter: function(response){
					var childs = [];
					var responseObj = JSON.parse(response);
					for(var i = 0; i < responseObj.childs.length; i++){
						var type = responseObj.childs[i].location.split('://')[0];
						if(type === 'dir'){
							childs[childs.length] = {attr: {rel: type, id: responseObj.childs[i].location}, data: responseObj.childs[i].name, state:type === 'dir' ? 'closed' : ''};
						}
					}
					return JSON.stringify(childs);
				}
			}
		},
		"types" : {
			"default" : {
				"icon" : {
					"image" : "/img/folder-16.png"
				}
			},
			"types" : {
				"dir" : {
					"icon" : {
						"image" : "/img/folder-16.png"
					}
				},
				"notice" : {
					"icon" : {
						"image" : "/img/notice-16.png"
					}
				},
				"image" : {
					"icon" : {
						"image" : "/img/img-16.png"
					}
				},
				"code" : {
					"icon" : {
						"image" : "/img/code-16.png"
					}
				},
			}
		},
	}).delegate('a', 'click', function (event) {
		event.preventDefault();
		var location = event.currentTarget.parentElement.id;
		if(location.split('://')[0] === 'dir'){
			globalAjax('/dir', {data: {dirLocation : location}}).success(showDirs);
		}
	}).delegate('a', 'dblclick', function (event) {
		var location = event.currentTarget.parentElement.id;
		if(location.split('://')[0] !== 'dir'){
			globalAjax('/file', {data:{dirLocation: location}}).success(showPreview);
		}
	});
}


function getTypeAndPreviewFromName(name){
	var dataTypes = [
		{
			search: /\.pdf$/,
			type: 'application/pdf',
			preview: 'object'
		},
		{
			search: /\.xml$/,
			type: 'text/xml',
			preview: 'ace',
			setMode: 'html'
		},
		{
			search: /\.rtf.html$/,
			type: 'text/rtf',
			preview: 'cke',
		},
		{
			search: /\.notice$/,
			type: 'text/notice',
			preview: 'ace'
		},
		{
			search: /(\.html$|\.htm)$/,
			type: 'text/html',
			preview: 'ace',
			setMode: 'html'
		},
		{
			search: /(\.jpeg|\.jpg|\.jpe)$/,
			type: 'image/jpeg',
			preview: 'image'
		},
		{
			search: /\.png$/,
			type: 'image/png',
			preview: 'image'
		},
		{
			search: /\.gif$/,
			type: 'image/gif',
			preview: 'image'
		},
		{
			search: /\.bmp$/,
			type: 'image/bmp',
			preview: 'image'
		},
		{
			search: /.*/,
			type: 'unknown/unknown',
			preview: 'unknown'
		},
		
	]
	for(var i = 0; i < dataTypes.length; i++){
		if(dataTypes[i].search.test(name)){
			return dataTypes[i];
		}
	}
}

/*
function showPreview(data){
	$('#infoHolder').remove();
	
	data.type = getTypeAndPreviewFromName(data.name).type;
	if(data.type === 'unknown/unknown'){
		globalNotification({warning: 'Dateityp nicht erlaubt'});
		return false;
	}
	
	var $infoHolder = $('<div id="infoHolder"></div>');
	var $informations = $('<div id="informations"></div>');
	var $preview = $('<div id="preview" ></div>');
	var $infos = $('<div></div>');
	var $actions = $('<div></div>');
	
	var $name = $('<input id="fileName" type="text" class="textinput" />');
	$name.val(data.name);
	$infos.append('<header>Dateiname</header>');
	$infos.append($name);
	
	
	
	var $owner = $('<input type="text" list="users" class="textinput" />');
	$owner.val(data.owner);
	$infos.append('<header>Besitzer</header>');
	$infos.append($owner);
	
	var $readOnly = $('<input type="checkbox"/>');
	$infos.append('<header>Read Only</header>');
	$infos.append($readOnly);
	
	
	if((typeof data.upload !== 'undefined' && data.upload) && (typeof data.fileIdForNewVersion === 'undefined')){

		// keywords
		$keywords =  $('<input type="text" class="tag" id="tags"/>');
		$infos.append('<header>Schlüsselwörter</header>');
		$infos.append($keywords);

		
		// Signieren von Dateien
		$signer =  $('<input type="text"/>');
		$infos.append('<header>Signierer</header>');
		$infos.append($signer);
		
		$infos.append('<header>Backup Level</header>');
		var $backUpLevel = $('<select class="selectbox"></select>');
		$backUpLevel.append('<option>1</option>');
		$backUpLevel.append('<option>2</option>');
		$backUpLevel.append('<option>3</option>');
		$infos.append($backUpLevel);
		
		var $upload =  $('<button class="dataBtn">Jetzt Hochladen</button>');
		$upload.click(function(){
			var formData = new FormData();
			if(typeof editor !== 'undefined'){
				formData.append('file', new Blob([editor.getValue()], {type: data.type}));
			}else{
				formData.append('file', data.file);				
			}
			formData.append('name', $name.val());
			formData.append('owner', $owner.val());
			formData.append('location', currentDir.location);
			formData.append('readOnly', $readOnly.is(':checked'));
			formData.append('backupLevel', $backUpLevel.val());
			formData.append('keywords', $keywords.val());
			formData.append('signer', $signer.val());
			
			globalAjax('upload', {data: formData, contentType: false, processData: false,}).success(function(response){
				if(typeof response.success !== 'undefined') $infoHolder.remove();
				globalAjax('/dir', {data: {dirLocation : currentDir.location}}).success(showDirs);
			});
		});
		$actions.append($upload);
	}else{
		
		if(typeof data.fileIdForNewVersion !== 'undefined') data.fileId = data.fileIdForNewVersion;
		
		// Select a Version
		if(typeof data.verAmount !== 'undefined' && data.verAmount > 1){
			var $selectVersion = $('<select></select>');
			
			for(var i = data.verAmount; i > 0; i--){
				if(i === data.version){
					$selectVersion.append('<option selected="selected">' + i + '</option>');
				}else{
					$selectVersion.append('<option>' + i + '</option>');
				}

			}
			$selectVersion.change(function(){
				$('#preview').remove(); // Is not needed longer so remove it to save RAM-space 
				globalAjax('/file', {data:{dirLocation: data.location, version: $(this).val() }}).success(showPreview);
			});
			$infos.append('<header>Version Auswählen</header>');
			$infos.append($selectVersion);
		}
		
		// Compare with previus Versions
		if(data.type.match('text') && data.version > 1){
			var $selectChange = $('<select></select>');
			for(var i = $selectVersion.val(); i > 0; i--){
				$selectChange.append('<option>' + i + '</option>');
			}
			$selectChange.change(function(){
				
				if($selectVersion.val() !== $selectChange.val()){
					globalAjax('/file', {data:{dirLocation: data.location, version: $(this).val() }}).success(function(response){
						$preview.html('<div id="changes">' + diffString(response.content, data.content)+ '</div>');
						updateSize()
					});
				}else{
					showPreview(data);	
				}
			});
			$infos.append('<header>Vergleiche mit</header>');
			$infos.append($selectChange);
		}
		
		// Upload new Version 
		$inputFile = $('<input style="display:none" type="file" />');
		$inputFile.change(handleChangeNewVersion);
		$actions.append($inputFile);
		$uploadNewVersion =  $('<button  class="dataBtn">Neue Version</button>');
		$uploadNewVersion.click(function(){
			$inputFile.data('fileId', data.fileId);
			$inputFile.data('parentName', data.parentName);
			$inputFile.data('parent', data.parent);
			$inputFile.click();
		});
		$actions.append($uploadNewVersion);
		
		
		/ Change Saving Place 
		var $location = $('<input type="text" class="textinput" />');
		$location.val(data.parentName);
		$location.data('location', data.parent);
		$location.click(function(){
			chosePathDialog(function(dir){
				$location.val(dir[0].name);
				$location.data('location', dir[0].location);
			},{alowedTypes: ['dir'], startDir: $location.data('location')});
		});
		$infos.append('<header>Ordner</header>');
		$infos.append($location);
		
		 //Download action
		var $download =  $('<form action="/data/download" method="post"></form>');
		var $downloadLocationInput = $('<input type="hidden" name="location"/>');
		$downloadLocationInput.val(data.location);
		$download.append($downloadLocationInput);
		$download.append($('<button class="btn" type="submit">Herunterladen</button>'));
		$actions.append($download);
		
		 //Signieren von Dateien 
		$infos.append('<header>Signierer</header>');
		var $signer =  $('<form action="/data/signer" method="post"></form>');
		var $signerLocationInput = $('<input type="hidden" name="location"/>');
		$signerLocationInput.val(data.location);
		$signer.append($signerLocationInput);
		$signer.append($('<input type="text">'));
		$signer.submit(function(e){
			e.preventDefault();
			globalAjax('/data/signer', $signer.serialize());
		});
		$infos.append($signer);
		
		// Checken von Dateien 
		var $checked =  $('<form action="/data/checked" method="post"></form>');
		var $checkedLocationInput = $('<input type="hidden" name="location"/>');
		$checkedLocationInput.val(data.location);
		$checked.append($checkedLocationInput);
		$checked.append($('<button class="btn">Datai aus/ein Checken</button>'));
		$checked.submit(function(e){
			e.preventDefault();
			globalAjax('/data/checked', $checked.serialize());
		});
		$infos.append($checked);
				
		//Save Changes 
		var $change =  $('<button  class="dataBtn">Änderungen Speichern</button>');
		$change.click(function(){
			var formData = new FormData();
			formData.append('id', data.fileId);
			formData.append('name', $name.val());
			formData.append('location', $location.data('location'));
			formData.append('owner', $owner.val());
			formData.append('readOnly', $readOnly.is(':checked'));
			if(typeof editor !== 'undefined'){
				formData.append('file', new Blob([editor.getValue()], {type: data.type}));
			}else if(typeof data.file !== 'undefined'){
				formData.append('file', data.file);
			}
			globalAjax('/data/edit', {data: formData, contentType: false, processData: false,}).success(function(){
				globalAjax('/dir', {data: {dirLocation : currentDir.location}}).success(showDirs);
			});
		});
		$actions.append($change);
		
		// Download action 
		var $download =  $('<form action="/data/download" method="post"></form>');
		var $downloadLocationInput = $('<input type="hidden" name="location"/>');
		$downloadLocationInput.val(data.location);
		$download.append($downloadLocationInput);
		$download.append($('<button  class="dataBtn clear" type="submit">Herunterladen</button>'));
		$actions.append($download);
	}
	
	
	var $close = $('<span class="delete-16" id="closeInformations"></span>');
	$close.click(function(){$infoHolder.remove();});
	

	$infoHolder.append($informations);
	$infoHolder.append($preview);

	
	$informations.append($close);
	$informations.append($infos);
	$informations.append($actions);
	$('body').append($infoHolder);
	$('#tags').tagsInput({width:'auto'});
	
	updateSize();
	
	
	if(typeof data.content !== 'undefined' && data.content !== null){
		var editor = generateEditor($preview, data.content, getTypeAndPreviewFromName(data.name));
	}
}
*/


/*
$(window).resize(updateSize);
function updateSize(){
	
	$('#preview, #changes').css('width', ($( window ).width() - 520) + 'px');
	$('#preview, #changes').css('height', ($( window ).height() - 110) + 'px');
	
	$('#editor').css('height', ($( window ).height() - 120) + 'px');
}
*/



//function html2rtf(content) {
//	//strong
//	content = content.replace(/<strong>/g, "{\\b ");
//	content = content.replace(/<\/strong>/g, "}");
//	
//	//italic
//	content = content.replace(/<em>/g, "{\\i ");
//	content = content.replace(/<\/em>/g, "}");
//	
//	//underline
//	content = content.replace(/<u>/g, "{\\u ");
//	content = content.replace(/<\/u>/g, "}");
//	
//	//paragraph
//	content = content.replace(/<p>/g, "{\\par ");
//	content = content.replace(/<\/p>/g, "");
//	
//	//linebreak
//	content = content.replace(/<br \/>/g, "{\\line ");
//	
//	//non-break space
//	content = content.replace(/\&nbsp;/g, "");
//	
//	//fontsize and fonttype
//	content = content.replace(/<span style="font-size:([0-9]*)px">/g, "\\fs$1 ");
//	content = content.replace(/<\/span>/g, "//fs24 ");
//	
//	return content;
//}


function generateEditor(content, typeInfo){
	var $holder = null;
	
	if(typeInfo.preview === 'ace'){
		var holder = document.createElement('pre');
		holder.id = 'editor';
		holder.setAttribute('name', 'editor');
		
		var contentNode = document.createTextNode(content);
		holder.appendChild(contentNode);
		
		var editor = ace.edit(holder);
		if(typeof typeInfo.setMode !== 'undefined'){
			editor.session.setMode('ace/mode/' + typeInfo.setMode);
		}
		$holder = $(holder);
		$holder.typeInfo = typeInfo;
		$holder.extend(editor);
	}else if(typeInfo.preview === 'cke'){
		var $holder = $('<textarea id="editor" name="editor" style="width:100%;height:100%"></textarea>');
		$holder.val(content);
		
		$holder.ckeditor({
			height: ($holder.height()-139),
			resize_enabled: false,
		});
		var editor = CKEDITOR.instances['editor'];
		editor.getValue = editor.getData;
		$holder.typeInfo = typeInfo;
		$holder.extend(editor);
	}else if(typeInfo.preview === 'object'){
		var $holder = $('<object data="" style="width:100%;height:100%"></object>');
		$holder.typeInfo = typeInfo;
		$holder.attr('data', content);
	}else if(typeInfo.preview === 'image'){
		var $holder = $('<img style="max-width:100%;max-height:100%;"/> ');
		$holder.typeInfo = typeInfo;
		$holder.attr('src', content);
	}
	return $holder;
}


function showPopup(){
	var _eventHandler;
	var _file;
	var $_editor;
	var $_infoHolder = $('<div id="infoHolder"></div>');
	
	var $_preview = $('<div id="preview"></div>');
	var $_informations = $('<div id="informations"></div>');
	
	var $_infos = $('<div></div>');
	var $_actions = $('<div></div>');
	var $_close = $('<span class="delete-16" id="closeInformations"></span>');
	
	var _fireEvent = function(event){
		if(typeof _eventHandler !== 'undefined' && typeof _eventHandler[event] !== 'undefined'){
			for(var i = 0; i < _eventHandler[event].length; i++){
				_eventHandler[event][i]();
			}
		}
	}
	
	this.addEventHandler = function(event, userFunction){
		if(typeof _eventHandler === 'undefined'){
			_eventHandler = {};
		}
		if(typeof _eventHandler[event] === 'undefined'){
			_eventHandler[event] = 	[];		
		}
		_eventHandler[event][_eventHandler[event].length] = userFunction;
	}
	
	this.addForm = function(action, method){
		action = action || '';
		method = method || 'POST';
		
		var $form = $('<form></form>');
		$form.attr('action', action);
		$form.attr('method', method);
		$_actions.append($form);
		
		$form.addInput = function(inputOptions, label){
			if(typeof label === 'string'){
				var $label = $('<header />');
				$label.text(label);
				$form.append($label);
			}
			
			var $input = $('<input />', inputOptions);
			$input.addClass('textinput');
			$form.append($input);
			return this;
		}
		
		$form.addSelect = function(inputOptions, options, label){
			if(typeof label === 'string'){
				var $label = $('<header />');
				$label.text(label);
				$form.append($label);
			}
			
			var $select = $('<select />', inputOptions);
			for(var i = 0; i < options.length; i++){
				$select.append('<option>' + options[i] + '</option>');				
			}

			$form.append($select);
			return this;
		}
		
		return $form;
	}
	
	this.addElement = function(element){
		$_actions.append(element);
	}
	
	this.setPreview = function(editor){
		$_editor = editor;
		$preview.empty().append($_editor);
		
	}
	
	this.getFile = function(){
		if(typeof $_editor.getValue === 'function'){
			console.log($_editor.typeInfo.type);
			return new Blob([$_editor.getValue()], {type: $_editor.typeInfo.type});
		}else if(typeof _file !== 'undefined'){
			return _file;
		}else{
			return null;
		}
	}
	
	this.setFile = function File(file, name){
		if(typeof file === 'string'){
			var fileInfo = getTypeAndPreviewFromName(name);
			$_editor = generateEditor(file, fileInfo);
			$_preview.empty().append($_editor);
		}else{
			_file = file;
			if(file.size < 1024 * 1024 * 0.2){ /* 0.2MB */
				var fileInfo = getTypeAndPreviewFromName(file.name);
				var reader = new FileReader();
				reader.onload = function(){
					$_editor = generateEditor(reader.result, fileInfo);
					$_preview.empty().append($_editor);
				};
				if(file.type.match('text')){
					reader.readAsText(file);
				}else if(file.type.match('pdf') || file.type.match('image')){
					reader.readAsDataURL(file);
				}
			}
		}
	}
	
	this.remove = function(){
		$('#infoHolder').remove();
		_fireEvent('close');
	}
	
	/* constructor */
	$('#infoHolder').remove();
	
	$_close.click(function(){_fireEvent('close'); $_infoHolder.remove();});

	$_informations.append($_close);
	$_informations.append($_infos);
	$_informations.append($_actions);
	
	$_infoHolder.append($_informations);
	$_infoHolder.append($_preview);
	
	$('body').append($_infoHolder);
	
	$(window).resize(function(){
		$_preview.css('width', ($( window ).width() - 520) + 'px')
				.css('height', ($( window ).height() - 110) + 'px');
	});
	$(window).resize();
}


function openFileUploadDialog(file, name, dir){
	name = name || file.name;
	dir = dir || {location: 'dir://', name: 'Basisordner'};
	var popup = new showPopup();
		popup.setFile(file, name);
		popup.addForm()
			.addInput({name: 'name', value: name}, 'Dateiname')
			.addInput({name: 'owner', list: 'users', value: currentUser}, 'Besitzer')
			.addSelect({name: 'backupLevel', value: name}, [1, 2, 3], 'Backup Level')
			.addInput({name: 'keywords', id: 'tags'}, 'Schlüsselwörter')
			.addInput({name: 'signer', list: 'users'}, 'Signierer')
			.addInput({name: 'location', value: dir.location}, 'Ordner')
			.addInput({type: 'submit', value: 'Hochladen'})
			.submit(function(e){
				e.preventDefault();
				var formData = new FormData(this);
				formData.append('file', popup.getFile());
				globalAjax('/data/upload', {data: formData, contentType: false, processData: false}).success(function(r){
					if(typeof r.success === 'string'){
						popup.remove();
						popup = null;
					}
				});
			});
	$('#tags').tagsInput({width:'auto'});
	return popup;
}

function openFileEditDialog(data){
	var popup = new showPopup();
		popup.setFile(data.content, data.name);
		popup.addForm()
			.addInput({name: 'name', value: data.name}, 'Dateiname')
			.addInput({name: 'owner', list: 'users', value: data.owner}, 'Besitzer')
			.addInput({name: 'signer', list: 'users'}, 'Signierer')
			.addInput({name: 'readOnly', type:'checkbox'}, 'Read Only')
			.addInput({name: 'location', value: data.parent}, 'Ordner')
			.addInput({name: 'id', value: data.fileId, type: 'hidden'})
			.addInput({type: 'submit', value: 'Änderungen speichern'})
			.submit(function(e){
				e.preventDefault();
				var formData = new FormData(this);
				formData.append('file', popup.getFile());
				globalAjax('/data/edit', {data: formData, contentType: false, processData: false});
			});
		popup.addForm()
			.addInput({type: 'hidden', name: 'location', value: data.location})
			.addInput({type: 'submit', value: 'Datai aus/ein Checken'})
			.submit(function(e){
				e.preventDefault();
				var formData = new FormData(this);
				globalAjax('/data/checked', {data: formData, contentType: false, processData: false});
			});
		popup.addElement($('<a href="/data/download?location=' + encodeURIComponent(data.location) + '">Download</a>'));
	return popup;
}