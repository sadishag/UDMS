<?php

/** MemberController.php
 * @package    Admin\Controller
 * @copyright  Copyright (c) 2013 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Roland Starke <roland.starke@unister.de>
 *
 */
namespace Admin\Controller;

use Zend\View\Model\JsonModel;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
/**
 * ermöglicht es temporäre nutzer zur gruppe hinzuzufügen
 */
class MemberController extends AbstractActionController
{

    /**
     * @return ViewModel gibt ein ViewModel mit gruppenname und gruppenid zurück
     */
    public function indexAction()
    {
        $groupId = (int) $this->params()->fromRoute('groupid', 0);

        /**
         * TODO lerne Coding-Standard
         */
        $em = $this->getServiceLocator()->get('ObjectManager');
        $group = $em->getRepository('RoleAPI\Entity\GroupName')->find($groupId);
        if ($group === null) {
            // gruppe existiert nicht auf eine error seite leiten??
            return false;
        }

        return new ViewModel(array('groupname' => $group->getName(), 'groupid' => $groupId));
    }

    /**
     * @return JsonModel gibt ein array mit allen temporären mitgliedern einer gruppe zurück
     */
    public function showAction()
    {
        $groupId = (int) $this->params()->fromRoute('groupid', 0);

        $em = $this->getServiceLocator()->get('ObjectManager');
        /*
        $query = $em->createQuery(
            'SELECT CONCAT(u.FirstName, \' \', u.LastName) AS name, g.Id AS id
             FROM RoleAPI\Entity\Groups g
             JOIN RoleAPI\Entity\User u
             WHERE u.UserId = g.UserId AND g.Temp = 1 AND g.GroupId = ' .
         $groupId);
         $returnArray['members'] = $query->getResult();
         */

        $returnArray = array();
        $members = $em->getRepository('RoleAPI\Entity\Groups')->findBy(array('GroupId' => $groupId, 'Temp' => 1));
        $userRepo = $em->getRepository('RoleAPI\Entity\User');
        foreach ($members as $member) {
            $user = $userRepo->find($member->getUserId());
            $returnArray['members'][] = array(
                'name' => $user->getFirstName() . ' ' . $user->getLastName(),
                'id' => $member->getId()
            );
        }


        return new JsonModel($returnArray);
    }

    /**
     * macht ein temporäres mitglied zu einen vollen mitglied
     * @return JsonModel gibt das mitglied mit leeren namen zurück
     */
    public function addAction()
    {

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $em = $this->getServiceLocator()->get('ObjectManager');
            $member = $em->getRepository('RoleAPI\Entity\Groups')->find($post['id']);


            if ($member !== null && $member->getTemp() == '1') {
                $member->setTemp(0);
                $em->persist($member);
                $em->flush();

                $returnArray['members'][] = array('id' => $post['id'], 'name' => '');
                $returnArray['success'] = 'Anfrage zugestimmt';
            } else {
                $returnArray['error'] = 'Anfrage existiert nicht';
            }
        } else {
            $returnArray['error'] = 'Ungültiger Reguest';
        }
        return new JsonModel($returnArray);
    }

    /**
     * löscht einen temporären Nutzer aus einer gruppe
     * @return JsonModel gibt das rausgewurfene mitglied mit leeren namen zurück
     */
    public function deleteAction()
    {

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $em = $this->getServiceLocator()->get('ObjectManager');
            $member = $em->getRepository('RoleAPI\Entity\Groups')->find($post['id']);

            if ($member !== null && $member->getTemp() == '1') {
                $em->remove($member);
                $em->flush();

                $returnArray['members'][] = array('id' => $post['id'], 'name' => '');
                $returnArray['success'] = 'Anfrage abgelehnt';
            } else {
                $returnArray['error'] = 'Anfrage existiert nicht';
            }
        } else {
            $returnArray['error'] = 'Ungültiger Reguest';
        }
        return new JsonModel($returnArray);
    }

}
