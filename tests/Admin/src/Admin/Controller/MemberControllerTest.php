<?php
use Admin\form\CrudRights;

use Doctrine\ORM\Query\Parameter;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;

use Admin\Controller\MemberController;

class MemberControllerTest extends PHPUnit_Framework_TestCase
{
    protected $_controller;

    protected $_serviceLocatorMock;

    protected $_mvcEvent;

    public function __construct()
    {
        // mock Entity BranchName
        $entityBranchName = $this->getMock('RoleAPI\Entity\BranchName', array('getBranchId'));
        $entityBranchName->expects($this->any())
            ->method('getBranchId')
            ->will($this->returnValue(1));

        // mock Repository BranchName
        $repositoryBranchName = $this->getMock('RepositoryBranchName', array('findBy', 'findAll', 'findOneBy'));
        $repositoryBranchName->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($entityBranchName));
        $repositoryBranchName->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($entityBranchName)));
        $repositoryBranchName->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($entityBranchName)));

        // mock Entity Groups
        $entityGroups = $this->getMock('RoleAPI\Entity\Groups');

        // mock Repository Groups
        $repositoryGroups = $this->getMock('RepositoryGroups', array('findBy', 'findAll', 'find'));
        $repositoryGroups->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue($entityGroups));
        $repositoryGroups->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($entityGroups)));
        $repositoryGroups->expects($this->any())
            ->method('find')
            ->will($this->returnValue($entityGroups));

        // mock Entity GroupName
        $entityGroupName = $this->getMock('RoleAPI\Entity\GroupName');

        // mock Repository GroupName
        $repositoryGroupName = $this->getMock('RepositoryGroupName',
        array('findBy', 'findAll', 'findOneBy', 'find'));
        $repositoryGroupName->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue(null)); // inportant
        $repositoryGroupName->expects($this->any())
            ->method('find')
            ->will($this->returnValue($entityGroupName)); // inportant
        $repositoryGroupName->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($entityGroupName)));
        $repositoryGroupName->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($entityGroupName)));

        // mock Query
        $queryMock = $this->getMock('Query', array('getResult'));
        $queryMock->expects($this->any())
            ->method('getResult')
            ->will($this->returnValue(1));

        // mock EntityManager
        $entityManagerGetRepositoryMap = array(array('RoleAPI\Entity\GroupName', $repositoryGroupName),

        array('RoleAPI\Entity\Groups', $repositoryGroups),
        array('RoleAPI\Entity\BranchName', $repositoryBranchName));
        $entityManagerMock = $this->getMock('ObjectManager',
        array('getRepository', 'persist', 'flush', 'createQuery', 'remove'));
        $entityManagerMock->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($entityManagerGetRepositoryMap));
        $entityManagerMock->expects($this->any())
            ->method('createQuery')
            ->will($this->returnValue($queryMock));

        // mock RoleAPI
        $roleAPIMock = $this->getMock('RoleAPI', array('isAllowed'));
        $roleAPIMock->expects($this->any())
            ->method('isAllowed')
            ->will($this->returnValue(true));

        // mock ServiceLocator
        $serviceLocatorGetMap = array(array('RoleAPI', true, $roleAPIMock),
        array('ObjectManager', true, $entityManagerMock));
        $serviceLocatorMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $serviceLocatorMock->expects($this->any())
            ->method('get')
            ->will($this->returnValueMap($serviceLocatorGetMap));

        $this->_serviceLocatorMock = $serviceLocatorMock;

        // mock route
        $mvcEvent = new MvcEvent();
        $routeMach = new RouteMatch(array('groupid' => '1'));
        $mvcEvent->setRouteMatch($routeMach);
        $this->_mvcEvent = $mvcEvent;
    }

    private function _getServiceManager()
    {
        // mock Entity BranchName
        $entityBranchName = $this->getMock('RoleAPI\Entity\BranchName', array('getBranchId'));
        $entityBranchName->expects($this->any())
            ->method('getBranchId')
            ->will($this->returnValue(1));

        // mock Repository BranchName
        $repositoryBranchName = $this->getMock('RepositoryBranchName', array('findBy', 'findAll', 'findOneBy'));
        $repositoryBranchName->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($entityBranchName));
        $repositoryBranchName->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($entityBranchName)));
        $repositoryBranchName->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($entityBranchName)));

        // mock Entity Groups
        $entityGroups = $this->getMock('RoleAPI\Entity\Groups');

        // mock Repository Groups
        $repositoryGroups = $this->getMock('RepositoryGroups', array('findBy', 'findAll', 'find'));
        $repositoryGroups->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue($entityGroups));
        $repositoryGroups->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($entityGroups)));
        $repositoryGroups->expects($this->any())
            ->method('find')
            ->will($this->returnValue(null));

        // mock Entity GroupName
        $entityGroupName = $this->getMock('RoleAPI\Entity\GroupName');

        // mock Repository GroupName
        $repositoryGroupName = $this->getMock('RepositoryGroupName',
        array('findBy', 'findAll', 'findOneBy', 'find'));
        $repositoryGroupName->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue(null)); // inportant
        $repositoryGroupName->expects($this->any())
            ->method('find')
            ->will($this->returnValue(null)); // inportant
        $repositoryGroupName->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($entityGroupName)));
        $repositoryGroupName->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($entityGroupName)));

        // mock Query
        $queryMock = $this->getMock('Query', array('getResult'));
        $queryMock->expects($this->any())
            ->method('getResult')
            ->will($this->returnValue(1));

        // mock EntityManager
        $entityManagerGetRepositoryMap = array(array('RoleAPI\Entity\GroupName', $repositoryGroupName),

        array('RoleAPI\Entity\Groups', $repositoryGroups),
        array('RoleAPI\Entity\BranchName', $repositoryBranchName));
        $entityManagerMock = $this->getMock('EntityManager',
        array('getRepository', 'persist', 'flush', 'createQuery', 'remove'));
        $entityManagerMock->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($entityManagerGetRepositoryMap));
        $entityManagerMock->expects($this->any())
            ->method('createQuery')
            ->will($this->returnValue($queryMock));

        // mock RoleAPI
        $roleAPIMock = $this->getMock('RoleAPI', array('isAllowed'));
        $roleAPIMock->expects($this->any())
            ->method('isAllowed')
            ->will($this->returnValue(true));

        // mock ServiceLocator
        $serviceLocatorGetMap = array(array('RoleAPI', true, $roleAPIMock),
        array('ObjectManager', true, $entityManagerMock));
        $serviceLocatorMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $serviceLocatorMock->expects($this->any())
            ->method('get')
            ->will($this->returnValueMap($serviceLocatorGetMap));

        return $serviceLocatorMock;

    }

    public function setUp() // construct
    {
        $_SESSION['Userid'] = 1;
        $this->_controller = new MemberController();
        $this->_controller->setEvent($this->_mvcEvent);
    }

    public function testIndexActionWithLogin()
    {
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->indexAction();
    }

    public function testshowActionWithLogin()
    {
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->showAction();
    }

    public function testaddActionWithLoginWithoutPost()
    {
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->addAction();
    }

    public function testaddActionWithLoginWithPost()
    {
        $this->_controller->getRequest()
            ->setMethod('POST')
            ->setPost(new Parameters(array('id' => '5')));
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->addAction();
    }

    public function testdeleteActionWithLoginWithoutPost()
    {
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->deleteAction();
    }

    public function testdeleteActionWithLoginWithPost()
    {
        $this->_controller->getRequest()
            ->setMethod('POST')
            ->setPost(new Parameters(array('id' => '5')));
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->deleteAction();
    }

    public function testallActionsWithWrongGroupId()
    {
        $this->_controller->getRequest()
            ->setMethod('POST')
            ->setPost(new Parameters(array('id' => '5')));
        $this->_controller->setServiceLocator($this->_getServiceManager());

        $this->_controller->indexAction();
        $this->_controller->addAction();
        $this->_controller->deleteAction();
    }

    public function tearDown() // destruct
    {

    }
}