<?php
namespace User\Controller;

use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use User\Controller\TempController;
use Zend\Stdlib\Parameters;

class TempControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {

        $this->_controller = new TempController();

    }

    public function tearDown()
    {

    }
    public function testtemploginAction()
    {
        $_SESSION['TempUserid']=1;
        $post = array('codecheck'=>'5','tempchange2'=>'1','tempchange1'=>'2','tempchange3'=>'3','newHash'=>sha1('a'.'b'));
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId','getTemp','getEmail','getLogin'));
        $albumMock->expects($this->any())
        ->method('getTemp')
        ->will($this->returnValue(3));
        $albumMock->expects($this->any())
        ->method('getEmail')
        ->will($this->returnValue('a'));
        $albumMock->expects($this->any())
        ->method('getLogin')
        ->will($this->returnValue('b'));


        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays','isallowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));


        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));


        $this->_controller->setServiceLocator($smMock);
        $this->_controller->temploginAction();
    }
    public function testtemploginActioncodefalsch()
    {
        $_SESSION['TempUserid']=1;
        $post = array('codecheck'=>'5','tempchange2'=>'1','tempchange1'=>'2','tempchange3'=>'3','newHash'=>sha1('aa'.'b'));
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId','getTemp','getEmail','getLogin'));
        $albumMock->expects($this->any())
        ->method('getTemp')
        ->will($this->returnValue(3));
        $albumMock->expects($this->any())
        ->method('getEmail')
        ->will($this->returnValue('a'));
        $albumMock->expects($this->any())
        ->method('getLogin')
        ->will($this->returnValue('b'));


        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays','isallowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));


        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));


        $this->_controller->setServiceLocator($smMock);
        $this->_controller->temploginAction();
    }
    public function testtemploginActiontempfalsch()
    {
        $_SESSION['TempUserid']=1;
        $post = array('codecheck'=>'5','tempchange2'=>'1','tempchange1'=>'2','tempchange3'=>'3','newHash'=>sha1('aa'.'b'));
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId','getTemp','getEmail','getLogin'));
        $albumMock->expects($this->any())
        ->method('getTemp')
        ->will($this->returnValue(2));
        $albumMock->expects($this->any())
        ->method('getEmail')
        ->will($this->returnValue('a'));
        $albumMock->expects($this->any())
        ->method('getLogin')
        ->will($this->returnValue('b'));


        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays','isallowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));


        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $mvcEvent = new MvcEvent();
        $mvcEvent->setResponse(new Response());
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->temploginAction();
    }
}