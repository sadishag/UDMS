<h1><strong>Metadatentyp erstellen</strong></h1>
<a class="btn-primary" href="../metadata">Zurück zur Übersicht </a>
<div>
<?php  //echo '<pre>  '; print_r($this->success); echo '</pre>'; echo "</br>";?>
<section>
<form action="" method="post" id="createtyp" class=""><br/>
    <table>
      <tr>
          <th>Typname: </th>
          <th> <input type="text" name="typeName" value="<?php echo $this->typeName?>" placeholder="Typname" required="required"></th>
      <tr>
          <td>Mediatyp: </td>
          <td><input type="text" name="mediaType" value="<?php echo $this->mediaType?>" placeholder="Mediatyp" required="required"></td>
      <tr>
          <td>Subtyp:</td>
          <td><input type="text" name="subType" value="<?php echo $this->subType?>" placeholder="Subtyp" required="required"></td>
      <tr>
          <td>Maximale Länge:</td>
          <td><input type="text" name="maxLength" value="<?php echo $this->maxLength?>" placeholder="Maximale Länge"  pattern="[0-9]*"></td>
      <tr>
          <td>Maximale Größe: </td>
          <td><input type="text" name="maxSize" value="<?php echo $this->maxSize?>" placeholder="Maximale Größe"  pattern="[0-9]*"></td>
      <tr>
          <td>Interactive:</td>
          <td>
              <select name="interactive">
                  <option value="<?php if (isset($this->interactive)) echo $this->interactive; else echo '0';?>" selected="selected"><?php if ($this->interactive == 0) echo 'Nein'; else echo 'Ja';?> </option>
                  <option value="<?php if (isset($this->interactive)) echo abs($this->interactive -1); else echo '1';?>"><?php if ($this->interactive == 0) echo 'Ja'; else echo 'Nein';?> </option>
              </select>
          </td>
      <tr>
          <td>Speicherpfad: </td>
          <td><input type="text" name="resourceUrl" value="<?php echo $this->resourceUrl;?>" placeholder="Speicherpfad" required="required"></td>
      <tfoot>
          <tr>
              <td> <input class="btn-default" type="reset"> </td>
              <td> <input class="btn-primary" type="submit" value="Speichern" name="newMetaTypeSave" id="saveNewMetaType"></td>
          </tr>
      </tfoot>
    </table>
</form>
</section>
</div>

<div id="slider">
    <div>
        <?php
            include 'help.html';
        ?>
    </div>
    <p id="sliderButton"><span>Hilfe!</span></p>
</div>

<script type="text/javascript">
function ResetCheck () {
  var chk = window.confirm("Wollen Sie alle Eingaben löschen?");
  return (chk);
}
</script>

<script>
<?php
if(isset($this->error)){
    ?>
    globalNotification({error: '<?php echo $this->error ?>'});
    <?php
}
?>
</script>
<script>
<?php
if(isset($this->success)){
    ?>
    globalNotification({success: '<?php echo $this->success ?>'});
    <?php
}
?>
</script>

