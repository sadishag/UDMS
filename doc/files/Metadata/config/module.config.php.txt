<?php
return array(

        'controllers' => array(
            'invokables' => array(
                'Metadata\Controller\Metadata' => 'Metadata\Controller\MetadataController',
                'Metadata\Controller\Create' => 'Metadata\Controller\CreateController',
            ),
        ),
'router' => array(
    'routes' => array(
        'metadata' => array(
            'type'    => 'segment',
                'options' => array(
                    'route'    => '/metadata[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Metadata\Controller\Metadata',
                        'action'     => 'show',
                    ),
                ),
            ),
        ),
        'createnewmeta' => array(
            'type'    => 'segment',
            'options' => array(
                'route'    => '/metadata/create',
                'constraints' => array(
                    'defaults' => array(
                    		'controller' => 'Metadata\Controller\Create',
                    		'action'     => 'create',
                    ),
                )
            )
        ),
    ),
        'view_manager' => array(
            'template_path_stack' => array(
                __DIR__ . '/../view',
            ),
            'strategies' => array('ViewJsonStrategy'),
       ),
);

