<?php
namespace User\Controller;
/**
 * SearchController.php
 *
 * @package User\Controller
 * @copyright Copyright (c) 2013 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Dennis Krüger <dennis.krueger@unister.de>
 */
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * Controller for view\User\search
 *
 * @copyright Copyright (c) 2013 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Dennis Krüger <dennis.krueger@unister.de>
 *
 */
class SearchController extends AbstractActionController
{

    public function searchresultsAction()
    {
        $postvar = $this->getRequest()->getPost();
        $results = array();

        if (isset($postvar['search'])) {
            $em = $this->getServiceLocator()->get('ObjectManager');
            if ($postvar['searchtype'] === 'keywords') {
                $userRepo = $em->getRepository('RoleAPI\Entity\User');

                $searchString = str_replace(',', ' ', $postvar['search']);
                $searchArray = explode(' ', $searchString);

                foreach ($searchArray as $search) {
                    $queryBuilder = $em->createQueryBuilder('MetaAPI');
                    if ($queryBuilder) {
                        $queryBuilder->select('u')
                            ->from('MetaAPI\Entity\File', 'u')
                            ->where('u.KeyWords LIKE :search OR u.Name LIKE :search')
                            ->setParameter('search', '%' . $search . '%');

                        $results = array_merge($results, $queryBuilder->getQuery()->getResult());
                    }
                }
            } else if ($postvar['searchtype'] === 'volltext') {
                    $rsm = new ResultSetMapping();
                    $rsm->addEntityResult('MetaAPI\Entity\File', 'u');
                    $rsm->addFieldResult('u', 'FileId', 'FileId');
                    $rsm->addFieldResult('u', 'Name', 'Name');
                    $rsm->addFieldResult('u', 'VerAmount', 'VerAmount');
                    $rsm->addFieldResult('u', 'Type', 'Type');
                    $rsm->addFieldResult('u', 'Branch', 'Branch');
                    $rsm->addFieldResult('u', 'Author', 'Author');
                    $rsm->addFieldResult('u', 'Backup', 'Backup');
                    $rsm->addFieldResult('u', 'Vision', 'Vision');
                    $rsm->addFieldResult('u', 'ReadOnly', 'ReadOnly');
                    $rsm->addFieldResult('u', 'KeyWords', 'KeyWords');

                    $sql = 'SELECT * FROM udms_container.FileVersion LEFT JOIN udms_meta.File USING(FileId) WHERE MATCH (Search) AGAINST(?) AND VerAmount = VerNr';
                    $nativeQuery = $em->createNativeQuery($sql, $rsm, 'MetaAPI');
                    $nativeQuery->setParameter(1, $postvar['search']);
                    $results = $nativeQuery->getResult();
                }

        }

        $result = array_unique($results);
        return new ViewModel(array('r' => $result));
    }
}