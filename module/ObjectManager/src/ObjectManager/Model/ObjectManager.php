<?php
/**
 * ObjectManager.php Main Class for the ObjectManager
 *
 * @package    ObjectManager\Model
 * @copyright  Copyright (c) 2013 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Lucas Mikulla <lucas.mikulla@unister.de>
 */

namespace ObjectManager\Model;

/**
 * Main Class of the ObjectManager
 *
 * @package ObjectManager\Model
 * @copyright Copyright (c) 2013 Unister GmbH
 */
class ObjectManager
{
    /**
     * instance of the ServiceLocator
     *
     * @var ServiceLocator
     */
    protected $_serviceManager;

    /**
     * instance of the EntityManager
     *
     * @var Doctrine\ORM\EntityManager
     */
    protected $_entityManagers;

    /**
     * all mysql EntityManager
     *
     * @var Doctrine\ORM\EntityManager
     */
    protected $_mysqlManager;

    /**
     * array of namespaces of entities
     *
     * @var unknown_type
     */
    protected $_entityNamespaces;

    /**
     * Constructor for setting up entityManagers and their Namespaces
     *
     * @param ServiceLocator $sm
     */
    public function __construct($sm)
    {
        $this->_serviceManager = $sm;
        $conf = include (getcwd() . '/Services/StorageEngineConf.php');
        $length = count($conf);
        for ($i = 1; $i <= $length; $i++) {
            $managerConf = $conf[$i];
            if ($managerConf['type'] === 'doctrine') {

                $em = $this->_serviceManager->get($managerConf['name']);
                $em->type = 'doctrine';
                $this->_entityManagers[] = $em;
                $this->_entityNamespaces[] = $managerConf['namespace'];
                if ($managerConf['namespace'] === 'DataAPI') {
                    $this->_mysqlManager[] = $em;
                }
            } elseif ($managerConf['type'] === 'sqli') {
                $em = $this->_serviceManager->get($managerConf['name']);
                $em->type = 'sqli';
                $this->_entityManagers[] = $em;
                $this->_entityNamespaces[] = $managerConf['namespace'];
            } elseif ($managerConf['type'] === 'FileManager') {
                $em = new \ObjectManager\Model\EntityManager($managerConf['name'], $this->_serviceManager);
                $em->type = 'FileManager';
                $this->_entityManagers[] = $em;
                $this->_entityNamespaces[] = $managerConf['namespace'];
            }
        }

    }

    /**
     * function for handling getRepository requests for specified entity
     *
     * @param string $entityName
     *            Namespace of the required Repository
     * @return Repository $repository the returned Repository fitting the
     *         EntityName from the correct EntityManager
     */
    public function getRepository($entityName)
    {
        $entityNameSpace = explode('\\', $entityName);
        $entityNameSpace = $entityNameSpace[0];
        foreach ($this->_entityManagers as $key => $em) {
            try {
                if ($this->_entityNamespaces[$key] == $entityNameSpace) {
                    $repository = $em->getRepository($entityName);

                    // $repository->find(0);
                    // if ($repository)
                    break;
                }
            } catch (\Exception $e) {}
        }
        return $repository;
    }

    /**
     * function for handling persist requests of entityManagers, also handles
     * redundant persists
     *
     * @param unknown_type $entity
     *            the entity that is to be persisted
     * @param unknown_type $redLevel
     *            the redundancy level for that entity (higher -> more
     *            redundant)
     */
    public function persist($entity, $redLevel = 0)
    {
        $entityNameSpace = explode('\\', get_class($entity));
        $entityNameSpace = $entityNameSpace[0];
        if ($entityNameSpace === 'DataAPI') {
            foreach ($this->_mysqlManager as $em) {
                try {
                    $em->persist($entity);
                } catch (\Exception $e) {

                }
            }
        }
        if ($redLevel <= 0) {
            if ($entityNameSpace !== 'DataAPI') {
                foreach ($this->_entityManagers as $key => $em) {
                    try {

                        if ($this->_entityNamespaces[$key] == $entityNameSpace) {
                            $em->persist($entity);

                            break;
                        }

                    } catch (\Exception $e) {}
                }
            }
        } else {
            $redLevel--;
            foreach ($this->_entityManagers as $key => $em) {
                $tempEntity = $entity;
                try {
                    if ($em->type !== 'doctrine') {
                        if ($this->_entityNamespaces[$key] == $entityNameSpace) {
                            if ($tempEntity->getId()) {
                                $preEntity = $em->getRepository(get_class($tempEntity))->findOneBy(
                                array('FileId' => $tempEntity->getFileId()));
                                if ($preEntity) {
                                    $preEntity->setDataArray($tempEntity->getDataArray());
                                    $em->persist($preEntity);
                                }
                            } else {
                                $em->persist($tempEntity);
                            }

                            if ($redLevel === 0) {
                                break;
                            } else {
                                $redLevel--;
                            }
                        }
                    }
                } catch (\Exception $e) {}
            }
        }
    }

    /**
     * calls flush on all entityManagers
     */
    public function flush()
    {
        foreach ($this->_entityManagers as $em) {
            try {
                $em->flush();
            } catch (\Exception $e) {
            }
        }
    }

    /**
     * removes a specified entity from all EntityManagers of fitting Namespace
     *
     * @param unknown_type $entity
     *            the entity to be removed
     */
    public function remove($entity)
    {
        /**
         *
         * @todo Fix remove function to consider redundancy of entities in
         *       udms_container
         */
        $entityNameSpace = explode('\\', get_class($entity));
        $entityNameSpace = $entityNameSpace[0];
        foreach ($this->_entityManagers as $key => $em) {
            try {
                if ($this->_entityNamespaces[$key] == $entityNameSpace) {
                    $em->remove($entity);
                }
            } catch (\Exception $e) {

            }
        }
    }

    /**
     * function to generate the QueryBuilder for the specified namespace
     *
     * @param string $namespace
     *            the namespace for the Queries defaults to MetaAPI
     * @return mixed $queryBuilder returns the QueryBuilder or false if there
     *         was no working one found
     */
    public function createQueryBuilder($namespace = 'MetaAPI')
    {
        $queryBuilder = false;
        foreach ($this->_entityManagers as $key => $em) {
            try {
                if ($this->_entityNamespaces[$key] == $namespace) {
                    $queryBuilder = $em->createQueryBuilder();
                    break;
                }
            } catch (\Exception $e) {
                $queryBuilder = false;
            }
        }
        return $queryBuilder;
    }

    /**
     * function to generate a native Query for the specified namespace
     *
     * @param string $sql
     *            the Query to be generated
     * @param unknown_type $rsm
     *            the resultmapping
     * @param string $namespace
     *            the namespace for the query
     */
    public function createNativeQuery($sql, $rsm, $namespace = 'MetaAPI')
    {
        $query = false;
        foreach ($this->_entityManagers as $key => $em) {
            try {
                if ($this->_entityNamespaces[$key] == $namespace) {
                    $query = $em->createNativeQuery($sql, $rsm);
                    break;
                }
            } catch (\Exception $e) {
                $query = false;
            }
        }
        return $query;
    }
}