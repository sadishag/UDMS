<?php
/**
 * AuthController.php
 *
 * @package    Auth\Controller
 * @copyright  Copyright (c) 201 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Thomas Kröning <thomas.kroening@unister.de>
 */
namespace Auth\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Auth\Entity\User;
use Model\LoginFilter;

use Doctrine\DBAL\Schema\View;
use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\EntityManager;
/**
 * Authentication-Klasse
 *
 * Diese Klasse überprüft die eingegebenen Logindaten und
 * Loggt den User wieder aus
 *
 * @package Auth/Controller
 * @copyright Copyright (c) 2013 Unister GmbH
 */
class AuthController extends AbstractActionController
{

    /**
     * Überprüft die Logindaten und setzte die Userid
     * für die anderen Module
     *
     * @return JsonModel -> (error/success)
     */
    public function loginAction()
    {
        unset($_SESSION['Userid']);

        $request = $this->getRequest();
        if ($request->isPost()) {

            $postvar = $this->getRequest()->getPost();
            $em = $this->getServiceLocator()->get('ObjectManager');
            $login = $em->getRepository('RoleAPI\Entity\User')->findOneBy(array('Login' => $postvar['username']));

            if ($login !== null) {
                if ($login->getPassword() === $postvar['password']) {
                    $temp = $login->getTemp();
                    if($temp === 3){
                        $_SESSION['TempUserid'] = $login->getUserid();
                        $responseArray['warning'] = 'Bestätigung per E-Mail und Admin fehlt!';

                    } else if($temp === 2){
                        $_SESSION['TempUserid'] = $login->getUserid();
                        $responseArray['information'] = 'Bestätigung vom Admin fehlt!';

                    } else if($temp === 1) {
                        $_SESSION['TempUserid'] = $login->getUserid();
                        $responseArray['warning'] = 'E-Mail wurde nicht bestätigt!';
                    } else if($temp === 0){
                        $_SESSION['Userid'] = $login->getUserid();
                        $responseArray['success'] = 'Erfolgreich';
                    }

                } else {
                    $responseArray['error'] = 'Logindaten falsch';
                }
            } else {
                $responseArray['error'] = 'Logindaten falsch';
            }

            return new JsonModel($responseArray);
        }

return array();
    }

    /**
     * Löcht die Userid des eingelogten ind der Session
     * und Routet ihn zurück zur Startseite
     *
     * @return redirect->toRoute
     */
    public function logoutAction()
    {
        /*
         * Löscht nach drücken des Logout Button die Session und Routet zurück
         * zur Loginseite
         *
         *
         */
        unset($_SESSION['Userid']);
        return $this->redirect()->toUrl('/');

    }
}