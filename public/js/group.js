Group = {};
Group.data = [];
Group.updateAndDisplayData = function(groups){
	//for each group
	for(var i = 0; groups.length > i; i++){
		var group = groups[i];
		//create a row
		var $row = $('<tr></tr>');
		$row.data('id', group.id);
		$row.data('name', group.name);
		
		//create a td with a name and a link to the group
		var $name = $('<td></td>');
		
		var $link = $('<a></a>');
		$link.text(group.name);
		$link.attr('href', group.id +'/member/');
		$name.append($link);
		
		//mark groups with temp users
		if(typeof group.temp !== 'undefined' && group.temp > 0){
			$name.append('<span class="circle-16" title="Änderungsanfragen">' + group.temp + '</span>');
		}
		//create td for actions
		var $actions = $('<td></td>');
		//create span for rename
		var $rename = $('<span class="edit-16" title="Umbenennen"></span>');
		//add event listener
		$rename.click(function(){
			//get td $name
			var $element = $(this).parent().prev();
			
			//if the input fild already exists
			if($element.children('input').length > 0){
				return false;
			}
			var id = $element.parent().data('id');
			var name = $element.parent().data('name');
			//create a input fild with value of group name
			var $input = $('<input style="width:100%" />');
			$input.val(name);
			//add event listener
			$input.blur(function(){
				var $input = $(this);
				var newName = $input.val();
				var id = $input.parent().parent().data('id');
				var currName = $input.parent().parent().data('name');
				if(currName === newName){
					Group.updateAndDisplayData([{'id': id, 'name': currName}]);
				}else{
					Group.rename(id, newName);					
				}

			});
			//fill $name td with input fild
			$element.html('');
			$element.append($input);
			
		});
		//create span for delete
		var $delete = $('<span class="delete-16" title="Löschen"></span>');
		//add event listener
		$delete.click(function(){
			//open a confirm dialog
			if(confirm('Gruppe ' + $(this).parent().parent().data('name') + ' löschen?')){
				Group.delete($(this).parent().parent().data('id'));				
			}
		});

		
		//add rename span to action td
		$actions.append($rename);
		//add delete span to action td
		$actions.append($delete);
		
		//if the group has a name add tds to $row else there will be an empty row
		if(group.name.length > 0){
			$row.append($name);
			$row.append($actions);			
		}

		//if the group isn't in the table add a row
		if(typeof(Group.data[group.id]) === 'undefined'){
			$('#groupsTableBody').prepend($row);
		}else{//else update the row
			Group.data[group.id].replaceWith($row);
		}
		
		//save rows
		Group.data[group.id] = $row;

	}
};

Group.handleResponse = function(response){
	
	if(typeof(response.groups) !== 'undefined' && response.groups.length > 0){
		//emptying input filds
		$('#addname').val('');
		$('#adddir').val('');
		//create table if don't exist
		if($('#groupsTable').length === 0){
			$('#content').append('<table id="groupsTable" class="clear"><thead><tr id="groupsTableHeader"><th>Gruppenname</th><th>Aktionen</th></tr></thead><tbody id="groupsTableBody"></tbody></table>');
			//$('#groupsTable').append('')
		}
		
		Group.updateAndDisplayData(response.groups);
		$('#groupsTable').dataTable()
	}
};
/**
 * Creates or updates a table(id=groupsTable) with all groups
 */
Group.show = function(){
	globalAjax('show').success(Group.handleResponse);
};
/**
 * Renames a Group and update table
 *
 * @param int groupid GroupId of Group
 * @param string newName new name of Group
 *
 */
Group.rename = function(groupid, newName){
	globalAjax('rename', {type: 'POST', data:{id: groupid, name: newName}}).success(Group.handleResponse);
}
/**
 * creates a new Group and updates table
 *
 * @param string name name of Group
 * @param string branchLocation a Location from a BranchName
 */
Group.add = function(name, branchLocation){
	globalAjax('add', {type: 'POST', data:{name: name, location: branchLocation}}).success(Group.handleResponse);
}
/**
 * deletes a group and all references from GroupName, Groups and Roles
 * and uptates table
 *
 * @param int $id A GroupId from GroupName
 */
Group.delete = function(id){
	globalAjax('delete', {type: 'POST', data:{id:  id}}).success(Group.handleResponse);
}





//events
$('#add').click(function(){
	if($('#addname').val().length > 0){
		Group.add($('#addname').val(), $('#adddir').data('location'));
	}
});
//autoload groups
Group.show();


$('#adddir').click(function(){
	//open dialog if there is no dialog open already
	if($('.dirs').length === 0){
		chosePathDialog(function(dirs){
			$('#adddir').val(dirs[0].name);
			$('#adddir').data('location', dirs[0].location);
		},{
			multiple: false
		});
	}
});



