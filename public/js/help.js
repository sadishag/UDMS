function help(path) {
    var helpFile;
    switch(path) {
        case '/admin/crud':
            helpFile = 'adminCrud.html';
            break;
        case '/admin/group/':
            helpFile = 'adminGroup.html';
            break;    
        case '/admin/role':
            helpFile = 'adminRole.html';
            break;    
        case '/admin/tempuser':
        	helpFile = 'adminTempuser.html';
        	break;
        case '/admin/verwaltung':
            helpFile = 'adminVerwaltung.html';
            break;
        case '/admin/removedata':
            helpFile = 'adminRemovedata.html';
            break;  
        case '/groupchange':
            helpFile = 'groupchange.html';
            break;    
        case '/user/create':
        	helpFile = 'userCreate.html';
        	break;
        case '/metadata':
        	helpFile = 'metadata.html';
        	break;
        case '/notice':
        	helpFile = 'notice.html';
        	break;
        case '/notice/vision/2':
        	helpFile = 'notice.html';
        	break;
        case '/data/':
        	helpFile = 'data.html';
        	break;	
	case '/user/data':
        	helpFile = 'userData.html';
        	break;	
    }
    $('#slider div').html('<object id="helpObject" data="/src/help/' + helpFile + '"></object>');
    return helpFile;
}

function about() {
	$('#myAlert').html('<span id="close">X</span><object id="aboutObject" data="/src/about/about.html"></object>');
}

