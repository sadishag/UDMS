<?php
/**
 * NoticeActionValidator.php
 *
 * @package    Notice\Service
 * @copyright  Copyright (c) 2013 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Sebastian Fritze <sebastian.fritze@unister.de>
 */

namespace Notice\Service;

use Zend\ServiceManager\ServiceLocatorAwareInterface;

class NoticeActionValidator
{
    /**
     * sets the service manager
     *
     * @param $sm instance of ServiceManager
     */
    public function __construct($sm)
    {
        $this->_serviceManager = $sm;
    }

    /**
     * validates if the user is eligible to perform an action on the given notice
     *
     * @param mixed $userId
     * @param mixed $noticeId
     * @param string $action one of the CRUD Rights
     * @return boolean
     */
    public function validate($userId, $noticeId, $action)
    {
        $return = false;

        $em = $this->_serviceManager->get('ObjectManager');

        $note = $em->getRepository('MetaAPI\Entity\File')
            ->findOneBy(array('FileId' => $noticeId));
        if ($_SESSION['Userid'] === $note->getAuthor()) {
            $return = true;
        }
        if ('text/notice' === $note->getType()) {
            if (is_resource($note->getVision())) {
                $stringContent = stream_get_contents($note->getVision());
                $vision = unserialize(base64_decode($stringContent));
            } else {
                $vision = array('user' => array(), 'group' => array(), 'role' => array());
            }

            //User
            if (in_array($userId, array_keys($vision['user']))) {
                $role = $em->getRepository('RoleAPI\Entity\RoleName')
                    ->findOneBy(array('RoleId' => $vision['user'][$userId]));
                $stringContent = stream_get_contents($role->getrightKey());
                $rightKey = unserialize(base64_decode($stringContent));
                if ($rightKey[$action] === true) {
                    $return = true;
                }
            }

            //group
            $groups = $em->getRepository('RoleAPI\Entity\Groups')
                ->findBy(array('UserId' => $userId));

            foreach ($groups as $group) {
                $groupId = $group->getGroupId();

                if (in_array($groupId, array_keys($vision['group']))) {
                    $role = $em->getRepository('RoleAPI\Entity\RoleName')
                        ->findOneBy(array('RoleId' => $vision['group'][$groupId]));
                    $stringContent = stream_get_contents($role->getrightKey());
                    $rightKey = unserialize(base64_decode($stringContent));
                    if ($rightKey[$action] === true) {
                        $return = true;
                    }
                }
            }

            //Role
            $roles = $em->getRepository('RoleAPI\Entity\Roles')
                ->findBy(array('UserId' => $userId, 'GroupId' => 0));

            foreach ($roles as $role) {
                $roleId = $role->getRoleId();

                if (in_array($roleId, array_keys($vision['role']))) {
                    $role = $em->getRepository('RoleAPI\Entity\RoleName')
                        ->findOneBy(array('RoleId' => $vision['role'][$roleId]));
                    $stringContent = stream_get_contents($role->getrightKey());
                    $rightKey = unserialize(base64_decode($stringContent));
                    if ($rightKey[$action] === true) {
                        $return = true;
                    }
                }
            }
        } else {
            $return = true;
        }
        return $return;
    }
}