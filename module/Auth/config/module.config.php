<?php


return array(

        'controllers' => array(
                'invokables' => array(
                    'Auth\Controller\Auth' => 'Auth\Controller\AuthController',
                    'Application\Controller\Index' => 'Application\Controller\IndexController',
                              ),
                            ),
// The following section is new and should be added to your file
'router' => array(
    'routes' => array(
        'auth' => array(
            'type'    => 'segment',
                'options' => array(
                    'route'    => '/',
                        'constraints' => array(
                            'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            'id'     => '[0-9]+',
                                            ),
                            'defaults' => array(
                            'controller' => 'Auth\Controller\Auth',
                            'action'     => 'login',
                                            ),

                                    ),
                            ),
            'Auth' => array(
                    'type'    => 'Literal',
                    'options' => array(
                            'route'    => '/auth',
                            'defaults' => array(
                                    '__NAMESPACE__' => 'Auth\Controller',
                                    'controller'    => 'Auth',
                                    'action'        => 'login',
                            ),
                    ),
                    'may_terminate' => true,
                    'child_routes' => array(
                            'default' => array(
                                    'type'    => 'Segment',
                                    'options' => array(
                                            'route'    => '/[:controller[/:action]]',
                                            'constraints' => array(
                                                    'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                    'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                            ),
                                            'defaults' => array(
                                            ),
                                    ),
                            ),
                    ),
            ),
                        ),
                ),



        'view_manager' => array(
            'template_path_stack' => array(
                'album' => __DIR__ . '/../view',
                                ),
                       'strategies' => array(
                        'ViewJsonStrategy'
                        )
                                            ),

);
