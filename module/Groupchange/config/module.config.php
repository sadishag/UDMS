<?php
return array(
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    	'strategies' => array(
    		'ViewJsonStrategy'
    	)
    ),
    'router' => array(
        'routes' => array(
            'groupchange' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/groupchange',
                    'defaults' => array(
                        'controller' => 'Groupchange\Controller\Index',
                        'action' => 'index',


                    ))))),


    'controllers' => array(
        'invokables' => array(
            'Groupchange\Controller\Index' => 'Groupchange\Controller\IndexController',
        )
    ),
);
