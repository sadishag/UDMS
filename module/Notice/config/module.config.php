<?php


return array(

        'controllers' => array(
            'invokables' => array(
                'Notice\Controller\Notice' => 'Notice\Controller\NoticeController',
                'Notice\Controller\Noticeupdate' => 'Notice\Controller\NoticeupdateController',
                'Application\Controller\Index' => 'Application\Controller\IndexController',
                'Notice\Controller\Vision' => 'Notice\Controller\VisionController',
            ),
        ),
// The following section is new and should be added to your file
'router' => array(
    'routes' => array(
        'notice' => array(
            'type'    => 'segment',
                'options' => array(
                    'route'    => '/notice[/:action][/:noticeid]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Notice\Controller\Noticeupdate',
                        'action'     => 'show',
                    ),
                ),
            ),
        ),
        'create' => array(
            'type'    => 'segment',
            'options' => array(
                'route'    => '/notice/create',
                'constraints' => array(
                    'defaults' => array(
                    'controller' => 'Notice\Controller\Notice',
                    'action'     => 'create',
                    ),
                )
            )
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array('ViewJsonStrategy'),
   ),
);
