<?php

namespace Data\Controller;

use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Data\Model\SignerModel;

class SignerController extends AbstractActionController
{

    public function showAction()
    {
        $signierer = new SignerModel();
        $signierer->setServiveLocator($this->getServiceLocator());
        $signer = $signierer->showSignFile();
        return new ViewModel(array('signer' => $signer));
    }

    public function addAction()
    {
        $postvar = $this->getRequest()->getPost();

        $signierer = new SignerModel();
        $signierer->setServiveLocator($this->getServiceLocator());

        try {
            $signierer->add($postvar['signer'], $postvar['fileId']);
            $try['success'] = 'Erfolgreich geändert';
        } catch (\Exception $e) {
            $try['error'] = 'Fehlgeschlagen';
        }
        return new JsonModel($try);
    }

    public function signAcceptAction()
    {
        $postvar = $this->getRequest()->getPost();

        $signierer = new SignerModel();
        $signierer->setServiveLocator($this->getServiceLocator());

        try {
            $signierer->signAccept($postvar['id'], $postvar['key'], $postvar['comment']);
            $try['success'] = 'Erfolgreich geändert';
        } catch (\Exception $e) {
            $try['error'] = 'Fehlgeschlagen';
        }
        return new JsonModel($try);
    }

    public function signRejectAction()
    {
        $postvar = $this->getRequest()->getPost();

        $signierer = new SignerModel();
        $signierer->setServiveLocator($this->getServiceLocator());

        try {
            $signierer->signReject($postvar['id'], $postvar['key'], $postvar['comment']);
            $try['success'] = 'Erfolgreich geändert';
        } catch (\Exception $e) {
            $try['error'] = 'Fehlgeschlagen';
        }
        return new JsonModel($try);
    }
}