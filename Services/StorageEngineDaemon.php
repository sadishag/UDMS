<?php

if ($pid = pcntl_fork()) {
    return;
}
// ob_end_clean();

// fclose(STDIN);
// fclose(STDOUT);
// fclose(STDERR);

if (posix_setsid() < 0) {
    return;
}

if ($pid = pcntl_fork()) {
    echo $pid;
    return;
}

$daemon = new StorageEngineDaemon();
$daemon->setup();
$daemon->run();

class StorageEngineDaemon
{
    private $_conf;
    private $_localDefaultConf = '<?php
return array(
  \'doctrine\' => array(
    \'connection\' => array(
      \'defname\' => array(
        \'driverClass\' => \'Doctrine\\DBAL\\Driver\\PDOMySql\\Driver\',
        \'params\' => array(
          \'host\'     => \'defaddres\',
          \'port\'     => \'defport\',
          \'user\'     => \'defuser\',
          \'password\' => \'defpassword\',
          \'dbname\'   => \'udms_container\',
          \'driverOptions\' => array(
              1002 => \'SET NAMES utf8\'
          ),
        ),
      ),
    ),
    \'entitymanager\' => array(
            \'defname\' => array(
                \'connection\' => \'defname\',
                \'configuration\' => \'udms_container\'
            ),
         ),
    \'authentication\' => array(
        \'defname\' => array(
                \'objectManager\' => \'doctrine.entitymanager.defname\'
            ),
         ),
                 \'eventmanager\' => array(
            \'defname\' => array(),
        ),
        \'sql_logger_collector\' => array(
            \'defname\' => array(),
        ),
        \'entity_resolver\' => array(
            \'defname\' => array(),
        ),
  ),
  \'service_manager\' => array(
        \'factories\' => array(
            \'doctrine.authenticationadapter.defname\' => new DoctrineModule\Service\Authentication\AdapterFactory(\'defname\'),
            \'doctrine.authenticationstorage.defname\' => new DoctrineModule\Service\Authentication\StorageFactory(\'defname\'),
            \'doctrine.authenticationservice.defname\' => new DoctrineModule\Service\Authentication\AuthenticationServiceFactory(\'defname\'),
            \'doctrine.connection.defname\' => new DoctrineORMModule\Service\DBALConnectionFactory(\'defname\'),
            \'doctrine.configuration.defname\' => new DoctrineORMModule\Service\ConfigurationFactory(\'defname\'),
            \'doctrine.entitymanager.defname\' => new DoctrineORMModule\Service\EntityManagerFactory(\'defname\'),
            \'doctrine.driver.defname\' => new DoctrineModule\Service\DriverFactory(\'defname\'),
            \'doctrine.eventmanager.defname\' => new DoctrineModule\Service\EventManagerFactory(\'defname\'),
            \'doctrine.entity_resolver.defname\' => new DoctrineORMModule\Service\EntityResolverFactory(\'defname\'),
            \'doctrine.sql_logger_collector.defname\' => new DoctrineORMModule\Service\SQLLoggerCollectorFactory(\'defname\'),
            \'doctrine.mapping_collector.defname\' => function (Zend\ServiceManager\ServiceLocatorInterface $sl)
            {
                $em = $sl->get(\'doctrine.entitymanager.defname\');

                return new DoctrineORMModule\Collector\MappingCollector($em->getMetadataFactory(), \'defname_mappings\');
            },
        ),
    ),
);';

    public function setup()
    {
        chdir(dirname(__DIR__));
        $this->getConf();
        $this->updateConf('lastpid', getmypid());
    }

    public function run()
    {
        while (true) {
            sleep($this->_conf['sleeptime']);
            if (!$this->_conf['suppressrunninglog']) {
                $this->appendLog('running daemon');
            }
            if ($this->_conf['shutdown'] > 0) {
                $this->updateConf('shutdown', 0);
                $this->appendLog('shutting down');
                break;
            }
            try {
                $this->getConf();
                if ($this->_conf['addnewstorage']) {

                    $settings = include ('NewStorageEngines.php');
                    if (isset($settings['name']) && isset($settings['namespace']) && isset($settings['user']) &&
                     isset($settings['address']) && isset($settings['port']) && isset($settings['password'])) {
                        $this->addNewStorageEngine($settings);
                    } else {
                        $this->appenLog('missing Value for new StorageEngine in config');
                    }
                    $this->updateConf('addnewstorage', 0);
                }
                //balance storage engines here
            } catch (\Exception $e) {
                $this->appendLog($e->__toString());
            }
        }
    }

    public function getConf()
    {
        $this->_conf = include 'ServiceConf.php';
        return $this->_conf;
    }

    public function updateConf($key, $value, $conf = 'Services/ServiceConf.php')
    {
        $content = file(getcwd() . '/' . $conf);
        $fileStream = fopen(getcwd() . '/' . $conf, 'w+');
        foreach ($content as $line) {
            $writeLine = $line;
            if (strpos($line, "'" . $key . "'")) {
                $temp = explode('=>', $line);
                $writeLine = implode('=>', array($temp[0], ' ' . $value . ',' . PHP_EOL));
            }
            fwrite($fileStream, $writeLine);
        }
        fclose($fileStream);
        $this->appendLog('updating Conf ' . $conf . ' setting ' . $key . ' to value ' . $value);
        $this->getConf();
    }

    public function swapStorageEnginePriority($firstPriority, $secondPriority)
    {
        $content = file(getcwd() . '/Services/' . 'StorageEngineConf.php');
        $fileStream = fopen(getcwd() . '/Services/' . 'StorageEngineConf.php', 'w+');
        foreach ($content as $line) {
            $line = str_replace("'" . $firstPriority . "'", "'123412341234'", $line);
            $line = str_replace("'" . $secondPriority . "'", "'" . $firstPriority . "'", $line);
            $line = str_replace("'123412341234'", "'" . $secondPriority . "'", $line);
            fwrite($fileStream, $line);
        }
        $this->appendLog('swapped EnginePriority : swapping ' . $firstPriority . ' with ' . $secondPriority);
        fclose($fileStream);
    }

    public function createConfFileFromArrayStructure($array, $targetFilePath)
    {
        $fileStream = fopen($targetFilePath, 'w+');
        fwrite($fileStream, '<?php ' . PHP_EOL . 'return' . PHP_EOL);
        $dump = print_r($array, true);
        $dump = str_replace('[', "'", $dump);
        $dump = str_replace(']', "'", $dump);
        $dump = str_replace('=> ', "=> '", $dump);
        $dump = str_replace(PHP_EOL, ',' . PHP_EOL, $dump);
        $dump = str_replace('Array,', 'array', $dump);
        $dump = str_replace(PHP_EOL . ',' . PHP_EOL, PHP_EOL, $dump);
        $dump = str_replace('array,', 'array', $dump);
        $dump = str_replace('(,', '(', $dump);
        $dump = str_replace(',' . PHP_EOL, "'," . PHP_EOL, $dump);
        $dump = str_replace(")',", '),', $dump);
        $dump = str_replace("'array", 'array', $dump);
        $splinters = explode(',', $dump);
        $i = 0;
        $count = count($splinters);
        $final = '';
        foreach ($splinters as $splinter) {
            $i++;
            if ($i === $count) {
                $final = implode(';', array($final, $splinter));
            } elseif ($i === 1) {
                $final = $splinter;
            } else {
                $final = implode(',', array($final, $splinter));
            }
        }
        fwrite($fileStream, $final);
        $this->appendLog('creating conf file from array structure in ' . $targetFilePath);
        fclose($fileStream);
    }

    public function addNewStorageEngine($settings)
    {
        $storageEnginePath = './Services/StorageEngineConf.php';
        $localConfPath = './config/autoload/' . $settings['name'] . '.local.php';
        $storageEngineConf = include ($storageEnginePath);
        $nextNum = 1 + count($storageEngineConf);
        $storageEngineConf[$nextNum] = array('name' => 'doctrine.entitymanager.' . $settings['name'],
        'type' => 'doctrine', 'namespace' => $settings['namespace']);
        $localConfString = str_replace('defname', $settings['name'], $this->_localDefaultConf);
        $localConfString = str_replace('defuser', $settings['user'], $localConfString);
        $localConfString = str_replace('defaddres', $settings['address'], $localConfString);
        $localConfString = str_replace('defpassword', $settings['password'], $localConfString);
        $localConfString = str_replace('defport', $settings['port'], $localConfString);
        file_put_contents($localConfPath, $localConfString);
        $this->appendLog('installing new StorageEngine ' . $settings['name']);
        $this->createConfFileFromArrayStructure($storageEngineConf, $storageEnginePath);
    }

    public function appendLog($msg)
    {
        $logStream = fopen($this->_conf['logpath'], 'a');
        if (!$logStream) {
            $logStream = fopen(getcwd() . '/elogDaemon.log', 'a');
        }
        fwrite($logStream, date('d.m.y -- G:i:s => ', time()));
        fwrite($logStream, $msg);
        fwrite($logStream, PHP_EOL);
    }
}