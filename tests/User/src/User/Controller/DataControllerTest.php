<?php

namespace User\Controller;

use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Admin\Controller\RoleController;
use Zend\Stdlib\Parameters;

class DataControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {

        $this->_controller = new DataController();

    }

    public function tearDown()
    {

    }
    public function testindexAction()
    {
        $_SESSION['Userid']=1;

        $albumMock = $this->getMock('RoleAPI\Entity\User');

        $repo = $this->getMock('Repository', array('find'));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }

    public function providerTestChangeAction()
    {
        $email = $this->getMock('RoleAPI\Entity\User');

        return array(
            array(array('password' => 'test', 'password2' => 'test'), $email),
            array(array('password' => 'test', 'password2' => 'test'), ''),
            array(array('password' => 'test', 'password2' => 'test2'), ''),
            array(array('style' => 'test'), '')
        );
    }

    /**
     * @dataProvider providerTestChangeAction
     */
    public function testChangeAction($post, $email)
    {
        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $_SESSION['Userid']=1;

        $userMock = $this->getMock('RoleAPI\Entity\User');

        $repo = $this->getMock('Repository', array('findOneBy', 'find'));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($userMock));
         $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($email));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->changeAction();
    }
}