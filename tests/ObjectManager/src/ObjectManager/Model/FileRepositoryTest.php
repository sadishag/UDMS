<?php
namespace ObjectManager\Model;

use Zend\Http\Response;

use ObjectManager\Model\FileRepository;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;

class FileRepositoryTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;
    protected $_controller1;
    protected $_testdatapath;

    public function setUp()
    {
        $this->_testdatapath = __DIR__ . '/';
        $testfile = fopen($this->_testdatapath . 'DataAPI\Entity\FileVersion.txt', 'w');
        $data = serialize(
        $dataarray = array('Id' => '1', 'FileId' => '2', 'VerNr' => '3', 'Time' => '4', 'Editor' => '5',
        'Owner' => '6', 'Content' => '7')) . PHP_EOL . serialize('blabla');
        file_put_contents($this->_testdatapath . 'DataAPI\Entity\FileVersion.txt', $data);
        fclose($testfile);
    }

    public function tearDown()
    {
        try {
            unlink(__DIR__ . '/DataAPI\Entity\FileVersion.txt');
        } catch (\Exception $e) {

        }
    }

    public function testfind()
    {

        $id = 1;
        $this->_controller = new FileRepository('DataAPI\Entity\FileVersion', $this->_testdatapath);
        $this->_controller->find($id);

    }

    public function testfindby()
    {

        $id = 1;
        $this->_controller = new FileRepository('DataAPI\Entity\FileVersion', $this->_testdatapath);
        $this->_controller->findby(array('Id' => '1', 'FileId' => '2'));

    }

    public function testfindall()
    {

        $id = 1;
        $this->_controller = new FileRepository('DataAPI\Entity\FileVersion', $this->_testdatapath);
        $this->_controller->findall();

    }

    public function testfindoneby()
    {

        $id = 1;
        $this->_controller = new FileRepository('DataAPI\Entity\FileVersion', $this->_testdatapath);
        $this->_controller->findoneby(array('Id' => '1', 'FileId' => '2'));

    }

    public function testfindnot()
    {

        $id = 2;
        $this->_controller = new FileRepository('DataAPI\Entity\FileVersion', $this->_testdatapath);
        $this->_controller->find($id);

    }

    public function testfindonebynotfound()
    {

        $id = 1;
        $this->_controller = new FileRepository('DataAPI\Entity\FileVersion', $this->_testdatapath);
        $this->_controller->findoneby(array('Id' => '2', 'FileId' => '2'));

    }

    public function testfindbynotfound()
    {

        $id = 1;
        $this->_controller = new FileRepository('DataAPI\Entity\FileVersion', $this->_testdatapath);
        $this->_controller->findby(array('Id' => '2', 'FileId' => '2'));

    }

    public function testfindempty()
    {
        $f = fopen(__DIR__ . '/DataAPI\Entity\FileVersion.txt', 'w');
        fclose($f);
        $id = 2;
        $this->_controller = new FileRepository('DataAPI\Entity\FileVersion', $this->_testdatapath);
        $this->_controller->find($id);

    }

    public function testfindonebyempty()
    {
        $f = fopen(__DIR__ . '/DataAPI\Entity\FileVersion.txt', 'w');
        fclose($f);
        $id = 1;
        $this->_controller = new FileRepository('DataAPI\Entity\FileVersion', $this->_testdatapath);
        $this->_controller->findoneby(array('Id' => '2', 'FileId' => '2'));

    }

    public function testfindbyempty()
    {
        $f = fopen(__DIR__ . '/DataAPI\Entity\FileVersion.txt', 'w');
        fclose($f);
        $id = 1;
        $this->_controller = new FileRepository('DataAPI\Entity\FileVersion', $this->_testdatapath);
        $this->_controller->findby(array('Id' => '2', 'FileId' => '2'));

    }

    public function testfindAllempty()
    {
        $f = fopen(__DIR__ . '/DataAPI\Entity\FileVersion.txt', 'w');
        fclose($f);
        $id = 1;
        $this->_controller = new FileRepository('DataAPI\Entity\FileVersion', $this->_testdatapath);
        $this->_controller->findAll();

    }
}