<?php
namespace User\Controller;

use Zend\Stdlib\Parameters;

class SearchControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {
        $this->_controller = new SearchController();
    }

    public function provideTestSearchresultsAction()
    {
        return array(
            array(array('searchtype' => 'keywords', 'search' => 'test')),
            array(array('searchtype' => 'volltext', 'search' => 'test'))
        );
    }

    /**
     * @dataProvider provideTestSearchresultsAction
     */
    public function testSearchresultsAction($post)
    {
        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $whereMock = $this->getMock('Query', array('setParameter', 'getResult'));
        $whereMock->expects($this->any())
            ->method('getResult')
            ->will($this->returnValue(array()));

        $fromMock = $this->getMock('Query', array('where'));
        $fromMock->expects($this->any())
            ->method('where')
            ->will($this->returnValue($whereMock));

        $selectMock = $this->getMock('Query', array('from'));
        $selectMock->expects($this->any())
            ->method('from')
            ->will($this->returnValue($fromMock));

        $query = $this->getMockBuilder('Doctrine\ORM\Querybuilder')
            ->disableOriginalConstructor()
            ->setMethods(array('setParameter', 'select', 'getQuery', 'getResult', 'getFileId'))
            ->getMock();
        $query->expects($this->any())
            ->method('select')
            ->will($this->returnValue($selectMock));
        $query->expects($this->any())
            ->method('getQuery')
            ->will($this->returnValue($whereMock));
        $query->expects($this->any())
            ->method('getResult')
            ->will($this->returnValue(array($query)));

        $repo = $this->getMock('Repository', array('find'));

        $em = $this->getMock('Doctrine\ORM\Entitymanager', array('createNativeQuery', 'getRepository', 'createQueryBuilder'));
        $em->expects($this->any())
            ->method('createNativeQuery')
            ->will($this->returnValue($query));
        $em->expects($this->any())
            ->method('createQueryBuilder')
            ->will($this->returnValue($query));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->searchresultsAction();
    }
}