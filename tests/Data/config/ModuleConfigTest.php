<?php
namespace Data;
class ModuleConfigTest extends \PHPUnit_Framework_TestCase
{

    public function setUp()
    {

    }

    public function testModuleConfig()
    {
        $mockmock = $this->getMock('Doctrine\Common\Persistence\Mapping\ClassMetadataFactory');
        $mock = $this->getMock('Doctrine\ORM\Entitymanager', array('getMetaDataFactory'));
        $mock->expects($this->any())
            ->method('getMetaDataFactory')
            ->will($this->returnValue($mockmock));

        $test = include __DIR__ . '/../../../module/Data/config/module.config.php';
        $sm = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface', array('get', 'has'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($mock));
    }

    public function tearDown()
    {

    }
}