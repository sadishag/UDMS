<?php
namespace Data\Controller;

use Zend\Stdlib\Parameters;

function time()
{
    return '1';
}

class DataControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {
        $this->_controller = new DataController();
    }

    public function tearDown()
    {}

    public function testShowAction()
    {
        $_SESSION['Userid'] = 1;

        $repo = $this->getMock('Repository', array('findAll', 'find'));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->showAction();
    }

    public function provideTestEditAction()
    {
        return array(
        array(array('name' => 'mock.txt', 'location' => 'test', 'owner' => 1, 'readOnly' => 'true'), 1, 0, true),
        array(array('name' => 'mock.txt', 'location' => 'test', 'owner' => 1, 'readOnly' => 'false'), 2, 0, true),
        array(array(), 1, 0, false), array(array(), 1, 1, false));
    }

    /**
     * @dataProvider provideTestEditAction
     */
    public function testEditAction($post, $owner, $readOnly, $upload)
    {
        if(!file_exists(getcwd() . '/data/mock.txt'))
        fopen(getcwd() . '/data/mock.txt', 'w');
        chmod(getcwd() . '/data/mock.txt', 0777);
        fopen(getcwd() . '/data/readOnly/' . time() . 'mock.txt', 'w');
        chmod(getcwd() . '/data/readOnly/' . time() . 'mock.txt', 0777);
        $functionMock = $this->getMock('Data\Controller\DataController',
        array('isUpload', 'newLocation', 'validate'));
        $functionMock->expects($this->any())
            ->method('isUpload')
            ->will($this->returnValue($upload));

        $content = 'test';
        $stream = fopen(getcwd() . '/data/mock.txt', 'w+');
        fwrite($stream, base64_encode(serialize($content)));
        rewind($stream);

        file_put_contents(getcwd() . '/data/readOnly/' . time() . 'mock.txt', "bla");

        $file = array('file' => array('tmp_name' => getcwd() . '/data/mock.txt'));

        $functionMock->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post))
            ->setFiles(new Parameters($file));

        $_SESSION['Userid'] = 1;

        $fileMock = $this->getMock('MetaAPI\Entity\File', array('getReadOnly'));
        $fileMock->expects($this->any())
            ->method('getReadOnly')
            ->will($this->returnValue($readOnly));

        $fileVerMock = $this->getMock('DataAPI\Entity\FileVersion',
        array('getOwner', 'getAuthor', 'getType', 'setName', 'getLocation', 'setLocation', 'getContent'));
        $fileVerMock->expects($this->any())
            ->method('getOwner')
            ->will($this->returnValue($owner));
        $fileVerMock->expects($this->any())
            ->method('getContent')
            ->will($this->returnValue($stream));

        $repo = $this->getMock('Repository', array('findOneBy', 'find'));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($fileMock));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($fileVerMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager',
        array('getRepository', 'persist', 'flush', 'getParent', 'getUserId'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));
        $em->expects($this->any())
            ->method('getUserId')
            ->will($this->returnValue(2));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));
        $functionMock->setServiceLocator($smMock);
        $functionMock->editAction();
        chmod(getcwd() . '/data/readOnly/' . time() . 'mock.txt', 0777);
        chmod(getcwd() . '/data/mock.txt', 0777);
    }

    public function testDownloadAction()
    {
        $content = 'test';
        $stream = fopen('php://memory', 'r+');
        fwrite($stream, base64_encode(serialize($content)));
        rewind($stream);

        $branchMock = $this->getMock('RoleAPI\Entity\BranchName',
        array('getHash', 'getTime', 'getBranchId', 'getFileId', 'getVerAmount', 'getContent', 'getType'));
        $branchMock->expects($this->any())
            ->method('getContent')
            ->will($this->returnValue($stream));

        $repo = $this->getMock('Repository', array('findOneBy'));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($branchMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->downloadAction();
    }

    public function provideTestDeleteAction()
    {
        return array(array('dir'), array(''));
    }

    /**
     * @dataProvider provideTestDeleteAction
     */
    public function testDeleteAction($dir)
    {
        $_SESSION['Userid'] = 1;

        $functionMock = $this->getMock('Data\Controller\DataController', array('deleteChilds'));

        $branchMock = $this->getMock('RoleAPI\Entity\BranchName', array('getLocation'));
        $branchMock->expects($this->any())
            ->method('getLocation')
            ->will($this->returnValue($dir));

        $repo = $this->getMock('Repository', array('findOneBy'));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($branchMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository', 'persist', 'flush'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $functionMock->setServiceLocator($smMock);
        $functionMock->deleteAction();
    }

    public function provideTestNewLocation()
    {
        return array(array('dir://'), array('dir://A1'));
    }

    /**
     * @dataProvider provideTestNewLocation
     */
    public function testNewLocation($location)
    {
        $fileEntity = $this->getMock('MetaAPI\Entity\File', array('getName'));
        $fileEntity->expects($this->any())
            ->method('getName')
            ->will($this->returnValue('ich.notice'));

        $api = $this->getMock('RoleAPI', array('getChildrenEntities'));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($api));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->newLocation($fileEntity, $api, $location);
    }

    public function testTypeFile()
    {
        $this->assertEquals('doc', $this->_controller->typeFile('txt'));
        $this->assertEquals('archive', $this->_controller->typeFile('dwc'));
        $this->assertEquals('mail', $this->_controller->typeFile('cpe'));
        $this->assertEquals('code', $this->_controller->typeFile('css'));
        $this->assertEquals('excel', $this->_controller->typeFile('uos'));
    }

    public function provideTestUploadAction()
    {
        return array(array(array('name' => 'mock.txt', 'location' => 'dir://', 'readOnly' => 'true')),
        array(array('name' => 'mock.txt')));
    }

    /**
     * @dataProvider provideTestUploadAction
     */
    public function testUploadAction($post)
    {
        fopen(getcwd() . '/data/readOnly/' . time() . 'mock.txt', 'w');
        chmod(getcwd() . '/data/readOnly/' . time() . 'mock.txt', 0777);
        $functionMock = $this->getMock('Data\Controller\DataController', array('isUpload'));
        $functionMock->expects($this->any())
            ->method('isUpload')
            ->will($this->returnValue(true));
        $file = array('file' => array('tmp_name' => getcwd() . '/data/mock.txt', 'type' => 'text/html'));

        file_put_contents(getcwd() . '/data/readOnly/' . time() . 'mock.txt', "bla");

        $functionMock->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post))
            ->setFiles(new Parameters($file));

        $_SESSION['Userid'] = 1;

        $repo = $this->getMock('Repository', array('findOneBy'));

        $branchNameMock = $this->getMock('RoleAPI\Entity\BranchName', array('getName'));
        $branchNameMock->expects($this->any())
            ->method('getName')
            ->will($this->returnValue(array('asiudghbisafugz')));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager',
        array('getRepository', 'persist', 'flush', 'getUserId', 'getChildrenEntities'));
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));
        $em->expects($this->any())
            ->method('getChildrenEntities')
            ->will($this->returnValue(array($branchNameMock)));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $functionMock->setServiceLocator($smMock);
        $functionMock->uploadAction();
        chmod(getcwd() . '/data/readOnly/' . time() . 'mock.txt', 0777);
    }

    public function testdeleteChilds()
    {
        $child = $this->getMock('RoleAPI\Entity\BranchName', array('getLocation'));

        $api = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getChildrenEntities'));
        $api->expects($this->at(0))
            ->method('getChildrenEntities')
            ->will($this->returnValue(array($child)));
        $api->expects($this->at(1))
            ->method('getChildrenEntities')
            ->will($this->returnValue(array(null)));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($api));

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->deleteChilds('dir://', $api);
    }
}
