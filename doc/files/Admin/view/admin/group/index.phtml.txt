<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/flick/jquery-ui.min.css" />

<div id="content">
    <input id="addname" placeholder="Gruppenname" type="text"/>
    <input id="adddir" placeholder="Pfad" type="text"/>
    <input id="add" type="submit" value="Gruppe Erstellen"></input>
</div>

<div id="slider">
    <div>
        <?php
            include 'help.html';
        ?>
    </div>
    <p id="sliderButton"><span>Hilfe!</span></p>
</div>

<script src="/js/jquery-ui.js" defer></script>
<script src="/js/group.js" defer></script>


