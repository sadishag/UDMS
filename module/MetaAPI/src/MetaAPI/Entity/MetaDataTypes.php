<?php
namespace MetaAPI\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Zend\Form\Annotation; // !!!! Absolutely neccessary
/**
 @ORM\Entity
 * MetaDataTypes
 *
 * @ORM\Table(name="MetaDataTypes")
 * @property int $Id
 * @property string $Name
 * @property string $Mediatyp
 * @property string $Subtyp
 * @property int $MaxLength
 * @property int $MaxSize
 * @property boolean $Interactive
 * @property string $ResourceUrl
 */


class MetaDataTypes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $Id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $Name;

    /**
     * @ORM\Column(type="string")
     */
    protected $Mediatyp;

    /**
     * @ORM\Column(type="text")
     */
    protected $Subtyp;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $MaxLength;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $MaxSize;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $Interactive;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $ResourceUrl;


	/**
     * @return the $Id
     */
    public function getId()
    {
        return $this->Id;
    }

	/**
     * @return the $Name
     */
    public function getName()
    {
        return $this->Name;
    }

	/**
     * @return the $Mediatyp
     */
    public function getMediatyp()
    {
        return $this->Mediatyp;
    }

	/**
     * @return the $Subtyp
     */
    public function getSubtyp()
    {
        return $this->Subtyp;
    }

	/**
     * @return the $MaxLength
     */
    public function getMaxLength()
    {
        return $this->MaxLength;
    }

	/**
     * @return the $MaxSize
     */
    public function getMaxSize()
    {
        return $this->MaxSize;
    }

	/**
     * @return the $Interactive
     */
    public function getInteractive()
    {
        return $this->Interactive;
    }

	/**
     * @return the $ResourceUrl
     */
    public function getResourceUrl()
    {
        return $this->ResourceUrl;
    }

	/**
     * @param string $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

	/**
     * @param string $Mediatyp
     */
    public function setMediatyp($Mediatyp)
    {
        $this->Mediatyp = $Mediatyp;
    }

	/**
     * @param string $Subtyp
     */
    public function setSubtyp($Subtyp)
    {
        $this->Subtyp = $Subtyp;
    }

	/**
     * @param number $MaxLength
     */
    public function setMaxLength($MaxLength)
    {
        $this->MaxLength = $MaxLength;
    }

	/**
     * @param number $MaxSize
     */
    public function setMaxSize($MaxSize)
    {
        $this->MaxSize = $MaxSize;
    }

	/**
     * @param boolean $Interactive
     */
    public function setInteractive($Interactive)
    {
        $this->Interactive = $Interactive;
    }

	/**
     * @param string $ResourceUrl
     */
    public function setResourceUrl($ResourceUrl)
    {
        $this->ResourceUrl = $ResourceUrl;
    }
}
