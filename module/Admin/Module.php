<?php

namespace Admin;

use Zend\Mvc\MvcEvent;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
        'Zend\Loader\StandardAutoloader' => array(
        'namespaces' => array(__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__)));
    }

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach(MvcEvent::EVENT_DISPATCH, array($this, 'authenticatePreDispatch'), 100);
        //$eventManager->attach(MvcEvent::EVENT_DISPATCH, array($this, 'mainPreDispatch'), 99);
    }

    public function authenticatePreDispatch($event)
    {
        $return = false;
        if (!($event->getRouteMatch()->getParam('action') === 'login' ||
         $event->getRouteMatch()->getParam('action') === 'templogin')) {
            if (!isset($_SESSION['Userid'])) {
                $response = $event->getResponse();
                $response->getHeaders()->addHeaderLine('Location', '/');
                $response->setStatusCode(302);
                $response->sendHeaders();
                $return = true;
            }else{
                $this->mainPreDispatch($event);
            }
        }
        if ($return) {
            throw new \RuntimeException();
        }
    }
    public function mainPreDispatch($event){
        $serviceManager = $event->getApplication()->getServiceManager();
        $viewModel = $event->getApplication()->getMvcEvent()->getViewModel();
        $api = $serviceManager->get('RoleAPI');

        $userid = $_SESSION['Userid'];
        $em = $serviceManager->get('ObjectManager');
        $user = $em->getRepository('RoleAPI\Entity\User')->find($userid);
        if ($user->getStyleSettings() != 1) {
            $event->getViewModel()->setTemplate('layout/layoutWeb2');
        }

        /* name auslesen */
        $viewModel->userName = $user->getFirstName() . ' ' . $user->getLastName();

        $rights['groupadministration'] = $api->isAllowed($_SESSION['Userid'], 0, 'groupadministration');
        $rights['roleadministration'] = $api->isAllowed($_SESSION['Userid'], 0, 'roleadministration');
        $rights['createuser'] = $api->isAllowed($_SESSION['Userid'], 0, 'createuser');
        $rights['isadmin'] = $api->isAllowed($_SESSION['Userid'], 0, 'isadmin');
        $viewModel->rights = $rights;

    }

}
