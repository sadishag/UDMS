<?php

namespace RoleAPI\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * GroupName
 *
 * @ORM\Table(name="GroupName")
 *
 * @property int $GroupId
 * @property string $Name
 * @property int $BranchId
 */

class GroupName
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */

    protected $GroupId;

    /**
     * @ORM\Column(type="string")
     */
    protected $Name;

    /**
     * @ORM\Column(type="integer")
     */

    protected $BranchId;

    /**
     *
     * @return the $BranchId
     */
    public function getBranchId()
    {
        return $this->BranchId;
    }

    /**
     *
     * @param number $BranchId
     */
    public function setBranchId($BranchId)
    {
        $this->BranchId = $BranchId;
    }

    /**
     *
     * @return the $GroupId
     */
    public function getGroupId()
    {
        return $this->GroupId;
    }

    /**
     *
     * @return the $GroupId
     */
    public function getId()
    {
        return $this->GroupId;
    }

    /**
     *
     * @return the $Name
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     *
     * @param string $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

}