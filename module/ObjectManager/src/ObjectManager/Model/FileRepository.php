<?php
/**
 * FileRepository.php
 *
 * @package    ObjectManager\Model
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Sebastian Krebs <sebastian.krebs@unister.de>
 */
namespace ObjectManager\Model;

/**
 * Has the Functions to handle the FileRepository
 *
 * @copyright Copyright (c) 2012 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Sebastian Krebs <sebastian.krebs@unister.de>
 *
 */

class FileRepository
{
    /**
     * Has the Fullpath for the File
     *
     * @var string $_content;
     */
    protected $_content;

    /**
     * Has the Name of this Entity
     *
     * @var string $_entityName
     */
    protected $_entityName;

    /**
     * Create the Fullpath,the Name and the FileContent
     *
     * @param string $entityName
     */
    public function __construct($entityName, $path)
    {
        $fullpath = $path . $entityName . '.txt';
        $this->_content = file($fullpath);
        $this->_entityName = $entityName;

    }

    /**
     * Search in the File for the Right Objekt
     *
     * @param int $id
     * @return Object $content
     */
    public function find($id)
    {
        $contentdata = $this->_content;
        if (empty($contentdata)) {
            $i = 1;
        } else {
            foreach ($contentdata as $contentline) {
                $contentindata = unserialize($contentline);
                if ($contentindata['Id'] == $id) {
                    $content = new $this->_entityName();
                    $content->setDataArray($contentindata);
                    $i = 0;
                    break;
                } else {
                    $i = 1;
                }

            }
        }
        if ($i == 1) {
            return null;
        } else {
            return $content;
        }

    }

    /**
     * Search in the File for the Right Objekts
     *
     * @param array $arrayofoptions
     * @return array of Objects $content
     */
    public function findBy($arrayofoptions)
    {
        $contentdata = $this->_content;
        $content = array();
        if (empty($contentdata)) {
            $i = 1;
        } else {
            $i = 0;
            foreach ($contentdata as $contentline) {
                $contentindata = unserialize($contentline);
                $j = 0;
                foreach ($arrayofoptions as $key => $value) {
                    if (trim($contentindata[$key]) != $value) {
                        $j = 1;
                    }
                }
                if ($j == 0) {
                    $helpcontent = new $this->_entityName();
                    $helpcontent->setDataArray($contentindata);
                    $content[] = $helpcontent;
                }
            }
        }
        if (empty($content)) {
            $i = 1;
        }
        if ($i == 1) {
            return null;
        } else {
            return $content;
        }
    }

    /**
     * Search in the File for all Objekts
     *
     * @return array of Objects $content
     */

    public function findAll()
    {
        $contentdata = $this->_content;
        $content = array();
        if (empty($contentdata)) {
            $i = 1;
        } else {
            $i = 0;
            foreach ($contentdata as $contentline) {
                $contentindata = unserialize($contentline);

                $helpcontent = new $this->_entityName();
                $helpcontent->setDataArray($contentindata);
                $content[] = $helpcontent;
            }
        }
        if ($i == 1) {
            return null;
        } else {
            return $content;
        }
    }

    /**
     * Search in the File for the Right Objekts
     *
     * @param array $arrayofoptions
     * @return Objects $content
     */
    public function findOneBy($arrayofoptions)
    {
        $content = $this->findBy($arrayofoptions);
        return $content[0];
    }

}