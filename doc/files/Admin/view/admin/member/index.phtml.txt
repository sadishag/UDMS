<script src="/js/member.js" defer></script>

<div id="content">
    <h2><?php echo htmlspecialchars($this->groupname); ?></h2>

    <br/>
    <a href="<?php echo $this->url('groupadmin', array('groupid' => $this->groupid)); ?>">Mitglieder verwalten</a>

</div>

<div id="slider">
    <div>
        <?php
            include 'help.html';
        ?>
    </div>
    <p id="sliderButton"><span>Hilfe!</span></p>
</div>
