<?php

namespace ObjectManager\Model;

use Zend\Http\Response;

use ObjectManager\Model\EntityManager;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;

class EntitiyManagerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {
        $testdatapath = __DIR__ . '/';
        $testfile = fopen($testdatapath . 'DataAPI\Entity\FileVersion.txt', 'w');
        $data = serialize(array('Id' => 1)) . PHP_EOL . serialize('blabla');
        file_put_contents($testdatapath . 'DataAPI\Entity\FileVersion.txt', $data);
        fclose($testfile);

        $testfile = fopen($testdatapath . '.txt', 'w');
        $data = serialize(array('Id' => 1)) . PHP_EOL . serialize(array('bla' => 'bla'));
        file_put_contents($testdatapath . '.txt', $data);
        fclose($testfile);

        $entmock = $this->getMock('ent', array('getId'));
        $entmock->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));
        $repomock = $this->getMock('repo', array('findOneBy'));
        $repomock->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($entmock));
        $emmock = $this->getMock('em', array('getRepository'));
        $emmock->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repomock));
        $smmock = $this->getMock('sm', array('get'));
        $smmock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($emmock));

        $this->_controller = new EntityManager($testdatapath, $smmock);
    }

    public function tearDown()
    {
        unlink(__DIR__ . '/.txt');
        unlink(__DIR__ . '/DataAPI\Entity\FileVersion.txt');
    }

    public function testpersist()
    {
        $dataarray = array('Id' => '1', 'FileId' => '2', 'VerNr' => '3', 'Time' => '4', 'Editor' => '5',
        'Owner' => '6', 'Content' => '7');
        $albumMock = new \DataAPI\Entity\FileVersion();
        $albumMock->setDataArray($dataarray);
        $this->_controller->persist($albumMock);

    }

    public function testfilerepo()
    {
        $albumMock = new \DataAPI\Entity\FileVersion();

        $this->_controller->getRepository(get_class($albumMock));

    }

    public function testremove()
    {
        $albumMock = new \DataAPI\Entity\FileVersion();
        $dataarray = array('Id' => '1', 'FileId' => '2', 'VerNr' => '3', 'Time' => '4', 'Editor' => '5',
        'Owner' => '6', 'Content' => '7');
        $albumMock = new \DataAPI\Entity\FileVersion();
        $albumMock->setDataArray($dataarray);
        $this->_controller->remove($albumMock);

    }

    public function testremovenotfound()
    {
        $dataarray = array('Id' => '2', 'FileId' => '2', 'VerNr' => '3', 'Time' => '4', 'Editor' => '5',
        'Owner' => '6', 'Content' => '7');
        $albumMock = new \DataAPI\Entity\FileVersion();
        $albumMock->setDataArray($dataarray);

        $this->_controller->remove($albumMock);

    }

    public function testflush()
    {
        $albumMock = new \DataAPI\Entity\FileVersion();
        $helpPath = getcwd();
        $fullPath = $helpPath . '/data/' . get_class($albumMock) . '.txt';
        $this->_controller->flush();

    }
}