<?php
return array(
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array('ViewJsonStrategy')
    ),
    'router' => array(
        'routes' => array(
            'roleverwaltung' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/verwaltung',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Role',
                        'action' => 'role',
                    )
                )
            ),
            'crudRights' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/admin/crud',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Crud',
                        'action' => 'index',
                    )
                )
            ),
            'group' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/group/[:action]',
                    'constraints' => array('action' => '[a-zA-Z]*'),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Group',
                        'action'    => 'index',
                    )
                )
            ),
            'roleRights' => array(
               'type' => 'Zend\Mvc\Router\Http\Literal',
               'options' => array(
                   'route' => '/admin/role',
                   'defaults' => array(
                       'controller' => 'Admin\Controller\Role',
                       'action' => 'index',
                   )
               )
            ),
         'adminusertemp' => array(
               'type' => 'Zend\Mvc\Router\Http\Literal',
               'options' => array(
                   'route' => '/admin/tempuser',
                   'defaults' => array(
                       'controller' => 'Admin\Controller\Admintemp',
                       'action' => 'index',
                   )
               )
            ),
         'removedata' => array(
               'type' => 'Zend\Mvc\Router\Http\Literal',
               'options' => array(
                   'route' => '/admin/removedata',
                   'defaults' => array(
                       'controller' => 'Admin\Controller\Removedata',
                       'action' => 'index',
                   )
               )
            ),
            'member' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'    => '/admin/group/:groupid/member/[:action]',
                    'constraints' => array(
                        'action'     => '[a-zA-Z]*',
                        'groupid'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Member',
                        'action'    => 'index',
                    )
                ),
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Crud' => 'Admin\Controller\CrudController',
            'Admin\Controller\Group' => 'Admin\Controller\GroupController',
            'Admin\Controller\Member' => 'Admin\Controller\MemberController',
            'Admin\Controller\Role' => 'Admin\Controller\RoleController',
            'Admin\Controller\Admintemp' => 'Admin\Controller\AdmintempController',
            'Admin\Controller\Removedata' => 'Admin\Controller\RemovedataController',
        )
    ),
);
