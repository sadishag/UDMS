<?php

use Zend\Http\Response;

use Doctrine\Tests\Common\Annotations\Fixtures\Annotation\Route;

use Doctrine\ORM\Query\Parameter;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;

use Auth\Controller\AuthController;

class AuthControllerTest extends PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {
        $this->_controller = new AuthController();
    }

    public function testLoginMitDatenAction()
    {
        $_SESSION['Userid'] = 1;

        $post = array('username' => 't ', 'password' => '1');

        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $userMock = $this->getMock('RoleAPI\Entity\User', array('getPassword','getTemp'));
        $userMock->expects($this->any())
            ->method('getPassword')
            ->will($this->returnValue('1'));
        $userMock->expects($this->at(1))
            ->method('getTemp')
            ->will($this->returnValue(3));
        $userMock->expects($this->at(3))
            ->method('getTemp')
            ->will($this->returnValue(2));
        $userMock->expects($this->at(5))
            ->method('getTemp')
            ->will($this->returnValue(1));
        $userMock->expects($this->at(7))
            ->method('getTemp')
            ->will($this->returnValue(0));

        $repo = $this->getMock('Repository', array('findOneBy'));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($userMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'), array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $route = new RouteMatch(array('controller' => 'auth', 'action' => 'login'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->loginAction();
        $this->_controller->loginAction();
        $this->_controller->loginAction();
        $this->_controller->loginAction();

    }

    public function testLoginOhnePwAction()
    {
        $_SESSION['Userid'] = 1;

        $post = array('username' => 't ', 'password' => '1');

        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $userMock = $this->getMock('RoleAPI\Entity\User', array('getPassword'));
        $userMock->expects($this->any())
            ->method('getPassword')
            ->will($this->returnValue(''));

        $repo = $this->getMock('Repository', array('findOneBy'));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($userMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'), array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $route = new RouteMatch(array('controller' => 'auth', 'action' => 'login'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);

        $this->_controller->setServiceLocator($smMock);
        $this->_controller->loginAction();
    }

    public function testLoginOhneUsernameAction()
    {
        $_SESSION['Userid'] = 1;

        $post = array('username' => 't ', 'password' => '1');

        $this->_controller->getRequest()
            ->setMethod('post')
            ->setPost(new Parameters($post));

        $userMock = $this->getMock('RoleAPI\Entity\User', array('getPassword'));
        $userMock->expects($this->any())
            ->method('getPassword')
            ->will($this->returnValue(''));

        $repo = $this->getMock('Repository', array('findOneBy'));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($userMock));

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'), array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $route = new RouteMatch(array('controller' => 'auth', 'action' => 'login'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->loginAction();

    }

    public function testLoginOhnePostAction()
    {

        $_SESSION['Userid'] = 1;

        $em = $this->getMock('Doctrine\ORM\Entity\Entitymanager', array('getRepository'), array(), '', false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue(null));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));

        $route = new RouteMatch(array('controller' => 'auth', 'action' => 'login'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->loginAction();

    }

    public function testLogoutAction()
    {
        $_SESSION['Userid'] = 1;

        $route = new RouteMatch(array('controller' => 'auth', 'action' => 'logout'));
        $mvcEvent = new MvcEvent();
        $mvcEvent->setResponse(new Response());
        $mvcEvent->setRouteMatch($route);
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->logoutAction();
    }

    public function tearDown()
    {

    }
}