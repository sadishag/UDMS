<?php

return array(

    'controllers' => array(
        'invokables' => array(
            'Data\Controller\Data' => 'Data\Controller\DataController',
            'Data\Controller\Dir' => 'Data\Controller\DirController',
            'Data\Controller\File' => 'Data\Controller\FileController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'files' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/data/[:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Data\Controller\Data',
                        'action'     => 'show',
                    ),
                ),
            ),
            'dir' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/dir',
                    'defaults' => array(
                        'controller' => 'Data\Controller\Dir',
                        'action' => 'index',
                    )
                )
            ),
            'file' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/file',
                    'defaults' => array(
                        'controller' => 'Data\Controller\File',
                        'action' => 'index',
                    )
                )
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'strategies' => array('ViewJsonStrategy'),
    ),
);


