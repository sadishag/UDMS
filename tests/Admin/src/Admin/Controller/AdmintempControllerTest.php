<?php
namespace Admin\Controller;

use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Admin\Controller\AdmintempController;
use Zend\Stdlib\Parameters;

class AdmintempControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {

        $this->_controller = new AdmintempController();

    }

    public function tearDown()
    {

    }

    public function testindexAction()
    {
        $_SESSION['Userid']='ads';


        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays','isallowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('getStandardRoleArrays')
        ->will($this->returnValue(array('$key' => '3', '$value' => true)));


        $em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('Doctrine\ORM\EntityManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));

        $mvcEvent = new MvcEvent();
        $mvcEvent->setResponse(new Response());
        $this->_controller->setEvent($mvcEvent);
        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }
    public function testindexActionisallowed()
    {
        $_SESSION['Userid']='ads';


        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy','findBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays','isallowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(true));


        $em = $this->getMock('ObjectManager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));


        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }
    public function testindexActionisalloweduserexist()
    {
        $_SESSION['Userid']='ads';


        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy','findBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue(array($albumMock,$albumMock)));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays','isallowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(true));


        $em = $this->getMock('ObjectManager', array("getRepository", "persist", "flush"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));


        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }
    public function testindexActionisalloweduserexistchange()
    {
        $_SESSION['Userid']='ads';
        $post = array('sizeofpost'=>'5','tempchange2'=>'1','tempchange1'=>'2','tempchange3'=>'3');
        $this->_controller->getRequest()
        ->setMethod('post')
        ->setPost(new Parameters($post));

        $albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
        $albumMock->expects($this->any())
        ->method('getRoleId')
        ->will($this->returnValue(array()));
        $albumMock->expects($this->any())
        ->method('setRoleId');

        $repo = $this->getMock('Repository', array('findAll', 'findOneBy','findBy'));
        $repo->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue(array($albumMock,$albumMock)));

        $apiMock = $this->getMock('RoleAPI',
        array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays','isallowed'), array(), "", false);
        $apiMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(true));


        $em = $this->getMock('ObjectManager', array("getRepository", "persist", "flush","remove"), array(), "",
        false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));

        $mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($mapmock));


        $this->_controller->setServiceLocator($smMock);
        $this->_controller->indexAction();
    }

}