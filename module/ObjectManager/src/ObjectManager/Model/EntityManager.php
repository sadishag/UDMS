<?php

/**
 * EntityManager.php
 *
 * @package    ObjectManager\Model
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Sebastian Krebs <sebastian.krebs@unister.de>
 */
namespace ObjectManager\Model;

/**
 * Has the Functions to handle Entitys
 *
 * @copyright Copyright (c) 2012 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Sebastian Krebs <sebastian.krebs@unister.de>
 *
 */

class EntityManager
{
    /**
     * Has the Classname of the last Entity
     *
     * @var string $_entityname
     */
    protected $_entityname;

    /**
     * Full Path to the Data Folder of the Project
     *
     * @var string $_dataPath
     */
    protected $_dataPath;

    /**
     * Instance of the ServiceManager
     *
     * @var object $_serviceManager
     */
    protected $_serviceManager;

    /**
     * constructor for setting the DataPath and the service locator
     *
     * @param string $path
     */
    public function __construct($path, $sm)
    {
        $this->_dataPath = $path;
        $this->_serviceManager = $sm;
    }

    /**
     * Write the Object in a File
     *
     * @param objekt $entity
     */
    public function persist($entity)
    {
        $this->_entityname = get_class($entity);
        $fullPath = $this->_dataPath . $this->_entityname . '.txt';
        $fp = fopen($fullPath, 'a+');
        $contentForData = $entity->getDataArray();
        fwrite($fp, serialize($contentForData));
        fwrite($fp, PHP_EOL);

    }

    /**
     * Call the Function FileRepository
     *
     * @param string $entityName
     * @return a valid Repository
     */
    public function getRepository($entityName)
    {
        return new FileRepository($entityName, $this->_dataPath);
    }

    /**
     * Delete the Objekt from the File
     *
     * @param objekt $entity
     */
    public function remove($entity)
    {
        $this->_entityname = get_class($entity);
        $fullPath = $this->_dataPath . $this->_entityname . '.txt';
        $contentData = file($fullPath);
        $fp = fopen($fullPath, 'w+');
        $contentForData = $entity->getDataArray();
        foreach ($contentData as $contentLine) {
            $contentInData = unserialize($contentLine);
            if ($contentInData['Id'] == $contentForData['Id']) {
                unset($contentInData);
            }
            if (isset($contentInData)) {
                fwrite($fp, serialize($contentForData));
                fwrite($fp, PHP_EOL);
            }
        }
    }

    /**
     * Check the File
     * Write for all Objekts in the File the real Id
     */
    public function flush()
    {
        $fullPath = $this->_dataPath . $this->_entityname . '.txt';
        $contentData = file($fullPath);
        $fp = fopen($fullPath, 'w+');
        foreach ($contentData as $contentLine) {
            $contentindata = unserialize($contentLine);
            if (empty($contentindata['Id'])) {
                unset($contentindata['Id']);
                $em = $this->_serviceManager->get('ObjectManager');
                $user = $em->getRepository('DataAPI\Entity\FileVersion')->findOneBy($contentindata);
                if (isset($user)) {
                    $contentindata['Id'] = $user->getId();
                }
            }
            fwrite($fp, serialize($contentindata));
            fwrite($fp, PHP_EOL);
        }
    }

}