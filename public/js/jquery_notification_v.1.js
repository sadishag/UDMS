/**
 * Javascript functions to show top nitification
 * Error/Success/Info/Warning messages
 * Developed By: Ravi Tamada
 * url: http://androidhive.info
 * © androidhive.info
 * 
 * Created On: 10/4/2011
 * version 1.0
 * 
 * Usage: call this function with params 
 showNotification(params);
 **/

function showNotification(params){
    // options array
    var options = { 
        'showAfter': 0, // number of sec to wait after page loads
        'duration': 0, // display duration
        'autoClose' : false, // flag to autoClose notification message
        'type' : 'success', // type of info message error/success/info/warning
        'message': '', // message to dispaly
        'link_notification' : '', // link flag to show extra description
        'description' : '' // link to desciption to display on clicking link message
    }; 
    // Extending array from params
    $.extend(true, options, params);
    
    var msgclass = 'succ_bg'; // default success message will shown
    if(options['type'] == 'error'){
        msgclass = 'error_bg'; // over write the message to error message
    } else if(options['type'] == 'information'){
        msgclass = 'info_bg'; // over write the message to information message
    } else if(options['type'] == 'warning'){
        msgclass = 'warn_bg'; // over write the message to warning message
    }
    
    // Parent Div container
    var container = '<div id="info_message" class="'+msgclass+'"><div class="center_auto"><div class="info_message_text message_area">';
    container += options['message'];
    container += '</div><div class="info_close_btn button_area" onclick="return closeNotification()"></div><div class="clear"></div>';
    container += '</div><div class="info_more_descrption"></div></div>';
    
    $notification = $(container);
    
    // Appeding notification to Body
    $('body').append($notification);
    
    var divHeight = $('div#info_message').height();
    // see CSS top to minus of div height
    $('div#info_message').css({
    	bottom : '-'+divHeight+'px'
    });
    
    // showing notification message, default it will be hidden
    $('div#info_message').show();
    
    // Slide Down notification message after startAfter seconds
    slideDownNotification(options['showAfter'], options['autoClose'],options['duration']);
    /*
    $('.link_notification').live('click', function(){
        $('.info_more_descrption').html(options['description']).slideDown('fast');
    });
    */
}
// function to close notification message
// slideUp the message
function closeNotification(duration){
    var divHeight = $('div#info_message').height();
    setTimeout(function(){
        $('div#info_message').animate({
        	bottom: '-'+divHeight
        }); 
        // removing the notification from body
        setTimeout(function(){
            $('div#info_message').remove();
        },200);
    }, parseInt(duration * 1000));   
    

    
}

// sliding down the notification
function slideDownNotification(startAfter, autoClose, duration){    
    setTimeout(function(){
        $('div#info_message').animate({
            bottom: 0
        }); 
        if(autoClose){
            setTimeout(function(){
                closeNotification(duration);
            }, duration);
        }
    }, parseInt(startAfter * 1000));    
}

function globalNotification(response){
	if(typeof response.status === 'undefined'){
		if(typeof response.success !== 'undefined'){
			showNotification({
				type : "success",
				message: response.success,
				autoClose: true,
				duration: 2
				})
		}
		if(typeof response.warning !== 'undefined'){
			showNotification({
				type : "warning",
				message: response.warning,
				autoClose: true,
				duration: 2
				})
		}
		if(typeof response.error !== 'undefined'){
			showNotification({
				type : "error",
				message: response.error,
				autoClose: true,
				duration: 2
				})
		}
		if(typeof response.information !== 'undefined'){
			showNotification({
				type : "information",
				message: response.information,
				autoClose: true,
				duration: 2
				})
		}
	} else if(response.status === 500){
		showNotification({
			type : "error",
			message: 'Bitte <a href="/">&gt;Einloggen!&lt;</a> Internal Server Error',
			
		})		
	} else {
		console.log(response);
		if(response.responseText.indexOf("id='login'") === -1){
			showNotification({
				type : "error",
				message: 'ERROR: Unbekannter Fehler! Bitte Seite neu Laden!',
				
			})
		}else{
			showNotification({
				type : "error",
				message: 'Bitte <a href="/">&gt;Einloggen!&lt;</a> ',
				
			})
		}
	}
}

function globalAjax(url, options){
	if(options === null || typeof(options) === 'undefined'){
		options = {};
	}else{
		if(typeof options === 'string') options = {data: options};
	}
	if(typeof options.type === 'undefined') options.type = 'POST';
	if(typeof options.dataType === 'undefined') options.dataType = 'json';
	
	return $.ajax(url, options).always(globalNotification);
}


/**
 * 
 * @param success function a function witch is caled if a user has chosen a dir
 * the success functions first parameter is a array of the selected dirs{location, name}
 */
function chosePathDialog(success, conf){
	
	function inArray(needle, haystack) {
	    for(var i = 0; i < haystack.length; i++) {
	        if(haystack[i] == needle) return true;
	    }
	    return false;
	}
	
	function handleResponse(response){
		if(typeof response.dir !== 'undefined'){
			currentDir = response.dir;
		}
		//emptying dirs div
		$dialog.children('.dirs').children().remove();
		if(typeof response.childs !== 'undefined'){
			//go trought a childs
			for(var i = 0; i < response.childs.length; i++){
				//check if type is allowed if not go to next child
				var type = response.childs[i].location.split('://')[0];
				if(settings.alowedTypes.length > 0 && !inArray(type, settings.alowedTypes))continue;


				
				
				
				
				var $dir = $('<div></div>');
				$dir.text(response.childs[i].name);
				$dir.addClass(type);
				$dir.data('location', response.childs[i].location);
				$dir.data('name', response.childs[i].name);
				$dir.click(function(e){
					if(!(e.ctrlKey && settings.multiple)){
						$(this).siblings().removeClass('selected');
					}
					$(this).addClass('selected');
				});
				$dir.dblclick(function(){
					//open file if it is a dir
					if($(this).data('location').split('://')[0] === 'dir'){
						globalAjax('/dir', {type: 'POST', data: {dirLocation:  $(this).data('location')}}).success(handleResponse);						
					}
				});

				$dialog.children('.dirs').append($dir);
			}
		}
	}
	

	
	var settings = {};
	settings.multiple = typeof(conf.multiple) !== 'undefined' ? conf.multiple : false;
	settings.alowedTypes = typeof(conf.alowedTypes) !== 'undefined' ? conf.alowedTypes : [];
	settings.startDir = typeof(conf.startDir) !== 'undefined' ? conf.startDir : 'dir://';
	

	var currentDir = {};
	var xxx = document.createElement('div');
	xxx.title = 'Pfad wählen';
	xxx.innerHTML = '<div class="menue"></div><div class="dirs"></div>';
	
	var $dialog = $(xxx);

	var $parrent = $('<span class="back-16"></span>');
	$parrent.click(function(){
		globalAjax('/dir', {type: 'POST', data: {dirLocation: currentDir.location + ':..'}}).success(handleResponse);
	});
	
	
	$dialog.children('.menue').append($parrent);
	globalAjax('/dir', {data: {dirLocation: settings.startDir}}).success(handleResponse);
	$dialog.dialog({
		draggable: false,
		close: function() {$(this).remove();},
        width: 500,
        height: 400,
        closeText: 'Schließen',
        modal: true,
		buttons:{
			'Auswählen': function(){
				var dirs = [];
				$(this).children('.dirs').children('.selected').each(function(){
					dirs[dirs.length] = {location: $(this).data('location'), name: $(this).data('name')};
				});
				if(dirs.length === 0){
					dirs[0] = currentDir;
				}
				success(dirs);
				$(this).remove();
			},
			'Abbrechen': function(){
				$(this).remove();
			}
		}
	});
}


