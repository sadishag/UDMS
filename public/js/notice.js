/**
 * notice.js
 *
 * @copyright  Copyright (c) 2013 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Roland Starke <roland.starke@unister.de>
 */


Notice = {};
/**
 * Opens a notice on the screen
 * 
 * @param int id of the file
 */
Notice.openDialog = function(id){
	//check if notice isn't already open
	if($('#notice' + id).length === 1){
		return false;
	}
	
	
	
	var $notice = $('<div id="notice' + id + '" class="notice"></div>');
	var $noticeHeader = $('<div class="noticeHeader"></div>');
	var $noticeInfo = $('<div style="display:none;" class="noticeInfo">' +
		'<span>Bearbeitet:</span> <span class="time"></span><br/> ' + 
		'<span>Besitzer:</span> <span class="owner"></span><br/>' +
		'<span>Editor:</span> <span class="editor"></span><br/>' +
		'<span>Version:</span> <span class="version"></span><br/>' +
		'<span>Änderungen gegenüber Version:</span> <span class="changes"></span><br/>' +
	'</div>');
	var $noticeContent = $('<textarea maxlength="255" class="noticeContent">Inhalt</textarea>');
	var $noticeChanges = $('<div class="displayChanges" style="display:none"></div>');

	var $actions = $('<span style="float:right"></span>');
	var $title = $('<span>Titel</span>');

	var $close = $('<span class="delete-16"></span>');
	var $save = $('<span class="save-16"></span>');
	var $info = $('<span class="info-16"></span>');
	


	$noticeHeader.append($title);
	$noticeHeader.append($actions);
	$actions.append($info);
	$actions.append($save);
	$actions.append($close);



	$notice.append($noticeHeader);
	$notice.append($noticeInfo);
	$notice.append($noticeContent);
	$notice.append($noticeChanges);
	


	$notice.draggable({
		containment: document.body,
		start:function(){
			var bigestZIndex = 1;
			$('.notice').each(function(i,notice){
				var thisZIndex = $(notice).css('z-index');
				if(thisZIndex !== 'auto' && bigestZIndex <= thisZIndex){
					bigestZIndex = parseInt(thisZIndex) + 1;
				}
			});
			$(this).css('z-index', bigestZIndex);
		},
		//funst net
	    position: { my: "center", at: "center", of: $('#content')}
	});
	
	$close.click(function(e){
		$notice.parent().remove();
	});
	$save.click(function(e){
		function sendFormData(url, formData){
			var xhr = new XMLHttpRequest();
			xhr.open('POST', url, true);
			xhr.onload = function(){
				try{
					var responseObject = JSON.parse(this.responseText);
					handleServerResponse(responseObject);
				}catch(e){
					var responseObject = this;
				}
				globalNotification(responseObject);			
			};
			xhr.send(formData);
		}
		formData = new FormData();
		formData.append('id', id);
		formData.append('file', new Blob([$noticeContent.val()], {type: 'text/notice'}));
		sendFormData('/data/edit',formData);
		
	});
	$info.click(function(e){
		$noticeInfo.slideToggle();
	})
	//stop draging
	$actions.mousedown(function(e){
		e.stopPropagation();
	});
	$noticeContent.mousedown(function(e){
		e.stopPropagation();
	});
	$noticeChanges.mousedown(function(e){
		e.stopPropagation();
	});
	$noticeInfo.mousedown(function(e){
		e.stopPropagation();
	});

	//element added to document
	
	
	//now load the notice content
	function handleServerResponse(response){
		$notice.children('.noticeContent').css('display','block');
		$notice.children('.displayChanges').css('display','none');
		
		if(typeof response === 'object'){
			if(typeof response.name !== 'undefined'){
				$noticeHolder = $('<div style="height:0"></div>');
				$noticeHolder.append($notice);
				$('#content').prepend($noticeHolder);
				
				var nameparts = response.name.split('.');
				nameparts.pop();
				$title.text(nameparts.join('.'));
			}
			
			if(typeof response.content !== 'undefined'){
				$noticeContent.val(response.content);
			}
			
			if(typeof response.verAmount !== 'undefined'){
				var $selectVersion = $('<select></select>');
				
				for(var i = response.verAmount; i > 0; i--){
					if(i === response.version){
						$selectVersion.append('<option selected="selected">' + i + '</option>');
					}else{
						$selectVersion.append('<option>' + i + '</option>');						
					}

				}
				$selectVersion.change(function(){
					globalAjax('/file', {data:{fileId: id, version: $(this).val() }}).success(handleServerResponse);
				});
				$noticeInfo.children('.version').html($selectVersion);
				
			}
			
			if(typeof response.time !== 'undefined'){
				$noticeInfo.children('.time').text(response.time);
			}
			
			
			if(typeof response.owner !== 'undefined'){			
				$noticeInfo.children('.owner').text(response.owner);
			}
			
			if(typeof response.editor !== 'undefined'){
				$noticeInfo.children('.editor').text(response.editor);
			}
			
			
			var $selectChange = $('<select></select>');
			for(var i = $noticeInfo.children('.version').children('select').val(); i > 0; i--){
				$selectChange.append('<option>' + i + '</option>');
			}
			$selectChange.change(function(){
				if($noticeInfo.children('.version').children('select').val() !== $selectChange.val()){
						globalAjax('/file', {type:'post', data:{fileId: id, version: $(this).val() }}).success(function(response){
						$notice.children('.noticeContent').css('display','none');
						$notice.children('.displayChanges').css('display','block');
						$notice.children('.displayChanges').html(diffString(response.content, $noticeContent.val()));
					});
					}else{
						$notice.children('.noticeContent').css('display','block');
						$notice.children('.displayChanges').css('display','none');	
					}	
			});
			$noticeInfo.children('.changes').html($selectChange);

	
		}
	}
	
	globalAjax('/file', {data:{fileId: id}}).success(handleServerResponse);
}
