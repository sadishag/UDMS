<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(

    'router' => array(
        'routes' => array(
            'data' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/user/data[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\Data',
                        'action'    => 'index'
                    )
                )
            ),
            'user' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/user',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                       'controller' => 'User\Controller\Data',
                       'action'    => 'index'
                    )
                )
            ),
            'createUser' => array(
               'type' => 'Zend\Mvc\Router\Http\Literal',
               'options' => array(
                   'route' => '/user/create',
                   'defaults' => array(
                       'controller' => 'User\Controller\Create',
                       'action' => 'index',
                   )
               )
            ),

            'tempuser' => array(
               'type' => 'Zend\Mvc\Router\Http\Literal',
               'options' => array(
                   'route' => '/tempuser',
                   'defaults' => array(
                       'controller' => 'User\Controller\Temp',
                       'action' => 'templogin',
                   )
               )
            ),

            'searchResults' => array(
               'type' => 'Zend\Mvc\Router\Http\Literal',
               'options' => array(
                   'route' => '/searchResults',
                   'defaults' => array(
                       'controller' => 'User\Controller\Search',
                       'action' => 'searchresults',
                   )
               )
            )
        )
    ),
    'controllers' => array(
        'invokables' => array(
            'User\Controller\Data' => 'User\Controller\DataController',
            'User\Controller\Create' => 'User\Controller\CreateController',
            'User\Controller\Temp' => 'User\Controller\TempController',
            'User\Controller\Search' => 'User\Controller\SearchController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);
