<?php
/** GroupController.php
 * @package    Admin\Controller
 * @copyright  Copyright (c) 2013 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Roland Starke <roland.starke@unister.de>
 *
 */

namespace Admin\Controller;

use Zend\Http\Response;

use RoleAPI\Entity\Roles;
use RoleAPI\Entity\GroupName;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;

use Doctrine\ORM\EntityManager;

/**
 * Ermögicht es gruppen anzulegen, umzubenennen und zu löschen
 */
class GroupController extends AbstractActionController
{
    private $_returnArray = array();

    /**
     * @return ViewModel Gibt ein leeres ViewModel zurück oder ein Response mit den Content 'keine Berechtigung'
     */
    public function indexAction()
    {
        $api = $this->getServiceLocator()->get('RoleAPI');

        if ($api->isAllowed($_SESSION['Userid'], 0, 'groupadministration')) {
            return new ViewModel();
        } else {
            $response = new Response();
            $response->setContent('keine Berechtigung');
            return $response;
        }

    }

    /**
     * @return JsonModel Gibt alle Gruppen mit namen id und Anzahl der temporären Mitglieder zurück
     */
    public function showAction()
    {
        $api = $this->getServiceLocator()->get('RoleAPI');
        if (!$api->isAllowed($_SESSION['Userid'], 0, 'groupadministration')) {
            return new JsonModel(array('error' => 'keine Berechtigung'));
        }

        $em = $this->getServiceLocator()->get('ObjectManager');
        $groupRepo = $em->getRepository('RoleAPI\Entity\Groups');
        // $query = $em->createQuery('SELECT G.GroupId AS id, G.Name AS name,
        // SUM(M.Temp) AS temp FROM RoleAPI\Entity\GroupName G LEFT JOIN
        // RoleAPI\Entity\Groups M ON M.GroupId = G.GroupId GROUP BY G.GroupId
        // ');
        $groups = $em->getRepository('RoleAPI\Entity\GroupName')->findBy(array(), array('Name' => 'desc'));
        foreach ($groups as $group) {
            $this->_returnArray['groups'][] = array(
                'id' => $group->getGroupId(), 'name' => $group->getName(),
                'temp' => sizeof(
                    $groupRepo->findBy(
                        array('GroupId' => $group->getGroupId(), 'Temp' => '1')
                    )
                )
            );
        }
        return new JsonModel($this->_returnArray);
    }

    /**
     * Fügt eine Gruppe hinzu
     * @return JsonModel gibt die hinzugefügte gruppe zurück
     */
    public function addAction()
    {
        $api = $this->getServiceLocator()->get('RoleAPI');
        if (!$api->isAllowed($_SESSION['Userid'], 0, 'groupadministration')) {
            return new JsonModel(array('error' => 'keine Berechtigung'));
        }

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $group = $this->_addGroup($post['name'], $post['location']);
        } else {
            $this->_returnArray['error'] = 'Ungültiger Reguest';
        }
        return new JsonModel($this->_returnArray);
    }

    /**
     * löscht eine Gruppe
     * @return JsonModel gibt die gelöschte gruppe zurück
     */
    public function deleteAction()
    {
        $api = $this->getServiceLocator()->get('RoleAPI');
        if (!$api->isAllowed($_SESSION['Userid'], 0, 'groupadministration')) {
            return new JsonModel(array('error' => 'keine Berechtigung'));
        }

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $this->_deleteGroup($post['id']);
        } else {
            $this->_returnArray['error'] = 'Ungültiger Reguest';
        }

        return new JsonModel($this->_returnArray);
    }

    /**
     * gibt einer gruppe einen neuen namen
     * @return JsonModel gibt die umbenante gruppe zurück
     */
    public function renameAction()
    {
        $api = $this->getServiceLocator()->get('RoleAPI');
        if (!$api->isAllowed($_SESSION['Userid'], 0, 'groupadministration')) {
            return new JsonModel(array('error' => 'keine Berechtigung'));
        }

        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost();
            $this->_renameGroup($post['name'], $post['id']);
        } else {
            $this->_returnArray['error'] = 'Ungültiger Reguest';
        }
        return new JsonModel($this->_returnArray);
    }

    /**
     * checks if a name is valid
     *
     * @param string $name
     *            a name to use for a group
     * @return boolean returns true if name is valid else false
     */
    private function _isValidName($name)
    {
        return preg_match('/^.{3,255}$/', $name) === 1;
    }

    /**
     * checks whether a group exist or not
     *
     * @param string $name
     *            a name of a group
     * @return boolean returns true if group exist else false
     */
    private function _isNameFree($name)
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        return $em->getRepository('RoleAPI\Entity\GroupName')->findOneBy(array('Name' => $name)) === null;
    }

    /**
     * checks whether a group exist or not
     *
     * @param int $id
     *            GroupId from a BranchName
     * @return boolean returns true if group exist else false
     */
    private function _groupExist($id)
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        return $em->getRepository('RoleAPI\Entity\GroupName')->find($id) !== null;
    }

    /**
     * creates a new Group
     *
     * @param string $name
     *            name of Group
     * @param string $location
     *            a Location from a BranchName
     */
    private function _addGroup($name, $location)
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $branchid = $this->_getBranchId($location);
        $this->_trim($name);
        if (!$this->_isValidName($name)) {
            $this->_returnArray['error'] =
                    'Name muss mindestens 3 Zeichen lang sein';
            return false;
        } elseif (!$this->_isNameFree($name)) {
            $this->_returnArray['error'] = 'Name schon benutzt';
            return false;
        } elseif (!$branchid) {
            $this->_returnArray['error'] = 'Pfad existiert nicht';
            return false;
        } else {
            $group = new GroupName();
            $group->setName($name);
            $group->setBranchId($branchid);

            $em->persist($group);
            $em->flush();

            $role = new Roles();
            $role->setGroupId($group->getGroupId());
            $role->setRoleId(1);
            $role->setUserId(0);

            $em->persist($role);
            $em->flush();

            $this->_returnArray['groups'][] = array('id' => $group->getGroupId(), 'name' => $group->getName());
            $this->_returnArray['success'] = 'Gruppe erstellt!';
            return true;
        }
    }

    /**
     * Renames a Group
     *
     * @param string $name
     *            new name of Group
     * @param int $id
     *            GroupId of Group
     *
     * @return boolean on success true on error false
     */
    private function _renameGroup($name, $id)
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $this->_trim($name);
        if (!$this->_groupExist($id)) {
            $this->_returnArray['error'] = 'Gruppe Existiert nicht';
            return false;
        } elseif (!$this->_isValidName($name)) {
                $this->_returnArray['error'] =
                        'Name muss mindestens 3 Zeichen lang sein';
                return false;
            } elseif (!$this->_isNameFree($name)) {
                    $this->_returnArray['error'] = 'Name schon benutzt';
                    return false;
                } else {

                    $group = $em->getRepository('RoleAPI\Entity\GroupName')->find($id);
                    $group->setName($name);
                    $em->persist($group);
                    $em->flush();
                    $this->_returnArray['groups'][] = array('id' => $group->getGroupId(),
                    'name' => $group->getName());
                    $this->_returnArray['success'] = 'Gruppe umbenannt!';
                    return true;
                }
    }


    /**
     * deletes a group and all references from GroupName, Groups and Roles
     *
     * @param int $id
     *            A GroupId from GroupName
     * @return boolean on Success true on Error false
     */
    private function _deleteGroup($id)
    {
        $api = $this->getServiceLocator()->get('RoleAPI');
        if (!$this->_groupExist($id)) {
            $this->_returnArray['error'] = 'Gruppe Existiert nicht';
            return false;
        } else {
            $api->deleteGroup($id);

            $this->_returnArray['groups'][] = array('id' => $id, 'name' => '');
            $this->_returnArray['success'] = 'Gruppe gelöscht!';
            return true;
        }
    }

    /**
     * Gets the BranchId from a given Location
     *
     * @param string $location
     *            a Location from a BranchName
     *
     * @return int BranchId from a Location
     */
    private function _getBranchId($location)
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $branch = $em->getRepository('RoleAPI\Entity\BranchName')->findOneBy(array('Location' => $location));
        if ($branch !== null) {
            return $branch->getBranchId();
        } else {
            return false;
        }
    }

    /**
     * Removes whitespace at start and end of string and removes multible
     * whitespace with one space
     *
     * @param
     *            string &$name a groupname
     */
    private function _trim(&$name)
    {
        $name = trim($name);
        $name = preg_replace('/\s+/', ' ', $name);
    }
}
