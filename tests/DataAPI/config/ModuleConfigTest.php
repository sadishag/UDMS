<?php
namespace DataAPI;
class ModuleConfigTest extends \PHPUnit_Framework_TestCase
{

    public function setUp()
    {

    }

    public function testModuleConfig()
    {
        $mockmock = $this->getMock('Doctrine\Common\Persistence\Mapping\ClassMetadataFactory');
        $mock = $this->getMock('Doctrine\ORM\Entitymanager', array('getMetaDataFactory'));
        $mock->expects($this->any())
            ->method('getMetaDataFactory')
            ->will($this->returnValue($mockmock));

        $test = include __DIR__ . '/../../../module/DataAPI/config/module.config.php';
        $sm = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface', array('get', 'has'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($mock));
        call_user_func(
        $test['service_manager']['factories']['DoctrineORMModule\Form\Annotation\AnnotationBuilder'], $sm);

    }

    public function testModuleConfig2()
    {
        $mockmock = $this->getMock('Doctrine\Common\Persistence\Mapping\ClassMetadataFactory');
        $mock = $this->getMock('Doctrine\ORM\Entitymanager', array('getMetaDataFactory'));
        $mock->expects($this->any())
            ->method('getMetaDataFactory')
            ->will($this->returnValue($mockmock));

        $test = include __DIR__ . '/../../../module/DataAPI/config/module.config.php';
        $sm = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface', array('get', 'has'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($mock));

        call_user_func($test['service_manager']['factories']['doctrine.mapping_collector.udms_container'], $sm);
    }

    public function testModuleConfig3()
    {
        $mockmock = $this->getMock('Doctrine\Common\Persistence\Mapping\ClassMetadataFactory');
        $mock = $this->getMock('Doctrine\ORM\Entitymanager', array('getMetaDataFactory'));
        $mock->expects($this->any())
            ->method('getMetaDataFactory')
            ->will($this->returnValue($mockmock));

        $test = include __DIR__ . '/../../../module/DataAPI/config/module.config.php';
        $sm = $this->getMock('Zend\ServiceManager\ServiceLocatorInterface', array('get', 'has'));
        $sm->expects($this->any())
            ->method('get')
            ->will($this->returnValue($mock));

        call_user_func($test['service_manager']['factories']['doctrine.mapping_collector.udms_sqli_container'],
        $sm);
    }

    public function tearDown()
    {

    }
}