<?php
return
array(
    'sleeptime' => 10,
    'shutdown' => 0,
    'lastpid' => 5548,
    'logpath' => getcwd() . '/Services/log/daemon.log',
    'suppressrunninglog' => 0,
    'addnewstorage' => 0,

);