<?php

namespace RoleAPI\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity
 * RoleName
 *
 * @ORM\Table(name="RoleName")
 *
 * @property int $RoleId
 * @property string $Name
 * @property blob $RightKey
 */

class RoleName
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */

    protected $RoleId;

    /**
     * @ORM\Column(type="string")
     */
    protected $Name;
    /**
     * @ORM\Column(type="blob")
     */
    protected $RightKey;

    /**
     *
     * @return the $RoleId
     */
    public function getRoleId()
    {
        return $this->RoleId;
    }

    /**
     *
     * @return the $RoleId
     */
    public function getId()
    {
        return $this->RoleId;
    }

    /**
     *
     * @return the $Name
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     *
     * @param number $Name
     */
    public function setName($Name)
    {
        $this->Name = $Name;
    }

    /**
     *
     * @return the $RightKey
     */
    public function getRightKey()
    {
        return $this->RightKey;
    }

    /**
     *
     * @param number $RightKey
     */
    public function setRightKey($RightKey)
    {
        $this->RightKey = $RightKey;
    }

}