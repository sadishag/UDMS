<h1>Papierkorb</h1>
<?php if(isset($this->trashdata)){?>
<div id="wrapper">
    <section id="content">
        <form method="post" name="newform" id="form">

            <table id="tempuser">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Datei Name</th>
                        <th>Gelöscht von</th>
                        <th>Gelöscht am</th>
                        <th>Bearbeiten</th>

                    </tr>
                </thead>

                <tbody>
                <?php $i=0?>
                <?php foreach($this->trashdata as $trashline){?>

                    <tr>
                        <td><?php echo $trashline->getBranchId()?> <input name="<?php echo 'ID'. $i ;?>" type="hidden"
                            value="<?php echo $trashline->getBranchId()?> "></td>
                        <td><?php echo $trashline->getName()?></td>
                        <td><?php echo $this->deleteuser[$trashline->getBranchId()]?></td>
                        <td><?php echo $this->deletedata[$trashline->getBranchId()]?></td>
                        <td><select id="<?php echo $trashline->getBranchId()?>" name="<?php echo 'Aktion'. $i ;?>" class="vAlignMiddle"
                            onChange="handleChange(this)">
                                <option value=null selected="selected">-- Aktionen --</option>
                                <option value="1">Löschen</option>
                                <option value="2">Wiederherstellen</option>
                        </select></td>
                    </tr>
                    <?php $i++?>
                   <?php }?>
               </tbody>

                <tfoot>
                    <tr>
                        <td><input name="sizeofarray" type="hidden"
                            value="<?php echo $i?>"></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>

                    </tr>
                </tfoot>
            </table>
        </form>

    </section>
</div>
<?php }?>

<div id="slider">
    <div>
        <?php
            include 'help.html';
        ?>
    </div>
    <p id="sliderButton"><span>Hilfe!</span></p>
</div>

<script>
function handleChange(element){
	if($(element).val()==1){
	    if(confirm('Möchten Sie wirklich Löschen')){
		    $('#form').submit();
		}else{
			element.selectedIndex = 0;
		}
	}else{
		if(confirm('Möchten Sie wirklich wiederherstellen')){
		    $('#form').submit();
		}else{
			element.selectedIndex = 0;
		}
	}
}

</script>
