<?php
/**
 * CreateController.php
*
* @package    Metadata\Controller
* @copyright  Copyright (c) 2012 Unister GmbH
* @author     Unister GmbH <entwicklung@unister.de>
* @author     Mathias Piehl <mathias.piehl@unister.de>
*/

namespace Metadata\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Console\Response;
use Doctrine\ORM\EntityManager;

/**
 * Controll unit for view\MetaData\create
 *
 * @copyright Copyright (c) 2012 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Mathias Piehl <mathias.piehl@unister.de>
 */
class CreateController extends AbstractActionController
{
    /**
	 * Function for controlling create.phtml
	 * Provides interface for creating new meta types.
	 *
	 * @return mixed $returnArray
	 *         Objekte, Arrays und Integers
	 *
	 */
	public function createAction(){
	    
		/**
		 * function for debugging
		 *
		 * @return mixed $returnArray
		 *         Objekte, Arrays und Integers
		 *
		 */
		/*/
		 function show($var, $info = " ")
		 {
		 echo '<pre> '. $info . ': '; echo print_r($var); echo '</pre>'; echo "</br>";
		 }
		 //*/
			    
	    $postvar = $this->getRequest()->getPost();
	    //show($postvar, 'postvar');
   
	    if (isset($postvar['newMetaTypeSave'])) {
	    	$validCheck = true;
	        foreach ($postvar as $key => $eingabe) {
	        	$trimmedPostVar = str_replace(' ', '', $eingabe);
	        	
	        	if (!isset($trimmedPostVar) or empty($trimmedPostVar)) {
	                if ($key == 'interactive' or $key == 'maxLength' or $key == 'maxSize') continue;
	                $validCheck = false;
	            }
	        }
	        
	        $returnArray['typeName'] = str_replace(' ', '', $postvar['typeName']);
	        $returnArray['mediaType'] = str_replace(' ', '', $postvar['mediaType']);
	        $returnArray['subType'] = str_replace(' ', '', $postvar['subType']);
	        $returnArray['maxLength'] = $postvar['maxLength'];
	        $returnArray['maxSize'] = $postvar['maxSize'];
	        $returnArray['resourceUrl'] = str_replace(' ', '', $postvar['resourceUrl']);
	        $returnArray['interactive'] = $postvar['interactive'];

	        if (!$validCheck) {
	            $returnArray['error'] = 'Bitte füllen Sie alle Felder aus.'; 
	        }

	        else {
	        	$em = $this->getServiceLocator()->get('ObjectManager');
	            $metaTypeRepo = $em->getRepository('MetaAPI\Entity\MetaDataTypes');
	            $metaTypeData = $metaTypeRepo->findBy(array('Name' => $returnArray['typeName']));
	            
	            if (empty($metaTypeData)) {
	            	if (is_numeric($postvar['maxLength'])) {
	            		if (is_numeric($postvar['maxSize'])) {	
	            				
	            			$newType = new \MetaAPI\Entity\MetaDataTypes();
	            			$newType->setName($returnArray['typeName']);
	            			$newType->setMediatyp($returnArray['mediaType']);
	            			$newType->setSubtyp($returnArray['subType']);
	            			$newType->setMaxLength($postvar['maxLength']);
	            			$newType->setMaxSize($postvar['maxSize']);
	            			$newType->setInteractive($postvar['interactive']);
	            			$newType->setResourceUrl($returnArray['resourceUrl']);
	            			
	            			$em->persist($newType);
	            			$em->flush();
	            			$returnArray['success'] = 'Erfolgreich erstellt';
	            		}
	            		else $returnArray['error'] = 'Ungültiger Wert in "Maximale Größe"';
	            	}		
	            	else $returnArray['error'] = 'Ungültiger Wert in "Maximale Länge"';
	            }
	            else $returnArray['error'] = 'Name schon vergeben!';
	            return $returnArray;
	        }
	         
	    }
    	
	}
}