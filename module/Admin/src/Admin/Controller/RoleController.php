<?php
/**
 * RoleController.php
 *
 * @package    Admin\Controller
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Sebastian Krebs <sebastian.krebs@unister.de>
 */
namespace Admin\Controller;

use Zend\Console\Response;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Doctrine\ORM\EntityManager;

/**
 * Controlleinheit für view\Admin\role
 *
 * @copyright Copyright (c) 2012 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Sebastian Krebs <sebastian.krebs@unister.de>
 *
 */
class RoleController extends AbstractActionController
{

    /**
     * Funktion zur Steuerung der index.phtml
     * Sucht für einen User die Globalerolle
     * Sucht für eine Gruppe die Globalerolle
     * Speichert die Änderrung der Rollen für den User oder für die Gruppe
     *
     * @return mixed $returnArray
     *         Objekt u Arrays
     */

    public function indexAction()
    {
        $postvar = $this->getRequest()->getPost();
        $api = $this->serviceLocator->get('RoleAPI');
        $em = $this->getServiceLocator()->get('ObjectManager');
        $groups = $em->getRepository('RoleAPI\Entity\GroupName')->findAll();
        $user = $em->getRepository('RoleAPI\Entity\User')->findAll();
        $rols = $api->getStandardRoleArrays();

        if ($postvar['userglobalsearch']) {
            $postvar = $this->getRequest()->getPost();
            if ($postvar['nutzer']) {
                $usersObjekt = $em->getRepository('RoleAPI\Entity\User')->findOneby(
                array('Login' => $postvar['nutzer']));

                if ($usersObjekt) {

                    $globalrole = $em->getRepository('RoleAPI\Entity\Roles')->findOneBy(
                    array('UserId' => $usersObjekt->getUserId(), 'GroupId' => ''));

                    $globalName = $em->getRepository('RoleAPI\Entity\RoleName')->findOneBy(
                    array('RoleId' => $globalrole->getRoleId()));

                    $returnArray['userobjekt'] = $usersObjekt;
                    $returnArray['globalname'] = $globalName;

                } else {
                    $returnArray['error'] = 'User nicht vorhanden';

                }
            } else {
                $returnArray['error'] = 'Bitte Nutzer eingeben';
            }

        }

        if ($postvar['rolesave']) {

            $globalrolechange = $em->getRepository('RoleAPI\Entity\Roles')->findOneBy(
            array('UserId' => $postvar['hiddenuserid'], 'GroupId' => ''));
            if ($globalrolechange->getRoleId() != $postvar['role']) {
                $globalrolechange->setRoleId($postvar['role']);

                $em->persist($globalrolechange);
                $em->flush();
                $returnArray['success'] = 'Geändert';
            } else {
                $returnArray['warning'] = 'Gleiche Rolle ausgewählt. Keine Änderrung vorgenommen';
            }
        }

        if ($postvar['groupSearch']) {
            $postvar = $this->getRequest()->getPost();

            $onlygrp = $em->getRepository('RoleAPI\Entity\Roles')->findOneBy(
            array('GroupId' => $postvar['groupSearch'], 'UserId' => '0'));

            if ($onlygrp) {
                $onlygrpname = $em->getRepository('RoleAPI\Entity\GroupName')->findOneBy(
                array('GroupId' => $postvar['groupSearch']));

                $onlygrprole = $em->getRepository('RoleAPI\Entity\RoleName')->findOneBy(
                array('RoleId' => $onlygrp->getRoleId()));

                $returnArray['onlygrprole'] = $onlygrprole;
                $returnArray['onlygrpname'] = $onlygrpname;

            } else {
                $returnArray['error'] = 'Keine Gruppe ausgewählt';

            }

        }

        if ($postvar['grupperolesichern']) {
            $postvar = $this->getRequest()->getPost();

            $onlygrpsave = $em->getRepository('RoleAPI\Entity\Roles')->findOneBy(
            array('GroupId' => $postvar['hiddengrpname']));
            if ($onlygrpsave->getRoleId() != $postvar['rolegrpname']) {
                $onlygrpsave->setRoleId($postvar['rolegrpname']);

                $em->persist($onlygrpsave);
                $em->flush();
                $returnArray['success'] = 'Geändert';
            } else {
                $returnArray['warning'] = 'Gleiche Rolle ausgewählt. Keine Änderrung vorgenommen';
            }

        }
        $returnArray['Groups'] = $groups;
        $returnArray['RoleName'] = $rols;
        $returnArray['user'] = $user;
        return $returnArray;
    }

    /**
     * Funktionseinheit zur Steuerung der role.phtml
     * Ermöglicht das Erstellen, bearbeiten und löschen der Rollen.
     *
     * @return mixed $returnArray
     *         Objekten und Arrays
     */

    public function roleAction()
    {

        $em = $this->getServiceLocator()->get('ObjectManager');
        $api = $this->serviceLocator->get('RoleAPI');
        $rols = $api->getStandardRoleArrays();
        unset($rols[1]);
        $postvar = $this->getRequest()->getPost();

        if ($postvar['createrole']) {
            $postvar = $this->getRequest()->getPost();
            if ($postvar['rolenname']) {
                $api->createNewRole('!' . $postvar['rolenname'], array());
                $returnArray['success'] = 'Rolle erstellt';

                $rols = $api->getStandardRoleArrays();
            } else {
                $returnArray['error'] = 'Kein Namen eingeben';
            }
        }

        if ($postvar['changerole']) {
            $postvar = $this->getRequest()->getPost();
            if ($postvar['rolechange'] != 0) {
                $roleRightArray = $api->getRightArrayByRoleId($postvar['rolechange']);

                $roleName = $em->getRepository('RoleAPI\Entity\RoleName')->findOneBy(
                array('RoleId' => $postvar['rolechange']));

                $returnArray['rolerightarray'] = $roleRightArray;
                $returnArray['rolename'] = $roleName;
            } else {
                $returnArray['error'] = 'Keine Rolle ausgewählt';
            }
        }

        if ($postvar['deleterole']) {
            $postvar = $this->getRequest()->getPost();
            if ($postvar['rolechange'] != 0) {
                $api->deleteRole($postvar['rolechange']);
                $returnArray['success'] = 'Rolle gelöscht';

                $rols = $api->getStandardRoleArrays();
            } else {
                $returnArray['error'] = 'Keine Rolle ausgewählt';
            }
        }

        if ($postvar['rolechangesave']) {
            $postvar = $this->getRequest()->getPost();

            $roleRightArray = $api->getRightArrayByRoleId($postvar['activrole']);
            $i = 1;

            foreach ($roleRightArray as $key => &$value) {
                if ($postvar['checkbox' . $i]) {
                    $value = true;
                } else {
                    $value = false;
                }
                $i++;
            }

            $api->saveRightArray($postvar['activrole'], $roleRightArray);
            $returnArray['success'] = 'Geändert';
        }

        $returnArray['RoleName'] = $rols;
        return $returnArray;
    }

}