<?php
namespace User\Controller;
/**
 * CreateController.php
 *
 * @package    User\Controller
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Sebastian Krebs <sebastian.krebs@unister.de>
 */

use ZendTest\Mail\TestAsset\StringSerializableObject;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Controller for view\User\create
 *
 * @copyright Copyright (c) 2012 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Sebastian Krebs <sebastian.krebs@unister.de>
 *
 */

class CreateController extends AbstractActionController
{

    /**
     * Function to Controlle the index.phtml
     * Check the Rights for the current User
     * Give a Interface to Create a New User
     * Call the Function saveNewMember($postvar, $returnarray, $rightkey)
     * @return mixed array $returnarray
     *     strings
     *     Objects
     *
     */

    public function indexAction()
    {
        $userid = $_SESSION['Userid'];
        $api = $this->serviceLocator->get('RoleAPI');
        $postvar = $this->getRequest()->getPost();
        $em = $this->getServiceLocator()->get('ObjectManager');
        $returnarray = array();
        $ressource = 0;
        $action = 'createuser';
        if ($api->isAllowed($userid, $ressource, $action)) {
            $rightkey = 3;
            $action1 = 'isadmin';
            if ($api->isAllowed($userid, $ressource, $action1)) {
                $rightkey = 1;
            }
            if (isset($postvar['save'])) {
                $returnarray = $this->saveNewMember($postvar, $returnarray, $rightkey);

            }

            return $returnarray;
        } else {
            return $this->redirect()->toUrl('/Angemeldet');
        }

    }
    /**
     * Build a New Login based on the First u Lastname
     * Check if this Login free in the Database
     * @param post $postvar
     * @return string $newlogin
     */
    public function createNewLogin($postvar)
    {
        $i = 1;
        $j = 1;
        $firstNameLengh = strlen($postvar['userFirst']);
        $em = $this->getServiceLocator()->get('ObjectManager');
        do {
            if ($i <= $firstNameLengh) {
                $newlogin = substr($postvar['userFirst'], 0, $i) . '.' . $postvar['userLast'];
            } else {
                $newlogin = $postvar['userFirst'] . $j . '.' . $postvar['userLast'];
                $j++;
            }
            $login = $em->getRepository('RoleAPI\Entity\User')->findOneBy(array('Login' => $newlogin));
            $i++;

        } while (isset($login));
        return $newlogin;
    }
    /**
     * Check If this Email free in the Datbase
     * @param post $postvar
     * @param mixed array $returnarray
     * @return mixed array $returnarray
     *         strings
     */
    public function createNewEmail($postvar, $returnarray)
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $newEmail = $em->getRepository('RoleAPI\Entity\User')->findOneBy(array('Email' => $postvar['email']));
        if (isset($newEmail)) {
            $returnarray['error'] = 'Email bereits vorhanden';
            $returnarray['First'] = $postvar['userFirst'];
            $returnarray['Last'] = $postvar['userLast'];
            $returnarray['pass1'] = $postvar['pass1'];
            $returnarray['pass2'] = $postvar['pass2'];

        } else {
            $returnarray['NewEmail'] = $postvar['email'];
        }

        return $returnarray;
    }
    /**
     * Create the NewPassword
     * Check if Password1 like Password2
     * @param post $postvar
     * @param mixed array $returnarray
     *         strings
     */
    public function createNewPassword($postvar, $returnarray)
    {

        if ($postvar['pass1'] === $postvar['pass2']) {
            $returnarray['pass'] = $postvar['pass1'];
        } else {
            $returnarray['First'] = $postvar['userFirst'];
            $returnarray['Last'] = $postvar['userLast'];
            $returnarray['Email'] = $postvar['email'];
            $returnarray['error'] = 'Passwort nicht Identisch';
        }
        return $returnarray;
    }
    /**
     * Call these Functions:
     *     createNewEmail
     *     createNewPassword
     *     createNewLogin
     * Save the NewUser Inforamtion in the Database
     * @param post $postvar
     * @param mixed array $returnarray
     * @param int $rightkey
     * @return mixed array $returnarray
     *         strings
     *         Objects
     */
    public function saveNewMember($postvar, $returnarray, $rightkey)
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $returnarray = $this->createNewPassword($postvar, $returnarray);
        if (isset($returnarray['pass'])) {
            $returnarray = $this->createNewEmail($postvar, $returnarray);
            if (isset($returnarray['NewEmail'])) {
                $newUser = new \RoleAPI\Entity\User();
                $newUser->setFirstName($postvar['userFirst']);
                $newUser->setLastName($postvar['userLast']);
                $newUser->setLogin($this->createNewLogin($postvar));
                $newUser->setEmail($returnarray['NewEmail']);
                $newUser->setPassword($returnarray['pass']);
                $newUser->setTemp($rightkey);
                $em->persist($newUser);
                $em->flush();
                $this->createNewRole($newUser);
                $returnarray['newUser'] = $newUser;
                $returnarray['success'] = 'Nutzer wurde erstellt';
                $returnarray['hashvalue'] = sha1($newUser->getEmail() . $newUser->getLogin());

            }
        }
        return $returnarray;
    }
    /**
     * Create for the new User his OwnRole
     * @param Objekt $newUser
     */
    public function createNewRole($newUser)
    {
        $em = $this->getServiceLocator()->get('ObjectManager');
        $newRole = new \RoleAPI\Entity\Roles();
        $newRole->setUserId($newUser->getUserId());
        $newRole->setGroupId(0);
        $newRole->setRoleId(1);
        $em->persist($newRole);
        $em->flush();

    }
}