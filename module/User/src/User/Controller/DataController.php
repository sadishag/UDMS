<?php
/**
* DataController.php
*
* @package    User\Controller
* @copyright  Copyright (c) 2012 Unister GmbH
* @author     Unister GmbH <entwicklung@unister.de>
*/
namespace User\Controller;

use User\Form\UserForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use GroupAdmin\Entity\User;

/**
 * Controlleinheit für User\view\data
 *
 * @copyright Copyright (c) 2012 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 *
 */

class DataController extends AbstractActionController
{

    /**
     * Gibt die Daten für den Aktuellen User aus
     *
     * @return newViewModel
     *
     */

    public function indexAction()
    {

        $currentUserId = $_SESSION['Userid'];
        $entityManager = $this->getServiceLocator()->get('ObjectManager');
        $userData = $entityManager->getRepository('RoleAPI\Entity\User')->find($currentUserId);
        return new ViewModel(array('userdata' => $userData));
    }

    /**
     * Nimmt die Änderrungen der Persönlichen Daten auf und speichert sie.
     * Achtet auf die Eingaben
     *
     * @return new ViewModel
     *
     */
    public function changeAction()
    {
        $currentUserId = $_SESSION['Userid'];
        $form = new UserForm();
		$returnarray['form'] = $form;
        $form->get('password')->setValue('');

        $request = $this->getRequest();

        if ($request->isPost()) {
        	$return = '';
        	$success = '';
            $content = $request->getPost();
            $entityManager = $this->getServiceLocator()->get('ObjectManager');
            $user = $entityManager->getRepository('RoleAPI\Entity\User')->find($currentUserId);
            if(!isset($content['style'])){
                if ("" != $content->get('password') && "" != $content->get('password2')) {
                    if ($content->get('password') === $content->get('password2')) {
                        $user->setPassword($content->get('password'));
                        $returnarray['success'] = 'Änderung erfolgreich';
                    } else {
                        $returnarray['error'] = 'Passwörter ungleich';
                    }
                }
                if (empty($returnarray['error'])) {
                    if ("" !== $content->get('email')) {
                        $email = $entityManager->getRepository('RoleAPI\Entity\User')
                            ->findOneBy(array('Email' => $content->get('email')));

                        if (empty($email)) {
                            $user->setEmail($content->get('email'));
                            $user->setTemp(1);
                            $returnarray['success'] = 'Änderung erfolgreich';
                        } else {
                            $returnarray['error'] = 'Email schon vorhanden';
                        }
                    }
                }

            }else{
                $user->setStyleSettings($content['style']);
                $returnarray['success'] = 'Änderung erfolgreich';
            }
            $entityManager->persist($user);
            $entityManager->flush();
            if (isset($content['style'])) {
                return $this->redirect()->toUrl('/Angemeldet');
            }
        }
        $entityManager = $this->getServiceLocator()->get('ObjectManager');
        $user = $entityManager->getRepository('RoleAPI\Entity\User')->find($currentUserId);
        $settings = $user->getStyleSettings();
        $returnarray['setting'] = $settings;


        return new ViewModel($returnarray);
    }

}
