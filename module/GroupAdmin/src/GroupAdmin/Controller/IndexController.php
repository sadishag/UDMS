<?php
/**
 * IndexController.php
 *
 * @package    GroupAdmin\Controller
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Sebastian Fritze <sebastian.fritze@unister.de>
 */
namespace GroupAdmin\Controller;

use GroupAdmin\Form\UserForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use RoleAPI\Entity\Groups;
/**
 * gestattet die Mitgliederverwaltung in einzelnen Gruppen
 */
class IndexController extends AbstractActionController
{

    /**
     * Übersicht der in der Gruppe vorhandenen Mitglieder
     * @return ViewModel mit der Übersicht (Name, temporär-Status)
     */
    public function indexAction()
    {
        $currentGroupId = (int) $this->params('groupid', false);

        $page = (int) $this->params('page', 1);

        $currentGroupName = $this->getGroupName($currentGroupId);

        $result = $this->getUserOfGroup($currentGroupId);

        return new ViewModel(array(
            'user' => $result,
            'groupid' => $currentGroupId,
            'groupname' => $currentGroupName
        ));
    }

    /**
     * Funktion zu suche von hinzuzufügenden Mitgliedern
     * @return ViewModel mit Suchmaske und ggf. Suchergebnissen
     */
    public function addAction()
    {
        $currentGroupId = (int) $this->params('groupid', false);
        $currentGroupName = $this->getGroupName($currentGroupId);

        $form = new UserForm();

        $request = $this->getRequest();

        if ($request->isPost()) {
            $content = $request->getPost();

            $entityManager = $this->getServiceLocator()->get('ObjectManager');

            $user = $entityManager->getRepository('RoleAPI\Entity\User')->findBy(array(
                'FirstName' => $content->get('FirstName'),
                'LastName' => $content->get('LastName')
            ));

            return new ViewModel(array(
                'form' => $form,
                'user' => $user,
                'groupid' => $currentGroupId,
                'groupname' => $currentGroupName
            ));
        }
        return new ViewModel(array(
            'form' => $form,
            'groupid' => $currentGroupId,
            'groupname' => $currentGroupName
        ));
    }

    /**
     * Funktion zum temporären hinzufügen von Nutzern
     * @return ViewModel mit dem Ergebnis des Hinzufügens
     */
    public function addUserToGroupAction()
    {
        $id = (int) $this->params('id', false);

        $entityManager = $this->getServiceLocator()->get('ObjectManager');

        $currentGroupId = (int) $this->params('groupid', false);
        $currentGroupName = $this->getGroupName($currentGroupId);

        $hashnew = md5('user.' . $id . '.group.' . $currentGroupId);

        $users = $entityManager->getRepository('RoleAPI\Entity\Groups')->findBy(array(
            'UserId' => $id,
            'GroupId' => $currentGroupId
        ));

        if (!empty($users)) {
            return new ViewModel(array(
                'text' => 'Benutzer schon vorhanden.',
                'groupid' => $currentGroupId,
                'groupname' => $currentGroupName
            ));
        }

        $groups = new Groups();
        $groups->setUserId($id);
        $groups->setGroupId($currentGroupId);
        $groups->setTemp(1);
        $groups->setHash($hashnew);

        $entityManager->persist($groups);
        $entityManager->flush();

        return new ViewModel(array(
            'text' => 'Benutzer hinzugefügt',
            'groupid' => $currentGroupId,
            'groupname' => $currentGroupName
        ));
    }

    /**
     * findet den Namen zur GroupId
     * @return string $currentGroupName
     */
    public function getGroupName()
    {
        if (!isset($this->currentGroupName)) {
            $currentGroupId = (int) $this->params('groupid', false);

            $entityManager = $this->getServiceLocator()->get('ObjectManager');

            $this->currentGroupName = $entityManager->getRepository('RoleAPI\Entity\GroupName')
                ->find($currentGroupId)
                ->getName();
        }
        return $this->currentGroupName;
    }

    /**
     * findet die Nutzer aus einer Gruppe
     * @return array $result
     */
    public function getUserOfGroup($currentGroupId)
    {
        $entityManager = $this->getServiceLocator()->get('ObjectManager');

        $groupRepository = $entityManager->getRepository('RoleAPI\Entity\Groups');
        $userRepository = $entityManager->getRepository('RoleAPI\Entity\User');

        $groups = $groupRepository->findBy(array('GroupId' => $currentGroupId));

        $result = array();

        foreach ($groups as $group) {
            $user = $userRepository->find($group->getUserId());
            $temp = array(
                'FirstName' => $user->getFirstName(),
                'LastName' => $user->getLastName(),
                'UserId' => $user->getUserId(),
                'Temp' => $group->getTemp()
            );

            $result[] = $temp;
        }
        return $result;
    }
}
