<?php
namespace Metadata\Controller;

use Metadata\Controller\CreateController;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;


class CreateControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {
    	$this->_controller = new CreateController();
    }
    public function tearDown()
    {
    }
    
    public function testcreateactionValidCheckTrue()
    {
    	$postvar = array('typeName' => 'MP3', 'mediaType' => 'music', 'subType' => 'mp3',
    			'maxLength' => '10', 'maxSize' => '10', 'interactive' => '0', 'resourceUrl' => '/mp3', 'newMetaTypeSave' => 'Speichern');
    	    	    	
    	$this->_controller->getRequest()
    	->setMethod('post')
    	->setPost(new Parameters($postvar));
    	
    	$albumMock = $this->getMock('MetaAPI\Entity\MetaDataTypes'); 
    	    	
    	$repo = $this->getMock('Repository', array('findby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue($albumMock));
    	
    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'), array(), '',false);
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));
    	
    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValue($em));
    	
    	$this->_controller->setServiceLocator($smMock);
     	$this->_controller->createAction();
    }
    
    public function testcreateactionEmptyTypeName()
    {
    	$postvar = array('typeName' => '  ', 'mediaType' => 'music', 'subType' => 'mp3',
    			'maxLength' => '0', 'maxSize' => '10', 'interactive' => '0', 'resourceUrl' => '/mp3', 'newMetaTypeSave' => 'Speichern');
    	 
    	$this->_controller->getRequest()
    	->setMethod('post')
    	->setPost(new Parameters($postvar));
    	
    	$albumMock = $this->getMock('MetaAPI\Entity\MetaDataTypes');
        	
    	$repo = $this->getMock('Repository', array('findby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue($albumMock));
    	
    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'), array(), '',false);
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));
    	
    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValue($em));
    	  	
    	 
    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->createAction();
    }
    
    public function testcreateactionInvalidMaxLength()
    {
    	$postvar = array('typeName' => 'MP3', 'mediaType' => 'music', 'subType' => 'mp3',
    			'maxLength' => 'fsef', 'maxSize' => '10', 'interactive' => '0', 'resourceUrl' => '/mp3', 'newMetaTypeSave' => 'Speichern');
    	 
    	$this->_controller->getRequest()
    	->setMethod('post')
    	->setPost(new Parameters($postvar));
    	 
    	$albumMock = array(); 
    	
    	$repo = $this->getMock('Repository', array('findby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue($albumMock));
    	 
    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'), array(), '',false);
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));
    	 
    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValue($em));
    	 
    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->createAction();
    }
    
    public function testcreateactionInvalidMaxSize()
    {
    	$postvar = array('typeName' => 'MP3', 'mediaType' => 'music', 'subType' => 'mp3',
    			'maxLength' => '255', 'maxSize' => 'invalid', 'interactive' => '0', 'resourceUrl' => '/mp3', 'newMetaTypeSave' => 'Speichern');
    
    	$this->_controller->getRequest()
    	->setMethod('post')
    	->setPost(new Parameters($postvar));
    
    	$albumMock = array();
    	 
    	$repo = $this->getMock('Repository', array('findby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue($albumMock));
    
    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'), array(), '',false);
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));
    
    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValue($em));
    
    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->createAction();
    }
    
    public function testcreateactionAllPostParametersValid()
    {
    	$postvar = array('typeName' => 'MP3', 'mediaType' => 'music', 'subType' => 'mp3',
    			'maxLength' => '255', 'maxSize' => '666', 'interactive' => '0', 'resourceUrl' => '/mp3', 'newMetaTypeSave' => 'Speichern');
    
    	$this->_controller->getRequest()
    	->setMethod('post')
    	->setPost(new Parameters($postvar));
    
    	$albumMock = array();
    
    	$repo = $this->getMock('Repository', array('findby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue($albumMock));
    
    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array('getRepository', 'persist', 'flush'), array(), '',false);
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));
    
    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValue($em));
    
    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->createAction();
    }
    
    public function testcreateactionNoSave()
    {
    	$postvar = array();
    	$this->_controller->createAction();
    }
}