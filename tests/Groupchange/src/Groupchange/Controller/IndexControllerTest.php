<?php
namespace Groupchange\Controller;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Groupchange\Controller\IndexController;
use Zend\Stdlib\Parameters;

class GroupchangeControllerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {

        $this->_controller = new IndexController();

    }

    public function tearDown()
    {

    }

    public function testindexaction()
    {
    	$_SESSION='2312';
    	$post = array('userFirst' => '123123', 'userLast' => '354345', 'userglobalsearch' => 'test',
    			'rolesave' => 'sadd', 'groupSearch' => 'asdasd', 'grupperolesichern' => 'asdasd');
    	$this->_controller->getRequest()
    	->setMethod('post')
    	->setPost(new Parameters($post));

    	$albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
    	$albumMock->expects($this->any())
    	->method('getRoleId')
    	->will($this->returnValue(array()));

    	$repo = $this->getMock('Repository', array('findAll', 'findOneBy','findby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue($albumMock));

    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
    			false);
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));

    	$apiMock = $this->getMock('RoleAPI',
    			array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
    	$apiMock->expects($this->any())
    	->method('getStandardRoleArrays')
    	->will($this->returnValue(array('$key' => '3', '$value' => true)));


    	$mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValueMap($mapmock));

    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->indexAction();
    }
    public function testindexactionpostvar()
    {
    	$_SESSION='2312';
    	$post = array('userFirst' => '123123','hiddenuser2'=>'asda','role2'=>'adasda', 'userLast' => '354345', 'userglobalsearch' => 'test',
    			'rolesave' => 'sadd', 'groupSearch' => 'asdasd','groupsave'=>'true','sizeofarray'=>'5','role1'=>'0', 'grupperolesichern' => 'asdasd');
    	$this->_controller->getRequest()
    	->setMethod('post')
    	->setPost(new Parameters($post));

    	$albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
    	$albumMock->expects($this->any())
    	->method('getRoleId')
    	->will($this->returnValue('asdasda'));

    	$repo = $this->getMock('Repository', array('findAll', 'findOneBy','findby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue($albumMock));
    	$repo->expects($this->any())
    	->method('findOneBy')
    	->will($this->returnValue($albumMock));

    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
    			false);
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));

    	$apiMock = $this->getMock('RoleAPI',
    			array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
    	$apiMock->expects($this->any())
    	->method('getStandardRoleArrays')
    	->will($this->returnValue(array('$key' => '3', '$value' => true)));


    	$mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValueMap($mapmock));

    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->indexAction();
    }
    public function testindexactionpostvarelse()
    {
    	$_SESSION='2312';
    	$post = array('userFirst' => '123123','hiddenuser2'=>'asda','role2'=>'adasda', 'userLast' => '354345', 'userglobalsearch' => 'test',
    			'rolesave' => 'sadd', 'groupSearch' => 'asdasd','groupsave'=>'true','sizeofarray'=>'5','role1'=>'0', 'grupperolesichern' => 'asdasd');
    	$this->_controller->getRequest()
    	->setMethod('post')
    	->setPost(new Parameters($post));

    	$albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
    	$albumMock->expects($this->any())
    	->method('getRoleId')
    	->will($this->returnValue('asdasda'));

    	$repo = $this->getMock('Repository', array('findAll', 'findOneBy','findby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue($albumMock));


    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
    			false);
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));

    	$apiMock = $this->getMock('RoleAPI',
    			array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
    	$apiMock->expects($this->any())
    	->method('getStandardRoleArrays')
    	->will($this->returnValue(array('$key' => '3', '$value' => true)));


    	$mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValueMap($mapmock));

    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->indexAction();
    }
    public function testindexactionshow()
    {
    	$_SESSION='2312';
    	$post = array('userFirst' => '123123','group'=>'asdasd','hiddenuser2'=>'asda','role2'=>'adasda', 'userLast' => '354345', 'userglobalsearch' => 'test',
    			'rolesave' => 'sadd', 'groupSearch' => 'asdasd','groupsave'=>'true','sizeofarray'=>'5','role1'=>'0', 'grupperolesichern' => 'asdasd');
    	$this->_controller->getRequest()
    	->setMethod('post')
    	->setPost(new Parameters($post));

    	$albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId'));
    	$albumMock->expects($this->any())
    	->method('getRoleId')
    	->will($this->returnValue('asdasda'));

    	$repo = $this->getMock('Repository', array('findAll', 'findOneBy','findby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue($albumMock));


    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
    			false);
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));

    	$apiMock = $this->getMock('RoleAPI',
    			array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays'), array(), "", false);
    	$apiMock->expects($this->any())
    	->method('getStandardRoleArrays')
    	->will($this->returnValue(array('$key' => '3', '$value' => true)));


    	$mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValueMap($mapmock));

    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->indexAction();
    }
    public function testindexactionshowarray()
    {
    	$_SESSION='2312';
    	$post = array('userFirst' => '123123','group'=>'asdasd','hiddenuser2'=>'asda','role2'=>'adasda', 'userLast' => '354345', 'userglobalsearch' => 'test',
    			'rolesave' => 'sadd', 'groupSearch' => 'asdasd','groupsave'=>'true','sizeofarray'=>'5','role1'=>'0', 'grupperolesichern' => 'asdasd');
    	$this->_controller->getRequest()
    	->setMethod('post')
    	->setPost(new Parameters($post));

    	$albumMock = $this->getMock('RoleAPI\Entity\User', array('getRoleId', 'setRoleId','getGroupId','getUserId'));
    	$albumMock->expects($this->any())
    	->method('getRoleId')
    	->will($this->returnValue('5'));
    	$albumMock->expects($this->any())
    	->method('getUserId')
    	->will($this->returnValue('2'));

    	$repo = $this->getMock('Repository', array('findAll', 'findOneBy','findby'));
    	$repo->expects($this->any())
    	->method('findby')
    	->will($this->returnValue(array($albumMock,$albumMock,$albumMock)));
    	$repo->expects($this->any())
    	->method('findOneBy')
    	->will($this->returnValue($albumMock));


    	$em = $this->getMock('Doctrine\ORM\Entitymanager', array("getRepository", "persist", "flush"), array(), "",
    			false);
    	$em->expects($this->any())
    	->method('getRepository')
    	->will($this->returnValue($repo));

    	$apiMock = $this->getMock('RoleAPI',
    			array("createNewRole", 'getRightArrayByRoleId', 'deleteRole', 'saveRightArray','getStandardRoleArrays','isAllowedForGroup','getUserInGroupStandardRole'), array(), "", false);
    	$apiMock->expects($this->any())
    	->method('getStandardRoleArrays')
    	->will($this->returnValue(array('1' => '3', '6' => '3','3' => '3', '5' => '3', '4' => '3','2' => '3',)));
    	$apiMock->expects($this->any())
    	->method('getUserInGroupStandardRole')
    	->will($this->returnValue(1));
    	$apiMock->expects($this->any())
    	->method('isAllowedForGroup')
    	->will($this->returnValue(1));


    	$mapmock = array(array('ObjectManager', true, $em), array('RoleAPI', true, $apiMock));

    	$smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
    	$smMock->expects($this->any())
    	->method('get')
    	->will($this->returnValueMap($mapmock));

    	$this->_controller->setServiceLocator($smMock);
    	$this->_controller->indexAction();
    }
}