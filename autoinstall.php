<?php
// Omnipotent Legendary Automatic Filling

system('clear');
$thispath = __DIR__;
$composerpath = $thispath;
$dbfillpath = $thispath . '/tools/dbfill/';
$docmodpath = $thispath . '/vendor/doctrine/doctrine-module/bin/';
$docormmodulepath = $thispath . '/vendor/doctrine/doctrine-orm-module/src/DoctrineORMModule/Module.php';
echo "Welcome to the automatic Installer" . PHP_EOL;
sleep(5);
echo "getting composer" . PHP_EOL;
system('curl -sS https://getcomposer.org/installer | php');
echo "running composer install" . PHP_EOL;
system('php composer.phar update');
echo "composer install finished." . PHP_EOL;
echo "initializing database configs" . PHP_EOL;
system('cp ./Services/localStorageEngineConf.php ./Services/StorageEngineConf.php');
system('mkdir ./data');
system('mkdir ./data/sqlite');
system('mkdir ./Services/log');
$sqlitepath = __DIR__ . '/data/sqlite/';
$lines = file($dbfillpath . 'dbfill.conf');
$lineAmount = (count($lines) - 1) / 5;
echo "creating config/autoload/local.php" . PHP_EOL;
$file = fopen(__DIR__ . '/config/autoload/local.php', 'w');
fclose($file);
$file = fopen(__DIR__ . '/config/autoload/local.php', 'a');
fwrite($file, "<?php
return array(
  'doctrine' => array(
    'connection' => array(");
$docmodels = array();
$docmodels[] = 'orm_default';
for ($i = 0; $i <= $lineAmount - 1; $i++) {
    $user = trim($lines[1 + 5 * $i]);
    $pass = trim($lines[2 + 5 * $i]);
    $dbaddress = trim($lines[3 + 5 * $i]);
    $dbport = trim($lines[4 + 5 * $i]);
    $dbname = trim($lines[5 + 5 * $i]);
    $docmodels[] = $dbname;
    if ($dbaddress === "sqli") {
        fwrite($file,
        "
      '" . $dbname . "' => array(
        'driverClass' =>'Doctrine\DBAL\Driver\PDOSqlite\Driver',
        'params' => array(
          'path'=> '" . $sqlitepath . $dbname . "'
        ),
      ),
");
        if (fopen($sqlitepath . $dbname, 'w'))
            unlink($sqlitepath . $dbname);
    } else {
        $dbh = new PDO('mysql:host=' . $dbaddress . ';port=' . $dbport, $user, $pass);
        fwrite($file,
        "
      '" . $dbname . "' => array(
        'driverClass' =>'Doctrine\DBAL\Driver\PDOMySql\Driver',
        'params' => array(
          'host'     => '" . $dbaddress . "',
          'port'     => '" . $dbport . "',
          'user'     => '" . $user . "',
          'password' => '" . $pass . "',
          'dbname'   => '" . $dbname . "',
          'driverOptions' => array(
              1002 => 'SET NAMES utf8'
          ),
        ),
      ),
");
        echo "Setting up database :" . $dbname . PHP_EOL;
        $dbh->exec('DROP DATABASE ' . $dbname);
        $dbh->exec('create database ' . $dbname . ' character set utf8 collate utf8_general_ci;');
    }
}
fwrite($file, "
    ),
  ),
);");
fclose($file);
$docmodels[] = 'orm_default';
$ormamount = count($docmodels);

for ($i = 1; $i < $ormamount; $i++) {
    $str = file_get_contents($docormmodulepath);
    $str = str_replace($docmodels[$i - 1], $docmodels[$i], $str);
    file_put_contents($docormmodulepath, $str);
    if ($i == $ormamount - 1)
        break;
    system($docmodpath . 'doctrine-module orm:schema-tool:create');
    if ($docmodels[$i] === 'udms_container') {
        $dbh->exec('ALTER TABLE `udms_container`.`FileVersion` ADD FULLTEXT (`Search`);');
    }
}
system('cd ' . $dbfillpath);
echo "Filling Database with test data" . PHP_EOL;
system('php ' . $dbfillpath . 'databasefill.php');
system('cd ' . $thispath);
echo PHP_EOL . "DONE! remember to create a VHost on the public folder" . PHP_EOL;