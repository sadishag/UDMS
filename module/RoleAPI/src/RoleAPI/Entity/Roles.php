<?php

namespace RoleAPI\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 @ORM\Entity
 * Roles
 *
 * @ORM\Table(name="Roles")
 * @property int $Id
 * @property int $UserId
 * @property int $GroupId
 * @property int $RoleId
 */





class Roles
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */

    protected $Id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $UserId;

    /**
     * @ORM\Column(type="integer")
     */

    protected $GroupId;

    /**
     * @ORM\Column(type="integer")
     */

    protected $RoleId;


	/**
     * @return the $Id
     */
    public function getId()
    {
        return $this->Id;
    }

	/**
     * @return the $UserId
     */
    public function getUserId()
    {
        return $this->UserId;
    }

	/**
     * @return the $GroupId
     */
    public function getGroupId()
    {
        return $this->GroupId;
    }

	/**
     * @return the $RoleId
     */
    public function getRoleId()
    {
        return $this->RoleId;
    }


	/**
     * @param number $UserId
     */
    public function setUserId($UserId)
    {
        $this->UserId = $UserId;
    }

	/**
     * @param number $GroupId
     */
    public function setGroupId($GroupId)
    {
        $this->GroupId = $GroupId;
    }

	/**
     * @param number $Right_ID
     */
    public function setRoleId($RoleId)
    {
        $this->RoleId = $RoleId;
    }




}