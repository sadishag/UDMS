<?php

use Doctrine\ORM\Query\Parameter;

use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;

use Admin\Controller\GroupController;

class GroupControllerTest extends PHPUnit_Framework_TestCase
{
    protected $_controller;

    protected $_serviceLocatorMock;

    protected $_post;

    public function __construct()
    {
        //mock Entity BranchName
        $entityBranchName = $this->getMock('RoleAPI\Entity\BranchName', array('getBranchId'));
        $entityBranchName->expects($this->any())
            ->method('getBranchId')
            ->will($this->returnValue(1));

        //mock Repository BranchName
        $repositoryBranchName = $this->getMock('RepositoryBranchName', array('findBy' , 'findAll', 'findOneBy'));
        $repositoryBranchName->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($entityBranchName));
        $repositoryBranchName->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue(array($entityBranchName)));
        $repositoryBranchName->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue(array($entityBranchName)));



        //mock Entity Groups
        $entityGroups = $this->getMock('RoleAPI\Entity\Groups');

        //mock Repository Groups
        $repositoryGroups = $this->getMock('RepositoryGroups', array('findBy' , 'findAll'));
        $repositoryGroups->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue($entityGroups));
        $repositoryGroups->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($entityGroups)));

        //mock Entity GroupName
        $entityGroupName = $this->getMock('RoleAPI\Entity\GroupName');

        //mock Repository GroupName
        $repositoryGroupName = $this->getMock('RepositoryGroupName', array('findBy' , 'findAll', 'findOneBy', 'find'));
        $repositoryGroupName->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue(null));//inportant
        $repositoryGroupName->expects($this->any())
            ->method('find')
            ->will($this->returnValue($entityGroupName));//inportant
        $repositoryGroupName->expects($this->any())
            ->method('findBy')
            ->will($this->returnValue(array($entityGroupName)));
        $repositoryGroupName->expects($this->any())
            ->method('findAll')
            ->will($this->returnValue(array($entityGroupName)));

        //mock Query
        $queryMock = $this->getMock('Query', array('getResult'));
        $queryMock->expects($this->any())
            ->method('getResult')
            ->will($this->returnValue(1));

        //mock EntityManager
        $entityManagerGetRepositoryMap = array(
            array('RoleAPI\Entity\GroupName', $repositoryGroupName),
            array('RoleAPI\Entity\Groups', $repositoryGroups),
            array('RoleAPI\Entity\BranchName', $repositoryBranchName),
        );
        $entityManagerMock = $this->getMock('ObjectManager', array('getRepository' , 'persist', 'flush', 'createQuery', 'remove'));
        $entityManagerMock->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValueMap($entityManagerGetRepositoryMap));
        $entityManagerMock->expects($this->any())
            ->method('createQuery')
            ->will($this->returnValue($queryMock));


        //mock RoleAPI
        $RoleAPIMock = $this->getMock("RoleAPI", array('isAllowed', 'deleteGroup'));
        $RoleAPIMock->expects($this->any())
            ->method('isAllowed')
            ->will($this->returnValue(true));


        //mock ServiceLocator
        $serviceLocatorGetMap = array(
            array('RoleAPI', true, $RoleAPIMock),
            array('ObjectManager', true, $entityManagerMock)
        );
        $serviceLocatorMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $serviceLocatorMock->expects($this->any())
            ->method('get')
            ->will($this->returnValueMap($serviceLocatorGetMap));

        $this->_serviceLocatorMock = $serviceLocatorMock;
    }

    private function _getAnotherServiceLocator()
    {
        //mock Entity BranchName
        $entityBranchName = $this->getMock('RoleAPI\Entity\BranchName', array('getBranchId'));
        $entityBranchName->expects($this->any())
        ->method('getBranchId')
        ->will($this->returnValue(1));

        //mock Repository BranchName
        $repositoryBranchName = $this->getMock('RepositoryBranchName', array('findBy' , 'findAll', 'findOneBy'));
        $repositoryBranchName->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($entityBranchName));
        $repositoryBranchName->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue(array($entityBranchName)));
        $repositoryBranchName->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue(array($entityBranchName)));



        //mock Entity Groups
        $entityGroups = $this->getMock('RoleAPI\Entity\Groups');

        //mock Repository Groups
        $repositoryGroups = $this->getMock('RepositoryGroups', array('findBy' , 'findAll'));
        $repositoryGroups->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue($entityGroups));
        $repositoryGroups->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue(array($entityGroups)));

        //mock Entity GroupName
        $entityGroupName = $this->getMock('RoleAPI\Entity\GroupName');

        //mock Repository GroupName
        $repositoryGroupName = $this->getMock('RepositoryGroupName', array('findBy' , 'findAll', 'findOneBy', 'find'));
        $repositoryGroupName->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue('nnnnnasd'));//inportant
        $repositoryGroupName->expects($this->any())
        ->method('find')
        ->will($this->returnValue('nnnnnasd'));//inportant
        $repositoryGroupName->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue(array($entityGroupName)));
        $repositoryGroupName->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue(array($entityGroupName)));

        //mock Query
        $queryMock = $this->getMock('Query', array('getResult'));
        $queryMock->expects($this->any())
        ->method('getResult')
        ->will($this->returnValue(1));

        //mock EntityManager
        $entityManagerGetRepositoryMap = array(
        array('RoleAPI\Entity\GroupName', $repositoryGroupName),
        array('RoleAPI\Entity\Groups', $repositoryGroups),
        array('RoleAPI\Entity\BranchName', $repositoryBranchName),
        );
        $entityManagerMock = $this->getMock('ObjectManager', array('getRepository' , 'persist', 'flush', 'createQuery', 'remove'));
        $entityManagerMock->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValueMap($entityManagerGetRepositoryMap));
        $entityManagerMock->expects($this->any())
        ->method('createQuery')
        ->will($this->returnValue($queryMock));


        //mock RoleAPI
        $RoleAPIMock = $this->getMock("RoleAPI", array('isAllowed'));
        $RoleAPIMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(true));


        //mock ServiceLocator
        $serviceLocatorGetMap = array(
        array('RoleAPI', true, $RoleAPIMock),
        array('ObjectManager', true, $entityManagerMock)
        );
        $serviceLocatorMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $serviceLocatorMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($serviceLocatorGetMap));

        return $serviceLocatorMock;
    }

    private function _getAnotherServiceLocator2()
    {
        //mock Entity BranchName
        $entityBranchName = $this->getMock('RoleAPI\Entity\BranchName', array('getBranchId'));
        $entityBranchName->expects($this->any())
        ->method('getBranchId')
        ->will($this->returnValue(1));

        //mock Repository BranchName
        $repositoryBranchName = $this->getMock('RepositoryBranchName', array('findBy' , 'findAll', 'findOneBy'));
        $repositoryBranchName->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue(null));
        $repositoryBranchName->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue(array($entityBranchName)));
        $repositoryBranchName->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue(array($entityBranchName)));



        //mock Entity Groups
        $entityGroups = $this->getMock('RoleAPI\Entity\Groups');

        //mock Repository Groups
        $repositoryGroups = $this->getMock('RepositoryGroups', array('findBy' , 'findAll'));
        $repositoryGroups->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue($entityGroups));
        $repositoryGroups->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue(array($entityGroups)));

        //mock Entity GroupName
        $entityGroupName = $this->getMock('RoleAPI\Entity\GroupName');

        //mock Repository GroupName
        $repositoryGroupName = $this->getMock('RepositoryGroupName', array('findBy' , 'findAll', 'findOneBy', 'find'));
        $repositoryGroupName->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue(null));//inportant
        $repositoryGroupName->expects($this->any())
            ->method('find')
            ->will($this->returnValue(null));//inportant
        $repositoryGroupName->expects($this->any())
        ->method('findBy')
        ->will($this->returnValue(array($entityGroupName)));
        $repositoryGroupName->expects($this->any())
        ->method('findAll')
        ->will($this->returnValue(array($entityGroupName)));

        //mock Query
        $queryMock = $this->getMock('Query', array('getResult'));
        $queryMock->expects($this->any())
        ->method('getResult')
        ->will($this->returnValue(1));

        //mock EntityManager
        $entityManagerGetRepositoryMap = array(
        array('RoleAPI\Entity\GroupName', $repositoryGroupName),
        array('RoleAPI\Entity\Groups', $repositoryGroups),
        array('RoleAPI\Entity\BranchName', $repositoryBranchName),
        );
        $entityManagerMock = $this->getMock('ObjectManager', array('getRepository' , 'persist', 'flush', 'createQuery', 'remove'));
        $entityManagerMock->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValueMap($entityManagerGetRepositoryMap));
        $entityManagerMock->expects($this->any())
        ->method('createQuery')
        ->will($this->returnValue($queryMock));


        //mock RoleAPI
        $RoleAPIMock = $this->getMock("RoleAPI", array('isAllowed'));
        $RoleAPIMock->expects($this->any())
        ->method('isAllowed')
        ->will($this->returnValue(true));


        //mock ServiceLocator
        $serviceLocatorGetMap = array(
        array('RoleAPI', true, $RoleAPIMock),
        array('ObjectManager', true, $entityManagerMock)
        );
        $serviceLocatorMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $serviceLocatorMock->expects($this->any())
        ->method('get')
        ->will($this->returnValueMap($serviceLocatorGetMap));

        return $serviceLocatorMock;
    }

    public function setUp() // construct
    {
        $_SESSION['Userid'] = 1;
        $this->_controller = new GroupController();
    }

    public function testShowAction(){

        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->showAction();
    }
    public function testAddAction(){

        $this->_controller->getRequest()->setMethod('POST')->setPost(new Parameters(array('name' => 'gruppenname', 'location' => 'dir://A1')));
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->addAction();
    }
    public function testAddActionWithWrongName(){

        $this->_controller->getRequest()->setMethod('POST')->setPost(new Parameters(array('name' => 'n', 'location' => 'dir://A1')));
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->addAction();
    }
    public function testAddActionWithDublicatName(){
        $this->_controller->getRequest()->setMethod('POST')->setPost(new Parameters(array('name' => 'nnnnnasd', 'location' => 'dir://A1')));
        $this->_controller->setServiceLocator($this->_getAnotherServiceLocator());
        $this->_controller->addAction();
    }
    public function testAddActionWithNotExistingBranch(){
        $this->_controller->getRequest()->setMethod('POST')->setPost(new Parameters(array('name' => 'nnnnnasd', 'location' => 'dir://A1')));
        $this->_controller->setServiceLocator($this->_getAnotherServiceLocator2());
        $this->_controller->addAction();
    }
    public function testDeleteAction(){

        $this->_controller->getRequest()->setMethod('POST')->setPost(new Parameters(array('id' => '5')));
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->deleteAction();
    }
    public function testDeleteActionOnNoGroup(){

        $this->_controller->getRequest()->setMethod('POST')->setPost(new Parameters(array('id' => '5')));
        $this->_controller->setServiceLocator($this->_getAnotherServiceLocator2());
        $this->_controller->deleteAction();
    }
    public function testRenameAction(){
        $this->_controller->getRequest()->setMethod('POST')->setPost(new Parameters(array('name' => 'gruppenname', 'id' => '5')));
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->renameAction();
    }
    public function testRenameActionWithWrongName(){
        $this->_controller->getRequest()->setMethod('POST')->setPost(new Parameters(array('name' => 'n', 'id' => '5')));
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->renameAction();
    }

    public function testRenameActionWithNameDublicate(){
        $this->_controller->getRequest()->setMethod('POST')->setPost(new Parameters(array('name' => 'gruppenname', 'id' => '5')));
        $this->_controller->setServiceLocator($this->_getAnotherServiceLocator());
        $this->_controller->renameAction();
    }
    public function testRenameActionOnNoGroup(){
        $this->_controller->getRequest()->setMethod('POST')->setPost(new Parameters(array('name' => 'gruppenname', 'id' => '5')));
        $this->_controller->setServiceLocator($this->_getAnotherServiceLocator2());
        $this->_controller->renameAction();
    }
    public function testIndexAction(){
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);
        $this->_controller->indexAction();
    }
    public function testWithoutRights(){
         $apiMock = $this->getMock("Api", array('isAllowed'));
         $apiMock->expects($this->any())
             ->method('isAllowed')
             ->will($this->returnValue(false));
         $serviceLocator = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
         $serviceLocator->expects($this->any())
             ->method('get')
             ->will($this->returnValue($apiMock));

        $this->_controller->setServiceLocator($serviceLocator);

        $this->_controller->showAction();
        $this->_controller->deleteAction();
        $this->_controller->renameAction();
        $this->_controller->addAction();
        $this->_controller->indexAction();
    }
    public function testAllWithoutPost(){
        $this->_controller->setServiceLocator($this->_serviceLocatorMock);

        $this->_controller->showAction();
        $this->_controller->deleteAction();
        $this->_controller->renameAction();
        $this->_controller->addAction();
        $this->_controller->indexAction();
    }



    public function tearDown() // destruct
    {

    }
}