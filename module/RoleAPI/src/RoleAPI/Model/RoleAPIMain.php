<?php
/**
 * RoleAPIMain.php Main Class for the RoleAPI
 *
 * @package    RoleAPI\Model
 * @copyright  Copyright (c) 2013 Unister GmbH
 * @author     Unister GmbH <entwicklung@unister.de>
 * @author     Lucas Mikulla <lucas.mikulla@unister.de>
 */
namespace RoleAPI\Model;

/**
 * Main Class of the RoleAPI
 *
 * @package RoleAPI\Model
 * @copyright Copyright (c) 2013 Unister GmbH
 */
class RoleAPIMain
{
    /**
     * instance of the ServiceLocator
     *
     * @var ServiceLocator
     */
    protected $_serviceManager;

    /**
     * instance of the EntityManager
     *
     * @var ObjectManager
     */
    protected $_entityManager;

    /**
     * Constructor for creating EntityManager and the Rights Constant
     *
     * @param ServiceLocator $sm
     */
    public function __construct($sm)
    {
        $this->_serviceManager = $sm;
        $this->_entityManager = $this->_serviceManager->get('doctrine.entitymanager.udms_role');
        if (!(defined('RIGHT_KEYS'))) {
            define('RIGHT_KEYS',
            serialize(
            array('create', 'read', 'update', 'delete', 'roleadministration', 'groupadministration', 'createuser',
            'isadmin')));
        }
    }

    /**
     * get an array of all currently defined Rights
     *
     * @return array(string) Array with the names of all defined Rights
     */
    public function getRightKeys()
    {
        return unserialize(RIGHT_KEYS);
    }

    /**
     * Checks if a $user has the right to use $action in $ressource
     *
     * @param mixed $user
     *            the user Id
     * @param mixed $ressource
     *            the branch Id on which the user wants to perform the action
     * @param string $action
     *            the action to be checked
     * @return boolean has Right?
     */
    public function isAllowed($user, $ressource, $action)
    {
        $ressource = $this->handleLocationParameter($ressource, false);
        $user = $this->handleUserParameter($user, false);
        $action = $this->handleActionParameter($action);
        $roles = $this->getNonDoubleRoles($user, $ressource);
        foreach ($roles as $role) {
            $permissionArray = $this->getRightArrayByRoleId($role);
            if ($permissionArray && isset($permissionArray[$action]) && $permissionArray[$action]) {

                return true;
            }
        }
        return false;

    }

    /**
     * checks if a User has a specific Right in a group, without inheriting
     * rights from parents
     *
     * @param mixed $user
     *            Loginname or UserId of the User
     * @param integer $group
     *            the GroupId
     * @param string $action
     *            the action that is to be checked
     * @return boolean has the Right
     */
    public function isAllowedForGroup($user, $group, $action)
    {
        $user = $this->handleUserParameter($user, false);
        $action = $this->handleActionParameter($action);
        $roleRepository = $this->_entityManager->getRepository('RoleAPI\Entity\Roles');
        $userInGroupRoles = $roleRepository->findBy(array('UserId' => $user, 'GroupId' => $group));
        $userInGroupRoles[] = $roleRepository->findOneBy(array('GroupId' => $group, 'UserId' => 0));
        foreach ($userInGroupRoles as $role) {
            if ($role) {
                $permissionArray = $this->getRightArrayByRoleId($role->getRoleId());

                if ($permissionArray && isset($permissionArray[$action]) && $permissionArray[$action]) {
                    return true;
                }
            }

        }

        return false;

    }

    /**
     * get a copy of the Rightarray stored in the DB
     *
     * @param integer $roleId
     *            the Role of which to get the array
     * @return array(boolean) the array
     */
    public function getRightArrayByRoleId($roleId)
    {
        $rightKey = $this->_entityManager->getRepository('RoleAPI\Entity\RoleName')
            ->find($roleId)
            ->getRightKey();
        if (is_string($rightKey)) {
            return unserialize(base64_decode($rightKey));
        } else {
            $contentString = stream_get_contents($rightKey);
            rewind($rightKey);
            return unserialize(base64_decode($contentString));
        }
    }

    /**
     * function to get an array of roleIds that have to be applied for the
     * current user on the current branch after stripping all duplciate RoleIds
     *
     * @param mixed $userId
     *            the current User
     * @param mixed $branchId
     *            the current Branch
     * @return array(int) $niceroles return array containing all the roles after
     *         duplicates are stripped
     */
    public function getNonDoubleRoles($userId, $branchId)
    {
        $userId = $this->handleUserParameter($userId, false);
        $branchId = $this->handleLocationParameter($branchId, false);
        $roles = array();

        $branchLocation = $this->getBranchLocation($branchId);

        $globalRights = $this->_entityManager->getRepository('RoleAPI\Entity\Roles')->findBy(
        array('UserId' => $userId, 'GroupId' => 0));

        foreach ($globalRights as $gRight) {
            $roles[] = $gRight->getRoleId();
        }
        $memberGroups = $this->_entityManager->getRepository('RoleAPI\Entity\Groups')->findBy(
        array('UserId' => $userId));
        foreach ($memberGroups as $group) {

            $groupLocation = $this->getBranchLocation(
            $this->_entityManager->getRepository('RoleAPI\Entity\GroupName')
                ->find($group->getGroupId())
                ->getBranchId());
            if ($groupLocation) {
                if ($this->isBranchChildOf($branchLocation, $groupLocation)) {

                    $groupRoles = $this->_entityManager->getRepository('RoleAPI\Entity\Roles')->findBy(
                    array('GroupId' => $group->getGroupId(), 'UserId' => 0));
                    foreach ($groupRoles as $gRole) {
                        $roles[] = $gRole->getRoleId();
                    }

                    $userOnGroupRoles = $this->_entityManager->getRepository('RoleAPI\Entity\Roles')->findBy(
                    array('GroupId' => $group->getGroupId(), 'UserId' => $userId));
                    foreach ($userOnGroupRoles as $gRole) {
                        $roles[] = $gRole->getRoleId();
                    }
                }
            }
        }
        $nonDoubleRoles = array();
        foreach ($roles as $role) {

            if (!(in_array($role, $nonDoubleRoles))) {
                $nonDoubleRoles[] = $role;
            }

        }

        return $nonDoubleRoles;
    }

    /**
     * returns the entities of all StandardRoles
     *
     * @return entities: the Entities of the Roles
     */
    public function getStandardRoleEntities()
    {
        $roleNameRepository = $this->_entityManager->getRepository('RoleAPI\Entity\RoleName');
        $roles = $roleNameRepository->findAll();
        $standardRoles[] = $roleNameRepository->find(1);
        $standardRoles = array();
        foreach ($roles as $role) {
            if (count(explode('!', $role->getName())) > 1) {
                $standardRoles[] = $role;

            }
        }

        return $standardRoles;
    }

    /**
     * returns an array filled with StandardRoleIds=>StandardRoleName
     *
     * @return array the returned array
     */
    public function getStandardRoleArrays()
    {
        $roles = $this->getStandardRoleEntities();
        $returnArray = array();
        $returnArray[1] = 'ohne Rolle';
        foreach ($roles as $role) {
            $temp = $role->getName();
            $temp = explode('!', $temp);
            if (count($temp) > 1) {
                $returnArray[$role->getRoleId()] = str_replace('!', '', $role->getName());
            }
        }

        return $returnArray;
    }

    /**
     * returns the entities of all CRUD Roles
     *
     * @return entities: the Entities of the Roles
     */
    public function getCrudRoleEntities()
    {
        $roleNameRepository = $this->_entityManager->getRepository('RoleAPI\Entity\RoleName');
        $roles = $roleNameRepository->findAll();
        $crudRoles = array();
        foreach ($roles as $role) {
            if (count(explode('#', $role->getName())) > 1) {
                $crudRoles[] = $role;

            }
        }

        return $crudRoles;
    }

    /**
     * returns an array filled with CrudRoleIds=>CrudRoleName
     *
     * @return array the returned array
     */
    public function getCrudRoleArrays()
    {
        $roles = $this->getCrudRoleEntities();
        $returnArray = array();
        foreach ($roles as $role) {
            $temp = $role->getName();
            $temp = explode('#', $temp);
            if (count($temp) > 1) {
                $returnArray[$role->getRoleId()] = str_replace('#', '', $role->getName());
            }
        }

        return $returnArray;
    }

    /**
     * returns a RoleId of the StandardRole a User has in a group
     *
     * @param mixed $user
     *            The User
     * @param integer $group
     *            The Group
     * @throws \Exception User Not In Group Exception
     * @return integer the RoleId of the StandardRole
     */
    public function getUserInGroupStandardRole($user, $group)
    {
        $user = $this->handleUserParameter($user, false);
        $isInGroup = $this->_entityManager->getRepository('RoleAPI\Entity\Groups')->findOneBy(
        array('UserId' => $user, 'GroupId' => $group));
        $roleId = 0;
        if ($isInGroup) {
            $roleRepository = $this->_entityManager->getRepository('RoleAPI\Entity\Roles');
            $standardRoles = $this->getStandardRoleArrays();
            $standardRoleIds = array();
            foreach ($standardRoles as $key => $value) {
                $standardRoleIds[] = $key;
            }
            $userRoleInGroup = $roleRepository->findBy(array('UserId' => $user, 'GroupId' => $group));
            if ($userRoleInGroup) {
                $temproleId = 0;
                foreach ($userRoleInGroup as $role) {
                    $temproleId = $role->getRoleId();

                    if (in_array($temproleId, $standardRoleIds)) {
                        $roleId = $temproleId;
                        break;
                    }
                }
            }
            if ($roleId === 0) {
                $groupRole = $roleRepository->findOneBy(array('GroupId' => $group, 'UserId' => 0));
                $roleId = $groupRole->getRoleId();
            }

        } else {
            throw new \Exception('User not in Group : User ' . $user . ' is not in the group ' . $group . '.');
        }
        return $roleId;
    }

    /**
     * returns a RoleId of the CrudRole a User has in a group
     *
     * @param mixed $user
     *            The User
     * @param integer $group
     *            The Group
     * @throws \Exception User Not In Group Exception
     * @return integer the RoleId of the CrudRole
     */
    public function getUserInGroupCrudRole($user, $group)
    {
        $user = $this->handleUserParameter($user, false);
        $isInGroup = $this->_entityManager->getRepository('RoleAPI\Entity\Groups')->findOneBy(
        array('UserId' => $user, 'GroupId' => $group));
        $roleId = 8;
        if ($isInGroup) {
            $roleRepository = $this->_entityManager->getRepository('RoleAPI\Entity\Roles');
            $crudRoles = $this->getCrudRoleArrays();
            $crudRoleIds = array();
            foreach ($crudRoles as $key => $value) {
                $crudRoleIds[] = $key;
            }
            $userRoleInGroup = $roleRepository->findBy(array('UserId' => $user, 'GroupId' => $group));
            if ($userRoleInGroup) {
                $temproleId = 0;
                foreach ($userRoleInGroup as $role) {
                    $temproleId = $role->getRoleId();

                    if (in_array($temproleId, $crudRoleIds)) {
                        $roleId = $temproleId;
                        break;
                    }
                }
            }

        } else {
            throw new \Exception('User not in Group : User ' . $user . ' is not in the group ' . $group . '.');
        }
        return $roleId;
    }

    /**
     * save a Right array to the DB for later use
     *
     * @param integer $roleId
     *            the Role that gets an updated Array
     * @param array(boolean) $perm
     *            the Rightarray that gets saved
     */
    public function saveRightArray($roleId, $perm)
    {
        $permissionArray = $this->handlePermArrayParameter($perm);
        $role = $this->_entityManager->getRepository('RoleAPI\Entity\RoleName')->find($roleId);
        $role->setRightKey($this->serializeRightArray($permissionArray));
        $this->_entityManager->persist($role);
        $this->_entityManager->flush();
    }

    /**
     * function to serialize given Right array
     *
     * @param array(boolean) $perm
     *            the right array
     */
    public function serializeRightArray($perm)
    {
        $permissionArray = $this->handlePermArrayParameter($perm);
        return base64_encode(serialize($permissionArray));
    }

    /**
     * create a new Role with a given Rightarray.
     *
     * @param string $name
     *            Name of the new Role
     * @param array(boolean) $perm
     *            Rightarray of the new Role
     */
    public function createNewRole($name, $perm)
    {
        $permissionArray = $this->handlePermArrayParameter($perm);
        $role = new \RoleAPI\Entity\RoleName();
        $role->setName($name);
        $role->setRightKey($this->serializeRightArray($permissionArray));
        $this->_entityManager->persist($role);
        $this->_entityManager->flush();
        return $role;
    }

    /**
     * adds all known Rights from the constant to an array, to complete said
     * array
     *
     * @param array(boolean) $perm
     *            the array to be completed
     * @return array(boolean) $nperm
     *         the saturated array
     */
    public function saturateRightArray($perm)
    {
        $saturatedPermissionArray = array();
        $const = unserialize(RIGHT_KEYS);
        foreach ($const as $key) {
            if (isset($perm[$key])) {
                $saturatedPermissionArray[$key] = ($perm[$key]) ? true : false;
            } else {
                $saturatedPermissionArray[$key] = false;
            }
        }
        return $saturatedPermissionArray;
    }

    /**
     * Update a single Right in a given Role directly in the DB
     *
     * @param integer $id
     *            the Role that gets updated
     * @param string $action
     *            the action that gets updated
     * @param boolean $key
     *            the key the action gets set to
     */
    public function updateRole($id, $action, $key)
    {
        $action = $this->handleActionParameter($action);
        $permissionArray = $this->getRightArrayByRoleId($id);
        $permissionArray = $this->saturateRightArray($permissionArray);
        $permissionArray[$action] = $key;
        $role = $this->_entityManager->getRepository('RoleAPI\Entity\RoleName')->find($id);
        $role->setRightKey($this->serializeRightArray($permissionArray));
        $this->_entityManager->persist($role);
        $this->_entityManager->flush();
        return $role;
    }

    /**
     * delete given Group and all references in other Tables
     *
     * @param integer $groupId
     *            Group to be deleted
     */
    public function deleteGroup($groupId)
    {
        if ($groupId === 0) {
            return;
        }
        $groups = $this->_entityManager->getRepository('RoleAPI\Entity\Groups')->findBy(
        array('GroupId' => $groupId));
        foreach ($groups as $group) {
            $this->_entityManager->remove($group);
        }
        $roles = $this->_entityManager->getRepository('RoleAPI\Entity\Roles')->findBy(array('GroupId' => $groupId));
        foreach ($roles as $role) {
            $this->_entityManager->remove($role);
        }
        $this->_entityManager->remove(
        $this->_entityManager->getRepository('RoleAPI\Entity\GroupName')
            ->find($groupId));
        $this->_entityManager->flush();
    }

    /**
     * delete given Role and all references in other Tables
     *
     * @param integer $roleId
     *            Role to be deleted
     */
    public function deleteRole($roleId)
    {
        if ($roleId === 0) {
            return;
        }
        $roles = $this->_entityManager->getRepository('RoleAPI\Entity\Roles')->findBy(array('RoleId' => $roleId));
        foreach ($roles as $role) {
            $this->_entityManager->remove($role);
        }
        $this->_entityManager->remove(
        $this->_entityManager->getRepository('RoleAPI\Entity\RoleName')
            ->find($roleId));
        $this->_entityManager->flush();
    }

    /**
     * alters the Users Role or creates a new specific role for the User based
     * on group Assignment and Permission Array
     *
     * @param mixed $userId
     *            the user which role has to be altered
     * @param mixed $groupId
     *            the group for which the role has to be altered, leave 0 if
     *            global right
     * @param array(string=>bool) $perm
     *            new permission array for the role
     */
    public function alterUserRights($userId, $groupId, $perm)
    {
        $permissionArray = $this->handlePermArrayParameter($perm);
        $userId = $this->handleUserParameter($userId, false);
        $roleName = 'UserRole:' . $userId;
        $rep = $this->_entityManager->getRepository('RoleAPI\Entity\Roles');
        if (!($groupId == 0)) {
            $roleName .= ':' . $groupId;
        }
        $role = $this->createUserSpecificRole($roleName, $permissionArray);
        if (!($rep->findBy(array('UserId' => $userId, 'GroupId' => $groupId, 'RoleId' => $role->getRoleId())))) {
            $roleAssign = new \RoleAPI\Entity\Roles();
            $roleAssign->setUserId($userId);
            $roleAssign->setGroupId($groupId);
            $roleAssign->setRoleId($role->getRoleId());
            $this->_entityManager->persist($roleAssign);
            $this->_entityManager->flush();
        }

    }

    /**
     * create Roles for a specific User, or updated said Role should it exist
     * already
     *
     * @param string $roleName
     *            Rolename of new Role, consist of 'UserRole':UserId:GroupId
     * @param array(string=>bool) $perm
     *            permission array for new Role
     * @return RoleAPI\Entity\RoleName object of the created or altered Role
     */
    public function createUserSpecificRole($roleName, $perm)
    {
        $permissionArray = $perm;
        $rep = $this->_entityManager->getRepository('RoleAPI\Entity\RoleName');
        $role = $rep->findOneBy(array('Name' => $roleName));
        if ($role) {
            $id = $role->getRoleId();
            foreach ($permissionArray as $key => $value) {
                $returnRole = $this->updateRole($id, $key, $value);
            }
        } else {
            $returnRole = $this->createNewRole($roleName, $this->handlePermArrayParameter($permissionArray));
        }
        return $returnRole;
    }

    /**
     * Designates a New Admin while setting the old admin to a new role
     *
     * @param unknown_type $user
     *            the new Admin
     * @param unknown_type $newRole
     *            the new role for the old admin
     * @param unknown_type $group
     *            the group, 0 if global admin
     */
    public function designateAdmin($user, $newRole, $group = 0)
    {
        $user = $this->handleUserParameter($user, false);
        $roleRepository = $this->_entityManager->getRepository('RoleAPI\Entity\Roles');
        $roleNameRepository = $this->_entityManager->getRepository('RoleAPI\Entity\RoleName');
        $oldAdmin = $this->findAdmin($group);
        if ($oldAdmin) {
            $oldAdminEntities = $roleRepository->findBy(array('UserId' => $oldAdmin, 'GroupId' => $group));
            $rolesId = 0;
            foreach ($oldAdminEntities as $role) {
                $rights = $this->getRightArrayByRoleId($role->getRoleId());
                $rights = $this->handlePermArrayParameter($rights);
                if ($rights['isadmin']) {
                    $rolesId = $role->getId();
                    break;
                }
            }
            $oldAdminTargetEntity = $roleRepository->find($rolesId);

            $oldAdminTargetEntity->setRoleId($newRole);
            $this->_entityManager->persist($oldAdminTargetEntity);
            $this->_entityManager->flush();
        }
        $this->alterUserRights($user, $group, array('isadmin' => true));

    }

    /**
     * finds the UserId of the Admin for given Group
     *
     * @param integer $group
     *            GroupId or 0 if global
     * @return mixed UserId if an Admin was found, false otherwise
     */
    public function findAdmin($group = 0)
    {
        $member = $this->_entityManager->getRepository('RoleAPI\Entity\Groups')->findBy(array('GroupId' => $group));
        $userId = 0;
        foreach ($member as $user) {
            $userId = $user->getUserId();
            if ($this->isAllowedForGroup($userId, $group, 'isadmin')) {
                break;
            }
        }
        return ($userId) ? $userId : false;
    }

    /**
     * Get the BranchLocation of the given BranchId
     * returns the path for root if given Id is 0
     *
     * @param integer $branchId
     *            the Branch
     * @return string the BranchLocation
     */
    public function getBranchLocation($branchId)
    {
        $branchId = $this->handleLocationParameter($branchId, false);
        if ($branchId == 0) {
            return 'dir://';
        } else {
            $branch = $this->_entityManager->getRepository('RoleAPI\Entity\BranchName')->find($branchId);
            if ($branch) {
                return $branch->getLocation();
            } else {
                return false;
            }
        }
    }

    /**
     * get the BranchId of a given Location of a Branch
     *
     * @param string $location
     *            location of the Branch
     * @return integer Id of the Branch
     */
    public function getBranchId($location)
    {
        $location = $this->handleLocationParameter($location, true);
        if ($location === 'dir://') {
            return 0;
        } else {
            $branch = $this->_entityManager->getRepository('RoleAPI\Entity\BranchName')->findOneBy(
            array('Location' => $location));
            if ($branch) {
                return $branch->getBranchId();
            } else {
                return false;
            }
        }
    }

    /**
     * returns a Users Id based on a given Loginname
     *
     * @param string $login
     *            login name of the user
     * @return integer UserId of the User
     */
    public function getUserId($login)
    {
        $login = $this->handleUserParameter($login, true);
        return $this->_entityManager->getRepository('RoleAPI\Entity\User')
            ->findOneBy(array('Login' => $login))
            ->getUserId();
    }

    /**
     * returns a Users Loginname based on a given UserId
     *
     * @param integer $user
     *            the UserId ofthe user
     * @return string the loginname of the user
     */
    public function getUserLogin($user)
    {
        $user = $this->handleUserParameter($user, false);
        return $this->_entityManager->getRepository('RoleAPI\Entity\User')
            ->findOneBy(array('UserId' => $user))
            ->getLogin();
    }

    /**
     * checks if a given Branch is a child of another given Path
     *
     * @param string $childLocation
     *            the assumed Child branch
     * @param string $parentLocation
     *            the assumed Parent branch
     * @return boolean returns if it is a child
     */
    public function isBranchChildOf($childLocation, $parentLocation)
    {
        $childLocation = $this->handleLocationParameter($childLocation, true);
        $parentLocation = $this->handleLocationParameter($parentLocation, true);
        $parentnum = sizeof(explode(':', $childLocation));
        $currentLocation = $childLocation;
        do {
            if ($currentLocation === $parentLocation) {
                return true;
            }
            $currentLocation = $this->getParent($currentLocation);
            $parentnum--;
        } while ($parentnum > 0);
        return false;
    }

    /**
     * Returns the Ids of all direct Child Branches of a given Parent
     *
     * @param string $location
     *            Location of the Parent Branch
     * @return array(integer) Ids of the children
     */
    public function getChildrenIds($location)
    {
        $location = $this->handleLocationParameter($location, true);
        $branches = $this->_entityManager->getRepository('RoleAPI\Entity\BranchName')->findAll();
        $children = array();
        foreach ($branches as $branch) {
            $path = $branch->getLocation();
            if ($this->getParent($path) === $location) {
                $children[] = $branch->getBranchId();
            }
        }
        return $children;
    }

    /**
     * Returns the Locations of all direct Child Branches of a given Parent
     *
     * @param string $location
     *            Location of the Parent Branch
     * @return array(string) Locations of the children
     */
    public function getChildrenLocations($location)
    {
        $location = $this->handleLocationParameter($location, true);
        $branches = $this->_entityManager->getRepository('RoleAPI\Entity\BranchName')->findAll();
        $children = array();
        foreach ($branches as $branch) {
            $path = $branch->getLocation();
            if ($this->getParent($path) === $location) {
                $children[] = $branch->getLocation();
            }
        }
        return $children;
    }

    /**
     * * Returns the Entities of all direct Child Branches of a given Parent
     *
     * @param string $location
     *            Location of the Parent Branch
     * @return array(Entity) entities of all children
     */
    public function getChildrenEntities($location)
    {
        $location = $this->handleLocationParameter($location, true);
        $branches = $this->_entityManager->getRepository('RoleAPI\Entity\BranchName')->findAll();
        $children = array();
        foreach ($branches as $branch) {
            $path = $branch->getLocation();
            if ($this->getParent($path) === $location) {
                $children[] = $branch;
            }
        }
        return $children;
    }

    /**
     * handles special location parameters
     *
     * @param string $location
     *            the location that is handled
     * @return string the handled location
     */
    public function handleStringLocationParam($location)
    {
        $locarr = explode('://', $location);
        $patharr = explode(':', $locarr[1]);
        $cLocation = $locarr[0] . '://';
        foreach ($patharr as $pathsplinter) {
            if ($pathsplinter !== '') {
                if ($pathsplinter === '..') {
                    $cLocation = $this->getParent($cLocation);
                } else {
                    $temp = explode(':', $cLocation);
                    if ($temp[1] === '//') {
                        $cLocation = $cLocation . $pathsplinter;
                    } else {
                        $cLocation = $cLocation . ':' . $pathsplinter;
                    }
                }
            }

        }
        return $cLocation;
    }

    /**
     * handles all Location parameters, both as Location or Id and returns a
     * Location or Id based on the $strreturn parameter
     *
     * @param mixed $location
     *            Location of a Branch or Id of a Branch, given as Parameter to
     *            another function
     * @param boolean $strreturn
     *            should the returned value be a string location or a branch id
     * @throws \Exception Bad Value Exception
     * @return mixed returns a string location or a branch id based on
     *         $strreturn
     */
    public function handleLocationParameter($location, $strreturn = false)
    {
        if (is_numeric($location)) {
            if ($strreturn) {
                return $this->getBranchLocation($location);
            } else {
                return $location;
            }
        } elseif (is_string($location)) {
            $location = $this->handleStringLocationParam($location);
            if (!($strreturn)) {
                return $this->getBranchId($location);
            } else {
                return $location;
            }
        } else {
            throw new \Exception('Bad Value : $location must be string or numeric. Location given : ' . $location);
        }
    }

    /**
     * handles all User Parameters to allow entering of both login name or
     * UserId as userparameter, as well as catching bad values
     *
     * @param mixed $user
     *            the user parameter to be handled
     * @param boolean $strreturn
     *            if it should be returned as string
     * @throws \Exception Bad Value Exception
     * @return mixed returns a string loginname or a int UserId based on
     *         $strreturn
     */
    public function handleUserParameter($user, $strreturn = false)
    {
        if (is_numeric($user)) {
            $user = (int) $user;
            if ($strreturn) {
                return $this->getUserLogin($user);
            } else {
                return $user;
            }

        } elseif (is_string($user)) {
            if ($strreturn) {
                return $user;
            } else {
                return $this->getUserId($user);
            }
        } else {
            throw new \Exception('Bad Value : $user must be string or numeric. User given : ' . $user);
        }
    }

    /**
     * handles all action parameters to check if they are valid rights
     *
     * @param string $action
     *            the action that is tested
     * @throws \Exception Bad Action Exception
     * @return string the handled action
     */
    public function handleActionParameter($action)
    {
        if (in_array($action, unserialize(RIGHT_KEYS)) && is_string($action)) {
            return $action;
        } else {
            throw new \Exception(
            'Bad Action: Action must be a string and must exist as a Right. Action given : ' . $action);
        }
    }

    /**
     * handles all permission array parameters, checking for invalid rights
     * (keys) and invalid values, as well as saturating the array to include all
     * rights
     *
     * @param array(string=>bool) $perm
     *            permission array to be checked
     * @throws \Exception Bad Key in Perm Array
     * @throws \Exception Bad Value in Perm Array
     * @return array(string=>bool) handled permission array
     */
    public function handlePermArrayParameter($perm)
    {
        $count = 0;
        $keys = unserialize(RIGHT_KEYS);
        foreach ($perm as $key => $value) {
            if (in_array($key, $keys)) {
                $count++;
            } else {
                throw new \Exception(
                'Bad Key in Perm Array: Found a non existent Right in permission Array. Key given : ' . $key);
            }
            if (is_bool($value) === false) {
                throw new \Exception(
                'Bad Value in Perm Array: Found a non boolean Value for a right in permissiong Arary. Key given : ' .
                 $key . '. Value given : ' . $value);
            }
        }
        if ($count !== count($keys)) {
            $perm = $this->saturateRightArray($perm);
        }
        return $perm;
    }

    /**
     * returns the Location of the direct Parent of any given Location
     *
     * @param mixed $location
     *            Child Location
     * @return string Parent Location
     */
    public function getParent($location)
    {
        $location = $this->handleLocationParameter($location, true);
        $strarr = explode('://', $location);
        $strarr[0] = 'dir';
        $patharr = explode(':', $strarr[1]);
        unset($patharr[sizeof($patharr) - 1]);
        $strarr[1] = implode(':', $patharr);
        $path = $strarr[0] . '://' . $strarr[1];
        return $path;
    }

}