<?php
namespace ObjectManager\Model;

use DataAPI\Entity\FileVersion;

use Zend\Http\Response;

use ObjectManager\Model\ObjectManager;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use Zend\Stdlib\Parameters;

class ObjectManagerTest extends \PHPUnit_Framework_TestCase
{
    protected $_controller;

    public function setUp()
    {
        $albumMock = $this->getMock('DataAPI\Entity\FileVersion',
        array('getRoleId', 'setRoleId', 'getId', 'setDataArray'));
        $albumMock->expects($this->any())
            ->method('getId')
            ->will($this->returnValue('5'));
        $repo = $this->getMock('Repository', array('find', 'findOneBy'));
        $repo->expects($this->any())
            ->method('find')
            ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
            ->method('findOneBy')
            ->will($this->returnValue($albumMock));
        $em = $this->getMock('Doctrine\ORM\Entitymanager',
        array("getRepository", "persist", "flush", "remove", 'get', 'createNativeQuery', 'createQueryBuilder'),
        array(), "", false);
        $em->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($repo));
        $em->expects($this->at(0))
            ->method('createQueryBuilder')
            ->will($this->throwException(new \Exception('bal')));
        $em->expects($this->at(0))
            ->method('createNativeQuery')
            ->will($this->throwException(new \Exception('bal')));



        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
            ->method('get')
            ->will($this->returnValue($em));
        $this->_controller = new ObjectManager($smMock);

    }

    public function tearDown()
    {}

    public function testgetRepository()
    {
        $entitymock = new \DataAPI\Entity\FileVersion();
        $entityName = get_class($entitymock);
        $this->_controller->getRepository($entityName);
    }

    public function testpersist()
    {
        $this->_controller->flush();
        $albumMock = $this->getMock('DataAPI\Entity\FileVersion',
        array('getRoleId', 'setRoleId', 'getId', 'setDataArray'));
        $albumMock->expects($this->any())
        ->method('getId')
        ->will($this->returnValue('5'));
        $repo = $this->getMock('Repository', array('find', 'findOneBy'));
        $repo->expects($this->any())
        ->method('find')
        ->will($this->returnValue($albumMock));
        $repo->expects($this->any())
        ->method('findOneBy')
        ->will($this->returnValue($albumMock));
        $fm = $this->getMockBuilder('\ObjectManager\Model\EntityManager')
            ->setMethods(array('getRepository','persist','flush','remove','get'))
            ->disableAutoload()
            ->disableOriginalConstructor()
            ->getMock();
        $fm->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($albumMock));
        $em = $this->getMock('Doctrine\ORM\Entitymanager',
        array("getRepository", "persist", "flush", "remove", 'get', 'createNativeQuery', 'createQueryBuilder'),
        array(), "", false);
        $em->expects($this->any())
        ->method('getRepository')
        ->will($this->returnValue($repo));
        $em->expects($this->at(1))
        ->method('persist')
        ->will($this->throwException(new \Exception));
        $em->expects($this->at(2))
        ->method('persist')
        ->will($this->throwException(new \Exception));
        $em->expects($this->at(3))
        ->method('persist')
        ->will($this->throwException(new \Exception));


        $smMock = $this->getMock('Zend\ServiceManager\ServiceManager', array('get'));
        $smMock->expects($this->any())
        ->method('get')
        ->will($this->returnValue($em));

        $controller = new ObjectManager($smMock);
        $entitymock = new \DataAPI\Entity\FileVersion();
        $entitymock->setId(5);
        $controller->persist($entitymock, 1);
        $entitymock = new \DataAPI\Entity\FileVersion();
        $controller->persist($entitymock, 2);
        $entitymock = new \MetaAPI\Entity\File();
        $controller->persist($entitymock);
    }

    public function testpersistredzero()
    {
        $entitymock = new \DataAPI\Entity\FileVersion();
        $redlevel = 0;
        $this->_controller->persist($entitymock, $redlevel);
    }

    public function testflush()
    {

        $this->_controller->flush();
    }

    public function testremove()
    {
        $entitymock = new \DataAPI\Entity\FileVersion();
        $this->_controller->remove($entitymock);
    }

    public function testCreateQueryBuilder()
    {

        $this->_controller->createQueryBuilder('DataAPI');

    }

    public function testNativeQuery()
    {
        $this->_controller->createNativeQuery('blabla', 'blabla', 'DataAPI');
    }
}