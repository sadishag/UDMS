<?php
namespace Admin;
use Zend\Mvc\MvcEvent;

class ModuleTest extends \PHPUnit_Framework_TestCase
{
    protected $_sm;
    public function setUp(){

    }
    public function testModule(){

        $module=new Module();
        $module->getAutoloaderConfig();
        $module->getConfig();



        $eventManegerMock = $this->getMock('eventManager', array('attach'));
        $eventManegerMock->expects($this->any())
            ->method('attach')
            ->will($this->returnValue(1));

        $aplicationMock = $this->getMock('aplication', array('getEventManager'));
        $aplicationMock->expects($this->any())
            ->method('getEventManager')
            ->will($this->returnValue($eventManegerMock));

        $routeMatchMock = $this->getMock('routeMatch', array('getParam'));
        $routeMatchMock->expects($this->at(0))
            ->method('getParam')
            ->will($this->returnValue('login'));
        $routeMatchMock->expects($this->at(1))
        ->method('getParam')
        ->will($this->returnValue('nichtlogin'));

        $RouterMock = $this->getMock('Router', array('assemble'));

        $mvcEventMock = $this->getMock('Zend\Mvc\MvcEvent', array('getApplication', 'getRouteMatch', 'getRouter', 'getResponse'));

        $headerMock = $this->getMock('header', array('addHeaderLine'));

        $ResponseMock = $this->getMock('Response', array('getHeaders', 'setStatusCode', 'sendHeaders'));
        $ResponseMock->expects($this->any())
        ->method('getHeaders')
        ->will($this->returnValue($headerMock));

        $mvcEventMock->expects($this->any())
            ->method('getApplication')
            ->will($this->returnValue($aplicationMock));
        $mvcEventMock->expects($this->any())
            ->method('getRouteMatch')
            ->will($this->returnValue($routeMatchMock));
        $mvcEventMock->expects($this->any())
            ->method('getRouter')
            ->will($this->returnValue($RouterMock));
        $mvcEventMock->expects($this->any())
            ->method('getResponse')
            ->will($this->returnValue($ResponseMock));

        //onBootstrap
        $module->onBootstrap($mvcEventMock);
        $module->authenticatePreDispatch($mvcEventMock);
        //authenticatePreDispatch
        $this->setExpectedException('RuntimeException');
        $module->authenticatePreDispatch($mvcEventMock);

    }
    public function tearDown(){

    }
}