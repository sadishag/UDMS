Unister Document Managing System
=======================

Introduction
------------
A Document Managing System in Development

Installation
------------
Pull the current Master into a directory of your choice.
In that directory run php autoinstall.php
this will install the composer, setup your database and setup the doctrine
databaseconnection

Virtual Host
------------
Afterwards, set up a virtual host to point to the public/ directory of the
project and you should be ready to go!
