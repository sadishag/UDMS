<?php
namespace GroupAdmin\Form;

use Zend\Form\Form;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Text;

class UserForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $firstName = new Text('FirstName');
        $firstName->setLabel('Vorname: ');
        $this->add($firstName);

        $lastName = new Text('LastName');
        $lastName->setLabel('Nachname: ');
        $this->add($lastName);

        $submit = new Submit('Submit');
        $submit->setValue('Ok');
        $this->add($submit);
    }
}
