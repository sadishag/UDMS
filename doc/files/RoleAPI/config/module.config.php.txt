<?php
return array(
    'doctrine' => array(
        'driver' => array(
            'role_entities' => array(
                'class' => '\Doctrine\ORM\Mapping\Driver\AnnotationDriver', 'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/RoleAPI/Entity'
                )
            ),
            'udms_role' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\DriverChain',
                'drivers' => array(
                    'RoleAPI\Entity' => 'role_entities'
                )
            ),

        ),
        'configuration' => array(
            'udms_role' => array(
                'metadata_cache' => 'array',
                'query_cache' => 'array',
                'result_cache' => 'array',
                'driver' => 'udms_role',
                'generate_proxies' => true,
                'proxy_dir' => 'data/DoctrineORMModule/Proxy',
                'proxy_namespace' => 'DoctrineORMModule\Proxy',
                'filters' => array()
            ),

        ),
        'entitymanager' => array(
            'udms_role' => array(
                'connection' => 'udms_role',
                'configuration' => 'udms_role'
            ),

        ),
        'eventmanager' => array(
            'udms_role' => array(),

        ),
        'sql_logger_collector' => array(
            'udms_role' => array(),

        ),
        'entity_resolver' => array(
            'udms_role' => array(),

        ),
        'authentication' => array(
            'udms_role' => array(
                'objectManager' => 'doctrine.entitymanager.udms_role'
            ),

        )
    ),
    'service_manager' => array(
        'factories' => array(
            'doctrine.authenticationadapter.udms_role' => new DoctrineModule\Service\Authentication\AdapterFactory('udms_role'),
            'doctrine.authenticationstorage.udms_role' => new DoctrineModule\Service\Authentication\StorageFactory('udms_role'),
            'doctrine.authenticationservice.udms_role' => new DoctrineModule\Service\Authentication\AuthenticationServiceFactory('udms_role'),
            'doctrine.connection.udms_role' => new DoctrineORMModule\Service\DBALConnectionFactory('udms_role'),
            'doctrine.configuration.udms_role' => new DoctrineORMModule\Service\ConfigurationFactory('udms_role'),
            'doctrine.entitymanager.udms_role' => new DoctrineORMModule\Service\EntityManagerFactory('udms_role'),
            'doctrine.driver.udms_role' => new DoctrineModule\Service\DriverFactory('udms_role'),
            'doctrine.eventmanager.udms_role' => new DoctrineModule\Service\EventManagerFactory('udms_role'),
            'doctrine.entity_resolver.udms_role' => new DoctrineORMModule\Service\EntityResolverFactory('udms_role'),
            'doctrine.sql_logger_collector.udms_role' => new DoctrineORMModule\Service\SQLLoggerCollectorFactory('udms_role'),
            'doctrine.mapping_collector.udms_role' => function (Zend\ServiceManager\ServiceLocatorInterface $sl)
            {
                $em = $sl->get('doctrine.entitymanager.udms_role');

                return new DoctrineORMModule\Collector\MappingCollector($em->getMetadataFactory(), 'udms_role_mappings');
            },
            'DoctrineORMModule\Form\Annotation\AnnotationBuilder' => function (Zend\ServiceManager\ServiceLocatorInterface $sl)
            {
                return new DoctrineORMModule\Form\Annotation\AnnotationBuilder($sl->get('doctrine.entitymanager.udms_role'));
            },

        )
    )
);
